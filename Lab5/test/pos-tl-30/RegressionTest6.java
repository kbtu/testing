import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest6 {

    public static boolean debug = false;

    @Test
    public void test3001() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3001");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test3002() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3002");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test3003() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3003");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test3004() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3004");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test3005() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3005");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass18 = soldItemList17.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test3006() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3006");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        SoldItem soldItem24 = null;
        shoppingCart1.addItem(soldItem24);
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList26);
    }

    @Test
    public void test3007() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3007");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test3008() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3008");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test3009() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3009");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3010() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3010");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass10 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test3011() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3011");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass11 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test3012() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3012");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass12 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3013() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3013");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test3014() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3014");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
    }

    @Test
    public void test3015() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3015");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test3016() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3016");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test3017() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3017");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test3018() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3018");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test3019() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3019");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3020() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3020");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass12 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3021() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3021");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test3022() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3022");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass8 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3023() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3023");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test3024() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3024");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test3025() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3025");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test3026() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3026");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test3027() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3027");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test3028() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3028");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3029() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3029");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test3030() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3030");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
    }

    @Test
    public void test3031() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3031");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test3032() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3032");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
    }
}

