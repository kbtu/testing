import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest3 {

    public static boolean debug = false;

    @Test
    public void test1501() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1501");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test1502() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1502");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.lang.Class<?> wildcardClass23 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test1503() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1503");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test1504() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1504");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(soldItemList23);
    }

    @Test
    public void test1505() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1505");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test1506() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1506");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1507() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1507");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1508() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1508");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass16 = soldItemList15.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1509() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1509");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test1510() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1510");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test1511() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1511");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass10 = soldItemList9.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test1512() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1512");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test1513() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1513");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test1514() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1514");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test1515() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1515");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass12 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1516() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1516");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test1517() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1517");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test1518() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1518");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test1519() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1519");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass11 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test1520() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1520");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1521() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1521");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass12 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1522() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1522");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1523() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1523");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass10 = soldItemList9.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test1524() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1524");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test1525() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1525");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1526() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1526");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass12 = soldItemList11.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1527() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1527");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass22 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test1528() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1528");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1529() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1529");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass11 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test1530() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1530");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.lang.Class<?> wildcardClass11 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test1531() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1531");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test1532() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1532");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass8 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1533() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1533");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        java.util.List<SoldItem> soldItemList25 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList25);
    }

    @Test
    public void test1534() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1534");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass16 = soldItemList15.getClass();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1535() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1535");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass12 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1536() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1536");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass20 = soldItemList19.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test1537() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1537");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test1538() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1538");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test1539() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1539");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1540() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1540");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
    }

    @Test
    public void test1541() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1541");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass13 = soldItemList12.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test1542() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1542");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1543() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1543");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1544() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1544");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass11 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test1545() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1545");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test1546() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1546");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1547() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1547");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
    }

    @Test
    public void test1548() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1548");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test1549() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1549");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test1550() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1550");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test1551() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1551");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1552() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1552");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test1553() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1553");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1554() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1554");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1555() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1555");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1556() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1556");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass13 = soldItemList12.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test1557() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1557");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.lang.Class<?> wildcardClass9 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test1558() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1558");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass15 = soldItemList14.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test1559() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1559");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
    }

    @Test
    public void test1560() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1560");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1561() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1561");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test1562() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1562");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1563() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1563");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass10 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test1564() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1564");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass21 = soldItemList20.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test1565() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1565");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test1566() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1566");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test1567() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1567");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1568() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1568");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass8 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1569() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1569");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test1570() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1570");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test1571() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1571");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1572() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1572");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        SoldItem soldItem24 = null;
        shoppingCart1.addItem(soldItem24);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList23);
    }

    @Test
    public void test1573() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1573");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test1574() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1574");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test1575() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1575");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test1576() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1576");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test1577() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1577");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass15 = soldItemList14.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test1578() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1578");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test1579() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1579");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1580() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1580");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test1581() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1581");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass6 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(wildcardClass6);
    }

    @Test
    public void test1582() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1582");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass7 = soldItemList6.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test1583() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1583");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test1584() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1584");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass10 = soldItemList9.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test1585() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1585");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass10 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test1586() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1586");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test1587() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1587");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test1588() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1588");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test1589() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1589");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.lang.Class<?> wildcardClass11 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test1590() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1590");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1591() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1591");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        java.lang.Class<?> wildcardClass24 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test1592() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1592");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test1593() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1593");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test1594() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1594");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
    }

    @Test
    public void test1595() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1595");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass23 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test1596() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1596");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test1597() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1597");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test1598() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1598");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1599() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1599");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test1600() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1600");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1601() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1601");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = soldItemList13.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test1602() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1602");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test1603() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1603");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass8 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1604() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1604");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1605() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1605");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test1606() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1606");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
    }

    @Test
    public void test1607() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1607");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1608() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1608");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test1609() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1609");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1610() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1610");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass24 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList23);
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test1611() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1611");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test1612() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1612");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test1613() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1613");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test1614() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1614");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test1615() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1615");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass8 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1616() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1616");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test1617() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1617");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test1618() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1618");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test1619() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1619");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test1620() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1620");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1621() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1621");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1622() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1622");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        org.junit.Assert.assertNotNull(soldItemList5);
    }

    @Test
    public void test1623() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1623");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1624() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1624");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test1625() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1625");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1626() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1626");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test1627() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1627");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test1628() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1628");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem26 = null;
        shoppingCart1.addItem(soldItem26);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList23);
        org.junit.Assert.assertNotNull(soldItemList24);
    }

    @Test
    public void test1629() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1629");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test1630() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1630");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test1631() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1631");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test1632() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1632");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = soldItemList13.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test1633() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1633");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList23);
    }

    @Test
    public void test1634() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1634");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test1635() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1635");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1636() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1636");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test1637() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1637");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1638() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1638");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass24 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList23);
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test1639() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1639");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test1640() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1640");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test1641() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1641");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test1642() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1642");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test1643() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1643");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test1644() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1644");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test1645() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1645");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass11 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test1646() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1646");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test1647() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1647");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test1648() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1648");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1649() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1649");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test1650() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1650");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test1651() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1651");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test1652() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1652");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test1653() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1653");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test1654() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1654");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test1655() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1655");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test1656() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1656");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test1657() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1657");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test1658() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1658");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass10 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test1659() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1659");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test1660() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1660");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
    }

    @Test
    public void test1661() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1661");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test1662() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1662");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1663() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1663");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test1664() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1664");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList27 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem29 = null;
        shoppingCart1.addItem(soldItem29);
        java.util.List<SoldItem> soldItemList31 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList26);
        org.junit.Assert.assertNotNull(soldItemList27);
        org.junit.Assert.assertNotNull(soldItemList31);
    }

    @Test
    public void test1665() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1665");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test1666() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1666");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1667() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1667");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test1668() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1668");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test1669() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1669");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test1670() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1670");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test1671() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1671");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test1672() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1672");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1673() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1673");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test1674() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1674");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.lang.Class<?> wildcardClass21 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test1675() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1675");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1676() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1676");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test1677() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1677");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1678() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1678");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1679() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1679");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test1680() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1680");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test1681() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1681");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test1682() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1682");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1683() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1683");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test1684() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1684");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test1685() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1685");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass13 = soldItemList12.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test1686() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1686");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1687() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1687");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass10 = soldItemList9.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test1688() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1688");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.lang.Class<?> wildcardClass9 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test1689() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1689");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        SoldItem soldItem25 = null;
        shoppingCart1.addItem(soldItem25);
        java.lang.Class<?> wildcardClass27 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass27);
    }

    @Test
    public void test1690() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1690");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test1691() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1691");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass10 = soldItemList9.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test1692() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1692");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test1693() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1693");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test1694() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1694");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1695() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1695");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass12 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1696() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1696");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass12 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1697() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1697");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
    }

    @Test
    public void test1698() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1698");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        java.lang.Class<?> wildcardClass25 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass25);
    }

    @Test
    public void test1699() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1699");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test1700() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1700");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
    }

    @Test
    public void test1701() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1701");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test1702() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1702");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList23);
    }

    @Test
    public void test1703() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1703");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test1704() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1704");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test1705() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1705");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test1706() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1706");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1707() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1707");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1708() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1708");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test1709() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1709");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test1710() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1710");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test1711() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1711");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test1712() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1712");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1713() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1713");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1714() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1714");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test1715() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1715");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1716() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1716");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test1717() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1717");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test1718() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1718");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test1719() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1719");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass15 = soldItemList14.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test1720() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1720");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass19 = soldItemList18.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test1721() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1721");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test1722() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1722");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test1723() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1723");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test1724() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1724");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test1725() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1725");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1726() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1726");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass21 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test1727() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1727");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test1728() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1728");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test1729() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1729");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test1730() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1730");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem24 = null;
        shoppingCart1.addItem(soldItem24);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test1731() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1731");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test1732() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1732");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1733() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1733");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test1734() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1734");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test1735() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1735");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test1736() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1736");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
    }

    @Test
    public void test1737() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1737");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1738() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1738");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1739() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1739");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1740() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1740");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1741() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1741");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test1742() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1742");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test1743() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1743");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test1744() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1744");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1745() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1745");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1746() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1746");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass13 = soldItemList12.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test1747() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1747");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test1748() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1748");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test1749() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1749");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1750() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1750");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
    }

    @Test
    public void test1751() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1751");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = soldItemList13.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test1752() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1752");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1753() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1753");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test1754() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1754");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1755() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1755");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test1756() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1756");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass12 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1757() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1757");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass10 = soldItemList9.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test1758() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1758");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test1759() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1759");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1760() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1760");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test1761() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1761");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test1762() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1762");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1763() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1763");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test1764() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1764");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test1765() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1765");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass23 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test1766() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1766");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
    }

    @Test
    public void test1767() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1767");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test1768() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1768");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test1769() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1769");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1770() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1770");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test1771() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1771");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test1772() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1772");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
    }

    @Test
    public void test1773() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1773");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test1774() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1774");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test1775() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1775");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        java.lang.Class<?> wildcardClass25 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(wildcardClass25);
    }

    @Test
    public void test1776() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1776");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        org.junit.Assert.assertNotNull(soldItemList2);
    }

    @Test
    public void test1777() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1777");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass11 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test1778() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1778");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test1779() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1779");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
    }

    @Test
    public void test1780() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1780");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test1781() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1781");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1782() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1782");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass15 = soldItemList14.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test1783() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1783");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1784() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1784");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test1785() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1785");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test1786() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1786");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test1787() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1787");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test1788() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1788");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test1789() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1789");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test1790() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1790");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test1791() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1791");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test1792() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1792");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(soldItemList23);
    }

    @Test
    public void test1793() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1793");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test1794() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1794");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test1795() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1795");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test1796() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1796");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1797() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1797");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test1798() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1798");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1799() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1799");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test1800() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1800");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test1801() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1801");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.lang.Class<?> wildcardClass11 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test1802() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1802");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test1803() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1803");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test1804() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1804");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass11 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test1805() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1805");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test1806() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1806");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test1807() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1807");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test1808() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1808");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test1809() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1809");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test1810() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1810");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test1811() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1811");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass12 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1812() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1812");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1813() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1813");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test1814() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1814");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
    }

    @Test
    public void test1815() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1815");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test1816() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1816");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test1817() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1817");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test1818() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1818");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test1819() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1819");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test1820() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1820");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1821() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1821");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1822() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1822");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass17 = soldItemList16.getClass();
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test1823() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1823");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1824() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1824");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList27 = shoppingCart1.getAll();
        SoldItem soldItem28 = null;
        shoppingCart1.addItem(soldItem28);
        java.util.List<SoldItem> soldItemList30 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList26);
        org.junit.Assert.assertNotNull(soldItemList27);
        org.junit.Assert.assertNotNull(soldItemList30);
    }

    @Test
    public void test1825() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1825");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass10 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test1826() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1826");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test1827() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1827");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass11 = soldItemList10.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test1828() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1828");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1829() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1829");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass22 = soldItemList21.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test1830() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1830");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test1831() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1831");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test1832() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1832");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1833() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1833");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test1834() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1834");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1835() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1835");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test1836() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1836");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test1837() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1837");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test1838() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1838");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1839() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1839");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1840() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1840");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        SoldItem soldItem27 = null;
        shoppingCart1.addItem(soldItem27);
        SoldItem soldItem29 = null;
        shoppingCart1.addItem(soldItem29);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList26);
    }

    @Test
    public void test1841() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1841");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1842() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1842");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList5);
    }

    @Test
    public void test1843() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1843");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test1844() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1844");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
    }

    @Test
    public void test1845() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1845");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass17 = soldItemList16.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test1846() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1846");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test1847() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1847");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.lang.Class<?> wildcardClass22 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test1848() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1848");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1849() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1849");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test1850() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1850");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test1851() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1851");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1852() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1852");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test1853() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1853");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test1854() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1854");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test1855() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1855");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test1856() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1856");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test1857() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1857");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1858() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1858");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1859() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1859");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1860() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1860");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test1861() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1861");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test1862() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1862");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test1863() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1863");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList25 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass26 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList24);
        org.junit.Assert.assertNotNull(soldItemList25);
        org.junit.Assert.assertNotNull(wildcardClass26);
    }

    @Test
    public void test1864() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1864");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test1865() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1865");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test1866() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1866");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test1867() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1867");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test1868() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1868");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test1869() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1869");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test1870() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1870");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test1871() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1871");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test1872() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1872");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass26 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList23);
        org.junit.Assert.assertNotNull(soldItemList24);
        org.junit.Assert.assertNotNull(wildcardClass26);
    }

    @Test
    public void test1873() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1873");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test1874() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1874");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test1875() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1875");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test1876() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1876");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test1877() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1877");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test1878() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1878");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test1879() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1879");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass7 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test1880() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1880");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test1881() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1881");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1882() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1882");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test1883() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1883");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test1884() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1884");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test1885() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1885");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass12 = soldItemList11.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1886() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1886");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test1887() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1887");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test1888() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1888");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1889() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1889");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test1890() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1890");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass20 = soldItemList19.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test1891() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1891");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass8 = soldItemList7.getClass();
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1892() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1892");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1893() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1893");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass21 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test1894() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1894");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test1895() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1895");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1896() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1896");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test1897() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1897");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test1898() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1898");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass12 = soldItemList11.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1899() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1899");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test1900() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1900");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = soldItemList13.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test1901() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1901");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test1902() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1902");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test1903() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1903");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass23 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test1904() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1904");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test1905() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1905");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test1906() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1906");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test1907() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1907");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1908() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1908");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test1909() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1909");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1910() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1910");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test1911() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1911");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1912() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1912");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass10 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test1913() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1913");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.lang.Class<?> wildcardClass10 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test1914() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1914");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1915() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1915");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1916() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1916");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1917() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1917");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test1918() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1918");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test1919() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1919");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test1920() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1920");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        SoldItem soldItem25 = null;
        shoppingCart1.addItem(soldItem25);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(soldItemList24);
    }

    @Test
    public void test1921() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1921");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test1922() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1922");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test1923() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1923");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test1924() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1924");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test1925() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1925");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(soldItemList24);
    }

    @Test
    public void test1926() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1926");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test1927() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1927");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test1928() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1928");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass9 = soldItemList8.getClass();
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test1929() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1929");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test1930() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1930");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
    }

    @Test
    public void test1931() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1931");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test1932() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1932");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test1933() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1933");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1934() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1934");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test1935() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1935");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1936() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1936");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1937() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1937");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test1938() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1938");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test1939() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1939");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1940() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1940");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test1941() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1941");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test1942() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1942");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
    }

    @Test
    public void test1943() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1943");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test1944() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1944");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass10 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test1945() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1945");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test1946() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1946");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass11 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test1947() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1947");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1948() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1948");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
    }

    @Test
    public void test1949() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1949");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass13 = soldItemList12.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test1950() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1950");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test1951() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1951");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = soldItemList13.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test1952() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1952");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test1953() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1953");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1954() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1954");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test1955() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1955");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
    }

    @Test
    public void test1956() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1956");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test1957() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1957");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test1958() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1958");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test1959() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1959");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test1960() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1960");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test1961() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1961");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test1962() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1962");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test1963() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1963");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test1964() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1964");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test1965() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1965");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass23 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test1966() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1966");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test1967() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1967");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test1968() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1968");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test1969() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1969");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test1970() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1970");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1971() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1971");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test1972() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1972");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test1973() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1973");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.lang.Class<?> wildcardClass22 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test1974() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1974");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test1975() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1975");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test1976() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1976");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test1977() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1977");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1978() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1978");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1979() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1979");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test1980() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1980");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass12 = soldItemList11.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1981() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1981");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test1982() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1982");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test1983() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1983");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass19 = soldItemList18.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test1984() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1984");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test1985() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1985");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1986() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1986");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test1987() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1987");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test1988() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1988");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test1989() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1989");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test1990() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1990");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass21 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test1991() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1991");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test1992() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1992");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test1993() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1993");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass19 = soldItemList18.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test1994() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1994");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test1995() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1995");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test1996() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1996");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test1997() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1997");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test1998() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1998");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(soldItemList23);
        org.junit.Assert.assertNotNull(soldItemList24);
    }

    @Test
    public void test1999() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1999");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test2000() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test2000");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass21 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }
}

