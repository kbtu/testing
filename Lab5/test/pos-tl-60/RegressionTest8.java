import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest8 {

    public static boolean debug = false;

    @Test
    public void test4001() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4001");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test4002() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4002");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass22 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test4003() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4003");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test4004() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4004");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test4005() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4005");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test4006() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4006");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass16 = soldItemList15.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4007() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4007");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test4008() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4008");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4009() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4009");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test4010() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4010");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test4011() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4011");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass10 = soldItemList9.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test4012() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4012");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4013() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4013");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test4014() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4014");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test4015() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4015");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test4016() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4016");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test4017() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4017");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass20 = soldItemList19.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test4018() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4018");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test4019() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4019");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test4020() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4020");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4021() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4021");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test4022() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4022");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test4023() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4023");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4024() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4024");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4025() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4025");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test4026() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4026");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test4027() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4027");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass12 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4028() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4028");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test4029() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4029");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        java.lang.Class<?> wildcardClass24 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test4030() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4030");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass22 = soldItemList21.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test4031() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4031");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test4032() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4032");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test4033() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4033");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test4034() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4034");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test4035() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4035");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test4036() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4036");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test4037() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4037");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test4038() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4038");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test4039() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4039");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4040() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4040");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test4041() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4041");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test4042() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4042");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test4043() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4043");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test4044() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4044");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test4045() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4045");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test4046() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4046");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4047() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4047");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test4048() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4048");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test4049() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4049");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test4050() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4050");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass17 = soldItemList16.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test4051() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4051");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test4052() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4052");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
    }

    @Test
    public void test4053() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4053");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test4054() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4054");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test4055() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4055");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test4056() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4056");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4057() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4057");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4058() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4058");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test4059() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4059");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test4060() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4060");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test4061() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4061");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test4062() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4062");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test4063() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4063");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
    }

    @Test
    public void test4064() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4064");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass21 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test4065() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4065");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test4066() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4066");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.lang.Class<?> wildcardClass21 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test4067() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4067");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        java.lang.Class<?> wildcardClass25 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass25);
    }

    @Test
    public void test4068() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4068");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList25 = shoppingCart1.getAll();
        SoldItem soldItem26 = null;
        shoppingCart1.addItem(soldItem26);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(soldItemList24);
        org.junit.Assert.assertNotNull(soldItemList25);
    }

    @Test
    public void test4069() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4069");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test4070() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4070");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test4071() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4071");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4072() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4072");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList25 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList24);
        org.junit.Assert.assertNotNull(soldItemList25);
    }

    @Test
    public void test4073() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4073");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test4074() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4074");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        java.util.List<SoldItem> soldItemList25 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass27 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList25);
        org.junit.Assert.assertNotNull(wildcardClass27);
    }

    @Test
    public void test4075() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4075");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test4076() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4076");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test4077() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4077");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test4078() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4078");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4079() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4079");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test4080() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4080");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test4081() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4081");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test4082() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4082");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test4083() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4083");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test4084() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4084");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass22 = soldItemList21.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test4085() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4085");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test4086() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4086");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        SoldItem soldItem24 = null;
        shoppingCart1.addItem(soldItem24);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test4087() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4087");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test4088() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4088");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test4089() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4089");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test4090() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4090");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test4091() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4091");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass17 = soldItemList16.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test4092() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4092");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.lang.Class<?> wildcardClass22 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test4093() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4093");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test4094() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4094");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList23);
    }

    @Test
    public void test4095() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4095");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4096() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4096");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test4097() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4097");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test4098() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4098");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test4099() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4099");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test4100() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4100");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass12 = soldItemList11.getClass();
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4101() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4101");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4102() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4102");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        SoldItem soldItem24 = null;
        shoppingCart1.addItem(soldItem24);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList23);
    }

    @Test
    public void test4103() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4103");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test4104() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4104");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4105() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4105");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass25 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass25);
    }

    @Test
    public void test4106() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4106");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4107() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4107");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test4108() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4108");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test4109() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4109");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList4);
    }

    @Test
    public void test4110() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4110");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4111() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4111");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test4112() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4112");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test4113() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4113");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem26 = null;
        shoppingCart1.addItem(soldItem26);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList24);
    }

    @Test
    public void test4114() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4114");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.lang.Class<?> wildcardClass12 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4115() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4115");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4116() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4116");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass22 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test4117() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4117");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test4118() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4118");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test4119() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4119");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test4120() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4120");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4121() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4121");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList25 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass26 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(soldItemList23);
        org.junit.Assert.assertNotNull(soldItemList25);
        org.junit.Assert.assertNotNull(wildcardClass26);
    }

    @Test
    public void test4122() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4122");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test4123() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4123");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test4124() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4124");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass13 = soldItemList12.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test4125() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4125");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test4126() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4126");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
    }

    @Test
    public void test4127() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4127");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass19 = soldItemList18.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test4128() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4128");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.lang.Class<?> wildcardClass21 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test4129() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4129");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass22 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test4130() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4130");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test4131() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4131");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test4132() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4132");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test4133() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4133");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4134() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4134");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test4135() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4135");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test4136() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4136");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass9 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test4137() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4137");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass12 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4138() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4138");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass12 = soldItemList11.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4139() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4139");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test4140() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4140");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test4141() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4141");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test4142() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4142");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test4143() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4143");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test4144() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4144");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass17 = soldItemList16.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test4145() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4145");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test4146() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4146");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test4147() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4147");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test4148() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4148");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test4149() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4149");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test4150() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4150");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4151() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4151");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test4152() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4152");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test4153() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4153");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test4154() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4154");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test4155() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4155");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test4156() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4156");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass15 = soldItemList14.getClass();
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test4157() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4157");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
    }

    @Test
    public void test4158() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4158");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test4159() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4159");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test4160() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4160");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test4161() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4161");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = soldItemList13.getClass();
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test4162() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4162");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test4163() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4163");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test4164() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4164");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4165() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4165");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test4166() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4166");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test4167() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4167");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test4168() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4168");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test4169() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4169");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test4170() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4170");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4171() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4171");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList28 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass29 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList26);
        org.junit.Assert.assertNotNull(soldItemList28);
        org.junit.Assert.assertNotNull(wildcardClass29);
    }

    @Test
    public void test4172() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4172");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test4173() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4173");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test4174() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4174");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test4175() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4175");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test4176() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4176");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test4177() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4177");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4178() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4178");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test4179() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4179");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test4180() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4180");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test4181() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4181");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass15 = soldItemList14.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test4182() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4182");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test4183() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4183");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4184() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4184");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass11 = soldItemList10.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test4185() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4185");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass19 = soldItemList18.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test4186() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4186");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test4187() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4187");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test4188() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4188");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test4189() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4189");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test4190() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4190");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test4191() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4191");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test4192() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4192");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test4193() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4193");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass7 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test4194() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4194");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test4195() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4195");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test4196() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4196");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        SoldItem soldItem24 = null;
        shoppingCart1.addItem(soldItem24);
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem30 = null;
        shoppingCart1.addItem(soldItem30);
        java.util.List<SoldItem> soldItemList32 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList26);
        org.junit.Assert.assertNotNull(soldItemList32);
    }

    @Test
    public void test4197() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4197");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass11 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test4198() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4198");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test4199() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4199");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4200() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4200");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4201() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4201");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test4202() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4202");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass21 = soldItemList20.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test4203() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4203");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test4204() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4204");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        java.lang.Class<?> wildcardClass24 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test4205() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4205");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass20 = soldItemList19.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test4206() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4206");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test4207() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4207");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass12 = soldItemList11.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4208() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4208");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test4209() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4209");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test4210() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4210");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList27 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem29 = null;
        shoppingCart1.addItem(soldItem29);
        java.util.List<SoldItem> soldItemList31 = shoppingCart1.getAll();
        SoldItem soldItem32 = null;
        shoppingCart1.addItem(soldItem32);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList26);
        org.junit.Assert.assertNotNull(soldItemList27);
        org.junit.Assert.assertNotNull(soldItemList31);
    }

    @Test
    public void test4211() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4211");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test4212() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4212");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test4213() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4213");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test4214() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4214");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        SoldItem soldItem25 = null;
        shoppingCart1.addItem(soldItem25);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem29 = null;
        shoppingCart1.addItem(soldItem29);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test4215() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4215");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass17 = soldItemList16.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test4216() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4216");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.lang.Class<?> wildcardClass10 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test4217() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4217");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test4218() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4218");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList25 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass26 = soldItemList25.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(soldItemList23);
        org.junit.Assert.assertNotNull(soldItemList25);
        org.junit.Assert.assertNotNull(wildcardClass26);
    }

    @Test
    public void test4219() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4219");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass21 = soldItemList20.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test4220() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4220");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
    }

    @Test
    public void test4221() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4221");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList25 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList24);
        org.junit.Assert.assertNotNull(soldItemList25);
        org.junit.Assert.assertNotNull(soldItemList26);
    }

    @Test
    public void test4222() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4222");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test4223() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4223");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test4224() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4224");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test4225() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4225");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test4226() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4226");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test4227() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4227");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test4228() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4228");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4229() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4229");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test4230() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4230");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test4231() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4231");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass17 = soldItemList16.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test4232() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4232");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass21 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test4233() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4233");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        SoldItem soldItem25 = null;
        shoppingCart1.addItem(soldItem25);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test4234() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4234");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass17 = soldItemList16.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test4235() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4235");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.lang.Class<?> wildcardClass8 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4236() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4236");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test4237() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4237");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test4238() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4238");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4239() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4239");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4240() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4240");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList24);
    }

    @Test
    public void test4241() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4241");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = soldItemList13.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test4242() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4242");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList25 = shoppingCart1.getAll();
        SoldItem soldItem26 = null;
        shoppingCart1.addItem(soldItem26);
        java.util.List<SoldItem> soldItemList28 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList23);
        org.junit.Assert.assertNotNull(soldItemList24);
        org.junit.Assert.assertNotNull(soldItemList25);
        org.junit.Assert.assertNotNull(soldItemList28);
    }

    @Test
    public void test4243() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4243");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test4244() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4244");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test4245() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4245");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test4246() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4246");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test4247() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4247");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
    }

    @Test
    public void test4248() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4248");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        SoldItem soldItem25 = null;
        shoppingCart1.addItem(soldItem25);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test4249() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4249");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4250() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4250");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test4251() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4251");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test4252() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4252");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test4253() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4253");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4254() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4254");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test4255() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4255");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test4256() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4256");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList25 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList23);
        org.junit.Assert.assertNotNull(soldItemList25);
    }

    @Test
    public void test4257() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4257");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass20 = soldItemList19.getClass();
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test4258() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4258");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass21 = soldItemList20.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test4259() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4259");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4260() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4260");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
    }

    @Test
    public void test4261() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4261");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        java.util.List<SoldItem> soldItemList25 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        SoldItem soldItem27 = null;
        shoppingCart1.addItem(soldItem27);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList25);
        org.junit.Assert.assertNotNull(soldItemList26);
    }

    @Test
    public void test4262() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4262");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test4263() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4263");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test4264() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4264");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test4265() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4265");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test4266() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4266");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test4267() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4267");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4268() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4268");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test4269() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4269");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass12 = soldItemList11.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4270() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4270");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
    }

    @Test
    public void test4271() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4271");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test4272() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4272");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass21 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test4273() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4273");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test4274() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4274");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4275() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4275");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test4276() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4276");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4277() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4277");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test4278() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4278");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test4279() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4279");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test4280() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4280");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test4281() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4281");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass24 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test4282() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4282");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = soldItemList13.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test4283() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4283");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test4284() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4284");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test4285() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4285");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
    }

    @Test
    public void test4286() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4286");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass16 = soldItemList15.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4287() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4287");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test4288() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4288");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test4289() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4289");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem24 = null;
        shoppingCart1.addItem(soldItem24);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test4290() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4290");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass22 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test4291() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4291");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test4292() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4292");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test4293() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4293");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList25 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass27 = soldItemList26.getClass();
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(soldItemList25);
        org.junit.Assert.assertNotNull(soldItemList26);
        org.junit.Assert.assertNotNull(wildcardClass27);
    }

    @Test
    public void test4294() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4294");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass23 = soldItemList22.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test4295() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4295");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test4296() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4296");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4297() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4297");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test4298() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4298");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test4299() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4299");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = soldItemList13.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test4300() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4300");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        SoldItem soldItem24 = null;
        shoppingCart1.addItem(soldItem24);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(soldItemList23);
    }

    @Test
    public void test4301() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4301");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test4302() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4302");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4303() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4303");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test4304() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4304");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4305() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4305");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test4306() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4306");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test4307() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4307");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test4308() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4308");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test4309() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4309");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4310() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4310");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass12 = soldItemList11.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4311() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4311");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.lang.Class<?> wildcardClass12 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4312() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4312");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test4313() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4313");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test4314() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4314");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test4315() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4315");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test4316() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4316");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
    }

    @Test
    public void test4317() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4317");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test4318() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4318");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test4319() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4319");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test4320() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4320");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass13 = soldItemList12.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test4321() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4321");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test4322() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4322");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4323() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4323");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass22 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test4324() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4324");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test4325() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4325");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test4326() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4326");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem26 = null;
        shoppingCart1.addItem(soldItem26);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(soldItemList24);
    }

    @Test
    public void test4327() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4327");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        java.lang.Class<?> wildcardClass24 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test4328() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4328");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test4329() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4329");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4330() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4330");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4331() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4331");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4332() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4332");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test4333() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4333");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList23);
    }

    @Test
    public void test4334() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4334");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList23);
    }

    @Test
    public void test4335() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4335");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test4336() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4336");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem25 = null;
        shoppingCart1.addItem(soldItem25);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList23);
    }

    @Test
    public void test4337() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4337");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test4338() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4338");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList27 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList23);
        org.junit.Assert.assertNotNull(soldItemList24);
        org.junit.Assert.assertNotNull(soldItemList27);
    }

    @Test
    public void test4339() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4339");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass15 = soldItemList14.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test4340() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4340");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test4341() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4341");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test4342() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4342");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass16 = soldItemList15.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4343() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4343");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass12 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4344() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4344");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = soldItemList13.getClass();
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test4345() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4345");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test4346() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4346");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test4347() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4347");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test4348() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4348");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test4349() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4349");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4350() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4350");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test4351() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4351");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass22 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test4352() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4352");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
    }

    @Test
    public void test4353() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4353");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass23 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test4354() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4354");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        SoldItem soldItem25 = null;
        shoppingCart1.addItem(soldItem25);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4355() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4355");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4356() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4356");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test4357() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4357");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass10 = soldItemList9.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test4358() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4358");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass12 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4359() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4359");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test4360() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4360");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4361() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4361");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test4362() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4362");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test4363() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4363");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test4364() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4364");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass25 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList23);
        org.junit.Assert.assertNotNull(soldItemList24);
        org.junit.Assert.assertNotNull(wildcardClass25);
    }

    @Test
    public void test4365() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4365");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test4366() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4366");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass21 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test4367() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4367");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
    }

    @Test
    public void test4368() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4368");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test4369() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4369");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test4370() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4370");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
    }

    @Test
    public void test4371() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4371");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test4372() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4372");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass11 = soldItemList10.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test4373() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4373");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass10 = soldItemList9.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test4374() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4374");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList25 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        SoldItem soldItem27 = null;
        shoppingCart1.addItem(soldItem27);
        java.lang.Class<?> wildcardClass29 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList24);
        org.junit.Assert.assertNotNull(soldItemList25);
        org.junit.Assert.assertNotNull(soldItemList26);
        org.junit.Assert.assertNotNull(wildcardClass29);
    }

    @Test
    public void test4375() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4375");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test4376() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4376");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test4377() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4377");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test4378() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4378");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test4379() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4379");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass23 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test4380() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4380");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test4381() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4381");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test4382() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4382");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test4383() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4383");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test4384() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4384");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test4385() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4385");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test4386() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4386");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test4387() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4387");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
    }

    @Test
    public void test4388() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4388");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
    }

    @Test
    public void test4389() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4389");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass22 = soldItemList21.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test4390() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4390");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass12 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4391() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4391");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test4392() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4392");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test4393() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4393");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test4394() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4394");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test4395() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4395");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test4396() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4396");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test4397() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4397");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test4398() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4398");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.lang.Class<?> wildcardClass21 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test4399() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4399");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test4400() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4400");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList23);
    }

    @Test
    public void test4401() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4401");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        SoldItem soldItem25 = null;
        shoppingCart1.addItem(soldItem25);
        java.util.List<SoldItem> soldItemList27 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass28 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList27);
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test4402() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4402");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test4403() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4403");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
    }

    @Test
    public void test4404() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4404");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test4405() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4405");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass23 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test4406() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4406");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test4407() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4407");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test4408() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4408");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test4409() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4409");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList25 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass26 = soldItemList25.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(soldItemList25);
        org.junit.Assert.assertNotNull(wildcardClass26);
    }

    @Test
    public void test4410() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4410");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test4411() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4411");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test4412() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4412");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test4413() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4413");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test4414() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4414");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test4415() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4415");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
    }

    @Test
    public void test4416() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4416");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test4417() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4417");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test4418() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4418");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.lang.Class<?> wildcardClass10 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test4419() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4419");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test4420() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4420");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test4421() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4421");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test4422() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4422");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4423() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4423");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test4424() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4424");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass16 = soldItemList15.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4425() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4425");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4426() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4426");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test4427() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4427");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.lang.Class<?> wildcardClass12 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4428() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4428");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4429() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4429");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test4430() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4430");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4431() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4431");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4432() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4432");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test4433() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4433");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test4434() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4434");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test4435() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4435");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test4436() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4436");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test4437() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4437");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test4438() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4438");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4439() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4439");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test4440() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4440");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test4441() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4441");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test4442() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4442");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test4443() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4443");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test4444() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4444");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4445() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4445");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test4446() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4446");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test4447() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4447");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
    }

    @Test
    public void test4448() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4448");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test4449() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4449");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4450() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4450");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test4451() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4451");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4452() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4452");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass13 = soldItemList12.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test4453() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4453");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test4454() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4454");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test4455() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4455");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test4456() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4456");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test4457() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4457");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass11 = soldItemList10.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test4458() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4458");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test4459() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4459");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4460() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4460");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList23);
    }

    @Test
    public void test4461() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4461");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = soldItemList13.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test4462() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4462");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass9 = soldItemList8.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test4463() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4463");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
    }

    @Test
    public void test4464() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4464");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test4465() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4465");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList24);
    }

    @Test
    public void test4466() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4466");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test4467() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4467");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4468() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4468");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test4469() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4469");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4470() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4470");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test4471() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4471");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test4472() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4472");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test4473() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4473");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test4474() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4474");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test4475() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4475");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4476() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4476");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test4477() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4477");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test4478() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4478");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test4479() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4479");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test4480() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4480");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass18 = soldItemList17.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test4481() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4481");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test4482() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4482");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test4483() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4483");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test4484() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4484");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test4485() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4485");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test4486() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4486");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4487() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4487");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test4488() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4488");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test4489() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4489");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test4490() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4490");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        SoldItem soldItem27 = null;
        shoppingCart1.addItem(soldItem27);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList23);
        org.junit.Assert.assertNotNull(soldItemList24);
        org.junit.Assert.assertNotNull(soldItemList26);
    }

    @Test
    public void test4491() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4491");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test4492() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4492");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test4493() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4493");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test4494() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4494");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4495() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4495");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test4496() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4496");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test4497() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4497");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test4498() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4498");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test4499() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4499");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test4500() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test4500");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
    }
}

