import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest7 {

    public static boolean debug = false;

    @Test
    public void test3501() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3501");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test3502() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3502");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test3503() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3503");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test3504() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3504");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList27 = shoppingCart1.getAll();
        SoldItem soldItem28 = null;
        shoppingCart1.addItem(soldItem28);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList31 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList26);
        org.junit.Assert.assertNotNull(soldItemList27);
        org.junit.Assert.assertNotNull(soldItemList31);
    }

    @Test
    public void test3505() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3505");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass12 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3506() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3506");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test3507() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3507");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass21 = soldItemList20.getClass();
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test3508() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3508");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test3509() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3509");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass18 = soldItemList17.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test3510() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3510");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test3511() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3511");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3512() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3512");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test3513() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3513");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3514() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3514");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3515() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3515");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test3516() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3516");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3517() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3517");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass15 = soldItemList14.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test3518() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3518");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = soldItemList13.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test3519() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3519");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test3520() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3520");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test3521() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3521");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test3522() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3522");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test3523() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3523");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3524() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3524");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test3525() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3525");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test3526() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3526");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test3527() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3527");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test3528() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3528");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3529() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3529");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test3530() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3530");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3531() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3531");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass24 = soldItemList23.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(soldItemList23);
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test3532() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3532");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem24 = null;
        shoppingCart1.addItem(soldItem24);
        SoldItem soldItem26 = null;
        shoppingCart1.addItem(soldItem26);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test3533() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3533");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass8 = soldItemList7.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3534() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3534");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test3535() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3535");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test3536() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3536");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
    }

    @Test
    public void test3537() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3537");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test3538() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3538");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test3539() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3539");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass10 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test3540() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3540");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test3541() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3541");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test3542() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3542");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass13 = soldItemList12.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test3543() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3543");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test3544() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3544");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test3545() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3545");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test3546() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3546");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3547() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3547");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test3548() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3548");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test3549() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3549");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test3550() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3550");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
    }

    @Test
    public void test3551() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3551");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test3552() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3552");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test3553() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3553");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass15 = soldItemList14.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test3554() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3554");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test3555() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3555");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test3556() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3556");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test3557() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3557");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3558() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3558");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test3559() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3559");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test3560() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3560");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test3561() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3561");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test3562() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3562");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass21 = soldItemList20.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test3563() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3563");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test3564() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3564");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test3565() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3565");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        SoldItem soldItem25 = null;
        shoppingCart1.addItem(soldItem25);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass28 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test3566() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3566");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test3567() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3567");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test3568() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3568");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3569() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3569");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test3570() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3570");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test3571() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3571");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test3572() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3572");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test3573() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3573");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test3574() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3574");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3575() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3575");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test3576() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3576");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test3577() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3577");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
    }

    @Test
    public void test3578() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3578");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass8 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3579() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3579");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test3580() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3580");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem24 = null;
        shoppingCart1.addItem(soldItem24);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test3581() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3581");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3582() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3582");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test3583() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3583");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test3584() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3584");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test3585() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3585");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass17 = soldItemList16.getClass();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test3586() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3586");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test3587() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3587");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass18 = soldItemList17.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test3588() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3588");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test3589() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3589");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test3590() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3590");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass9 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test3591() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3591");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test3592() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3592");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test3593() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3593");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test3594() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3594");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3595() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3595");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3596() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3596");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test3597() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3597");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test3598() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3598");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test3599() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3599");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test3600() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3600");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass15 = soldItemList14.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test3601() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3601");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem24 = null;
        shoppingCart1.addItem(soldItem24);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test3602() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3602");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3603() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3603");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test3604() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3604");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test3605() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3605");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test3606() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3606");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass11 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test3607() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3607");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test3608() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3608");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test3609() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3609");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
    }

    @Test
    public void test3610() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3610");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test3611() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3611");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass23 = soldItemList22.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test3612() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3612");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test3613() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3613");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test3614() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3614");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test3615() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3615");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test3616() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3616");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass21 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test3617() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3617");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test3618() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3618");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test3619() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3619");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test3620() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3620");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass10 = soldItemList9.getClass();
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test3621() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3621");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test3622() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3622");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test3623() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3623");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test3624() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3624");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        SoldItem soldItem24 = null;
        shoppingCart1.addItem(soldItem24);
        java.lang.Class<?> wildcardClass26 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList23);
        org.junit.Assert.assertNotNull(wildcardClass26);
    }

    @Test
    public void test3625() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3625");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.lang.Class<?> wildcardClass21 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test3626() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3626");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test3627() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3627");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
    }

    @Test
    public void test3628() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3628");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test3629() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3629");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test3630() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3630");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass21 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test3631() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3631");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test3632() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3632");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test3633() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3633");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        SoldItem soldItem24 = null;
        shoppingCart1.addItem(soldItem24);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test3634() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3634");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test3635() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3635");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test3636() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3636");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test3637() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3637");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test3638() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3638");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3639() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3639");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass27 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList26);
        org.junit.Assert.assertNotNull(wildcardClass27);
    }

    @Test
    public void test3640() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3640");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass13 = soldItemList12.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test3641() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3641");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3642() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3642");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test3643() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3643");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test3644() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3644");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass10 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test3645() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3645");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test3646() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3646");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.lang.Class<?> wildcardClass8 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3647() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3647");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test3648() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3648");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3649() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3649");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3650() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3650");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
    }

    @Test
    public void test3651() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3651");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test3652() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3652");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test3653() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3653");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem26 = null;
        shoppingCart1.addItem(soldItem26);
        java.util.List<SoldItem> soldItemList28 = shoppingCart1.getAll();
        SoldItem soldItem29 = null;
        shoppingCart1.addItem(soldItem29);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(soldItemList28);
    }

    @Test
    public void test3654() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3654");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.lang.Class<?> wildcardClass23 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test3655() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3655");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
    }

    @Test
    public void test3656() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3656");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test3657() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3657");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test3658() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3658");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test3659() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3659");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test3660() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3660");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass16 = soldItemList15.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3661() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3661");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test3662() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3662");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test3663() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3663");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test3664() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3664");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test3665() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3665");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
    }

    @Test
    public void test3666() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3666");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass21 = soldItemList20.getClass();
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test3667() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3667");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass10 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test3668() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3668");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass25 = soldItemList24.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList23);
        org.junit.Assert.assertNotNull(soldItemList24);
        org.junit.Assert.assertNotNull(wildcardClass25);
    }

    @Test
    public void test3669() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3669");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test3670() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3670");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test3671() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3671");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test3672() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3672");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3673() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3673");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass17 = soldItemList16.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test3674() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3674");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test3675() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3675");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3676() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3676");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass9 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test3677() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3677");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = soldItemList13.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test3678() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3678");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test3679() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3679");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test3680() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3680");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.lang.Class<?> wildcardClass11 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test3681() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3681");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test3682() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3682");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test3683() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3683");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass20 = soldItemList19.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test3684() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3684");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass10 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test3685() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3685");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.lang.Class<?> wildcardClass22 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test3686() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3686");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test3687() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3687");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test3688() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3688");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test3689() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3689");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test3690() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3690");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test3691() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3691");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3692() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3692");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test3693() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3693");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3694() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3694");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3695() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3695");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test3696() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3696");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3697() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3697");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3698() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3698");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test3699() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3699");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test3700() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3700");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test3701() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3701");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
    }

    @Test
    public void test3702() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3702");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test3703() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3703");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test3704() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3704");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3705() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3705");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test3706() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3706");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass7 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test3707() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3707");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3708() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3708");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test3709() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3709");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass10 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test3710() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3710");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test3711() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3711");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test3712() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3712");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList25 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList24);
        org.junit.Assert.assertNotNull(soldItemList25);
    }

    @Test
    public void test3713() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3713");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test3714() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3714");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass11 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test3715() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3715");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass9 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test3716() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3716");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        java.util.List<SoldItem> soldItemList25 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(soldItemList25);
    }

    @Test
    public void test3717() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3717");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test3718() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3718");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test3719() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3719");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass12 = soldItemList11.getClass();
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3720() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3720");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test3721() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3721");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test3722() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3722");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3723() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3723");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test3724() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3724");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test3725() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3725");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass24 = soldItemList23.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList23);
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test3726() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3726");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass21 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test3727() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3727");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3728() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3728");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList25 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(soldItemList23);
        org.junit.Assert.assertNotNull(soldItemList25);
    }

    @Test
    public void test3729() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3729");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test3730() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3730");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test3731() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3731");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test3732() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3732");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test3733() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3733");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test3734() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3734");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test3735() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3735");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test3736() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3736");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3737() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3737");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test3738() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3738");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3739() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3739");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test3740() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3740");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test3741() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3741");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3742() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3742");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass23 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test3743() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3743");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        java.lang.Class<?> wildcardClass25 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass25);
    }

    @Test
    public void test3744() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3744");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test3745() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3745");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass12 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3746() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3746");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(soldItemList24);
    }

    @Test
    public void test3747() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3747");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test3748() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3748");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass15 = soldItemList14.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test3749() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3749");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test3750() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3750");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test3751() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3751");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test3752() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3752");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3753() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3753");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass22 = soldItemList21.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test3754() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3754");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3755() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3755");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test3756() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3756");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test3757() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3757");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3758() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3758");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass11 = soldItemList10.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test3759() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3759");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test3760() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3760");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass25 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(wildcardClass25);
    }

    @Test
    public void test3761() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3761");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test3762() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3762");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test3763() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3763");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test3764() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3764");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
    }

    @Test
    public void test3765() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3765");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.lang.Class<?> wildcardClass21 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test3766() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3766");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList27 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList29 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList30 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList26);
        org.junit.Assert.assertNotNull(soldItemList27);
        org.junit.Assert.assertNotNull(soldItemList29);
        org.junit.Assert.assertNotNull(soldItemList30);
    }

    @Test
    public void test3767() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3767");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test3768() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3768");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test3769() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3769");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass24 = soldItemList23.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(soldItemList23);
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test3770() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3770");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test3771() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3771");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test3772() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3772");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3773() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3773");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test3774() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3774");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test3775() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3775");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test3776() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3776");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test3777() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3777");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test3778() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3778");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3779() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3779");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass12 = soldItemList11.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3780() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3780");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test3781() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3781");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test3782() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3782");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test3783() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3783");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
    }

    @Test
    public void test3784() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3784");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test3785() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3785");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass12 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3786() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3786");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test3787() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3787");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        java.lang.Class<?> wildcardClass25 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(wildcardClass25);
    }

    @Test
    public void test3788() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3788");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3789() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3789");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test3790() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3790");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass11 = soldItemList10.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test3791() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3791");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test3792() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3792");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.lang.Class<?> wildcardClass12 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3793() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3793");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test3794() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3794");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList23);
    }

    @Test
    public void test3795() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3795");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3796() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3796");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test3797() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3797");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass20 = soldItemList19.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test3798() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3798");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test3799() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3799");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test3800() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3800");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList24);
    }

    @Test
    public void test3801() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3801");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test3802() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3802");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test3803() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3803");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass20 = soldItemList19.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test3804() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3804");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test3805() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3805");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test3806() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3806");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList23);
    }

    @Test
    public void test3807() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3807");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test3808() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3808");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass13 = soldItemList12.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test3809() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3809");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test3810() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3810");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test3811() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3811");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3812() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3812");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test3813() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3813");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3814() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3814");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem26 = null;
        shoppingCart1.addItem(soldItem26);
        SoldItem soldItem28 = null;
        shoppingCart1.addItem(soldItem28);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList24);
    }

    @Test
    public void test3815() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3815");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3816() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3816");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3817() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3817");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test3818() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3818");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test3819() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3819");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test3820() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3820");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3821() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3821");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3822() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3822");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test3823() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3823");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test3824() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3824");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test3825() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3825");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3826() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3826");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test3827() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3827");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test3828() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3828");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test3829() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3829");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test3830() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3830");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass21 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test3831() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3831");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
    }

    @Test
    public void test3832() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3832");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test3833() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3833");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3834() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3834");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3835() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3835");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass25 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList24);
        org.junit.Assert.assertNotNull(wildcardClass25);
    }

    @Test
    public void test3836() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3836");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test3837() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3837");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass17 = soldItemList16.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test3838() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3838");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test3839() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3839");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass23 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test3840() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3840");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test3841() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3841");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3842() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3842");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass18 = soldItemList17.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test3843() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3843");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test3844() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3844");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test3845() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3845");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test3846() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3846");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test3847() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3847");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass24 = soldItemList23.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList23);
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test3848() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3848");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test3849() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3849");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test3850() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3850");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test3851() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3851");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass11 = soldItemList10.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test3852() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3852");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass7 = soldItemList6.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test3853() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3853");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass11 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test3854() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3854");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test3855() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3855");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList25 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(soldItemList23);
        org.junit.Assert.assertNotNull(soldItemList25);
    }

    @Test
    public void test3856() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3856");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test3857() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3857");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test3858() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3858");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test3859() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3859");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test3860() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3860");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3861() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3861");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test3862() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3862");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass21 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test3863() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3863");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test3864() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3864");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test3865() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3865");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test3866() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3866");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3867() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3867");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3868() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3868");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test3869() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3869");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test3870() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3870");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        SoldItem soldItem25 = null;
        shoppingCart1.addItem(soldItem25);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3871() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3871");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList24);
    }

    @Test
    public void test3872() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3872");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass13 = soldItemList12.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test3873() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3873");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass11 = soldItemList10.getClass();
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test3874() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3874");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList27 = shoppingCart1.getAll();
        SoldItem soldItem28 = null;
        shoppingCart1.addItem(soldItem28);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass31 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList26);
        org.junit.Assert.assertNotNull(soldItemList27);
        org.junit.Assert.assertNotNull(wildcardClass31);
    }

    @Test
    public void test3875() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3875");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test3876() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3876");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass15 = soldItemList14.getClass();
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test3877() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3877");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test3878() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3878");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test3879() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3879");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test3880() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3880");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test3881() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3881");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test3882() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3882");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3883() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3883");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test3884() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3884");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test3885() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3885");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test3886() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3886");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList27 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem29 = null;
        shoppingCart1.addItem(soldItem29);
        java.util.List<SoldItem> soldItemList31 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList32 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList26);
        org.junit.Assert.assertNotNull(soldItemList27);
        org.junit.Assert.assertNotNull(soldItemList31);
        org.junit.Assert.assertNotNull(soldItemList32);
    }

    @Test
    public void test3887() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3887");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test3888() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3888");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test3889() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3889");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test3890() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3890");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test3891() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3891");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3892() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3892");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test3893() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3893");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass19 = soldItemList18.getClass();
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test3894() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3894");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test3895() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3895");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3896() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3896");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass10 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test3897() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3897");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test3898() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3898");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test3899() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3899");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3900() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3900");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass9 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test3901() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3901");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass13 = soldItemList12.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test3902() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3902");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test3903() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3903");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList23);
    }

    @Test
    public void test3904() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3904");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3905() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3905");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test3906() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3906");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test3907() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3907");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.lang.Class<?> wildcardClass21 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test3908() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3908");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3909() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3909");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test3910() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3910");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.lang.Class<?> wildcardClass21 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test3911() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3911");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        SoldItem soldItem24 = null;
        shoppingCart1.addItem(soldItem24);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList23);
    }

    @Test
    public void test3912() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3912");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test3913() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3913");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass23 = soldItemList22.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test3914() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3914");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = soldItemList13.getClass();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test3915() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3915");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test3916() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3916");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3917() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3917");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test3918() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3918");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test3919() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3919");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test3920() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3920");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem25 = null;
        shoppingCart1.addItem(soldItem25);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList23);
    }

    @Test
    public void test3921() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3921");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3922() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3922");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test3923() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3923");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3924() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3924");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test3925() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3925");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3926() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3926");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass23 = soldItemList22.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test3927() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3927");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test3928() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3928");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3929() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3929");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test3930() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3930");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
    }

    @Test
    public void test3931() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3931");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.lang.Class<?> wildcardClass12 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3932() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3932");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test3933() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3933");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
    }

    @Test
    public void test3934() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3934");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test3935() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3935");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test3936() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3936");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test3937() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3937");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test3938() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3938");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test3939() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3939");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test3940() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3940");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test3941() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3941");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass10 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test3942() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3942");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.lang.Class<?> wildcardClass12 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3943() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3943");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test3944() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3944");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test3945() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3945");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test3946() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3946");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test3947() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3947");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test3948() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3948");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test3949() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3949");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test3950() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3950");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3951() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3951");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass22 = soldItemList21.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test3952() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3952");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3953() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3953");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test3954() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3954");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test3955() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3955");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass8 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3956() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3956");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test3957() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3957");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test3958() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3958");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        org.junit.Assert.assertNotNull(soldItemList2);
    }

    @Test
    public void test3959() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3959");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test3960() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3960");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test3961() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3961");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test3962() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3962");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test3963() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3963");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test3964() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3964");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test3965() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3965");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList23);
    }

    @Test
    public void test3966() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3966");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3967() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3967");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3968() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3968");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test3969() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3969");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test3970() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3970");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3971() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3971");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test3972() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3972");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass18 = soldItemList17.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test3973() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3973");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test3974() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3974");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass9 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test3975() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3975");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test3976() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3976");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3977() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3977");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test3978() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3978");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test3979() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3979");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test3980() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3980");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
    }

    @Test
    public void test3981() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3981");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass18 = soldItemList17.getClass();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test3982() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3982");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3983() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3983");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3984() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3984");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test3985() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3985");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test3986() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3986");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3987() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3987");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3988() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3988");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test3989() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3989");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.lang.Class<?> wildcardClass21 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test3990() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3990");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList23);
        org.junit.Assert.assertNotNull(soldItemList24);
    }

    @Test
    public void test3991() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3991");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test3992() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3992");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test3993() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3993");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test3994() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3994");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test3995() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3995");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3996() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3996");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test3997() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3997");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test3998() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3998");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test3999() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test3999");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass8 = soldItemList7.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4000() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest7.test4000");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
    }
}

