import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest14 {

    public static boolean debug = false;

    @Test
    public void test7001() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7001");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem24 = null;
        shoppingCart1.addItem(soldItem24);
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass27 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList26);
        org.junit.Assert.assertNotNull(wildcardClass27);
    }

    @Test
    public void test7002() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7002");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test7003() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7003");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test7004() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7004");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        java.util.List<SoldItem> soldItemList25 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList25);
        org.junit.Assert.assertNotNull(soldItemList26);
    }

    @Test
    public void test7005() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7005");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test7006() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7006");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test7007() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7007");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test7008() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7008");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test7009() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7009");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test7010() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7010");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList23);
    }

    @Test
    public void test7011() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7011");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        java.lang.Class<?> wildcardClass24 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test7012() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7012");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass19 = soldItemList18.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test7013() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7013");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test7014() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7014");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test7015() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7015");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test7016() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7016");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test7017() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7017");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test7018() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7018");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test7019() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7019");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test7020() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7020");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test7021() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7021");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        SoldItem soldItem25 = null;
        shoppingCart1.addItem(soldItem25);
        java.util.List<SoldItem> soldItemList27 = shoppingCart1.getAll();
        SoldItem soldItem28 = null;
        shoppingCart1.addItem(soldItem28);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList27);
    }

    @Test
    public void test7022() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7022");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test7023() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7023");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test7024() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7024");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test7025() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7025");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test7026() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7026");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test7027() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7027");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass20 = soldItemList19.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test7028() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7028");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test7029() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7029");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test7030() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7030");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test7031() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7031");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test7032() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7032");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test7033() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7033");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass10 = soldItemList9.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test7034() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7034");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test7035() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7035");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test7036() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7036");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test7037() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7037");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test7038() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7038");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test7039() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7039");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test7040() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7040");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test7041() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7041");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test7042() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7042");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass13 = soldItemList12.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test7043() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7043");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test7044() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7044");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test7045() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7045");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
    }

    @Test
    public void test7046() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7046");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test7047() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7047");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test7048() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7048");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test7049() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7049");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass21 = soldItemList20.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test7050() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7050");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass24 = soldItemList23.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList23);
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test7051() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7051");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass15 = soldItemList14.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test7052() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7052");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test7053() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7053");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList24);
    }

    @Test
    public void test7054() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7054");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        SoldItem soldItem24 = null;
        shoppingCart1.addItem(soldItem24);
        SoldItem soldItem26 = null;
        shoppingCart1.addItem(soldItem26);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList23);
    }

    @Test
    public void test7055() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7055");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test7056() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7056");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        org.junit.Assert.assertNotNull(soldItemList5);
    }

    @Test
    public void test7057() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7057");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test7058() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7058");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass9 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test7059() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7059");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test7060() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7060");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test7061() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7061");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test7062() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7062");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test7063() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7063");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.lang.Class<?> wildcardClass23 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test7064() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7064");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.lang.Class<?> wildcardClass10 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test7065() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7065");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test7066() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7066");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test7067() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7067");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass18 = soldItemList17.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test7068() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7068");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test7069() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7069");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test7070() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7070");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test7071() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7071");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test7072() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7072");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test7073() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7073");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test7074() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7074");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
    }

    @Test
    public void test7075() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7075");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem26 = null;
        shoppingCart1.addItem(soldItem26);
        SoldItem soldItem28 = null;
        shoppingCart1.addItem(soldItem28);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList23);
        org.junit.Assert.assertNotNull(soldItemList24);
    }

    @Test
    public void test7076() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7076");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test7077() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7077");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        SoldItem soldItem24 = null;
        shoppingCart1.addItem(soldItem24);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem27 = null;
        shoppingCart1.addItem(soldItem27);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList30 = shoppingCart1.getAll();
        SoldItem soldItem31 = null;
        shoppingCart1.addItem(soldItem31);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList30);
    }

    @Test
    public void test7078() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7078");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.lang.Class<?> wildcardClass23 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test7079() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7079");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test7080() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7080");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(soldItemList23);
    }

    @Test
    public void test7081() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7081");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test7082() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7082");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test7083() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7083");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        SoldItem soldItem24 = null;
        shoppingCart1.addItem(soldItem24);
        java.lang.Class<?> wildcardClass26 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(soldItemList23);
        org.junit.Assert.assertNotNull(wildcardClass26);
    }

    @Test
    public void test7084() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7084");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test7085() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7085");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test7086() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7086");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.lang.Class<?> wildcardClass21 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test7087() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7087");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test7088() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7088");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test7089() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7089");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList27 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem29 = null;
        shoppingCart1.addItem(soldItem29);
        java.util.List<SoldItem> soldItemList31 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList32 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList33 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList26);
        org.junit.Assert.assertNotNull(soldItemList27);
        org.junit.Assert.assertNotNull(soldItemList31);
        org.junit.Assert.assertNotNull(soldItemList32);
        org.junit.Assert.assertNotNull(soldItemList33);
    }

    @Test
    public void test7090() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7090");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test7091() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7091");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test7092() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7092");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test7093() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7093");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test7094() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7094");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test7095() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7095");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test7096() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7096");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test7097() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7097");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test7098() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7098");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test7099() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7099");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test7100() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7100");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test7101() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7101");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test7102() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7102");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test7103() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7103");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass29 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(wildcardClass29);
    }

    @Test
    public void test7104() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7104");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test7105() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7105");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList24);
    }

    @Test
    public void test7106() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7106");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test7107() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7107");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test7108() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7108");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test7109() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7109");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test7110() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7110");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem26 = null;
        shoppingCart1.addItem(soldItem26);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList24);
    }

    @Test
    public void test7111() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7111");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test7112() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7112");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test7113() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7113");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test7114() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7114");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test7115() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7115");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test7116() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7116");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test7117() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7117");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test7118() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7118");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        SoldItem soldItem24 = null;
        shoppingCart1.addItem(soldItem24);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem27 = null;
        shoppingCart1.addItem(soldItem27);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList30 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass31 = soldItemList30.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList30);
        org.junit.Assert.assertNotNull(wildcardClass31);
    }

    @Test
    public void test7119() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7119");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test7120() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7120");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test7121() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7121");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test7122() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7122");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test7123() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7123");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test7124() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7124");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test7125() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7125");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test7126() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7126");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test7127() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7127");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test7128() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7128");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test7129() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7129");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test7130() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7130");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test7131() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7131");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test7132() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7132");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test7133() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7133");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test7134() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7134");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test7135() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7135");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test7136() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7136");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test7137() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7137");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass12 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test7138() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7138");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test7139() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7139");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass16 = soldItemList15.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test7140() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7140");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test7141() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7141");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass10 = soldItemList9.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test7142() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7142");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test7143() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7143");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test7144() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7144");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test7145() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7145");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test7146() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7146");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test7147() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7147");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test7148() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7148");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass22 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test7149() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7149");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem26 = null;
        shoppingCart1.addItem(soldItem26);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test7150() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7150");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test7151() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7151");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test7152() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7152");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test7153() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7153");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test7154() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7154");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test7155() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7155");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test7156() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7156");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test7157() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7157");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test7158() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7158");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList27 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList23);
        org.junit.Assert.assertNotNull(soldItemList24);
        org.junit.Assert.assertNotNull(soldItemList26);
        org.junit.Assert.assertNotNull(soldItemList27);
    }

    @Test
    public void test7159() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7159");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test7160() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7160");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test7161() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7161");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test7162() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7162");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test7163() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7163");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test7164() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7164");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test7165() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7165");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        java.util.List<SoldItem> soldItemList25 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass26 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList25);
        org.junit.Assert.assertNotNull(wildcardClass26);
    }

    @Test
    public void test7166() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7166");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test7167() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7167");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test7168() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7168");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test7169() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7169");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test7170() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7170");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test7171() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7171");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test7172() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7172");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test7173() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7173");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test7174() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7174");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass24 = soldItemList23.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(soldItemList23);
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test7175() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7175");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test7176() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7176");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem24 = null;
        shoppingCart1.addItem(soldItem24);
        java.lang.Class<?> wildcardClass26 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass26);
    }

    @Test
    public void test7177() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7177");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test7178() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7178");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test7179() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7179");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test7180() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7180");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test7181() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7181");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test7182() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7182");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass18 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test7183() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7183");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test7184() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7184");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test7185() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7185");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test7186() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7186");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test7187() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7187");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        SoldItem soldItem25 = null;
        shoppingCart1.addItem(soldItem25);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test7188() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7188");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test7189() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7189");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test7190() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7190");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test7191() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7191");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test7192() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7192");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test7193() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7193");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test7194() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7194");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test7195() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7195");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test7196() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7196");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test7197() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7197");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test7198() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7198");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test7199() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7199");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test7200() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7200");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test7201() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7201");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test7202() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7202");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test7203() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7203");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test7204() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7204");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test7205() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7205");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass17 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test7206() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7206");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test7207() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7207");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test7208() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7208");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test7209() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7209");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass17 = soldItemList16.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test7210() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7210");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList23);
    }

    @Test
    public void test7211() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7211");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test7212() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7212");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test7213() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7213");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test7214() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7214");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        SoldItem soldItem24 = null;
        shoppingCart1.addItem(soldItem24);
        SoldItem soldItem26 = null;
        shoppingCart1.addItem(soldItem26);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test7215() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7215");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test7216() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7216");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test7217() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7217");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test7218() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7218");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test7219() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7219");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test7220() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7220");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test7221() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7221");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test7222() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7222");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test7223() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7223");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test7224() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7224");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test7225() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7225");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test7226() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7226");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass23 = soldItemList22.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test7227() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7227");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test7228() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7228");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test7229() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7229");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass21 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test7230() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7230");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test7231() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7231");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test7232() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7232");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test7233() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7233");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test7234() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7234");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test7235() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7235");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass20 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test7236() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7236");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test7237() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7237");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test7238() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7238");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test7239() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7239");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.lang.Class<?> wildcardClass11 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test7240() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7240");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList25 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList24);
        org.junit.Assert.assertNotNull(soldItemList25);
        org.junit.Assert.assertNotNull(soldItemList26);
    }

    @Test
    public void test7241() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7241");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test7242() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7242");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass21 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test7243() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7243");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test7244() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7244");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = soldItemList13.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test7245() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7245");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        java.util.List<SoldItem> soldItemList25 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList25);
        org.junit.Assert.assertNotNull(soldItemList26);
    }

    @Test
    public void test7246() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7246");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test7247() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7247");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test7248() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7248");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test7249() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7249");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test7250() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7250");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test7251() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7251");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem26 = null;
        shoppingCart1.addItem(soldItem26);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test7252() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7252");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test7253() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7253");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test7254() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7254");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test7255() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7255");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.lang.Class<?> wildcardClass21 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test7256() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7256");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test7257() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7257");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList27 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem29 = null;
        shoppingCart1.addItem(soldItem29);
        java.lang.Class<?> wildcardClass31 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList26);
        org.junit.Assert.assertNotNull(soldItemList27);
        org.junit.Assert.assertNotNull(wildcardClass31);
    }

    @Test
    public void test7258() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7258");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test7259() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7259");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem24 = null;
        shoppingCart1.addItem(soldItem24);
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass27 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList26);
        org.junit.Assert.assertNotNull(wildcardClass27);
    }

    @Test
    public void test7260() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7260");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test7261() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7261");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test7262() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7262");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
    }

    @Test
    public void test7263() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7263");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test7264() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7264");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test7265() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7265");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass9 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test7266() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7266");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass13 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test7267() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7267");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test7268() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7268");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList23 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(soldItemList23);
    }

    @Test
    public void test7269() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7269");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test7270() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7270");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test7271() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7271");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test7272() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7272");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test7273() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7273");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test7274() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7274");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test7275() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7275");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test7276() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7276");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList27 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem29 = null;
        shoppingCart1.addItem(soldItem29);
        java.util.List<SoldItem> soldItemList31 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList34 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList26);
        org.junit.Assert.assertNotNull(soldItemList27);
        org.junit.Assert.assertNotNull(soldItemList31);
        org.junit.Assert.assertNotNull(soldItemList34);
    }

    @Test
    public void test7277() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7277");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test7278() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7278");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test7279() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7279");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
    }

    @Test
    public void test7280() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7280");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList7);
    }

    @Test
    public void test7281() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7281");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
    }

    @Test
    public void test7282() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7282");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test7283() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7283");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test7284() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7284");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test7285() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7285");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList24);
    }

    @Test
    public void test7286() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7286");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test7287() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7287");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test7288() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7288");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test7289() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7289");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList27 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem29 = null;
        shoppingCart1.addItem(soldItem29);
        java.util.List<SoldItem> soldItemList31 = shoppingCart1.getAll();
        SoldItem soldItem32 = null;
        shoppingCart1.addItem(soldItem32);
        SoldItem soldItem34 = null;
        shoppingCart1.addItem(soldItem34);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList26);
        org.junit.Assert.assertNotNull(soldItemList27);
        org.junit.Assert.assertNotNull(soldItemList31);
    }

    @Test
    public void test7290() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7290");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test7291() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7291");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        SoldItem soldItem27 = null;
        shoppingCart1.addItem(soldItem27);
        org.junit.Assert.assertNotNull(soldItemList22);
        org.junit.Assert.assertNotNull(soldItemList24);
        org.junit.Assert.assertNotNull(soldItemList26);
    }

    @Test
    public void test7292() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7292");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass22 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test7293() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7293");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test7294() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7294");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test7295() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7295");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList25 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList24);
        org.junit.Assert.assertNotNull(soldItemList25);
    }

    @Test
    public void test7296() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7296");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test7297() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7297");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass16 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test7298() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7298");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test7299() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7299");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test7300() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7300");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
    }

    @Test
    public void test7301() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7301");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test7302() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7302");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList20);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test7303() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7303");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test7304() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7304");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
    }

    @Test
    public void test7305() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7305");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test7306() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7306");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList24);
    }

    @Test
    public void test7307() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7307");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
    }

    @Test
    public void test7308() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7308");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass18 = soldItemList17.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test7309() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7309");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test7310() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7310");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
    }

    @Test
    public void test7311() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7311");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test7312() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7312");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test7313() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7313");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test7314() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7314");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList6);
    }

    @Test
    public void test7315() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7315");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test7316() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7316");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList25 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList24);
        org.junit.Assert.assertNotNull(soldItemList25);
    }

    @Test
    public void test7317() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7317");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test7318() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7318");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test7319() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7319");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList15);
    }

    @Test
    public void test7320() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7320");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
    }

    @Test
    public void test7321() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7321");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList10);
    }

    @Test
    public void test7322() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7322");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test7323() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7323");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass23 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList21);
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test7324() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7324");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass17 = soldItemList16.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test7325() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7325");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
    }

    @Test
    public void test7326() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7326");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test7327() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7327");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList17);
    }

    @Test
    public void test7328() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7328");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        java.util.List<SoldItem> soldItemList24 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem26 = null;
        shoppingCart1.addItem(soldItem26);
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList24);
    }

    @Test
    public void test7329() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7329");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        SoldItem soldItem22 = null;
        shoppingCart1.addItem(soldItem22);
        SoldItem soldItem24 = null;
        shoppingCart1.addItem(soldItem24);
        java.util.List<SoldItem> soldItemList26 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList26);
    }

    @Test
    public void test7330() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7330");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test7331() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7331");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass14 = soldItemList13.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test7332() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7332");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        SoldItem soldItem20 = null;
        shoppingCart1.addItem(soldItem20);
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test7333() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7333");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test7334() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7334");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test7335() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7335");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList14);
    }

    @Test
    public void test7336() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7336");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList4 = shoppingCart1.getAll();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList4);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test7337() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7337");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem18 = null;
        shoppingCart1.addItem(soldItem18);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList22 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList22);
    }

    @Test
    public void test7338() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7338");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test7339() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7339");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList14 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList17 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.lang.Class<?> wildcardClass19 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList14);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList17);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test7340() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7340");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        java.util.List<SoldItem> soldItemList6 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.util.List<SoldItem> soldItemList15 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList6);
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList15);
        org.junit.Assert.assertNotNull(soldItemList16);
    }

    @Test
    public void test7341() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7341");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        SoldItem soldItem2 = null;
        shoppingCart1.addItem(soldItem2);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        java.lang.Class<?> wildcardClass14 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test7342() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7342");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        java.util.List<SoldItem> soldItemList5 = shoppingCart1.getAll();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem9 = null;
        shoppingCart1.addItem(soldItem9);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        java.lang.Class<?> wildcardClass15 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList5);
        org.junit.Assert.assertNotNull(soldItemList12);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test7343() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7343");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        java.util.List<SoldItem> soldItemList16 = shoppingCart1.getAll();
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList21 = shoppingCart1.getAll();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList13);
        org.junit.Assert.assertNotNull(soldItemList16);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList21);
    }

    @Test
    public void test7344() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7344");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList19 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList20 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList19);
        org.junit.Assert.assertNotNull(soldItemList20);
    }

    @Test
    public void test7345() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7345");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        SoldItem soldItem11 = null;
        shoppingCart1.addItem(soldItem11);
        java.util.List<SoldItem> soldItemList13 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        // The following exception was thrown during execution in test generation
        try {
            shoppingCart1.submitCurrentPurchase();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList13);
    }

    @Test
    public void test7346() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7346");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        SoldItem soldItem5 = null;
        shoppingCart1.addItem(soldItem5);
        java.util.List<SoldItem> soldItemList7 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList8 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList10 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        SoldItem soldItem12 = null;
        shoppingCart1.addItem(soldItem12);
        SoldItem soldItem14 = null;
        shoppingCart1.addItem(soldItem14);
        SoldItem soldItem16 = null;
        shoppingCart1.addItem(soldItem16);
        java.util.List<SoldItem> soldItemList18 = shoppingCart1.getAll();
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        SoldItem soldItem23 = null;
        shoppingCart1.addItem(soldItem23);
        java.lang.Class<?> wildcardClass25 = shoppingCart1.getClass();
        org.junit.Assert.assertNotNull(soldItemList7);
        org.junit.Assert.assertNotNull(soldItemList8);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList10);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(soldItemList18);
        org.junit.Assert.assertNotNull(wildcardClass25);
    }

    @Test
    public void test7347() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7347");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        java.util.List<SoldItem> soldItemList3 = shoppingCart1.getAll();
        SoldItem soldItem4 = null;
        shoppingCart1.addItem(soldItem4);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem7 = null;
        shoppingCart1.addItem(soldItem7);
        java.util.List<SoldItem> soldItemList9 = shoppingCart1.getAll();
        shoppingCart1.cancelCurrentPurchase();
        java.util.List<SoldItem> soldItemList11 = shoppingCart1.getAll();
        java.lang.Class<?> wildcardClass12 = soldItemList11.getClass();
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList3);
        org.junit.Assert.assertNotNull(soldItemList9);
        org.junit.Assert.assertNotNull(soldItemList11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test7348() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest14.test7348");
        SalesSystemDAO salesSystemDAO0 = null;
        ShoppingCart shoppingCart1 = new ShoppingCart(salesSystemDAO0);
        java.util.List<SoldItem> soldItemList2 = shoppingCart1.getAll();
        SoldItem soldItem3 = null;
        shoppingCart1.addItem(soldItem3);
        shoppingCart1.cancelCurrentPurchase();
        SoldItem soldItem6 = null;
        shoppingCart1.addItem(soldItem6);
        SoldItem soldItem8 = null;
        shoppingCart1.addItem(soldItem8);
        SoldItem soldItem10 = null;
        shoppingCart1.addItem(soldItem10);
        java.util.List<SoldItem> soldItemList12 = shoppingCart1.getAll();
        SoldItem soldItem13 = null;
        shoppingCart1.addItem(soldItem13);
        SoldItem soldItem15 = null;
        shoppingCart1.addItem(soldItem15);
        SoldItem soldItem17 = null;
        shoppingCart1.addItem(soldItem17);
        SoldItem soldItem19 = null;
        shoppingCart1.addItem(soldItem19);
        SoldItem soldItem21 = null;
        shoppingCart1.addItem(soldItem21);
        org.junit.Assert.assertNotNull(soldItemList2);
        org.junit.Assert.assertNotNull(soldItemList12);
    }
}

