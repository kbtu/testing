import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest28 {

    public static boolean debug = false;

    @Test
    public void test14001() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14001");
        NextDate nextDate3 = new NextDate(1, (int) (byte) 1, (-1));
        java.lang.String str7 = nextDate3.run(1, (int) (byte) 100, 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14002() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14002");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 100, 100);
    }

    @Test
    public void test14003() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14003");
        NextDate nextDate3 = new NextDate((int) '4', (int) '4', 100);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (byte) 1, (int) (short) 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14004() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14004");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (-1), 1);
        java.lang.String str7 = nextDate3.run((int) '#', 100, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14005() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14005");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) '4', (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (byte) 0, 0);
        java.lang.String str11 = nextDate3.run((-1), (int) (byte) 0, (int) (byte) -1);
        java.lang.String str15 = nextDate3.run((int) (byte) 1, (int) '#', 100);
        java.lang.String str19 = nextDate3.run(100, (-1), (int) '4');
        java.lang.String str23 = nextDate3.run((int) (byte) 100, (int) '#', (int) (byte) 10);
        java.lang.Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test14006() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14006");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 100, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (-1), (int) ' ');
        java.lang.String str11 = nextDate3.run(0, (int) '#', (int) (byte) 100);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test14007() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14007");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 100, 100);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (-1), (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14008() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14008");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 10, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, 1, (-1));
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14009() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14009");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) 1, 1);
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) (short) 0, (int) ' ');
        java.lang.String str11 = nextDate3.run((int) (byte) 0, (int) '4', (int) '4');
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test14010() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14010");
        NextDate nextDate3 = new NextDate(10, 1, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run(10, (int) '4', (int) (short) 100);
        java.lang.String str11 = nextDate3.run((int) (byte) 100, (int) (byte) 100, (int) (byte) 1);
        java.lang.String str15 = nextDate3.run((int) (short) 0, (int) (short) 10, (int) (byte) 100);
        java.lang.String str19 = nextDate3.run(100, (int) (byte) 1, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test14011() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14011");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 1, 100);
        java.lang.String str7 = nextDate3.run(10, (int) (byte) 100, (int) '4');
        java.lang.String str11 = nextDate3.run(100, (int) '#', (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14012() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14012");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 1, 0);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) '#', (int) (byte) 10);
        java.lang.String str11 = nextDate3.run((int) (short) 100, 100, 1);
        java.lang.String str15 = nextDate3.run((int) (short) 1, (-1), (int) (byte) 100);
        java.lang.String str19 = nextDate3.run((int) (short) 0, (int) 'a', 0);
        java.lang.String str23 = nextDate3.run((int) (short) -1, (int) (byte) 100, (int) (short) 1);
        java.lang.String str27 = nextDate3.run(1, (int) (short) 10, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test14013() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14013");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) -1, (int) '4');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14014() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14014");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 100, (int) 'a');
    }

    @Test
    public void test14015() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14015");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) -1, (int) (short) 100);
    }

    @Test
    public void test14016() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14016");
        NextDate nextDate3 = new NextDate((int) (short) -1, 10, (int) ' ');
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) 'a', (int) (short) -1);
        java.lang.String str11 = nextDate3.run((int) (byte) 0, (int) (short) 10, (int) (short) 10);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test14017() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14017");
        NextDate nextDate3 = new NextDate((int) ' ', (-1), (-1));
        java.lang.String str7 = nextDate3.run(0, (int) ' ', (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14018() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14018");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) ' ', (int) (short) 100);
        java.lang.String str7 = nextDate3.run(0, (int) '4', 100);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14019() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14019");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) ' ', (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (short) 1, 10, (int) (short) 100);
        java.lang.String str11 = nextDate3.run((-1), 10, (int) (byte) 10);
        java.lang.String str15 = nextDate3.run((int) (byte) -1, (int) (short) 1, (int) (byte) 10);
        java.lang.String str19 = nextDate3.run((int) (short) 0, (int) ' ', (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test14020() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14020");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) 0, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14021() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14021");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 100, (int) ' ');
        java.lang.String str7 = nextDate3.run((int) ' ', 0, 1);
        java.lang.String str11 = nextDate3.run((int) (byte) 0, (int) (short) 1, (int) (short) 10);
        java.lang.String str15 = nextDate3.run((int) (short) -1, (int) ' ', (int) (short) 1);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test14022() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14022");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 0, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, 0, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run((int) (short) 1, 10, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14023() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14023");
        NextDate nextDate3 = new NextDate(0, (int) ' ', (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (short) 1, 1, 0);
        java.lang.String str11 = nextDate3.run((int) (byte) -1, (int) (short) 0, 1);
        java.lang.String str15 = nextDate3.run((int) (short) -1, (int) (short) 10, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test14024() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14024");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) 'a', (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (short) 10, (int) 'a');
        java.lang.String str11 = nextDate3.run(0, (int) ' ', (int) (short) 1);
        java.lang.String str15 = nextDate3.run((int) (byte) 100, (int) ' ', (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test14025() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14025");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) '#', 1);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (byte) -1, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14026() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14026");
        NextDate nextDate3 = new NextDate((int) 'a', (int) ' ', (int) (byte) 100);
        java.lang.String str7 = nextDate3.run((int) '#', (int) (byte) 0, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((int) (short) -1, (int) (byte) 10, (int) (byte) 100);
        java.lang.String str15 = nextDate3.run(0, 1, (int) 'a');
        java.lang.String str19 = nextDate3.run((int) ' ', (-1), (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test14027() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14027");
        NextDate nextDate3 = new NextDate((int) ' ', 0, (int) '4');
        java.lang.String str7 = nextDate3.run((int) (short) 1, 10, 1);
        java.lang.String str11 = nextDate3.run((int) (short) 0, (int) (short) 10, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14028() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14028");
        NextDate nextDate3 = new NextDate(100, (int) (short) 1, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (short) 10, (int) (short) 100);
        java.lang.String str11 = nextDate3.run(1, (int) '#', 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14029() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14029");
        NextDate nextDate3 = new NextDate(100, (int) (short) -1, (int) (byte) 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14030() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14030");
        NextDate nextDate3 = new NextDate((int) (short) -1, 1, (-1));
        java.lang.String str7 = nextDate3.run((int) (byte) -1, 1, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run((-1), (int) 'a', (int) (short) -1);
        java.lang.String str15 = nextDate3.run(0, 1, (int) (short) 100);
        java.lang.String str19 = nextDate3.run((int) '#', 100, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test14031() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14031");
        NextDate nextDate3 = new NextDate((int) '4', (int) 'a', (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (-1), (int) (short) 1);
        java.lang.String str11 = nextDate3.run((int) (short) 0, (int) (short) 100, (int) (byte) 10);
        java.lang.String str15 = nextDate3.run((int) (byte) 100, 100, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test14032() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14032");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (short) 100, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) (short) 0, 1, (int) (byte) 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14033() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14033");
        NextDate nextDate3 = new NextDate(0, (int) '4', (int) (byte) -1);
    }

    @Test
    public void test14034() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14034");
        NextDate nextDate3 = new NextDate((int) ' ', (int) '#', (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (byte) 100, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14035() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14035");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 100, 10);
        java.lang.String str7 = nextDate3.run(1, 0, (int) ' ');
        java.lang.String str11 = nextDate3.run((int) ' ', 0, (int) 'a');
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test14036() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14036");
        NextDate nextDate3 = new NextDate((-1), (int) 'a', (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (short) -1, 100, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14037() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14037");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 0, 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14038() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14038");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 1, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((-1), (int) (short) 0, 0);
        java.lang.String str11 = nextDate3.run(1, 0, (int) '#');
        java.lang.String str15 = nextDate3.run((int) (byte) 1, (int) (short) 10, (int) ' ');
        java.lang.String str19 = nextDate3.run((int) (byte) 10, (int) (byte) 0, (int) (short) -1);
        java.lang.String str23 = nextDate3.run((int) (byte) 100, (int) (byte) 10, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test14039() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14039");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) -1, 1);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (byte) -1, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run((int) 'a', (-1), (int) ' ');
        java.lang.String str15 = nextDate3.run((int) (byte) 100, (int) (short) 0, 0);
        java.lang.String str19 = nextDate3.run((int) (byte) 0, 0, 0);
        java.lang.String str23 = nextDate3.run(10, (int) '4', (int) (short) 100);
        java.lang.String str27 = nextDate3.run((int) (short) 1, (int) '4', (int) (short) -1);
        java.lang.String str31 = nextDate3.run(0, 100, (int) (byte) 10);
        java.lang.String str35 = nextDate3.run((int) (byte) 0, 0, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
    }

    @Test
    public void test14040() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14040");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) '#', (int) 'a');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14041() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14041");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) 1, (int) ' ');
    }

    @Test
    public void test14042() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14042");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) 10, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (byte) -1, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((int) (short) -1, 100, (int) 'a');
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test14043() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14043");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) '4', (int) (short) 1);
        java.lang.String str7 = nextDate3.run(100, 100, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14044() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14044");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 1, 1);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) (short) 100, (int) (byte) 100);
        java.lang.String str11 = nextDate3.run(1, 1, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14045() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14045");
        NextDate nextDate3 = new NextDate((int) (short) 10, 1, 100);
        java.lang.String str7 = nextDate3.run(0, (-1), (int) (short) 10);
        java.lang.String str11 = nextDate3.run((int) (short) 0, (int) (short) -1, (int) (byte) -1);
        java.lang.String str15 = nextDate3.run(10, (int) (byte) 10, (int) (short) 1);
        java.lang.String str19 = nextDate3.run((int) (byte) 0, (int) (byte) -1, (int) (byte) -1);
        java.lang.String str23 = nextDate3.run((int) (byte) 10, (int) (byte) 100, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test14046() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14046");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 1, (int) 'a');
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (-1), (int) ' ');
        java.lang.String str11 = nextDate3.run((int) (byte) 10, (int) (byte) 1, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14047() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14047");
        NextDate nextDate3 = new NextDate((int) '4', 10, (int) (byte) 10);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14048() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14048");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) '4', (int) (byte) 100);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (-1), (int) (short) 1);
        java.lang.String str11 = nextDate3.run(100, (int) (short) 100, (int) (short) 100);
        java.lang.String str15 = nextDate3.run(0, 1, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test14049() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14049");
        NextDate nextDate3 = new NextDate((int) 'a', (-1), (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) (short) 0, 100, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14050() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14050");
        NextDate nextDate3 = new NextDate((int) (short) 100, 0, 0);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (short) 0, (int) ' ');
        java.lang.String str11 = nextDate3.run((int) (short) 10, (int) (short) -1, (int) (byte) 100);
        java.lang.String str15 = nextDate3.run(10, 0, (int) (byte) -1);
        java.lang.String str19 = nextDate3.run((int) '#', (int) '#', 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test14051() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14051");
        NextDate nextDate3 = new NextDate((int) (short) 0, 10, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) '#', (int) (short) 0);
        java.lang.String str11 = nextDate3.run(10, 0, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14052() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14052");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 10, (int) (short) 1);
        java.lang.String str7 = nextDate3.run(1, (int) (short) 1, (int) ' ');
        java.lang.String str11 = nextDate3.run((int) (short) 10, 0, (int) '4');
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test14053() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14053");
        NextDate nextDate3 = new NextDate(1, (int) (short) 1, 10);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (byte) -1, 1);
        java.lang.String str11 = nextDate3.run((int) (byte) 0, (int) (short) 10, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14054() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14054");
        NextDate nextDate3 = new NextDate(10, (int) (short) 1, 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14055() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14055");
        NextDate nextDate3 = new NextDate((int) (short) 10, 10, (int) (byte) 0);
    }

    @Test
    public void test14056() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14056");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 0, (int) 'a');
        java.lang.String str7 = nextDate3.run(100, 0, 0);
        java.lang.String str11 = nextDate3.run((int) '4', (int) ' ', (int) (byte) 0);
        java.lang.String str15 = nextDate3.run(100, (int) (short) -1, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test14057() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14057");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 10, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) ' ', (int) (byte) 1);
        java.lang.String str11 = nextDate3.run((int) (byte) 0, (int) (short) 0, (int) (short) 10);
        java.lang.String str15 = nextDate3.run((int) (short) -1, 0, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test14058() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14058");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) -1, (int) 'a');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14059() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14059");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) '4', 1);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (short) 100, (int) (short) 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14060() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14060");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) 1, 0);
        java.lang.String str7 = nextDate3.run((int) (short) 10, 0, (int) (byte) 1);
        java.lang.String str11 = nextDate3.run((int) (short) 100, (int) (byte) 1, (int) '4');
        java.lang.String str15 = nextDate3.run(100, (int) (byte) 10, 100);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test14061() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14061");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 1, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (short) -1, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 100, (int) (short) 0, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14062() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14062");
        NextDate nextDate3 = new NextDate((int) (short) 1, 100, (int) (short) -1);
    }

    @Test
    public void test14063() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14063");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) -1, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run((int) (short) -1, 0, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14064() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14064");
        NextDate nextDate3 = new NextDate((int) '4', (int) 'a', (int) ' ');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14065() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14065");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 1, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run(0, (-1), (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14066() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14066");
        NextDate nextDate3 = new NextDate((int) (short) 100, 0, 0);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (short) 0, (int) ' ');
        java.lang.String str11 = nextDate3.run((int) (byte) 1, 0, (int) (byte) 10);
        java.lang.String str15 = nextDate3.run((int) (byte) 1, 100, (int) (byte) 1);
        java.lang.String str19 = nextDate3.run((int) '4', (int) '#', (int) (short) 0);
        java.lang.String str23 = nextDate3.run((int) 'a', (int) (short) 10, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test14067() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14067");
        NextDate nextDate3 = new NextDate((int) (short) 1, 1, (int) (byte) 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14068() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14068");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) -1, (int) (short) 100);
    }

    @Test
    public void test14069() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14069");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) '#', (int) '#');
        java.lang.String str7 = nextDate3.run((-1), (int) (short) 100, (int) (short) 1);
        java.lang.String str11 = nextDate3.run(1, 10, 100);
        java.lang.String str15 = nextDate3.run((int) 'a', (int) (short) 1, 10);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test14070() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14070");
        NextDate nextDate3 = new NextDate((int) ' ', (-1), 100);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (short) 100, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14071() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14071");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) 1, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) ' ', (-1), (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14072() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14072");
        NextDate nextDate3 = new NextDate(10, (int) (short) 10, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (-1), 100);
        java.lang.String str11 = nextDate3.run((-1), 100, (int) (short) 100);
        java.lang.String str15 = nextDate3.run((int) (short) 1, (int) '4', 1);
        java.lang.String str19 = nextDate3.run((int) (byte) 1, (int) ' ', (int) (byte) 10);
        java.lang.Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test14073() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14073");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 10, 0);
        java.lang.String str7 = nextDate3.run((int) (short) 0, 0, (int) (byte) 100);
        java.lang.String str11 = nextDate3.run((int) (short) 100, 10, 100);
        java.lang.String str15 = nextDate3.run((int) (byte) 1, 10, (int) (byte) 0);
        java.lang.String str19 = nextDate3.run((int) (byte) 100, (int) (short) -1, (int) ' ');
        java.lang.String str23 = nextDate3.run(10, 0, 0);
        java.lang.String str27 = nextDate3.run((int) ' ', 1, (int) (short) 0);
        java.lang.Class<?> wildcardClass28 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test14074() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14074");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) 0, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, 1, 0);
        java.lang.String str11 = nextDate3.run((int) (short) 10, (int) (byte) 1, (int) (short) 1);
        java.lang.String str15 = nextDate3.run((int) (byte) 100, (int) (short) 0, (int) '#');
        java.lang.String str19 = nextDate3.run((int) (short) -1, (int) (short) 10, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test14075() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14075");
        NextDate nextDate3 = new NextDate(1, (int) (byte) 10, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (short) 10, 0, 0);
        java.lang.String str11 = nextDate3.run((int) 'a', 0, (int) (byte) 0);
        java.lang.String str15 = nextDate3.run((int) (byte) -1, (int) (short) 0, (-1));
        java.lang.String str19 = nextDate3.run(100, 10, (int) 'a');
        java.lang.Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test14076() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14076");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 10, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (byte) 1, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14077() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14077");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 1, (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (short) 1, 10);
        java.lang.String str11 = nextDate3.run(0, 100, (int) (byte) 0);
        java.lang.String str15 = nextDate3.run((int) (byte) 10, 10, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test14078() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14078");
        NextDate nextDate3 = new NextDate((int) '4', 0, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) '4', 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14079() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14079");
        NextDate nextDate3 = new NextDate(10, (int) (byte) -1, (int) (short) -1);
        java.lang.String str7 = nextDate3.run(0, 0, (int) '#');
        java.lang.String str11 = nextDate3.run((int) (byte) 10, (int) (byte) -1, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14080() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14080");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 1, 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, (int) ' ', (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14081() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14081");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (byte) 1, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14082() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14082");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) '#', (int) '#');
        java.lang.String str7 = nextDate3.run((int) '#', (int) ' ', (int) (byte) 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14083() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14083");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (-1), (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (short) 0, (int) 'a', (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14084() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14084");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 10, (int) ' ');
        java.lang.String str7 = nextDate3.run((int) '#', (int) (short) 10, 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14085() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14085");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) '4', 1);
        java.lang.String str7 = nextDate3.run((int) '#', (int) '#', (int) ' ');
        java.lang.String str11 = nextDate3.run((int) (byte) -1, (int) (short) 0, (int) '#');
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test14086() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14086");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 1, 10);
        java.lang.String str7 = nextDate3.run((-1), (int) (short) 1, (int) (short) 0);
        java.lang.String str11 = nextDate3.run((int) (short) 100, (int) (short) 0, 10);
        java.lang.String str15 = nextDate3.run((int) (byte) 100, (int) ' ', (-1));
        java.lang.String str19 = nextDate3.run((int) ' ', (int) (byte) -1, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test14087() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14087");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) ' ', 10);
        java.lang.String str7 = nextDate3.run(10, 0, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((-1), (int) (short) 0, (int) (byte) 10);
        java.lang.String str15 = nextDate3.run((int) (short) 1, (int) (byte) 0, 10);
        java.lang.String str19 = nextDate3.run((int) (short) 100, (int) ' ', 0);
        java.lang.String str23 = nextDate3.run((int) (byte) 100, (int) '#', (int) (byte) 10);
        java.lang.String str27 = nextDate3.run(0, (int) ' ', (int) 'a');
        java.lang.String str31 = nextDate3.run((int) 'a', 0, 100);
        java.lang.String str35 = nextDate3.run(0, 0, (int) (byte) 0);
        java.lang.Class<?> wildcardClass36 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass36);
    }

    @Test
    public void test14088() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14088");
        NextDate nextDate3 = new NextDate(10, 0, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run(1, 10, (int) (byte) 1);
        java.lang.String str11 = nextDate3.run((int) (short) 100, (int) (short) 100, (-1));
        java.lang.String str15 = nextDate3.run((int) (short) 100, (-1), (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test14089() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14089");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 100, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (short) 10, (int) (short) 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14090() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14090");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 1, 10);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (byte) 0, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run(0, 0, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14091() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14091");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 100, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (byte) 10, 100);
        java.lang.String str11 = nextDate3.run((int) 'a', (int) (short) -1, (int) (short) -1);
        java.lang.String str15 = nextDate3.run((int) (byte) 10, 1, (int) (byte) 100);
        java.lang.String str19 = nextDate3.run((int) (short) 0, (int) (byte) 10, (int) 'a');
        java.lang.Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test14092() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14092");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) 10, (int) (short) 10);
        java.lang.String str7 = nextDate3.run(0, (int) (short) -1, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run(1, (int) (short) 100, (int) (byte) 100);
        java.lang.String str15 = nextDate3.run((int) '#', (int) (short) -1, (int) (short) -1);
        java.lang.String str19 = nextDate3.run(1, (-1), (int) (short) 0);
        java.lang.String str23 = nextDate3.run((-1), (int) (byte) 10, (int) 'a');
        java.lang.String str27 = nextDate3.run((int) (byte) 1, (-1), (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test14093() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14093");
        NextDate nextDate3 = new NextDate(1, (int) (short) 1, (int) (short) 100);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14094() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14094");
        NextDate nextDate3 = new NextDate((int) ' ', (-1), 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14095() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14095");
        NextDate nextDate3 = new NextDate(0, 10, (int) (short) 10);
        java.lang.String str7 = nextDate3.run((-1), (int) (byte) 100, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run(1, (int) '#', (-1));
        java.lang.String str15 = nextDate3.run((int) (byte) 1, (int) (short) 1, (int) (short) 1);
        java.lang.String str19 = nextDate3.run((int) ' ', (int) (byte) 100, (int) 'a');
        java.lang.Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test14096() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14096");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) -1, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, 10, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 100, 100, (int) '#');
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test14097() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14097");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 0, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run(0, (int) '#', (int) (short) 0);
        java.lang.String str11 = nextDate3.run((int) (short) 10, (int) (byte) 100, (int) '4');
        java.lang.String str15 = nextDate3.run(0, (int) (byte) 10, (int) 'a');
        java.lang.String str19 = nextDate3.run((int) (short) 1, (int) (byte) 10, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test14098() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14098");
        NextDate nextDate3 = new NextDate((int) 'a', (int) 'a', (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (short) 100, (int) (short) 1);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, (int) (short) 0, 0);
        java.lang.String str15 = nextDate3.run((int) (short) 0, (int) 'a', (int) (byte) 0);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test14099() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14099");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 1, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run(0, 100, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14100() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14100");
        NextDate nextDate3 = new NextDate((int) '#', (int) ' ', (int) (short) 100);
    }

    @Test
    public void test14101() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14101");
        NextDate nextDate3 = new NextDate(1, (int) (short) -1, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((-1), 10, (int) '4');
        java.lang.String str11 = nextDate3.run((int) 'a', (int) '#', (int) (byte) 10);
        java.lang.String str15 = nextDate3.run((int) '#', (int) 'a', (int) (byte) 10);
        java.lang.String str19 = nextDate3.run((int) ' ', 0, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test14102() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14102");
        NextDate nextDate3 = new NextDate(1, 100, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) (short) -1, (int) (short) -1);
        java.lang.String str11 = nextDate3.run(0, (int) ' ', (int) (byte) 10);
        java.lang.String str15 = nextDate3.run(0, (int) ' ', (int) (short) -1);
        java.lang.String str19 = nextDate3.run((int) (byte) 100, (-1), (int) (byte) 1);
        java.lang.String str23 = nextDate3.run(0, (int) (byte) 0, (int) (byte) -1);
        java.lang.Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test14103() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14103");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 0, 1);
    }

    @Test
    public void test14104() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14104");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 0, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (byte) -1, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14105() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14105");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 1, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((-1), (int) (short) 100, (int) ' ');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14106() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14106");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) 10, 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) 'a', (int) (byte) 100);
        java.lang.String str11 = nextDate3.run((-1), (int) (byte) 10, (int) (byte) 0);
        java.lang.String str15 = nextDate3.run((int) ' ', (int) '4', (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test14107() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14107");
        NextDate nextDate3 = new NextDate((int) (short) -1, 1, (-1));
        java.lang.String str7 = nextDate3.run((int) (byte) -1, 1, (int) (byte) 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14108() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14108");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) -1, (int) (short) -1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14109() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14109");
        NextDate nextDate3 = new NextDate((-1), 1, (int) (short) 0);
        java.lang.String str7 = nextDate3.run(0, (int) (short) -1, 0);
        java.lang.String str11 = nextDate3.run(0, 0, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14110() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14110");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) 10, 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) 'a', (int) (byte) 100);
        java.lang.String str11 = nextDate3.run((int) (byte) -1, 0, 0);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test14111() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14111");
        NextDate nextDate3 = new NextDate((int) '4', (int) (byte) 0, (int) '4');
        java.lang.String str7 = nextDate3.run(10, 10, (int) 'a');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14112() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14112");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 100, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) '#', (int) (short) 100, (int) (short) 100);
        java.lang.String str11 = nextDate3.run((int) ' ', (-1), (int) ' ');
        java.lang.String str15 = nextDate3.run((int) (byte) 1, 0, 1);
        java.lang.String str19 = nextDate3.run((int) (byte) -1, (int) (short) 0, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test14113() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14113");
        NextDate nextDate3 = new NextDate((int) (short) 1, (-1), 1);
        java.lang.String str7 = nextDate3.run(100, (int) (byte) 10, (int) ' ');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14114() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14114");
        NextDate nextDate3 = new NextDate((-1), (int) '4', (int) 'a');
        java.lang.String str7 = nextDate3.run((int) '#', (-1), (int) (byte) 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14115() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14115");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 10, (int) (byte) -1);
    }

    @Test
    public void test14116() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14116");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 10, (int) '#');
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (short) 1, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14117() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14117");
        NextDate nextDate3 = new NextDate(1, (int) 'a', (-1));
        java.lang.String str7 = nextDate3.run((int) (short) 0, (int) (short) 100, (int) ' ');
        java.lang.String str11 = nextDate3.run(10, (int) (byte) 100, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14118() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14118");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (short) 10, 100);
        java.lang.String str7 = nextDate3.run((int) (short) 0, (int) '4', (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14119() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14119");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 0, (int) 'a');
        java.lang.String str7 = nextDate3.run(0, (int) 'a', 100);
        java.lang.String str11 = nextDate3.run((int) (short) -1, (int) (short) 1, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14120() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14120");
        NextDate nextDate3 = new NextDate(100, (int) (short) -1, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (short) -1, 0, (int) (byte) 1);
        java.lang.String str11 = nextDate3.run(0, (int) (byte) 1, (int) (byte) 0);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test14121() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14121");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) '#', (int) (byte) -1);
        java.lang.String str7 = nextDate3.run(0, (int) (short) 100, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((int) (short) 10, (int) '#', (int) (short) 100);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test14122() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14122");
        NextDate nextDate3 = new NextDate((int) (short) 0, 100, (int) (short) 10);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14123() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14123");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 10, 1);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) '#', 0);
        java.lang.String str11 = nextDate3.run(10, (int) ' ', (int) (short) 10);
        java.lang.String str15 = nextDate3.run((-1), (int) '#', (int) 'a');
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test14124() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14124");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 100, 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, (int) (byte) 100, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((int) '#', (int) (short) -1, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14125() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14125");
        NextDate nextDate3 = new NextDate(100, (int) '4', (int) ' ');
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (short) -1, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14126() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14126");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 0, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run((int) (short) 0, (-1), (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((int) (byte) -1, 10, (int) (byte) 1);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test14127() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14127");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 10, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (byte) 1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14128() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14128");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 0, (int) (short) 100);
        java.lang.String str7 = nextDate3.run(1, (-1), 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14129() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14129");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 0, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (short) 1, (int) ' ');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14130() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14130");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 0, (int) (short) 10);
        java.lang.String str7 = nextDate3.run(100, (int) (short) 100, 100);
        java.lang.String str11 = nextDate3.run((int) ' ', (int) (short) 0, (int) 'a');
        java.lang.String str15 = nextDate3.run((int) ' ', (int) ' ', (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test14131() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14131");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 1, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run(1, (int) '4', (-1));
        java.lang.String str11 = nextDate3.run((int) (short) 100, (int) (short) -1, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14132() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14132");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 0, (int) (short) 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14133() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14133");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) '#', 100);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (short) 0, 10);
        java.lang.String str11 = nextDate3.run((int) ' ', 100, (int) (byte) 100);
        java.lang.String str15 = nextDate3.run((-1), (int) (byte) 100, (int) '4');
        java.lang.String str19 = nextDate3.run((int) (short) 100, (int) (byte) 0, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test14134() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14134");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 100, 1);
    }

    @Test
    public void test14135() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14135");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) 100, (int) '4');
    }

    @Test
    public void test14136() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14136");
        NextDate nextDate3 = new NextDate((int) '#', 1, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) '4', (int) 'a');
        java.lang.String str11 = nextDate3.run((int) '4', (int) (byte) -1, 10);
        java.lang.String str15 = nextDate3.run((int) ' ', (int) (byte) 0, (int) (short) 0);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test14137() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14137");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) 1, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, 0, (int) ' ');
        java.lang.String str11 = nextDate3.run((int) (short) -1, (int) (byte) 1, (-1));
        java.lang.String str15 = nextDate3.run((int) (byte) 100, 1, (int) (short) 1);
        java.lang.String str19 = nextDate3.run(1, 0, (int) (short) 0);
        java.lang.String str23 = nextDate3.run(100, (int) (byte) -1, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test14138() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14138");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 0, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (byte) -1, (-1));
        java.lang.String str11 = nextDate3.run((int) ' ', 100, (int) 'a');
        java.lang.String str15 = nextDate3.run((int) (short) 1, 100, (int) '#');
        java.lang.String str19 = nextDate3.run((int) (byte) -1, 100, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test14139() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14139");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 1, 10);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14140() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14140");
        NextDate nextDate3 = new NextDate(100, (int) (short) 100, (int) ' ');
    }

    @Test
    public void test14141() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14141");
        NextDate nextDate3 = new NextDate((int) (short) 1, 10, (int) (short) 1);
        java.lang.String str7 = nextDate3.run(1, (int) (short) 1, (int) (byte) 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14142() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14142");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 10, (-1));
        java.lang.String str7 = nextDate3.run(1, (int) (short) 1, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((-1), (int) (short) 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14143() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14143");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 100, (int) (short) 1);
        java.lang.String str7 = nextDate3.run(10, (int) (short) -1, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14144() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14144");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 0, (int) 'a');
        java.lang.String str7 = nextDate3.run((int) '#', (int) 'a', 100);
        java.lang.String str11 = nextDate3.run(10, (int) (short) -1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14145() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14145");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) '4', (-1));
        java.lang.String str7 = nextDate3.run(0, (int) (short) -1, 0);
        java.lang.String str11 = nextDate3.run((int) ' ', 0, (int) (byte) 1);
        java.lang.String str15 = nextDate3.run((-1), (int) (short) 100, (int) '#');
        java.lang.String str19 = nextDate3.run((int) (byte) 100, (int) ' ', (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test14146() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14146");
        NextDate nextDate3 = new NextDate(100, 1, (int) (short) 10);
    }

    @Test
    public void test14147() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14147");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) 'a', (int) ' ');
        java.lang.String str7 = nextDate3.run(1, 0, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run((int) (short) -1, (int) (byte) -1, (int) '#');
        java.lang.String str15 = nextDate3.run((int) (short) 100, (int) (short) -1, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test14148() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14148");
        NextDate nextDate3 = new NextDate((int) '#', (int) 'a', (int) ' ');
        java.lang.String str7 = nextDate3.run(0, (int) (byte) 100, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14149() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14149");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) 'a', (int) (short) 10);
    }

    @Test
    public void test14150() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14150");
        NextDate nextDate3 = new NextDate((int) (short) 100, 0, 1);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) '#', (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14151() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14151");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 0, (int) (short) 10);
    }

    @Test
    public void test14152() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14152");
        NextDate nextDate3 = new NextDate((int) (short) -1, 10, (int) '#');
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (short) -1, 0);
        java.lang.String str11 = nextDate3.run((int) 'a', (int) ' ', (int) (byte) -1);
        java.lang.String str15 = nextDate3.run((int) 'a', (-1), 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test14153() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14153");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) 100, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14154() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14154");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) 10, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (byte) -1, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((int) 'a', 0, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14155() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14155");
        NextDate nextDate3 = new NextDate(10, 10, (int) (short) 10);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14156() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14156");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) ' ', (int) (short) 10);
    }

    @Test
    public void test14157() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14157");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 1, 0);
        java.lang.String str7 = nextDate3.run(0, 0, 0);
        java.lang.String str11 = nextDate3.run((int) '#', 100, 0);
        java.lang.String str15 = nextDate3.run(100, (int) (short) 10, (int) (short) 1);
        java.lang.String str19 = nextDate3.run((int) (byte) -1, (int) '4', (int) (short) 100);
        java.lang.String str23 = nextDate3.run((int) (byte) -1, (int) '4', (int) (byte) -1);
        java.lang.String str27 = nextDate3.run((int) (byte) 0, (int) '#', 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test14158() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14158");
        NextDate nextDate3 = new NextDate(1, (int) '#', (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) '#', (int) (byte) 10);
        java.lang.String str11 = nextDate3.run((int) (short) 100, 1, (int) (short) 0);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test14159() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14159");
        NextDate nextDate3 = new NextDate(0, 1, 0);
        java.lang.String str7 = nextDate3.run(100, (int) ' ', (int) (short) 100);
        java.lang.String str11 = nextDate3.run((int) '#', 0, 1);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test14160() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14160");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 100, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (byte) 10, (int) (short) 1);
        java.lang.String str11 = nextDate3.run(10, 0, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14161() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14161");
        NextDate nextDate3 = new NextDate(10, (int) (short) 0, (int) (byte) 0);
    }

    @Test
    public void test14162() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14162");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) 100, (int) (short) 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14163() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14163");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) '#', (int) (byte) 10);
        java.lang.String str7 = nextDate3.run(100, (int) (byte) 0, (int) 'a');
        java.lang.String str11 = nextDate3.run((int) '#', (int) (short) -1, (int) (short) 0);
        java.lang.String str15 = nextDate3.run((int) ' ', 100, 10);
        java.lang.String str19 = nextDate3.run((-1), (int) (byte) 1, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test14164() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14164");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (short) 100, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) '#', (int) (byte) 10, (int) '#');
        java.lang.String str11 = nextDate3.run((int) '#', 0, (int) '#');
        java.lang.String str15 = nextDate3.run((int) (short) 10, (int) '4', (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test14165() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14165");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 1, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) (byte) 0, (-1));
        java.lang.String str11 = nextDate3.run(0, (int) (byte) 10, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14166() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14166");
        NextDate nextDate3 = new NextDate(10, (int) '4', (int) (short) -1);
    }

    @Test
    public void test14167() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14167");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 100, 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, 0, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14168() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14168");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) 'a', (int) (byte) 0);
        java.lang.String str7 = nextDate3.run(1, (int) (byte) -1, (-1));
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14169() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14169");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) '#', 10);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (short) 10, 0);
        java.lang.String str11 = nextDate3.run((int) '#', (int) (short) -1, (int) (short) 1);
        java.lang.String str15 = nextDate3.run((int) (byte) 1, (-1), (int) (short) 100);
        java.lang.String str19 = nextDate3.run((int) ' ', (int) (byte) 10, (int) (short) 0);
        java.lang.String str23 = nextDate3.run((-1), (int) (short) 0, 100);
        java.lang.String str27 = nextDate3.run((int) 'a', (int) (short) 1, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test14170() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14170");
        NextDate nextDate3 = new NextDate((int) '#', 100, 10);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14171() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14171");
        NextDate nextDate3 = new NextDate((int) '#', (int) '4', (int) ' ');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14172() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14172");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) ' ', (int) '4');
    }

    @Test
    public void test14173() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14173");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) -1, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) 'a', (int) '#');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14174() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14174");
        NextDate nextDate3 = new NextDate((int) (short) 10, 0, 10);
        java.lang.String str7 = nextDate3.run((-1), (int) (short) 10, 0);
        java.lang.String str11 = nextDate3.run((int) (short) 1, (int) (byte) 0, (int) ' ');
        java.lang.String str15 = nextDate3.run((int) (short) 0, (int) (byte) 100, 1);
        java.lang.String str19 = nextDate3.run((int) (short) 1, (int) (short) 100, (int) (short) 10);
        java.lang.String str23 = nextDate3.run((int) (short) 1, (int) (byte) 10, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test14175() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14175");
        NextDate nextDate3 = new NextDate((int) (short) -1, 100, (int) '#');
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) (byte) 0, (int) (short) 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14176() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14176");
        NextDate nextDate3 = new NextDate((int) (short) 0, 0, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run((int) 'a', 0, (int) (short) 100);
        java.lang.String str11 = nextDate3.run((int) (short) 0, 100, (int) (short) -1);
        java.lang.String str15 = nextDate3.run((int) 'a', (-1), (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test14177() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14177");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 0, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (byte) 10, (int) (short) 10);
        java.lang.String str11 = nextDate3.run(0, (int) ' ', 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14178() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14178");
        NextDate nextDate3 = new NextDate(1, (int) (byte) 10, (int) (short) 1);
        java.lang.String str7 = nextDate3.run(1, (int) (short) 1, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run((int) (byte) -1, (int) (short) 100, (int) (short) -1);
        java.lang.String str15 = nextDate3.run((int) (short) 0, (int) (byte) 0, (int) 'a');
        java.lang.String str19 = nextDate3.run((int) 'a', (int) (short) 100, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test14179() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14179");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) -1, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (short) 0, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14180() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14180");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 1, 0);
        java.lang.String str7 = nextDate3.run(0, 0, 0);
        java.lang.String str11 = nextDate3.run((int) '#', 100, 0);
        java.lang.String str15 = nextDate3.run((int) (byte) 1, (int) (byte) 100, (int) (short) 1);
        java.lang.String str19 = nextDate3.run((int) (byte) 10, (-1), (int) (short) 0);
        java.lang.String str23 = nextDate3.run((int) ' ', (int) 'a', 10);
        java.lang.String str27 = nextDate3.run((int) (short) 0, 0, (int) ' ');
        java.lang.String str31 = nextDate3.run(10, (int) (byte) 1, (int) (short) -1);
        java.lang.String str35 = nextDate3.run(10, (int) (short) -1, (int) (byte) 1);
        java.lang.String str39 = nextDate3.run((int) (short) 10, 1, (int) ' ');
        java.lang.String str43 = nextDate3.run((int) (short) 0, (int) (byte) 10, (int) '#');
        java.lang.String str47 = nextDate3.run(0, (int) '4', (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str43 + "' != '" + "invalid Input Date" + "'", str43, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str47 + "' != '" + "invalid Input Date" + "'", str47, "invalid Input Date");
    }

    @Test
    public void test14181() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14181");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 10, (int) (short) 10);
        java.lang.String str7 = nextDate3.run(100, (int) (short) 10, (int) (short) 0);
        java.lang.String str11 = nextDate3.run((int) (short) 10, (int) (short) -1, 10);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test14182() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14182");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) 0, 1);
    }

    @Test
    public void test14183() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14183");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 100, (int) 'a');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14184() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14184");
        NextDate nextDate3 = new NextDate((int) ' ', (int) ' ', 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (short) 0, 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14185() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14185");
        NextDate nextDate3 = new NextDate((int) (short) -1, 100, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((-1), (int) ' ', (int) (byte) 10);
        java.lang.String str11 = nextDate3.run((int) (byte) 0, (int) (byte) 10, (int) (byte) 0);
        java.lang.String str15 = nextDate3.run((int) 'a', (int) ' ', (int) '4');
        java.lang.String str19 = nextDate3.run((int) (byte) 1, (int) 'a', (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test14186() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14186");
        NextDate nextDate3 = new NextDate(100, (int) (short) 1, (int) (short) -1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14187() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14187");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (byte) -1, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) '#', 0, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14188() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14188");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 1, 1);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (short) 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14189() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14189");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) 0, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((-1), (int) '4', (int) 'a');
        java.lang.String str11 = nextDate3.run((int) (short) 100, 0, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14190() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14190");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) 1, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (short) 10, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14191() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14191");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 1, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) ' ', 100);
        java.lang.String str11 = nextDate3.run((int) (short) 1, 0, (int) (byte) -1);
        java.lang.String str15 = nextDate3.run((int) (byte) 0, (int) (short) 10, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test14192() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14192");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) -1, (int) 'a');
        java.lang.String str7 = nextDate3.run(10, (int) 'a', (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14193() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14193");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 100, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run(1, 100, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14194() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14194");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) 100, (int) '4');
        java.lang.String str7 = nextDate3.run((int) '4', (int) (byte) 100, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run((-1), (int) (byte) 1, (int) (byte) 10);
        java.lang.String str15 = nextDate3.run(1, (int) '4', (int) (short) 100);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test14195() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14195");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 100, 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (short) 100, (int) (byte) 100);
        java.lang.String str11 = nextDate3.run(10, (int) '4', 0);
        java.lang.String str15 = nextDate3.run((int) (short) 100, 100, (int) '4');
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test14196() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14196");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 100, 100);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (byte) 0, 100);
        java.lang.String str11 = nextDate3.run((int) '#', (int) (byte) 1, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14197() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14197");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 100, (int) '#');
        java.lang.String str7 = nextDate3.run(0, (int) (byte) 0, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run(0, 100, (int) (byte) 100);
        java.lang.String str15 = nextDate3.run((int) (byte) 0, (int) 'a', (int) (short) 10);
        java.lang.String str19 = nextDate3.run(0, (int) (short) 100, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test14198() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14198");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 100, (int) 'a');
        java.lang.String str7 = nextDate3.run(0, (int) (short) 1, (int) (short) 100);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14199() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14199");
        NextDate nextDate3 = new NextDate(100, (int) '#', (int) (short) 100);
        java.lang.String str7 = nextDate3.run((-1), 100, (int) (short) 100);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14200() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14200");
        NextDate nextDate3 = new NextDate((int) ' ', 1, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (-1), (int) 'a');
        java.lang.String str11 = nextDate3.run((int) (byte) 1, (int) (byte) 0, (int) (short) 100);
        java.lang.String str15 = nextDate3.run((int) (byte) 0, (int) '#', 10);
        java.lang.String str19 = nextDate3.run((int) (short) 0, (int) (byte) 100, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test14201() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14201");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (byte) 0, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run(0, (int) ' ', 1);
        java.lang.String str11 = nextDate3.run((int) (byte) -1, (int) (byte) 10, (int) (byte) 1);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test14202() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14202");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) 100, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) (byte) 10, (int) '#');
        java.lang.String str11 = nextDate3.run((int) '4', (int) (byte) 10, (int) (byte) -1);
        java.lang.String str15 = nextDate3.run((int) (short) -1, (int) (byte) 1, 0);
        java.lang.String str19 = nextDate3.run((int) ' ', (int) (short) 1, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test14203() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14203");
        NextDate nextDate3 = new NextDate((int) '#', 1, 10);
        java.lang.String str7 = nextDate3.run(10, (int) (byte) 1, 0);
        java.lang.String str11 = nextDate3.run((int) 'a', (int) (short) 0, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14204() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14204");
        NextDate nextDate3 = new NextDate(0, (int) (short) 0, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) 'a', 100, (int) (short) 0);
        java.lang.String str11 = nextDate3.run((-1), (int) (short) 100, (int) 'a');
        java.lang.String str15 = nextDate3.run((int) (byte) -1, (int) (short) 1, (int) (byte) 0);
        java.lang.String str19 = nextDate3.run((int) (short) -1, (int) (short) 1, 10);
        java.lang.String str23 = nextDate3.run((int) (byte) 0, (int) (short) 0, 0);
        java.lang.Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test14205() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14205");
        NextDate nextDate3 = new NextDate(0, (int) ' ', (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) (short) 0, 10, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14206() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14206");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) '4', (int) (byte) 1);
        java.lang.String str7 = nextDate3.run(1, 1, 10);
        java.lang.String str11 = nextDate3.run((int) (short) 0, 1, (int) ' ');
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test14207() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14207");
        NextDate nextDate3 = new NextDate((int) '#', 1, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run(1, (int) (byte) 0, 0);
        java.lang.String str11 = nextDate3.run((-1), (int) (byte) 10, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14208() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14208");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) 0, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((-1), (int) (byte) 10, (int) (short) -1);
        java.lang.String str11 = nextDate3.run(1, 100, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14209() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14209");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) ' ', (int) '4');
        java.lang.String str7 = nextDate3.run((int) (short) 100, 0, (int) ' ');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14210() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14210");
        NextDate nextDate3 = new NextDate(10, (int) (short) -1, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (byte) 10, (int) (byte) 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14211() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14211");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 1, 0);
        java.lang.String str7 = nextDate3.run(0, 0, 0);
        java.lang.String str11 = nextDate3.run((int) ' ', (int) 'a', (int) 'a');
        java.lang.String str15 = nextDate3.run((-1), (int) ' ', (int) (byte) -1);
        java.lang.String str19 = nextDate3.run((int) ' ', (int) 'a', (int) '4');
        java.lang.String str23 = nextDate3.run(10, (int) (short) -1, 100);
        java.lang.String str27 = nextDate3.run(100, (int) (byte) -1, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test14212() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14212");
        NextDate nextDate3 = new NextDate((int) ' ', 1, (int) (short) 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14213() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14213");
        NextDate nextDate3 = new NextDate((int) (short) 0, (-1), (int) (byte) -1);
        java.lang.String str7 = nextDate3.run(10, (int) '#', (int) (short) 100);
        java.lang.String str11 = nextDate3.run((int) (short) 0, (int) '4', (int) (byte) 1);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test14214() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14214");
        NextDate nextDate3 = new NextDate((int) (short) 10, (-1), (int) (short) 10);
    }

    @Test
    public void test14215() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14215");
        NextDate nextDate3 = new NextDate((int) '#', 10, (int) 'a');
        java.lang.String str7 = nextDate3.run((int) ' ', (-1), (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14216() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14216");
        NextDate nextDate3 = new NextDate(0, (int) 'a', (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) '4', (int) '4', (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14217() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14217");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 0, 0);
        java.lang.String str7 = nextDate3.run(10, 1, (int) ' ');
        java.lang.String str11 = nextDate3.run((int) (short) 1, (int) (byte) 0, (int) (byte) -1);
        java.lang.String str15 = nextDate3.run((int) (short) -1, 0, (int) '4');
        java.lang.String str19 = nextDate3.run(10, (int) (byte) 10, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test14218() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14218");
        NextDate nextDate3 = new NextDate((int) '4', (int) '4', (int) '#');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14219() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14219");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (short) -1, (int) '#');
        java.lang.String str11 = nextDate3.run((int) '#', (int) (short) 10, (int) (short) 1);
        java.lang.String str15 = nextDate3.run((int) (short) 0, (-1), 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test14220() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14220");
        NextDate nextDate3 = new NextDate((int) (short) -1, 10, 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14221() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14221");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 100, (-1));
        java.lang.String str7 = nextDate3.run((int) '#', (int) (byte) 1, (int) (short) 100);
        java.lang.String str11 = nextDate3.run(10, (-1), (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14222() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14222");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (byte) 0, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, 100, 100);
        java.lang.String str11 = nextDate3.run((int) '4', (-1), (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14223() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14223");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) 1, 100);
    }

    @Test
    public void test14224() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14224");
        NextDate nextDate3 = new NextDate(10, (int) '4', 0);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (byte) 100, (int) (byte) 100);
        java.lang.String str11 = nextDate3.run(10, (-1), (int) (short) 10);
        java.lang.String str15 = nextDate3.run((int) 'a', (int) '#', (int) (byte) 10);
        java.lang.String str19 = nextDate3.run(0, (int) (byte) 100, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test14225() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14225");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (-1), (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) 'a', (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14226() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14226");
        NextDate nextDate3 = new NextDate(100, 100, (int) ' ');
        java.lang.String str7 = nextDate3.run(100, (int) (short) 0, (int) (short) -1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14227() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14227");
        NextDate nextDate3 = new NextDate((int) '4', (int) ' ', (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) (short) 0, 0, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run((int) 'a', (int) (short) 10, (int) (short) -1);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test14228() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14228");
        NextDate nextDate3 = new NextDate((int) (short) 100, 1, (-1));
        java.lang.String str7 = nextDate3.run(0, (int) (byte) -1, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14229() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14229");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (-1), (int) (byte) -1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14230() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14230");
        NextDate nextDate3 = new NextDate((int) (short) 100, 100, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (short) -1, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14231() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14231");
        NextDate nextDate3 = new NextDate(10, (int) (short) 0, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (byte) -1, (int) 'a');
        java.lang.String str11 = nextDate3.run(10, (int) (byte) 1, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14232() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14232");
        NextDate nextDate3 = new NextDate(10, 10, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (short) 0, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14233() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14233");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 0, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, 100, 0);
        java.lang.String str11 = nextDate3.run((int) '4', (int) '#', 0);
        java.lang.String str15 = nextDate3.run((int) (short) 1, 1, 1);
        java.lang.String str19 = nextDate3.run((-1), 0, (int) (short) 10);
        java.lang.String str23 = nextDate3.run((-1), (int) 'a', (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test14234() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14234");
        NextDate nextDate3 = new NextDate(100, (int) (short) -1, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (byte) 100, (int) (byte) 100);
        java.lang.String str11 = nextDate3.run(10, 0, (int) (byte) 1);
        java.lang.String str15 = nextDate3.run((int) '#', (int) (short) 0, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test14235() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14235");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 1, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run(0, (int) (byte) 0, 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14236() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14236");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) ' ', 10);
        java.lang.String str7 = nextDate3.run(1, 10, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((-1), (int) (byte) 0, 1);
        java.lang.String str15 = nextDate3.run((int) '#', (int) (short) 10, 1);
        java.lang.String str19 = nextDate3.run((int) 'a', (int) (short) 0, (int) (short) 100);
        java.lang.String str23 = nextDate3.run((int) (short) 1, (int) (byte) -1, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test14237() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14237");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) 'a', (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) '#', 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 0, 1, (int) ' ');
        java.lang.String str15 = nextDate3.run((int) (short) -1, (int) (byte) 0, (int) (short) 100);
        java.lang.String str19 = nextDate3.run((int) (byte) 0, 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test14238() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14238");
        NextDate nextDate3 = new NextDate((-1), (int) ' ', (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (byte) 0, (int) (byte) 1);
        java.lang.String str11 = nextDate3.run((int) '#', (int) (short) 10, (int) '4');
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test14239() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14239");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 100, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (-1), (int) ' ');
        java.lang.String str11 = nextDate3.run(0, (int) '#', (int) (byte) 100);
        java.lang.String str15 = nextDate3.run(0, (int) (byte) 1, (-1));
        java.lang.String str19 = nextDate3.run(10, (int) 'a', (int) (byte) 1);
        java.lang.String str23 = nextDate3.run((int) (short) 0, (int) (byte) -1, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test14240() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14240");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) 10, 100);
        java.lang.String str7 = nextDate3.run((-1), (-1), (-1));
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14241() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14241");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) 100, (int) ' ');
        java.lang.String str7 = nextDate3.run((int) (short) -1, 1, (-1));
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14242() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14242");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) 100, (-1));
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) (byte) 0, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((int) (byte) 100, (-1), (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14243() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14243");
        NextDate nextDate3 = new NextDate(1, (int) (short) 10, (int) (short) -1);
        java.lang.String str7 = nextDate3.run(1, (int) (byte) 1, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14244() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14244");
        NextDate nextDate3 = new NextDate((int) ' ', (int) '4', (int) (byte) 1);
        java.lang.String str7 = nextDate3.run(10, 0, (int) (byte) -1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14245() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14245");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 100, (int) ' ');
    }

    @Test
    public void test14246() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14246");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 1, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) (byte) 10, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14247() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14247");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) 1, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, (int) (short) 1, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14248() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14248");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) 1, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (byte) -1, 10);
        java.lang.String str11 = nextDate3.run((int) (short) 0, (-1), (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14249() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14249");
        NextDate nextDate3 = new NextDate(1, (int) (short) 10, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (short) -1, (int) (short) 1);
        java.lang.String str11 = nextDate3.run(10, 0, (int) (byte) 100);
        java.lang.String str15 = nextDate3.run((int) '4', (int) (short) -1, (int) (short) 10);
        java.lang.String str19 = nextDate3.run((int) '#', (int) (short) 0, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test14250() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14250");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) 'a', (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (short) 1, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14251() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14251");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) ' ', (int) (byte) 0);
        java.lang.String str7 = nextDate3.run(100, 0, (int) (short) 100);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14252() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14252");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 0, 100);
    }

    @Test
    public void test14253() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14253");
        NextDate nextDate3 = new NextDate(10, (int) '#', 0);
        java.lang.String str7 = nextDate3.run((int) ' ', 10, (int) (short) 1);
        java.lang.String str11 = nextDate3.run(10, (int) (byte) 100, (int) (byte) 10);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test14254() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14254");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 1, 0);
        java.lang.String str7 = nextDate3.run(0, 0, 0);
        java.lang.String str11 = nextDate3.run((int) '#', 100, 0);
        java.lang.String str15 = nextDate3.run((int) (byte) 1, (int) (byte) 100, (int) (short) 1);
        java.lang.String str19 = nextDate3.run(100, 100, (int) 'a');
        java.lang.String str23 = nextDate3.run((int) ' ', (int) (byte) -1, (int) (byte) 0);
        java.lang.String str27 = nextDate3.run((int) ' ', (int) ' ', (int) (short) 1);
        java.lang.String str31 = nextDate3.run((int) (short) 10, (int) '4', 10);
        java.lang.String str35 = nextDate3.run(100, (int) (short) 1, (int) (byte) 1);
        java.lang.Class<?> wildcardClass36 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass36);
    }

    @Test
    public void test14255() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14255");
        NextDate nextDate3 = new NextDate((int) (short) 0, 0, (int) '4');
        java.lang.String str7 = nextDate3.run((int) (byte) 100, 0, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14256() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14256");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 1, (-1));
        java.lang.String str7 = nextDate3.run(1, (int) (byte) 10, (int) (short) 0);
        java.lang.String str11 = nextDate3.run((int) (short) 1, 0, (int) (byte) 0);
        java.lang.String str15 = nextDate3.run((int) (byte) 100, 0, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test14257() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14257");
        NextDate nextDate3 = new NextDate(1, 100, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (byte) 0, (int) (byte) 100);
        java.lang.String str11 = nextDate3.run((int) (byte) 10, (int) '4', (int) (byte) 1);
        java.lang.String str15 = nextDate3.run((int) (byte) 100, (int) (byte) 100, (int) (byte) 10);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test14258() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14258");
        NextDate nextDate3 = new NextDate(1, (int) '4', (int) '4');
    }

    @Test
    public void test14259() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14259");
        NextDate nextDate3 = new NextDate((int) (short) 100, 1, 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14260() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14260");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 10, 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (byte) 10, (int) ' ');
        java.lang.String str11 = nextDate3.run((int) ' ', (int) (byte) 0, 100);
        java.lang.String str15 = nextDate3.run((int) (short) 10, (int) (short) 0, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test14261() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14261");
        NextDate nextDate3 = new NextDate((int) '#', 0, (int) (short) 100);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14262() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14262");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 100, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (byte) -1, (int) '4');
        java.lang.String str11 = nextDate3.run(0, (int) 'a', (-1));
        java.lang.String str15 = nextDate3.run((int) (byte) 100, (int) (short) 1, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test14263() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14263");
        NextDate nextDate3 = new NextDate((int) ' ', 100, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) 'a', 0);
        java.lang.String str11 = nextDate3.run(0, 10, 1);
        java.lang.String str15 = nextDate3.run((int) (byte) 10, (int) (byte) -1, (int) (byte) 10);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test14264() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14264");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) -1, (int) (short) 100);
        java.lang.String str7 = nextDate3.run(1, (int) '#', (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14265() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14265");
        NextDate nextDate3 = new NextDate((int) '#', (int) ' ', 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, 0, 1);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, (int) '4', (-1));
        java.lang.String str15 = nextDate3.run((int) (byte) 1, 100, (int) '#');
        java.lang.String str19 = nextDate3.run((int) (byte) 10, 100, (int) ' ');
        java.lang.String str23 = nextDate3.run((int) 'a', (int) (byte) 1, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test14266() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14266");
        NextDate nextDate3 = new NextDate(100, 0, (int) '4');
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (short) 100, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14267() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14267");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 100, (int) (byte) -1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14268() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14268");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) -1, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (short) 0, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14269() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14269");
        NextDate nextDate3 = new NextDate((int) 'a', (int) '#', (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, 100, (int) (short) 0);
        java.lang.String str11 = nextDate3.run(10, (int) '#', 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test14270() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14270");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) 'a', (int) (short) 10);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14271() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14271");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 100, 100);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14272() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14272");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) 0, 100);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (short) 10, 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test14273() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14273");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) '4', 1);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (short) 1, (int) 'a');
        java.lang.String str11 = nextDate3.run((int) (short) -1, (int) (short) 100, (int) '4');
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test14274() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14274");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 10, (int) (short) -1);
        java.lang.String str7 = nextDate3.run(1, 0, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test14275() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14275");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) ' ', (int) (short) -1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test14276() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14276");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) -1, 1);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (byte) -1, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run((int) 'a', (-1), (int) ' ');
        java.lang.String str15 = nextDate3.run((int) (byte) 100, (int) (short) 0, 0);
        java.lang.String str19 = nextDate3.run((int) (byte) 0, 0, 0);
        java.lang.String str23 = nextDate3.run((int) (short) 0, (int) (byte) 0, (int) (short) -1);
        java.lang.String str27 = nextDate3.run((int) (byte) 10, (int) (byte) 10, (int) (byte) -1);
        java.lang.String str31 = nextDate3.run((int) (byte) 100, (int) (byte) 1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
    }

    @Test
    public void test14277() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14277");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) '#', 10);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (byte) 1, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run((int) ' ', (int) '#', (int) (byte) 0);
        java.lang.String str15 = nextDate3.run((int) (short) 10, (int) ' ', (int) '#');
        java.lang.String str19 = nextDate3.run((int) (byte) 1, (-1), 1);
        java.lang.String str23 = nextDate3.run((int) (short) 100, (int) (byte) -1, 0);
        java.lang.Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test14278() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest28.test14278");
        NextDate nextDate3 = new NextDate(0, (int) (short) -1, 100);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }
}

