import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest18 {

    public static boolean debug = false;

    @Test
    public void test09001() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09001");
        NextDate nextDate3 = new NextDate(10, (int) (byte) -1, (int) '#');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09002() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09002");
        NextDate nextDate3 = new NextDate((int) '4', 100, 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09003() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09003");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) 'a', 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (byte) 1, (int) (byte) 100);
        java.lang.String str11 = nextDate3.run((int) (byte) -1, (int) (short) 1, (int) (byte) 10);
        java.lang.String str15 = nextDate3.run((int) '4', 0, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09004() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09004");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 10, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) ' ', (int) (byte) 100);
        java.lang.String str11 = nextDate3.run((-1), (int) (short) 10, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09005() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09005");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) -1, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (short) -1, (int) (short) 100);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09006() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09006");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) 0, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run((int) '4', 0, (int) (byte) 100);
        java.lang.String str11 = nextDate3.run((int) (short) 100, (int) (byte) 10, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09007() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09007");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 100, (int) (byte) 100);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09008() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09008");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) 10, (int) ' ');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09009() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09009");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) '#', 100);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (short) 0, 10);
        java.lang.String str11 = nextDate3.run((int) ' ', 100, (int) (byte) 100);
        java.lang.String str15 = nextDate3.run((int) (byte) 100, (int) (short) 10, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09010() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09010");
        NextDate nextDate3 = new NextDate(100, (int) (short) 100, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) ' ', (int) (short) -1);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, 0, 0);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09011() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09011");
        NextDate nextDate3 = new NextDate((int) '4', 1, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run(100, 10, (int) (byte) 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09012() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09012");
        NextDate nextDate3 = new NextDate((int) 'a', 0, (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) '#', (int) ' ');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09013() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09013");
        NextDate nextDate3 = new NextDate((int) '#', 0, 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) 'a', 100);
        java.lang.String str11 = nextDate3.run((int) (byte) 100, 10, (int) (byte) 1);
        java.lang.String str15 = nextDate3.run((int) (byte) 10, (int) (short) 0, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09014() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09014");
        NextDate nextDate3 = new NextDate((int) '#', 0, (int) '4');
        java.lang.String str7 = nextDate3.run((int) (short) 0, 0, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09015() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09015");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 1, 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09016() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09016");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) 0, (int) (short) -1);
    }

    @Test
    public void test09017() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09017");
        NextDate nextDate3 = new NextDate(100, (int) ' ', (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (byte) 10, (int) ' ');
        java.lang.String str11 = nextDate3.run((int) (short) 100, (int) (short) 1, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09018() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09018");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 100, 10);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (short) 1, (int) (byte) 100);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09019() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09019");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) '4', (int) (byte) 1);
        java.lang.String str7 = nextDate3.run(1, 1, 10);
        java.lang.String str11 = nextDate3.run((int) (short) 100, (int) (byte) 100, (-1));
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09020() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09020");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 1, 0);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) '#', (int) (byte) 10);
        java.lang.String str11 = nextDate3.run((int) (short) 100, 100, 1);
        java.lang.String str15 = nextDate3.run((int) '#', (int) (byte) 0, (int) 'a');
        java.lang.String str19 = nextDate3.run((int) '4', (int) ' ', (int) (short) -1);
        java.lang.String str23 = nextDate3.run((int) (byte) 0, (int) (short) 100, (int) '4');
        java.lang.String str27 = nextDate3.run(1, (int) (short) 100, (int) '#');
        java.lang.String str31 = nextDate3.run((-1), (int) (short) 10, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
    }

    @Test
    public void test09021() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09021");
        NextDate nextDate3 = new NextDate(0, (int) ' ', (int) '#');
        java.lang.String str7 = nextDate3.run((int) (byte) 1, (-1), 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09022() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09022");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) 0, (int) '4');
    }

    @Test
    public void test09023() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09023");
        NextDate nextDate3 = new NextDate((int) 'a', 10, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) '#', (int) (short) 10, (int) 'a');
        java.lang.String str11 = nextDate3.run((int) (byte) 10, 10, (int) (byte) -1);
        java.lang.String str15 = nextDate3.run((int) (byte) 100, (int) (byte) 0, (-1));
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09024() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09024");
        NextDate nextDate3 = new NextDate((int) '4', (int) (byte) 100, (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) ' ', (-1), (-1));
        java.lang.String str11 = nextDate3.run((int) (byte) 10, (int) ' ', (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09025() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09025");
        NextDate nextDate3 = new NextDate((int) ' ', 10, (int) 'a');
    }

    @Test
    public void test09026() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09026");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 10, (int) '4');
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (short) 100, (int) (short) 100);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09027() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09027");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (short) 100, (int) '4');
        java.lang.String str7 = nextDate3.run(1, (int) '4', 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09028() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09028");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 0, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (short) -1, (int) (short) 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09029() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09029");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 1, 0);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) '#', (int) (byte) 10);
        java.lang.String str11 = nextDate3.run((int) (short) 100, 100, 1);
        java.lang.String str15 = nextDate3.run((int) '#', (int) (byte) 0, (int) 'a');
        java.lang.String str19 = nextDate3.run(10, (int) ' ', (int) (byte) -1);
        java.lang.String str23 = nextDate3.run((int) (byte) 0, (int) (short) 100, (int) (short) 100);
        java.lang.String str27 = nextDate3.run((int) (short) -1, (int) '#', 1);
        java.lang.String str31 = nextDate3.run((int) (short) 1, (-1), (int) (short) -1);
        java.lang.String str35 = nextDate3.run(10, (int) '#', (int) (byte) 10);
        java.lang.String str39 = nextDate3.run((int) (short) 100, (int) (short) -1, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
    }

    @Test
    public void test09030() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09030");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) ' ', (-1));
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) (byte) -1, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run((int) (short) 10, 10, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09031() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09031");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) '#', (int) (short) 100);
    }

    @Test
    public void test09032() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09032");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 10, 10);
        java.lang.String str7 = nextDate3.run((int) '#', (int) (short) 10, (-1));
        java.lang.String str11 = nextDate3.run((int) '4', (int) '4', (int) (byte) 1);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09033() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09033");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 1, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (short) -1, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09034() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09034");
        NextDate nextDate3 = new NextDate((int) '4', 100, (int) (byte) 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09035() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09035");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) -1, (-1));
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (short) 10, 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09036() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09036");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) -1, (int) ' ');
        java.lang.String str7 = nextDate3.run(10, (int) ' ', (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09037() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09037");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 100, 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09038() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09038");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 0, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) '#', 0, 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09039() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09039");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 10, 0);
        java.lang.String str7 = nextDate3.run(1, (int) '#', 100);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09040() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09040");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) -1, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (short) 1, 0, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09041() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09041");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) 10, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) (short) 10, (int) '4');
        java.lang.String str11 = nextDate3.run((int) '4', (int) (short) 10, (int) (byte) 100);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09042() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09042");
        NextDate nextDate3 = new NextDate((int) 'a', (int) '4', 0);
        java.lang.String str7 = nextDate3.run((int) '#', 0, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run(1, (int) 'a', 1);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09043() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09043");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 1, 100);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09044() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09044");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 1, (int) 'a');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09045() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09045");
        NextDate nextDate3 = new NextDate(1, (int) (short) 1, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run(0, 1, 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09046() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09046");
        NextDate nextDate3 = new NextDate(1, (int) (short) 1, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) ' ', 10, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09047() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09047");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 100, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) 'a', 0, (int) (short) -1);
        java.lang.String str11 = nextDate3.run((int) (byte) 10, (int) 'a', (int) (short) 100);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09048() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09048");
        NextDate nextDate3 = new NextDate((int) ' ', (-1), (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (short) -1, (int) (short) 100);
        java.lang.String str11 = nextDate3.run((int) (short) 0, (int) '4', (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09049() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09049");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 1, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (byte) 0, (int) '#');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09050() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09050");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (-1), 1);
    }

    @Test
    public void test09051() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09051");
        NextDate nextDate3 = new NextDate(0, 0, (int) 'a');
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (short) 1, (int) '#');
        java.lang.String str11 = nextDate3.run((int) (short) -1, 100, 100);
        java.lang.String str15 = nextDate3.run((int) '4', 1, (int) (short) -1);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09052() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09052");
        NextDate nextDate3 = new NextDate((int) ' ', (int) 'a', (int) (byte) 100);
        java.lang.String str7 = nextDate3.run(1, (int) (short) 10, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run((int) (short) -1, (int) (byte) 1, (int) (short) 0);
        java.lang.String str15 = nextDate3.run(0, (int) (byte) -1, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09053() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09053");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) 1, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run(0, 0, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09054() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09054");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) '#', (int) ' ');
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (byte) 0, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run((int) (byte) -1, 10, (int) (short) 10);
        java.lang.String str15 = nextDate3.run(100, (int) ' ', (int) (byte) 10);
        java.lang.String str19 = nextDate3.run((int) ' ', (int) (byte) -1, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test09055() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09055");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) 1, (int) ' ');
        java.lang.String str7 = nextDate3.run((int) (byte) 0, 10, 100);
        java.lang.String str11 = nextDate3.run((int) (byte) -1, (int) (byte) 0, (int) (byte) 100);
        java.lang.String str15 = nextDate3.run(0, (int) (short) -1, (int) '4');
        java.lang.String str19 = nextDate3.run((int) (short) 10, (int) '#', 100);
        java.lang.Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test09056() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09056");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) ' ', 10);
        java.lang.String str7 = nextDate3.run(10, 0, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((-1), (int) (short) 0, (int) (byte) 10);
        java.lang.String str15 = nextDate3.run(0, (int) '4', 10);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09057() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09057");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 0, 0);
        java.lang.String str7 = nextDate3.run(10, 1, (int) ' ');
        java.lang.String str11 = nextDate3.run(1, (int) (short) 10, (int) (byte) 100);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09058() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09058");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) -1, (int) (byte) 10);
    }

    @Test
    public void test09059() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09059");
        NextDate nextDate3 = new NextDate(0, 1, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) ' ', (int) (byte) -1);
        java.lang.String str11 = nextDate3.run(0, (int) (short) -1, (-1));
        java.lang.String str15 = nextDate3.run((int) (short) 0, (int) ' ', (int) ' ');
        java.lang.String str19 = nextDate3.run((int) (byte) 100, (int) (short) 1, (-1));
        java.lang.String str23 = nextDate3.run((int) ' ', (int) (byte) 10, (int) (short) -1);
        java.lang.Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test09060() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09060");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) -1, (-1));
    }

    @Test
    public void test09061() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09061");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) 1, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (short) 10, 100, (int) '#');
        java.lang.String str11 = nextDate3.run((int) ' ', (int) ' ', (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09062() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09062");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 100, (int) '4');
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (short) -1, (int) (short) 100);
        java.lang.String str11 = nextDate3.run((int) (byte) -1, (int) ' ', 0);
        java.lang.String str15 = nextDate3.run((int) (short) 100, (int) (short) 1, (int) (byte) 100);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09063() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09063");
        NextDate nextDate3 = new NextDate((int) 'a', (int) ' ', (int) (byte) -1);
    }

    @Test
    public void test09064() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09064");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 10, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run(100, 0, (int) (byte) -1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09065() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09065");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) 'a', 1);
        java.lang.String str7 = nextDate3.run(0, (int) (short) 1, (int) ' ');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09066() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09066");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) -1, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, 0, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09067() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09067");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 0, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) '4', 10, (int) 'a');
        java.lang.String str11 = nextDate3.run(0, (int) (byte) 10, (int) (short) 10);
        java.lang.String str15 = nextDate3.run((int) '4', 10, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09068() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09068");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) 1, 0);
        java.lang.String str7 = nextDate3.run(1, (int) (short) 1, (int) (short) 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09069() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09069");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 1, 0);
        java.lang.String str7 = nextDate3.run(0, 0, 0);
        java.lang.String str11 = nextDate3.run((int) ' ', (int) 'a', (int) 'a');
        java.lang.String str15 = nextDate3.run((-1), (int) ' ', (int) (byte) -1);
        java.lang.String str19 = nextDate3.run((int) ' ', (int) 'a', (int) '4');
        java.lang.String str23 = nextDate3.run(0, 100, (int) (short) 100);
        java.lang.String str27 = nextDate3.run((int) (byte) -1, (-1), 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test09070() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09070");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 1, (int) (short) 10);
        java.lang.String str7 = nextDate3.run(0, 0, 1);
        java.lang.String str11 = nextDate3.run((int) 'a', (int) (short) 10, 1);
        java.lang.String str15 = nextDate3.run(0, (int) '#', (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09071() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09071");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 100, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) '#', 100, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09072() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09072");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) '#', 0);
        java.lang.String str7 = nextDate3.run((int) (short) 0, (-1), (int) ' ');
        java.lang.String str11 = nextDate3.run((int) '#', 0, (int) 'a');
        java.lang.String str15 = nextDate3.run((int) (short) 10, (int) '4', (int) 'a');
        java.lang.String str19 = nextDate3.run((int) (byte) 100, (int) (byte) 100, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test09073() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09073");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 1, 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) '#', (int) (short) 0);
        java.lang.String str11 = nextDate3.run((int) 'a', (int) '4', (-1));
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09074() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09074");
        NextDate nextDate3 = new NextDate(0, (int) (byte) -1, (int) (byte) 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09075() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09075");
        NextDate nextDate3 = new NextDate((int) (short) -1, 0, (int) (byte) 100);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09076() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09076");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) 'a', (int) (short) 100);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09077() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09077");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 100, 10);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09078() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09078");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 0, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (short) 0, 0, 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09079() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09079");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) '4', (int) (short) 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09080() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09080");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) -1, (int) (short) 10);
        java.lang.String str7 = nextDate3.run(0, 10, 100);
        java.lang.String str11 = nextDate3.run(100, (int) (byte) -1, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09081() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09081");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) ' ', (-1));
    }

    @Test
    public void test09082() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09082");
        NextDate nextDate3 = new NextDate(0, (int) (short) 1, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) 'a', (int) ' ');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09083() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09083");
        NextDate nextDate3 = new NextDate((-1), 1, (-1));
        java.lang.String str7 = nextDate3.run((-1), 0, (int) ' ');
        java.lang.String str11 = nextDate3.run((int) (byte) 1, (int) (byte) -1, (int) (byte) 0);
        java.lang.String str15 = nextDate3.run(100, (int) '#', (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09084() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09084");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (byte) -1, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run(0, (int) '#', (int) '#');
        java.lang.String str11 = nextDate3.run(10, 100, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09085() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09085");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 100, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run(100, 0, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09086() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09086");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 100, (int) '#');
    }

    @Test
    public void test09087() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09087");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 1, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) ' ', 10, (int) ' ');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09088() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09088");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (byte) 1, (int) (short) 0);
        java.lang.String str7 = nextDate3.run(100, (int) (byte) 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09089() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09089");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 1, 100);
        java.lang.String str7 = nextDate3.run(10, (int) (short) 100, 100);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09090() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09090");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) -1, (int) (short) 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09091() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09091");
        NextDate nextDate3 = new NextDate(1, (int) (byte) 10, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (short) -1, 100, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09092() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09092");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 10, 100);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09093() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09093");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) ' ', 100);
        java.lang.String str7 = nextDate3.run((-1), (int) '#', (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09094() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09094");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 10, (int) (byte) 100);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09095() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09095");
        NextDate nextDate3 = new NextDate(10, (int) ' ', (int) (byte) -1);
        java.lang.String str7 = nextDate3.run(0, 0, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run((int) (byte) -1, (int) (short) 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09096() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09096");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 0, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run(0, (int) (short) 100, (int) (short) -1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09097() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09097");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) 'a', (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (short) 1, 10, (int) (byte) 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09098() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09098");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 100, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, (-1), (int) 'a');
        java.lang.String str11 = nextDate3.run((int) (byte) 10, (int) (short) 100, (int) 'a');
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09099() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09099");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 1, 100);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) '4', (-1));
        java.lang.String str11 = nextDate3.run((int) (byte) 0, (int) (byte) 1, 100);
        java.lang.String str15 = nextDate3.run((int) (short) 100, (int) (short) 0, (int) (byte) 0);
        java.lang.String str19 = nextDate3.run(100, (int) (short) 10, 0);
        java.lang.String str23 = nextDate3.run((int) (byte) 0, (int) ' ', (int) (byte) 1);
        java.lang.String str27 = nextDate3.run((int) ' ', (int) (short) 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test09100() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09100");
        NextDate nextDate3 = new NextDate((int) (short) 1, 100, (int) 'a');
        java.lang.String str7 = nextDate3.run((int) (short) 10, 10, 0);
        java.lang.String str11 = nextDate3.run((int) (short) 10, (int) (byte) 0, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09101() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09101");
        NextDate nextDate3 = new NextDate((int) (short) 0, 100, (int) '#');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09102() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09102");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (byte) 0, (int) (short) 10);
        java.lang.String str7 = nextDate3.run(10, 0, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09103() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09103");
        NextDate nextDate3 = new NextDate((int) '#', 1, 10);
        java.lang.String str7 = nextDate3.run((int) '4', (int) 'a', 0);
        java.lang.String str11 = nextDate3.run((int) '4', (int) (byte) 0, (int) (byte) 1);
        java.lang.String str15 = nextDate3.run(100, (-1), (int) (short) 100);
        java.lang.String str19 = nextDate3.run((int) (byte) -1, (int) '4', (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test09104() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09104");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 10, (int) (short) 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09105() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09105");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) 100, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) (byte) -1, (int) (short) -1);
        java.lang.String str11 = nextDate3.run((int) ' ', (int) (short) 10, 0);
        java.lang.String str15 = nextDate3.run((int) (short) 0, (int) (byte) 10, 0);
        java.lang.String str19 = nextDate3.run((int) (short) 1, (int) 'a', 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test09106() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09106");
        NextDate nextDate3 = new NextDate(0, (-1), (int) (byte) 10);
        java.lang.String str7 = nextDate3.run(1, (int) (short) 1, (int) '#');
        java.lang.String str11 = nextDate3.run((int) ' ', (int) '#', 1);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09107() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09107");
        NextDate nextDate3 = new NextDate(1, (int) (byte) 1, (int) 'a');
        java.lang.String str7 = nextDate3.run(0, 10, 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09108() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09108");
        NextDate nextDate3 = new NextDate(0, 10, 100);
        java.lang.String str7 = nextDate3.run((int) 'a', (-1), (int) (byte) 10);
        java.lang.String str11 = nextDate3.run((int) (byte) 10, (int) '#', (int) (byte) 100);
        java.lang.String str15 = nextDate3.run(1, (int) (byte) 1, (int) (short) 1);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09109() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09109");
        NextDate nextDate3 = new NextDate((int) '4', 0, (int) '#');
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (short) 1, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09110() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09110");
        NextDate nextDate3 = new NextDate(10, (int) '#', (int) '#');
    }

    @Test
    public void test09111() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09111");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 0, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (short) 0, 0, 1);
        java.lang.String str11 = nextDate3.run((int) (short) 1, 100, (int) (byte) -1);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09112() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09112");
        NextDate nextDate3 = new NextDate(100, (int) (short) 0, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (short) -1, 0, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run((int) '4', (int) (byte) 100, (int) 'a');
        java.lang.String str15 = nextDate3.run(10, 0, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09113() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09113");
        NextDate nextDate3 = new NextDate((-1), 0, (int) '4');
        java.lang.String str7 = nextDate3.run(0, (int) '#', 0);
        java.lang.String str11 = nextDate3.run((int) ' ', (int) (short) -1, (int) (short) -1);
        java.lang.String str15 = nextDate3.run((int) (byte) 10, 100, (int) (short) 1);
        java.lang.String str19 = nextDate3.run((int) (short) 100, 0, (int) (byte) 1);
        java.lang.String str23 = nextDate3.run((-1), (int) (byte) 10, (int) (short) 1);
        java.lang.String str27 = nextDate3.run((int) '#', (int) (short) -1, (int) ' ');
        java.lang.Class<?> wildcardClass28 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test09114() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09114");
        NextDate nextDate3 = new NextDate(1, (int) (byte) 1, (-1));
        java.lang.String str7 = nextDate3.run((int) (short) 0, (int) (byte) 10, (int) (byte) 100);
        java.lang.String str11 = nextDate3.run((int) (short) 10, (int) ' ', (int) '#');
        java.lang.String str15 = nextDate3.run((int) (short) 100, (int) (short) 1, (int) (byte) 100);
        java.lang.String str19 = nextDate3.run((-1), (int) (short) 1, (int) (byte) 10);
        java.lang.String str23 = nextDate3.run((int) (byte) 10, (int) (short) -1, (int) (short) 10);
        java.lang.String str27 = nextDate3.run((int) 'a', (int) (byte) 100, (int) (byte) 0);
        java.lang.String str31 = nextDate3.run(10, (int) (short) 0, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
    }

    @Test
    public void test09115() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09115");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 0, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) '4', (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09116() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09116");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 100, (int) (byte) 10);
    }

    @Test
    public void test09117() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09117");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) ' ', 10);
        java.lang.String str7 = nextDate3.run(1, 10, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((int) '4', 1, (int) (byte) 100);
        java.lang.String str15 = nextDate3.run((-1), (int) (short) -1, 0);
        java.lang.String str19 = nextDate3.run((int) (byte) 1, (int) ' ', (int) ' ');
        java.lang.String str23 = nextDate3.run((int) (byte) 0, (int) (short) 100, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test09118() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09118");
        NextDate nextDate3 = new NextDate(0, (int) (short) 100, 10);
        java.lang.String str7 = nextDate3.run(0, (-1), (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((int) (byte) 0, 0, (int) (short) 0);
        java.lang.String str15 = nextDate3.run((int) (byte) 0, (int) (short) 0, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09119() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09119");
        NextDate nextDate3 = new NextDate((int) '4', (-1), (int) (short) -1);
        java.lang.String str7 = nextDate3.run(10, (-1), 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09120() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09120");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 1, (int) '#');
        java.lang.String str7 = nextDate3.run(10, (int) (short) -1, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09121() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09121");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) -1, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (short) 1, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09122() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09122");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) 0, (int) ' ');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09123() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09123");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 100, 100);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (short) 10, (int) (byte) 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09124() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09124");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 10, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (short) 1, 0);
        java.lang.String str11 = nextDate3.run(10, 0, (-1));
        java.lang.String str15 = nextDate3.run((int) (short) -1, 1, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09125() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09125");
        NextDate nextDate3 = new NextDate((int) (short) 100, 0, 10);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (short) 100, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09126() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09126");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) '4', (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (byte) 0, 0);
        java.lang.String str11 = nextDate3.run((-1), (int) (byte) 0, (int) (byte) -1);
        java.lang.String str15 = nextDate3.run(100, 10, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09127() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09127");
        NextDate nextDate3 = new NextDate((int) ' ', (-1), 10);
        java.lang.String str7 = nextDate3.run((int) '#', 100, 1);
        java.lang.String str11 = nextDate3.run((int) '4', (int) ' ', 0);
        java.lang.String str15 = nextDate3.run((int) (short) 1, (int) (short) -1, (int) (short) -1);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09128() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09128");
        NextDate nextDate3 = new NextDate((int) 'a', (int) 'a', 100);
        java.lang.String str7 = nextDate3.run(100, (int) (byte) 100, 1);
        java.lang.String str11 = nextDate3.run((int) ' ', (int) (byte) 100, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09129() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09129");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) 0, (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) (short) 100, 0, (int) (byte) 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09130() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09130");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) -1, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (byte) -1, (int) (byte) 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09131() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09131");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 10, (-1));
    }

    @Test
    public void test09132() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09132");
        NextDate nextDate3 = new NextDate((int) (short) 10, 100, (int) (byte) 10);
    }

    @Test
    public void test09133() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09133");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 1, (-1));
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (byte) 1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09134() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09134");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) 'a', (int) (short) 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09135() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09135");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 10, (int) 'a');
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (short) -1, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09136() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09136");
        NextDate nextDate3 = new NextDate((int) '4', (int) (byte) 100, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (short) -1, 0);
        java.lang.String str11 = nextDate3.run(0, 10, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09137() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09137");
        NextDate nextDate3 = new NextDate((int) 'a', (-1), 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, 0, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09138() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09138");
        NextDate nextDate3 = new NextDate((int) '4', (int) '4', (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, 0, 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09139() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09139");
        NextDate nextDate3 = new NextDate(100, 10, (int) (short) 0);
        java.lang.String str7 = nextDate3.run(100, (int) (byte) -1, (int) (byte) 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09140() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09140");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) -1, 100);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (short) 10, (int) '#');
        java.lang.String str11 = nextDate3.run((int) (short) 10, 100, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09141() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09141");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 10, (int) (short) -1);
        java.lang.String str7 = nextDate3.run(100, (int) (byte) 100, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09142() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09142");
        NextDate nextDate3 = new NextDate((int) ' ', (-1), (int) (byte) 0);
        java.lang.String str7 = nextDate3.run(10, (int) (byte) 100, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09143() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09143");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 100, (int) '4');
        java.lang.String str7 = nextDate3.run((-1), (int) (short) 10, (int) 'a');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09144() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09144");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 1, 0);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (byte) 0, 10);
        java.lang.String str11 = nextDate3.run((int) (short) 10, (int) (byte) 100, 10);
        java.lang.String str15 = nextDate3.run((int) (byte) 1, (int) 'a', 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09145() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09145");
        NextDate nextDate3 = new NextDate(1, (-1), (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (-1), (int) (short) 0);
        java.lang.String str11 = nextDate3.run(100, (int) (byte) 1, (int) (byte) 1);
        java.lang.String str15 = nextDate3.run((int) (short) -1, (int) '4', (int) (byte) 100);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09146() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09146");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 100, (int) '4');
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (short) -1, (int) (short) 100);
        java.lang.String str11 = nextDate3.run((int) (byte) 0, (int) (short) 0, (int) (byte) 10);
        java.lang.String str15 = nextDate3.run((int) (short) -1, (int) (byte) 100, (int) (short) 0);
        java.lang.String str19 = nextDate3.run((int) 'a', (int) (short) 100, (int) (short) 1);
        java.lang.String str23 = nextDate3.run((int) ' ', (int) (byte) 10, 1);
        java.lang.String str27 = nextDate3.run((int) (byte) 0, (int) (byte) 1, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test09147() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09147");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (byte) 10, (int) ' ');
        java.lang.String str7 = nextDate3.run((int) (short) -1, 1, (int) 'a');
        java.lang.String str11 = nextDate3.run((int) ' ', (int) (short) 1, (int) (short) 10);
        java.lang.String str15 = nextDate3.run((int) 'a', (-1), (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09148() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09148");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 100, 10);
        java.lang.String str7 = nextDate3.run(10, (int) 'a', (int) (byte) -1);
        java.lang.String str11 = nextDate3.run(1, (int) (byte) 100, (int) (short) 0);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09149() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09149");
        NextDate nextDate3 = new NextDate((int) (short) -1, (-1), (int) 'a');
    }

    @Test
    public void test09150() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09150");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) -1, (-1));
        java.lang.String str7 = nextDate3.run(10, (int) (byte) 1, 0);
        java.lang.String str11 = nextDate3.run(100, (int) ' ', 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09151() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09151");
        NextDate nextDate3 = new NextDate(0, (int) (short) 10, (int) (byte) 1);
    }

    @Test
    public void test09152() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09152");
        NextDate nextDate3 = new NextDate(100, (int) 'a', 0);
        java.lang.String str7 = nextDate3.run(0, 100, 0);
        java.lang.String str11 = nextDate3.run((int) (short) 10, (int) (short) 1, (int) (short) 1);
        java.lang.String str15 = nextDate3.run(10, (int) '4', (int) '#');
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09153() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09153");
        NextDate nextDate3 = new NextDate(10, (-1), (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (short) 100, (int) (byte) 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09154() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09154");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) 1, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) '#', (int) (short) 100, (int) '#');
        java.lang.String str11 = nextDate3.run(0, 10, (int) 'a');
        java.lang.String str15 = nextDate3.run((int) (byte) 1, (int) '#', (int) 'a');
        java.lang.String str19 = nextDate3.run((-1), (int) (byte) 1, 0);
        java.lang.String str23 = nextDate3.run((int) '#', 0, 0);
        java.lang.String str27 = nextDate3.run((int) (short) 0, 100, (int) '#');
        java.lang.String str31 = nextDate3.run((int) (short) 1, (int) (short) 100, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
    }

    @Test
    public void test09155() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09155");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) -1, 1);
        java.lang.String str7 = nextDate3.run(1, (int) (short) 10, (int) 'a');
        java.lang.String str11 = nextDate3.run(10, (int) (short) 10, (-1));
        java.lang.String str15 = nextDate3.run((int) (short) -1, (int) (byte) 1, (int) (byte) -1);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09156() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09156");
        NextDate nextDate3 = new NextDate(0, (int) (short) 0, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (short) 0, 100, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((-1), (int) (byte) 1, 10);
        java.lang.String str15 = nextDate3.run((int) (byte) 1, (int) (byte) 10, (int) (byte) 0);
        java.lang.String str19 = nextDate3.run((int) (byte) 10, (int) '#', (int) 'a');
        java.lang.Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test09157() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09157");
        NextDate nextDate3 = new NextDate((-1), 0, 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09158() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09158");
        NextDate nextDate3 = new NextDate((int) 'a', 10, 100);
        java.lang.String str7 = nextDate3.run((int) (short) 0, (int) ' ', (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09159() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09159");
        NextDate nextDate3 = new NextDate((-1), 0, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) '4', (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09160() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09160");
        NextDate nextDate3 = new NextDate((int) (short) 0, (-1), (int) (byte) -1);
        java.lang.String str7 = nextDate3.run(10, (int) '#', (int) (short) 100);
        java.lang.String str11 = nextDate3.run((int) (byte) 100, 0, (int) (short) 10);
        java.lang.String str15 = nextDate3.run((int) (short) 100, (int) (short) 1, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09161() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09161");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 10, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run(100, 0, 10);
        java.lang.String str11 = nextDate3.run(0, (int) ' ', 0);
        java.lang.String str15 = nextDate3.run((int) '4', (int) (short) -1, (int) (short) -1);
        java.lang.String str19 = nextDate3.run((int) '4', (int) (short) 100, (-1));
        java.lang.Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test09162() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09162");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 100, (int) '4');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09163() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09163");
        NextDate nextDate3 = new NextDate((int) (short) -1, 100, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((-1), 1, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09164() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09164");
        NextDate nextDate3 = new NextDate(100, (int) (short) 1, (int) '4');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09165() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09165");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) '#', (int) (byte) 0);
        java.lang.String str7 = nextDate3.run(0, (int) (byte) 0, (int) '4');
        java.lang.String str11 = nextDate3.run((int) ' ', (int) '#', 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09166() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09166");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (byte) 10, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (byte) 1, (int) (byte) 1);
        java.lang.String str11 = nextDate3.run((int) (short) 100, (int) '4', 10);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09167() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09167");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) -1, 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09168() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09168");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) ' ', 10);
        java.lang.String str7 = nextDate3.run(1, 10, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((-1), (int) (byte) 0, 1);
        java.lang.String str15 = nextDate3.run((int) '#', 0, 0);
        java.lang.String str19 = nextDate3.run(100, (int) ' ', (int) (short) 0);
        java.lang.String str23 = nextDate3.run((int) (byte) 10, (int) (byte) -1, (int) (short) 1);
        java.lang.String str27 = nextDate3.run((int) (byte) 10, (int) (short) 100, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test09169() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09169");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 10, 100);
        java.lang.String str7 = nextDate3.run(1, (int) (short) 0, 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09170() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09170");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 0, 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09171() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09171");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) 'a', (int) (byte) 0);
        java.lang.String str7 = nextDate3.run(1, (int) 'a', (int) '4');
        java.lang.String str11 = nextDate3.run((int) '4', (int) 'a', 10);
        java.lang.String str15 = nextDate3.run((int) (short) 100, (int) (short) 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09172() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09172");
        NextDate nextDate3 = new NextDate((int) (short) -1, 0, (int) '4');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09173() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09173");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 1, 0);
        java.lang.String str7 = nextDate3.run(0, 0, 0);
        java.lang.String str11 = nextDate3.run((int) '#', 100, 0);
        java.lang.String str15 = nextDate3.run((int) (byte) 1, (int) (byte) 100, (int) (short) 1);
        java.lang.String str19 = nextDate3.run(100, 100, (int) 'a');
        java.lang.String str23 = nextDate3.run((-1), (-1), 0);
        java.lang.String str27 = nextDate3.run(100, (int) (byte) 10, (int) '#');
        java.lang.String str31 = nextDate3.run((int) (short) 1, (int) (byte) 0, (int) '4');
        java.lang.String str35 = nextDate3.run(100, (int) ' ', (int) 'a');
        java.lang.String str39 = nextDate3.run((int) (byte) 10, (int) (short) 1, (int) (short) 10);
        java.lang.Class<?> wildcardClass40 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass40);
    }

    @Test
    public void test09174() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09174");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 10, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (byte) 1, (int) (byte) 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09175() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09175");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 10, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (short) 0, (int) (short) 1, 0);
        java.lang.String str11 = nextDate3.run((int) (short) 100, (int) (byte) 1, (int) (byte) 10);
        java.lang.String str15 = nextDate3.run((int) (short) 0, (int) 'a', (int) (byte) -1);
        java.lang.String str19 = nextDate3.run((int) '#', 0, (-1));
        java.lang.String str23 = nextDate3.run((int) 'a', (int) (byte) 10, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test09176() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09176");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 0, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run(0, 0, (int) (short) 100);
        java.lang.String str11 = nextDate3.run((int) '#', (int) (short) -1, (int) (short) 100);
        java.lang.String str15 = nextDate3.run(0, (int) ' ', (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09177() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09177");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 0, (-1));
    }

    @Test
    public void test09178() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09178");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) 'a', (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (short) 0, (int) (byte) -1, (int) (short) -1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09179() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09179");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 10, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) '4', (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09180() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09180");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) '#', (int) (short) 10);
        java.lang.String str7 = nextDate3.run(10, 100, (int) '#');
        java.lang.String str11 = nextDate3.run((int) (short) -1, (int) (byte) 100, (-1));
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09181() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09181");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) -1, 1);
        java.lang.String str7 = nextDate3.run((-1), 1, 100);
        java.lang.String str11 = nextDate3.run((int) (byte) 100, (int) (short) 0, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09182() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09182");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) -1, (int) (short) 1);
    }

    @Test
    public void test09183() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09183");
        NextDate nextDate3 = new NextDate(100, (int) '4', (int) (short) 0);
        java.lang.String str7 = nextDate3.run((-1), (int) (short) 100, (int) ' ');
        java.lang.String str11 = nextDate3.run((int) (byte) 0, (int) (short) 0, (-1));
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09184() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09184");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (short) 10, (int) (short) 100);
    }

    @Test
    public void test09185() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09185");
        NextDate nextDate3 = new NextDate(100, (int) (byte) -1, (int) '4');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09186() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09186");
        NextDate nextDate3 = new NextDate((int) '#', 10, (int) (short) 10);
    }

    @Test
    public void test09187() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09187");
        NextDate nextDate3 = new NextDate(1, 10, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (short) 1, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09188() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09188");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) 10, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (-1), (int) (short) 100);
        java.lang.String str11 = nextDate3.run((int) (byte) -1, (int) (byte) 100, (int) 'a');
        java.lang.String str15 = nextDate3.run((int) (short) 10, (int) '4', 1);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09189() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09189");
        NextDate nextDate3 = new NextDate(0, (int) (short) 1, (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, 0, (int) (byte) 100);
        java.lang.String str11 = nextDate3.run((-1), 0, (int) (short) 100);
        java.lang.String str15 = nextDate3.run(0, (int) '4', (int) (byte) 0);
        java.lang.String str19 = nextDate3.run(10, (int) (byte) -1, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test09190() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09190");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 0, (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) '4', (int) ' ');
        java.lang.String str11 = nextDate3.run((int) ' ', (int) (byte) 1, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09191() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09191");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) 100, (-1));
        java.lang.String str7 = nextDate3.run((int) '4', (int) (byte) 1, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09192() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09192");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 100, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, (-1), (int) 'a');
        java.lang.String str11 = nextDate3.run((int) (byte) 10, (int) (short) 100, (int) 'a');
        java.lang.String str15 = nextDate3.run((int) (byte) 100, 10, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09193() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09193");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 1, 0);
        java.lang.String str7 = nextDate3.run(0, 0, 0);
        java.lang.String str11 = nextDate3.run((int) '#', 100, 0);
        java.lang.String str15 = nextDate3.run((int) (byte) 1, (int) (byte) 100, (int) (short) 1);
        java.lang.String str19 = nextDate3.run((int) 'a', (int) ' ', (int) (byte) -1);
        java.lang.String str23 = nextDate3.run((int) (short) 100, (int) '#', 10);
        java.lang.String str27 = nextDate3.run((int) (short) 100, (int) (short) 100, (int) (short) 0);
        java.lang.String str31 = nextDate3.run((int) (short) -1, 100, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
    }

    @Test
    public void test09194() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09194");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 10, (int) (byte) 100);
    }

    @Test
    public void test09195() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09195");
        NextDate nextDate3 = new NextDate((-1), (int) ' ', 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, 10, (int) (short) 100);
        java.lang.String str11 = nextDate3.run((int) (short) -1, (int) '4', (int) (byte) 100);
        java.lang.String str15 = nextDate3.run(0, (int) '#', (int) (short) 1);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09196() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09196");
        NextDate nextDate3 = new NextDate((int) ' ', (-1), (int) (short) 100);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09197() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09197");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 0, (-1));
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (short) 100, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run((int) (byte) -1, (int) ' ', 10);
        java.lang.String str15 = nextDate3.run((int) (byte) -1, (int) (short) 0, 0);
        java.lang.String str19 = nextDate3.run(0, (int) (byte) 0, (int) ' ');
        java.lang.Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test09198() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09198");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) 'a', (int) (short) 100);
        java.lang.String str7 = nextDate3.run(0, (int) ' ', 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09199() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09199");
        NextDate nextDate3 = new NextDate((int) ' ', 0, (int) '4');
        java.lang.String str7 = nextDate3.run((int) (byte) 10, 0, (int) '#');
        java.lang.String str11 = nextDate3.run((int) '4', (int) (short) 100, (int) '#');
        java.lang.String str15 = nextDate3.run(1, 0, (int) (short) -1);
        java.lang.String str19 = nextDate3.run((int) (short) 10, (int) ' ', (int) '4');
        java.lang.String str23 = nextDate3.run((int) (short) 100, (int) (byte) -1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test09200() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09200");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (short) -1, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run(1, (int) (byte) 10, (int) (byte) 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09201() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09201");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 10, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (short) 0, (int) (short) 1, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09202() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09202");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 10, 10);
        java.lang.String str7 = nextDate3.run((int) ' ', 10, (int) (short) 1);
        java.lang.String str11 = nextDate3.run((int) (byte) 10, (int) 'a', 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09203() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09203");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 1, 0);
        java.lang.String str7 = nextDate3.run(0, 0, 0);
        java.lang.String str11 = nextDate3.run((int) ' ', (int) 'a', (int) 'a');
        java.lang.String str15 = nextDate3.run((-1), (int) ' ', (int) (byte) -1);
        java.lang.String str19 = nextDate3.run((int) ' ', (int) 'a', (int) '4');
        java.lang.String str23 = nextDate3.run((int) '4', (int) (short) 1, (int) ' ');
        java.lang.String str27 = nextDate3.run((int) (byte) 10, 100, (int) (short) 0);
        java.lang.String str31 = nextDate3.run((int) (short) 10, (int) (short) 1, (int) '4');
        java.lang.String str35 = nextDate3.run((int) (short) 100, (int) '#', (int) '4');
        java.lang.String str39 = nextDate3.run((int) (short) 100, 100, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
    }

    @Test
    public void test09204() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09204");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 10, (int) (short) 100);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09205() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09205");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) '#', 1);
        java.lang.String str7 = nextDate3.run(10, (int) (byte) 100, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09206() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09206");
        NextDate nextDate3 = new NextDate((int) '#', 1, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, (int) (short) -1, (int) (short) 1);
        java.lang.String str11 = nextDate3.run((int) '4', (int) '4', 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09207() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09207");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 1, 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09208() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09208");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) 10, (int) (short) 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09209() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09209");
        NextDate nextDate3 = new NextDate((int) '4', 100, (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) ' ', 0, (int) (byte) 100);
        java.lang.String str11 = nextDate3.run((int) (byte) 100, (int) (short) -1, (int) (byte) 0);
        java.lang.String str15 = nextDate3.run((int) '#', 1, 0);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09210() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09210");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) 100, (int) 'a');
    }

    @Test
    public void test09211() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09211");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) -1, (int) (byte) -1);
    }

    @Test
    public void test09212() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09212");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) -1, (int) '#');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09213() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09213");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (byte) 10, (int) (short) 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09214() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09214");
        NextDate nextDate3 = new NextDate((-1), 10, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) 'a', (int) (byte) 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09215() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09215");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) 10, (int) 'a');
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (byte) 10, 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 10, (int) (byte) 10, (int) (byte) 10);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09216() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09216");
        NextDate nextDate3 = new NextDate((int) '4', (int) '#', (int) 'a');
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (short) 1, 1);
        java.lang.String str11 = nextDate3.run(0, (int) (byte) 100, 10);
        java.lang.String str15 = nextDate3.run((int) (byte) 10, (int) (byte) -1, (int) (short) 10);
        java.lang.String str19 = nextDate3.run(0, (int) (short) 100, (int) ' ');
        java.lang.String str23 = nextDate3.run((int) (byte) -1, 0, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test09217() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09217");
        NextDate nextDate3 = new NextDate((int) (short) 100, 10, (int) (short) 0);
        java.lang.String str7 = nextDate3.run(100, 0, (int) (byte) 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09218() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09218");
        NextDate nextDate3 = new NextDate((int) ' ', 0, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (byte) 100, (int) (byte) 100);
        java.lang.String str11 = nextDate3.run((int) (byte) -1, (int) ' ', (int) (short) -1);
        java.lang.String str15 = nextDate3.run((int) (short) 0, (int) (byte) 0, (int) (short) 10);
        java.lang.String str19 = nextDate3.run((int) ' ', (int) (short) 100, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test09219() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09219");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 10, 0);
        java.lang.String str7 = nextDate3.run((int) (short) 0, 0, (int) (byte) 100);
        java.lang.String str11 = nextDate3.run((int) (short) 100, 10, 100);
        java.lang.String str15 = nextDate3.run((int) (byte) 1, 10, (int) (byte) 0);
        java.lang.String str19 = nextDate3.run((int) (byte) 0, (int) (short) 10, (int) (byte) 0);
        java.lang.Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test09220() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09220");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 0, (int) 'a');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09221() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09221");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) '4', (int) ' ');
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (short) 100, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09222() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09222");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 0, 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (-1), (int) (byte) 1);
        java.lang.String str11 = nextDate3.run((int) (short) -1, (-1), (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09223() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09223");
        NextDate nextDate3 = new NextDate(0, 1, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, 10, 1);
        java.lang.String str11 = nextDate3.run((int) (short) -1, (int) (short) 10, (int) (byte) 1);
        java.lang.String str15 = nextDate3.run((int) (byte) 10, (int) (byte) -1, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09224() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09224");
        NextDate nextDate3 = new NextDate(10, 1, (int) ' ');
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (short) 100, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09225() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09225");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) 1, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (byte) -1, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09226() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09226");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) ' ', (int) (byte) 0);
        java.lang.String str7 = nextDate3.run(1, (int) (short) -1, (int) (short) 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09227() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09227");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) '#', (int) (byte) 1);
    }

    @Test
    public void test09228() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09228");
        NextDate nextDate3 = new NextDate((int) '4', 0, (int) (byte) 10);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09229() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09229");
        NextDate nextDate3 = new NextDate((int) '4', 0, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) ' ', (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((int) (short) 1, 100, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09230() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09230");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) 1, (int) '4');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09231() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09231");
        NextDate nextDate3 = new NextDate(0, (int) (short) -1, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (byte) 10, (-1));
        java.lang.String str11 = nextDate3.run((int) (short) 1, (int) (short) 100, (int) 'a');
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09232() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09232");
        NextDate nextDate3 = new NextDate(100, (int) '#', (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) '4', (int) '#', (int) (byte) 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 0, 1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09233() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09233");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) ' ', (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) '#', (int) (short) 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09234() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09234");
        NextDate nextDate3 = new NextDate((int) '4', 10, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (-1), 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09235() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09235");
        NextDate nextDate3 = new NextDate((int) 'a', 10, 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09236() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09236");
        NextDate nextDate3 = new NextDate((int) '#', 10, 10);
        java.lang.String str7 = nextDate3.run(100, (int) (short) 10, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09237() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09237");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) '4', (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (short) 100, (int) (short) 1);
        java.lang.String str11 = nextDate3.run(10, (int) (short) 100, (int) (byte) 1);
        java.lang.String str15 = nextDate3.run((int) (byte) 10, (int) (byte) 10, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09238() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09238");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (-1), (int) '#');
        java.lang.String str7 = nextDate3.run((int) '#', (int) '#', 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09239() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09239");
        NextDate nextDate3 = new NextDate((int) ' ', (int) '#', (int) (short) 0);
        java.lang.String str7 = nextDate3.run((-1), (-1), 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09240() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09240");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (-1), (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (byte) 10, (int) (short) 1);
        java.lang.String str11 = nextDate3.run((int) (byte) 10, (int) ' ', (int) '4');
        java.lang.String str15 = nextDate3.run((int) (short) 10, (int) (byte) 100, (int) 'a');
        java.lang.String str19 = nextDate3.run(100, 100, 10);
        java.lang.Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test09241() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09241");
        NextDate nextDate3 = new NextDate(100, 10, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (byte) 0, (-1));
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09242() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09242");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) '4', (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, 1, (int) (short) 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, (int) (byte) 100, (int) (short) 100);
        java.lang.String str15 = nextDate3.run((-1), (int) (byte) 1, (int) (byte) 100);
        java.lang.String str19 = nextDate3.run((int) (short) 100, 100, (int) '#');
        java.lang.Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test09243() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09243");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 10, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (byte) -1, (-1));
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09244() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09244");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 0, (int) (short) 10);
    }

    @Test
    public void test09245() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09245");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 100, (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) (byte) 1, (int) (short) -1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09246() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09246");
        NextDate nextDate3 = new NextDate((int) (short) 0, (-1), (int) ' ');
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (byte) 10, 0);
        java.lang.String str11 = nextDate3.run((int) '4', 10, (int) (short) 0);
        java.lang.String str15 = nextDate3.run((int) (byte) 100, (int) (short) 10, (int) (byte) 1);
        java.lang.String str19 = nextDate3.run(0, 100, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test09247() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09247");
        NextDate nextDate3 = new NextDate((int) (short) 0, 10, (int) (short) 100);
        java.lang.String str7 = nextDate3.run(1, (int) (byte) 100, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09248() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09248");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) 1, (int) (short) 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09249() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09249");
        NextDate nextDate3 = new NextDate(0, (int) (short) 10, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (short) 100, 1);
        java.lang.String str11 = nextDate3.run((int) (byte) 10, (int) (byte) 100, (-1));
        java.lang.String str15 = nextDate3.run((int) 'a', (int) '#', (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09250() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09250");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) ' ', (-1));
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (byte) 100, 100);
        java.lang.String str11 = nextDate3.run((int) '#', (int) (byte) -1, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09251() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09251");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) '#', (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (byte) -1, (int) (byte) 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09252() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09252");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 10, (int) '#');
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) '#', (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09253() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09253");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) 1, 0);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (byte) 100, (int) '#');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09254() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09254");
        NextDate nextDate3 = new NextDate((int) '4', (int) 'a', (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) 'a', 100, (int) 'a');
        java.lang.String str11 = nextDate3.run((int) (byte) 10, (int) '#', 0);
        java.lang.String str15 = nextDate3.run((int) (byte) 10, 0, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09255() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09255");
        NextDate nextDate3 = new NextDate((int) (short) -1, 0, (int) (short) -1);
        java.lang.String str7 = nextDate3.run(10, (int) '#', (int) ' ');
        java.lang.String str11 = nextDate3.run((int) (byte) 1, (int) (byte) 1, (int) (short) 1);
        java.lang.String str15 = nextDate3.run(0, (int) (short) 10, 0);
        java.lang.String str19 = nextDate3.run((-1), (int) '#', (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test09256() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09256");
        NextDate nextDate3 = new NextDate((int) '4', 10, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) '#', (int) (short) 100, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09257() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09257");
        NextDate nextDate3 = new NextDate((int) '4', (int) ' ', (int) 'a');
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (byte) 10, (int) '#');
        java.lang.String str11 = nextDate3.run((int) (short) 0, (-1), (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09258() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09258");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) 0, 0);
        java.lang.String str7 = nextDate3.run(1, (int) 'a', (int) (short) 1);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, (int) (short) -1, (int) '4');
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09259() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09259");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 0, 100);
        java.lang.String str7 = nextDate3.run(1, 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09260() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09260");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 0, 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (byte) 1, (int) (short) -1);
        java.lang.String str11 = nextDate3.run((int) (short) 10, (int) (byte) -1, 0);
        java.lang.String str15 = nextDate3.run((int) (byte) -1, 0, (int) (byte) 1);
        java.lang.String str19 = nextDate3.run((int) (short) 100, (int) (byte) 10, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test09261() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09261");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) -1, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) (short) 100, 100, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09262() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09262");
        NextDate nextDate3 = new NextDate(1, (int) (byte) -1, (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, 100, 0);
        java.lang.String str11 = nextDate3.run((-1), (int) (short) 100, (int) (short) 0);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09263() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09263");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 1, (int) '#');
        java.lang.String str7 = nextDate3.run((int) ' ', 10, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run(10, 0, (int) (byte) 0);
        java.lang.String str15 = nextDate3.run((int) ' ', (int) (short) 100, (int) (short) 0);
        java.lang.String str19 = nextDate3.run((int) 'a', 100, (int) (byte) 1);
        java.lang.String str23 = nextDate3.run(10, (int) (byte) 1, (int) '4');
        java.lang.Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test09264() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09264");
        NextDate nextDate3 = new NextDate((int) (short) 10, 100, (int) (short) 10);
    }

    @Test
    public void test09265() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09265");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 0, (int) (short) -1);
        java.lang.String str7 = nextDate3.run(1, (int) (short) 0, (int) (byte) 1);
        java.lang.String str11 = nextDate3.run((int) (byte) -1, (int) (byte) 100, (int) '#');
        java.lang.String str15 = nextDate3.run((int) (short) 100, (int) ' ', (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09266() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09266");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (-1), (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (short) 0, 0, (int) (short) 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09267() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09267");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) '4', 0);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (byte) 0, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run((int) (short) 0, (int) (byte) 100, (int) '4');
        java.lang.String str15 = nextDate3.run((-1), (int) (short) 100, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09268() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09268");
        NextDate nextDate3 = new NextDate(0, (-1), (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (short) 0, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09269() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09269");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) -1, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) (byte) 100, (int) 'a');
        java.lang.String str11 = nextDate3.run(100, (int) '4', (int) '4');
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09270() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09270");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 100, 100);
        java.lang.String str7 = nextDate3.run((-1), (int) (byte) 10, (int) (short) -1);
        java.lang.String str11 = nextDate3.run((int) ' ', 100, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09271() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09271");
        NextDate nextDate3 = new NextDate((int) (short) -1, (-1), 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09272() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09272");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 1, 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (short) -1, (int) (short) 100);
        java.lang.String str11 = nextDate3.run((int) 'a', (int) (byte) 0, (int) '#');
        java.lang.String str15 = nextDate3.run(0, (int) (byte) -1, 10);
        java.lang.String str19 = nextDate3.run((int) (short) 0, 100, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test09273() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09273");
        NextDate nextDate3 = new NextDate((int) (short) 1, 0, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run(0, (int) (byte) 0, (int) '4');
        java.lang.String str11 = nextDate3.run((int) (byte) 0, (int) (byte) 1, (int) (byte) -1);
        java.lang.String str15 = nextDate3.run(0, (int) (short) 0, (int) (byte) 1);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09274() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09274");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 0, (int) (byte) 100);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09275() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09275");
        NextDate nextDate3 = new NextDate(0, (-1), (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((-1), (int) (byte) 100, (int) (short) 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09276() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09276");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 100, (int) (short) 100);
        java.lang.String str7 = nextDate3.run(0, (int) (short) 10, (int) (byte) 1);
        java.lang.String str11 = nextDate3.run((int) (byte) 100, (int) '#', (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09277() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09277");
        NextDate nextDate3 = new NextDate((-1), 0, (int) '4');
        java.lang.String str7 = nextDate3.run(0, (int) '#', 0);
        java.lang.String str11 = nextDate3.run((int) ' ', (int) (short) -1, (int) (short) -1);
        java.lang.String str15 = nextDate3.run((int) (short) 10, (int) (short) 1, 100);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09278() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09278");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 100, (-1));
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (byte) 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09279() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09279");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 1, 0);
        java.lang.String str7 = nextDate3.run(0, 0, 0);
        java.lang.String str11 = nextDate3.run((int) (byte) -1, 1, (int) (short) 100);
        java.lang.String str15 = nextDate3.run((int) (byte) 10, (int) (short) 0, 0);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09280() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09280");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 1, 0);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (-1), (int) '#');
        java.lang.String str11 = nextDate3.run((int) (short) 10, (int) (short) 0, (int) (short) 100);
        java.lang.String str15 = nextDate3.run((int) 'a', (int) (byte) 10, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09281() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09281");
        NextDate nextDate3 = new NextDate((int) '4', 100, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) (byte) 0, 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 10, (int) ' ', (int) (byte) -1);
        java.lang.String str15 = nextDate3.run(0, (int) (short) 10, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09282() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09282");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) 'a', (int) ' ');
        java.lang.String str7 = nextDate3.run((int) '4', (int) '4', 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09283() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09283");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) 1, (int) (short) 0);
    }

    @Test
    public void test09284() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09284");
        NextDate nextDate3 = new NextDate((int) '#', (-1), (int) (byte) 10);
    }

    @Test
    public void test09285() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09285");
        NextDate nextDate3 = new NextDate((int) '#', 100, 100);
        java.lang.String str7 = nextDate3.run((int) (short) 0, (int) (byte) 100, (int) '#');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09286() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09286");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 0, (int) (short) -1);
    }

    @Test
    public void test09287() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09287");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) -1, (int) ' ');
    }

    @Test
    public void test09288() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09288");
        NextDate nextDate3 = new NextDate((-1), (int) '#', (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (byte) 0, (int) (byte) 1);
        java.lang.String str11 = nextDate3.run((-1), 1, (-1));
        java.lang.String str15 = nextDate3.run((int) '4', (-1), 100);
        java.lang.String str19 = nextDate3.run(1, (int) '#', (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test09289() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09289");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) -1, 0);
        java.lang.String str7 = nextDate3.run(100, 10, (int) 'a');
        java.lang.String str11 = nextDate3.run(100, 10, 0);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09290() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09290");
        NextDate nextDate3 = new NextDate(1, (int) (short) 1, (int) (short) 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09291() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09291");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 1, 0);
        java.lang.String str7 = nextDate3.run(0, 0, 0);
        java.lang.String str11 = nextDate3.run((int) ' ', (int) 'a', (int) 'a');
        java.lang.String str15 = nextDate3.run((-1), (int) ' ', (int) (byte) -1);
        java.lang.String str19 = nextDate3.run((int) ' ', (int) 'a', (int) '4');
        java.lang.String str23 = nextDate3.run((int) (byte) 10, (int) '4', (int) (short) 10);
        java.lang.String str27 = nextDate3.run((int) '#', 100, (int) (short) 0);
        java.lang.String str31 = nextDate3.run(10, (int) (short) 10, (int) '#');
        java.lang.String str35 = nextDate3.run(10, 10, (int) (short) 10);
        java.lang.String str39 = nextDate3.run((-1), (int) (byte) 0, 100);
        java.lang.String str43 = nextDate3.run((int) (short) -1, 100, (int) (short) 1);
        java.lang.String str47 = nextDate3.run((int) (byte) 1, (int) '#', (-1));
        java.lang.String str51 = nextDate3.run((int) (byte) 10, (-1), (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str43 + "' != '" + "invalid Input Date" + "'", str43, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str47 + "' != '" + "invalid Input Date" + "'", str47, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str51 + "' != '" + "invalid Input Date" + "'", str51, "invalid Input Date");
    }

    @Test
    public void test09292() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09292");
        NextDate nextDate3 = new NextDate((int) (short) 1, 1, (int) (byte) -1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09293() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09293");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 0, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (byte) -1, (-1));
        java.lang.String str11 = nextDate3.run((int) ' ', 100, (int) 'a');
        java.lang.String str15 = nextDate3.run(0, (int) (short) 1, (int) (short) 1);
        java.lang.String str19 = nextDate3.run((int) (short) 0, (int) '#', (int) (short) 10);
        java.lang.String str23 = nextDate3.run((int) (short) -1, 10, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test09294() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09294");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 1, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (short) 10, 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09295() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09295");
        NextDate nextDate3 = new NextDate((int) (short) 100, (-1), (int) (short) 10);
    }

    @Test
    public void test09296() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09296");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (short) 10, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) (byte) 10, (int) (short) 100);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09297() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09297");
        NextDate nextDate3 = new NextDate(100, 10, (int) 'a');
        java.lang.String str7 = nextDate3.run(0, (int) (short) 1, (-1));
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09298() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09298");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 100, (int) (short) 100);
    }

    @Test
    public void test09299() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09299");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) -1, 1);
        java.lang.String str7 = nextDate3.run(10, (int) (byte) 1, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09300() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09300");
        NextDate nextDate3 = new NextDate((-1), 1, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run(1, (-1), (int) '#');
        java.lang.String str11 = nextDate3.run((int) ' ', (int) '4', (-1));
        java.lang.String str15 = nextDate3.run(1, 0, (int) '4');
        java.lang.String str19 = nextDate3.run(0, (int) '4', (int) ' ');
        java.lang.String str23 = nextDate3.run((int) '4', (int) (byte) 10, (int) ' ');
        java.lang.String str27 = nextDate3.run(1, (-1), 1);
        java.lang.String str31 = nextDate3.run(0, (int) '#', 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
    }

    @Test
    public void test09301() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09301");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 100, 0);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) (short) 0, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((int) (short) 10, (int) '#', (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09302() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09302");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 1, 0);
        java.lang.String str7 = nextDate3.run(0, 0, 0);
        java.lang.String str11 = nextDate3.run((int) ' ', (int) 'a', (int) 'a');
        java.lang.String str15 = nextDate3.run(0, (int) (byte) 10, 0);
        java.lang.String str19 = nextDate3.run((int) '#', (int) (byte) 1, (int) (byte) -1);
        java.lang.String str23 = nextDate3.run((int) (byte) 1, (int) '#', (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test09303() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09303");
        NextDate nextDate3 = new NextDate(0, (int) (short) -1, 10);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (-1), 0);
        java.lang.String str11 = nextDate3.run(1, 10, (int) (byte) 10);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09304() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09304");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) 0, 0);
        java.lang.String str7 = nextDate3.run(1, (int) 'a', (int) (short) 1);
        java.lang.String str11 = nextDate3.run((int) (byte) -1, (int) (byte) 0, (int) (byte) 10);
        java.lang.String str15 = nextDate3.run((int) (byte) 0, (int) (byte) 1, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09305() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09305");
        NextDate nextDate3 = new NextDate((int) (short) 0, (-1), (int) (short) 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09306() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09306");
        NextDate nextDate3 = new NextDate(10, (int) ' ', (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, 100, 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09307() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09307");
        NextDate nextDate3 = new NextDate((int) '#', 1, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) '#', (int) (byte) 0, (int) ' ');
        java.lang.String str11 = nextDate3.run((int) (short) 1, (int) (byte) 1, (-1));
        java.lang.String str15 = nextDate3.run(0, (int) '#', (-1));
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09308() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09308");
        NextDate nextDate3 = new NextDate(0, (int) 'a', 0);
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) (short) 1, 100);
        java.lang.String str11 = nextDate3.run(10, (int) '4', (-1));
        java.lang.String str15 = nextDate3.run(0, (int) (byte) 100, (int) '4');
        java.lang.String str19 = nextDate3.run((int) ' ', 0, (int) 'a');
        java.lang.Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test09309() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09309");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) -1, 0);
        java.lang.String str7 = nextDate3.run(100, (int) (byte) 100, (int) '#');
        java.lang.String str11 = nextDate3.run((int) (byte) -1, 1, (int) (byte) -1);
        java.lang.String str15 = nextDate3.run((int) (byte) 10, (int) (short) 10, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09310() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09310");
        NextDate nextDate3 = new NextDate((-1), (int) '#', (int) (short) 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09311() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09311");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 1, 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, (int) '4', (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09312() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09312");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 100, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (short) 100, 10, (int) (short) 0);
        java.lang.String str11 = nextDate3.run((int) (short) 100, 0, (int) (short) 0);
        java.lang.String str15 = nextDate3.run((int) (short) 0, (int) 'a', (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09313() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09313");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 10, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) (byte) 10, (int) '#');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09314() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09314");
        NextDate nextDate3 = new NextDate(1, (-1), 100);
        java.lang.String str7 = nextDate3.run(1, (-1), 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09315() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09315");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 10, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (byte) 0, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run((int) ' ', (int) ' ', (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09316() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09316");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) -1, 100);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, 10, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09317() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09317");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) 100, 0);
        java.lang.String str7 = nextDate3.run((int) 'a', 1, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09318() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09318");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (-1), (int) (byte) 10);
    }

    @Test
    public void test09319() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09319");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) -1, (int) ' ');
        java.lang.String str7 = nextDate3.run(0, (int) (byte) 10, 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09320() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09320");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 100, (int) (short) 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09321() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09321");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 10, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) '#', 0, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09322() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09322");
        NextDate nextDate3 = new NextDate((int) (short) 0, 1, 0);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (short) -1, (int) (short) 100);
        java.lang.String str11 = nextDate3.run((int) (byte) 10, (int) ' ', (-1));
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09323() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09323");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) 10, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) ' ', (int) '#');
        java.lang.String str11 = nextDate3.run(100, 10, (-1));
        java.lang.String str15 = nextDate3.run((int) (short) 10, (int) (short) 10, (int) (short) 100);
        java.lang.String str19 = nextDate3.run(100, (int) (byte) 100, (int) (byte) 10);
        java.lang.String str23 = nextDate3.run((int) (byte) 10, 100, (-1));
        java.lang.String str27 = nextDate3.run(10, (int) (byte) 0, (int) (short) 0);
        java.lang.Class<?> wildcardClass28 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test09324() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09324");
        NextDate nextDate3 = new NextDate((int) '#', (int) '#', (int) (short) 1);
    }

    @Test
    public void test09325() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09325");
        NextDate nextDate3 = new NextDate((int) 'a', (int) '#', 0);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (byte) -1, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09326() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09326");
        NextDate nextDate3 = new NextDate((int) ' ', (-1), (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (byte) 10, (int) (byte) 1);
        java.lang.String str11 = nextDate3.run(0, (int) (short) 1, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09327() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09327");
        NextDate nextDate3 = new NextDate((-1), 1, 10);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) ' ', (int) (short) -1);
        java.lang.String str11 = nextDate3.run((int) (short) 10, (int) (short) 1, (int) (byte) 1);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09328() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09328");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) -1, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run(0, 1, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09329() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09329");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 100, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run(10, (int) (short) 1, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09330() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09330");
        NextDate nextDate3 = new NextDate(0, 100, (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (byte) -1, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run((int) (short) 0, (int) '#', (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09331() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09331");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 10, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) ' ', 0, (int) 'a');
        java.lang.String str11 = nextDate3.run(0, 100, (int) (byte) 1);
        java.lang.String str15 = nextDate3.run((int) 'a', (int) 'a', (int) (short) 100);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09332() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09332");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 10, (-1));
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) '4', (int) (byte) 100);
        java.lang.String str11 = nextDate3.run(100, (int) (short) -1, (int) (short) 0);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09333() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09333");
        NextDate nextDate3 = new NextDate(1, (-1), (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) ' ', (int) ' ');
        java.lang.String str11 = nextDate3.run(100, (int) (byte) -1, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09334() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09334");
        NextDate nextDate3 = new NextDate(100, (int) 'a', (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (short) 100, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09335() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09335");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 10, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) ' ', 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09336() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09336");
        NextDate nextDate3 = new NextDate((int) (short) 0, (-1), (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, 1, (int) (short) 10);
        java.lang.String str11 = nextDate3.run((int) (byte) 10, (int) (byte) 100, (int) (byte) 1);
        java.lang.String str15 = nextDate3.run(0, (-1), (int) (byte) 0);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09337() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09337");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) -1, (int) '4');
    }

    @Test
    public void test09338() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09338");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 1, 100);
        java.lang.String str7 = nextDate3.run(100, (int) (short) -1, (int) (short) 100);
        java.lang.String str11 = nextDate3.run(10, (int) (byte) -1, (int) '#');
        java.lang.String str15 = nextDate3.run(100, (int) (short) -1, (int) 'a');
        java.lang.String str19 = nextDate3.run(100, 0, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test09339() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09339");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 0, (int) '4');
    }

    @Test
    public void test09340() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09340");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) '#', (int) '#');
        java.lang.String str7 = nextDate3.run((int) ' ', (int) '#', (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09341() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09341");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) '#', (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, 0, 1);
        java.lang.String str11 = nextDate3.run((int) (byte) -1, (int) (byte) 1, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09342() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09342");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 100, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) ' ', 100, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run((int) ' ', (int) (byte) 1, (int) '4');
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09343() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09343");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) '4', (int) (short) 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09344() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09344");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) '#', (int) (byte) 1);
    }

    @Test
    public void test09345() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09345");
        NextDate nextDate3 = new NextDate(0, (int) (short) 100, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (byte) -1, (int) (short) 10);
        java.lang.String str11 = nextDate3.run((int) (byte) 100, (int) 'a', (int) (byte) 1);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09346() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09346");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 1, (int) '#');
        java.lang.String str7 = nextDate3.run((int) (short) -1, 10, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09347() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09347");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 1, 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (byte) 100, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09348() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09348");
        NextDate nextDate3 = new NextDate(10, 0, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) '#', 0, (int) '#');
        java.lang.String str11 = nextDate3.run((int) '4', (int) '#', (int) '4');
        java.lang.String str15 = nextDate3.run((int) (short) 0, (int) (short) 10, (int) (short) 0);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09349() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09349");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 0, 100);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) (short) 100, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09350() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09350");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) 1, 10);
    }

    @Test
    public void test09351() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09351");
        NextDate nextDate3 = new NextDate(100, (int) '4', (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (byte) 0, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09352() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09352");
        NextDate nextDate3 = new NextDate(0, (int) (byte) -1, (int) '#');
        java.lang.String str7 = nextDate3.run((int) (short) 10, 1, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09353() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09353");
        NextDate nextDate3 = new NextDate((int) ' ', (int) 'a', (int) (short) 1);
    }

    @Test
    public void test09354() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09354");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 10, (int) (short) 10);
        java.lang.String str7 = nextDate3.run(100, (int) '#', (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09355() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09355");
        NextDate nextDate3 = new NextDate((int) '4', 100, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run(0, (int) '4', (int) (short) -1);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, 1, 0);
        java.lang.String str15 = nextDate3.run((int) (byte) 1, (int) (short) 10, 0);
        java.lang.String str19 = nextDate3.run(0, 1, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test09356() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09356");
        NextDate nextDate3 = new NextDate((int) '4', (-1), (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) '4', (-1), 100);
        java.lang.String str11 = nextDate3.run((int) (short) 100, 10, (int) (byte) 1);
        java.lang.String str15 = nextDate3.run((int) (short) 100, (int) (byte) 1, (int) (short) 100);
        java.lang.String str19 = nextDate3.run(0, (int) (short) 100, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test09357() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09357");
        NextDate nextDate3 = new NextDate(0, (int) ' ', (int) (short) 100);
        java.lang.String str7 = nextDate3.run(0, 0, (int) '#');
        java.lang.String str11 = nextDate3.run((int) (byte) 1, (-1), (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09358() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09358");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) '4', (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (-1), (int) '4');
        java.lang.String str11 = nextDate3.run(100, (int) (short) 0, (int) (byte) 0);
        java.lang.String str15 = nextDate3.run((int) (byte) 1, (int) '4', (int) (short) 10);
        java.lang.String str19 = nextDate3.run((int) '#', 100, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test09359() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09359");
        NextDate nextDate3 = new NextDate((-1), 100, (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) '#', (int) (byte) -1, 0);
        java.lang.String str11 = nextDate3.run(10, 10, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09360() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09360");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) 'a', 10);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09361() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09361");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 0, (int) (short) -1);
        java.lang.String str7 = nextDate3.run(1, (int) (short) 0, (int) (byte) 1);
        java.lang.String str11 = nextDate3.run((int) (byte) -1, (int) (byte) 100, (int) '#');
        java.lang.String str15 = nextDate3.run((int) (short) 10, (int) '4', (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09362() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09362");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) 'a', (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) 'a', (int) (byte) 1);
        java.lang.String str11 = nextDate3.run((-1), (int) '#', (int) (byte) 100);
        java.lang.String str15 = nextDate3.run((int) '4', (int) (byte) 100, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09363() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09363");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 0, (-1));
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09364() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09364");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (short) 0, 10);
    }

    @Test
    public void test09365() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09365");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 1, 1);
    }

    @Test
    public void test09366() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09366");
        NextDate nextDate3 = new NextDate(1, (int) (short) 100, 10);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (byte) 100, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09367() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09367");
        NextDate nextDate3 = new NextDate((-1), 0, (int) '4');
        java.lang.String str7 = nextDate3.run(0, (int) '#', 0);
        java.lang.String str11 = nextDate3.run((int) ' ', (int) (short) -1, (int) (short) -1);
        java.lang.String str15 = nextDate3.run((int) (short) -1, (int) (short) 10, 100);
        java.lang.String str19 = nextDate3.run((int) (byte) 0, 0, 10);
        java.lang.String str23 = nextDate3.run((int) (short) 0, (int) (byte) 100, (int) (byte) 10);
        java.lang.Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test09368() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09368");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) 10, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, 0, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09369() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09369");
        NextDate nextDate3 = new NextDate((int) (short) 100, (-1), 0);
    }

    @Test
    public void test09370() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09370");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) -1, (-1));
        java.lang.String str7 = nextDate3.run((int) (byte) 0, 0, (int) (short) 100);
        java.lang.String str11 = nextDate3.run((int) (byte) 10, (int) (byte) 1, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09371() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09371");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 100, 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, 0, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09372() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09372");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 10, 0);
        java.lang.String str7 = nextDate3.run((int) (short) 10, 100, (int) ' ');
        java.lang.String str11 = nextDate3.run(100, (int) '#', (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09373() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09373");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) -1, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) '#', (-1), (int) '#');
        java.lang.String str11 = nextDate3.run((int) (short) -1, (int) '4', (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09374() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09374");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (short) 1, 1);
    }

    @Test
    public void test09375() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09375");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) 0, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (byte) 0, (int) (byte) 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09376() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09376");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 100, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) '4', (int) ' ', 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09377() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09377");
        NextDate nextDate3 = new NextDate((int) '4', (int) ' ', (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((-1), (int) (byte) 100, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((int) ' ', 0, (-1));
        java.lang.String str15 = nextDate3.run((int) (short) 100, 0, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09378() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09378");
        NextDate nextDate3 = new NextDate(1, (int) (short) 1, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (-1), (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09379() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09379");
        NextDate nextDate3 = new NextDate((int) '4', (int) ' ', (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((-1), (int) (byte) 100, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run(0, (int) (byte) 100, (int) (byte) 10);
        java.lang.String str15 = nextDate3.run((int) '4', (int) (short) 10, (-1));
        java.lang.String str19 = nextDate3.run(100, (int) '4', (int) '#');
        java.lang.String str23 = nextDate3.run(100, (int) (byte) 100, (int) (short) 0);
        java.lang.Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test09380() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09380");
        NextDate nextDate3 = new NextDate((int) 'a', 10, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (byte) -1, (-1));
        java.lang.String str11 = nextDate3.run((int) (short) 0, (int) (short) 0, (int) 'a');
        java.lang.String str15 = nextDate3.run((int) (short) -1, (int) (short) 1, (int) (short) 100);
        java.lang.String str19 = nextDate3.run((int) (byte) -1, (int) (byte) 1, (int) (byte) 100);
        java.lang.Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test09381() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09381");
        NextDate nextDate3 = new NextDate((int) (short) 0, 10, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) '#', (int) (short) 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09382() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09382");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 10, 0);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) (short) 0, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09383() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09383");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 100, (int) '4');
        java.lang.String str7 = nextDate3.run((int) (short) -1, (-1), (int) (short) 1);
        java.lang.String str11 = nextDate3.run((int) 'a', 0, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09384() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09384");
        NextDate nextDate3 = new NextDate((int) (short) -1, 100, 0);
        java.lang.String str7 = nextDate3.run(0, (int) '4', (int) (byte) 10);
        java.lang.String str11 = nextDate3.run((int) (short) 1, 0, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09385() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09385");
        NextDate nextDate3 = new NextDate((int) '#', (int) ' ', 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, 0, 1);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, (int) '4', (-1));
        java.lang.String str15 = nextDate3.run((int) (byte) 1, 100, (int) '#');
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09386() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09386");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) -1, 0);
        java.lang.String str7 = nextDate3.run(100, (int) (byte) 100, (int) '#');
        java.lang.String str11 = nextDate3.run((int) (byte) -1, 1, (int) (byte) -1);
        java.lang.String str15 = nextDate3.run((int) (short) 10, (int) (short) 1, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09387() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09387");
        NextDate nextDate3 = new NextDate((int) ' ', 10, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (short) 1, 1, (int) (short) 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09388() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09388");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 100, 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) '#', 0);
        java.lang.String str11 = nextDate3.run(0, (int) (byte) 100, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09389() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09389");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) '4', 100);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) '#', (int) (short) -1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09390() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09390");
        NextDate nextDate3 = new NextDate((int) 'a', (int) 'a', (int) (byte) 100);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (byte) 10, (int) (short) 100);
        java.lang.String str11 = nextDate3.run((int) (short) 1, (int) (byte) 10, (int) (short) -1);
        java.lang.String str15 = nextDate3.run((int) (short) 10, (int) (short) 100, (int) (short) 100);
        java.lang.String str19 = nextDate3.run(1, (int) (byte) 10, (int) (short) 1);
        java.lang.String str23 = nextDate3.run(0, 10, (int) 'a');
        java.lang.Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test09391() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09391");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) '4', (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (short) 10, 0);
        java.lang.String str11 = nextDate3.run((-1), 0, 100);
        java.lang.String str15 = nextDate3.run(0, 10, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09392() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09392");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) 10, (int) '4');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09393() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09393");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) 10, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) ' ', 100);
        java.lang.String str11 = nextDate3.run(0, (int) (short) 1, (int) (byte) 10);
        java.lang.String str15 = nextDate3.run((int) (short) 100, (int) (short) 0, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09394() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09394");
        NextDate nextDate3 = new NextDate(0, (int) (short) -1, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (short) 1, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 0, 100, (int) (byte) 100);
        java.lang.String str15 = nextDate3.run((int) ' ', (int) ' ', (int) 'a');
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09395() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09395");
        NextDate nextDate3 = new NextDate(100, 10, (int) (byte) 0);
    }

    @Test
    public void test09396() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09396");
        NextDate nextDate3 = new NextDate(0, (int) ' ', (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) '#', (int) (byte) 0, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run((int) (short) -1, (int) (short) 10, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09397() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09397");
        NextDate nextDate3 = new NextDate(100, (int) (short) -1, (int) (short) -1);
        java.lang.String str7 = nextDate3.run(100, (int) (short) 100, 0);
        java.lang.String str11 = nextDate3.run(10, (int) (short) 10, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09398() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09398");
        NextDate nextDate3 = new NextDate((-1), 1, (-1));
        java.lang.String str7 = nextDate3.run((int) (short) 1, (-1), (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09399() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09399");
        NextDate nextDate3 = new NextDate((int) '4', (-1), 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (short) 10, (int) '4');
        java.lang.String str11 = nextDate3.run((int) (short) 0, 100, (int) '#');
        java.lang.String str15 = nextDate3.run((int) (short) 1, 10, (int) (byte) -1);
        java.lang.String str19 = nextDate3.run((int) '4', (int) (byte) 100, 0);
        java.lang.String str23 = nextDate3.run((int) '4', (int) (byte) -1, (-1));
        java.lang.Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test09400() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09400");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 0, (int) (short) 100);
    }

    @Test
    public void test09401() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09401");
        NextDate nextDate3 = new NextDate((int) '#', 1, 10);
        java.lang.String str7 = nextDate3.run((int) '4', (int) 'a', 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, 10, 10);
        java.lang.String str15 = nextDate3.run(1, (int) (byte) 10, (int) ' ');
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09402() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09402");
        NextDate nextDate3 = new NextDate(10, (int) '#', (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (short) -1, (-1));
        java.lang.String str11 = nextDate3.run((int) (short) -1, 1, (int) (byte) 10);
        java.lang.String str15 = nextDate3.run((int) (byte) 1, (int) (byte) 0, (int) (byte) -1);
        java.lang.String str19 = nextDate3.run((int) (byte) 0, (int) (byte) 0, (int) '4');
        java.lang.String str23 = nextDate3.run((int) (short) -1, (int) (short) 0, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test09403() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09403");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 10, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run(10, 0, (int) (byte) 100);
        java.lang.String str11 = nextDate3.run(100, (int) (short) 1, 0);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09404() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09404");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 1, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (-1), (int) (byte) 1);
        java.lang.String str11 = nextDate3.run((int) '4', (int) (short) 10, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09405() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09405");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) 'a', (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) '#', 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 0, 1, (int) ' ');
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09406() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09406");
        NextDate nextDate3 = new NextDate((-1), 0, (int) '4');
        java.lang.String str7 = nextDate3.run(0, (int) '#', 0);
        java.lang.String str11 = nextDate3.run((int) ' ', (int) (short) -1, (int) (short) -1);
        java.lang.String str15 = nextDate3.run((int) (short) -1, (int) (short) 10, 100);
        java.lang.String str19 = nextDate3.run((-1), (int) (byte) 1, 0);
        java.lang.String str23 = nextDate3.run((int) (short) 0, (int) ' ', (int) '4');
        java.lang.String str27 = nextDate3.run((int) (short) 100, (int) (byte) -1, 0);
        java.lang.String str31 = nextDate3.run(0, (int) '#', (int) (byte) 1);
        java.lang.String str35 = nextDate3.run((int) (short) -1, 0, (int) (byte) 0);
        java.lang.String str39 = nextDate3.run((int) (byte) -1, (int) (byte) 1, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
    }

    @Test
    public void test09407() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09407");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 100, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (byte) 10, 100);
        java.lang.String str11 = nextDate3.run((int) 'a', (int) (short) -1, (int) (short) -1);
        java.lang.String str15 = nextDate3.run((int) 'a', 0, (int) (short) -1);
        java.lang.String str19 = nextDate3.run((int) (byte) 100, 0, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test09408() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09408");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 0, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, (int) (byte) 1, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09409() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09409");
        NextDate nextDate3 = new NextDate(100, (-1), (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) (short) 100, 1, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09410() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09410");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) -1, (-1));
        java.lang.String str7 = nextDate3.run(0, 1, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09411() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09411");
        NextDate nextDate3 = new NextDate(0, (int) ' ', 0);
        java.lang.String str7 = nextDate3.run(100, (int) (byte) 1, 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 0, (int) '#', (int) '#');
        java.lang.String str15 = nextDate3.run((int) (byte) 1, (int) 'a', (int) 'a');
        java.lang.String str19 = nextDate3.run((int) (byte) 10, (int) '#', (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test09412() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09412");
        NextDate nextDate3 = new NextDate((int) '4', 1, (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) (short) -1, 1, (int) 'a');
        java.lang.String str11 = nextDate3.run((int) (byte) 100, (int) 'a', (int) (short) 1);
        java.lang.String str15 = nextDate3.run((int) '#', (int) (byte) 100, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09413() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09413");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 1, 1);
        java.lang.String str7 = nextDate3.run(0, (int) (short) 0, (int) (short) 1);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, (int) '4', (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09414() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09414");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 100, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (byte) -1, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09415() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09415");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) 1, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (byte) 10, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run((int) (short) -1, (int) '4', (int) (byte) 1);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09416() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09416");
        NextDate nextDate3 = new NextDate((int) '4', (int) (byte) 100, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) ' ', 100);
        java.lang.String str11 = nextDate3.run((int) 'a', (int) (short) 100, 0);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09417() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09417");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 0, 0);
        java.lang.String str7 = nextDate3.run(10, 1, (int) ' ');
        java.lang.String str11 = nextDate3.run((int) (short) 0, (int) (byte) 10, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09418() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09418");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) 100, 10);
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) (short) -1, (-1));
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09419() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09419");
        NextDate nextDate3 = new NextDate((int) (short) -1, 10, 0);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (short) 10, (int) (short) 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09420() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09420");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 100, 100);
        java.lang.String str7 = nextDate3.run(0, (int) (byte) 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09421() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09421");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 10, (int) (byte) -1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09422() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09422");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) '4', (-1));
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) (byte) 10, 10);
        java.lang.String str11 = nextDate3.run((int) '#', (int) (short) -1, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09423() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09423");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) 100, (int) 'a');
        java.lang.String str7 = nextDate3.run(0, (int) '4', (int) (byte) 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09424() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09424");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) ' ', (int) (short) -1);
    }

    @Test
    public void test09425() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09425");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) '4', (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (short) 0, (int) 'a', (int) (short) -1);
        java.lang.String str11 = nextDate3.run((int) 'a', 10, 1);
        java.lang.String str15 = nextDate3.run((int) (short) -1, (int) (short) 100, (int) '4');
        java.lang.String str19 = nextDate3.run(0, (int) (short) -1, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test09426() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09426");
        NextDate nextDate3 = new NextDate((int) ' ', 0, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (byte) 10, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((int) (short) 10, 0, 0);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09427() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09427");
        NextDate nextDate3 = new NextDate((int) (short) 1, 10, 1);
        java.lang.String str7 = nextDate3.run((int) (short) -1, (-1), (int) (short) 0);
        java.lang.String str11 = nextDate3.run(1, (int) (byte) 0, (int) '#');
        java.lang.String str15 = nextDate3.run((int) (byte) 1, (int) (byte) -1, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09428() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09428");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (short) 10, (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (byte) 10, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09429() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09429");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) '#', (int) (short) -1);
        java.lang.String str7 = nextDate3.run(100, (int) (short) 100, (int) (byte) 100);
        java.lang.String str11 = nextDate3.run((int) (short) 10, (int) '4', (int) 'a');
        java.lang.String str15 = nextDate3.run((int) (short) 1, 100, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09430() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09430");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 0, (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (short) 0, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09431() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09431");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (short) 100, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (short) 100, 1);
        java.lang.String str11 = nextDate3.run((-1), (int) (byte) 100, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09432() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09432");
        NextDate nextDate3 = new NextDate((int) 'a', (int) 'a', 0);
        java.lang.String str7 = nextDate3.run(1, 0, 1);
        java.lang.String str11 = nextDate3.run((-1), (int) (byte) 0, (int) (short) 1);
        java.lang.String str15 = nextDate3.run((int) (byte) 100, (int) (byte) 10, 1);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09433() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09433");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) -1, 0);
        java.lang.String str7 = nextDate3.run(0, (int) (short) 0, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09434() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09434");
        NextDate nextDate3 = new NextDate(10, (-1), (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (short) 100, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, (int) (byte) 0, (int) (short) 1);
        java.lang.String str15 = nextDate3.run((int) (byte) 10, (int) '#', (int) (byte) 0);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09435() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09435");
        NextDate nextDate3 = new NextDate((-1), 10, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run(0, (int) (byte) 1, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09436() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09436");
        NextDate nextDate3 = new NextDate((-1), (-1), 1);
    }

    @Test
    public void test09437() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09437");
        NextDate nextDate3 = new NextDate(100, 0, (int) '4');
        java.lang.String str7 = nextDate3.run((-1), 100, (int) '#');
        java.lang.String str11 = nextDate3.run((int) ' ', 0, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09438() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09438");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) 10, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) (short) 100, 100, 1);
        java.lang.String str11 = nextDate3.run(0, (int) ' ', (int) 'a');
        java.lang.String str15 = nextDate3.run((int) 'a', (-1), (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09439() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09439");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) ' ', 10);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09440() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09440");
        NextDate nextDate3 = new NextDate(0, 10, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) '#', 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09441() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09441");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 100, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (byte) -1, 100);
        java.lang.String str11 = nextDate3.run((int) '4', 10, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09442() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09442");
        NextDate nextDate3 = new NextDate((-1), 0, (int) '#');
        java.lang.String str7 = nextDate3.run(100, (int) ' ', (int) (short) -1);
        java.lang.String str11 = nextDate3.run((int) (short) 10, (int) '4', (int) (short) 1);
        java.lang.String str15 = nextDate3.run((int) (byte) 100, 10, 100);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09443() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09443");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 1, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (byte) 10, (int) 'a');
        java.lang.String str11 = nextDate3.run((int) (short) 0, 0, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09444() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09444");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 1, (int) ' ');
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) '4', (int) (short) 1);
        java.lang.String str11 = nextDate3.run(1, 0, (int) '#');
        java.lang.String str15 = nextDate3.run((int) (short) 10, (int) (byte) -1, (int) (short) 1);
        java.lang.String str19 = nextDate3.run((int) '#', (int) (short) -1, (int) (byte) 1);
        java.lang.Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test09445() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09445");
        NextDate nextDate3 = new NextDate(1, (int) '4', (int) 'a');
        java.lang.String str7 = nextDate3.run((int) '4', 1, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09446() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09446");
        NextDate nextDate3 = new NextDate((int) '4', 0, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) 'a', (int) 'a');
        java.lang.String str11 = nextDate3.run((int) (short) 1, (int) (byte) -1, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09447() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09447");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) 10, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) (short) 100, 100, 1);
        java.lang.String str11 = nextDate3.run(0, (int) ' ', (int) 'a');
        java.lang.String str15 = nextDate3.run(0, (int) (short) -1, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09448() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09448");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) ' ', (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (byte) 1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09449() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09449");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) ' ', (int) '4');
        java.lang.String str7 = nextDate3.run(10, (int) ' ', 100);
        java.lang.String str11 = nextDate3.run(0, (int) (short) 100, (int) (byte) 1);
        java.lang.String str15 = nextDate3.run((int) (short) 100, (int) (short) -1, (int) (byte) 100);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09450() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09450");
        NextDate nextDate3 = new NextDate(10, 0, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) '#', 0, (int) '#');
        java.lang.String str11 = nextDate3.run((-1), (int) (short) 100, (int) '4');
        java.lang.String str15 = nextDate3.run((int) (short) 0, (int) 'a', (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09451() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09451");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 1, 0);
        java.lang.String str7 = nextDate3.run(0, 0, 0);
        java.lang.String str11 = nextDate3.run((int) ' ', (int) 'a', (int) 'a');
        java.lang.String str15 = nextDate3.run((-1), (int) ' ', (int) (byte) -1);
        java.lang.String str19 = nextDate3.run((int) ' ', (int) 'a', (int) '4');
        java.lang.String str23 = nextDate3.run((int) '4', (int) (short) 1, (int) ' ');
        java.lang.String str27 = nextDate3.run((int) (short) 100, (int) (byte) 0, 10);
        java.lang.String str31 = nextDate3.run((int) (byte) 10, 0, (-1));
        java.lang.Class<?> wildcardClass32 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass32);
    }

    @Test
    public void test09452() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09452");
        NextDate nextDate3 = new NextDate((int) (short) -1, 0, 0);
        java.lang.String str7 = nextDate3.run(1, 10, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09453() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09453");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) '#', 100);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (short) 0, 10);
        java.lang.String str11 = nextDate3.run((int) '4', (-1), (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09454() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09454");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 0, (int) '4');
        java.lang.String str7 = nextDate3.run((int) (short) 0, (int) (short) 10, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09455() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09455");
        NextDate nextDate3 = new NextDate(1, (int) (byte) 10, (int) 'a');
        java.lang.String str7 = nextDate3.run(1, (int) (short) 10, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09456() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09456");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 1, (int) (short) 0);
        java.lang.String str7 = nextDate3.run(100, (int) (byte) -1, 0);
        java.lang.String str11 = nextDate3.run((int) 'a', 10, (int) (short) 10);
        java.lang.String str15 = nextDate3.run((int) (short) -1, (int) (byte) -1, (int) '4');
        java.lang.String str19 = nextDate3.run((int) (byte) 0, (int) (short) 0, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test09457() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09457");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) 10, (int) (short) 0);
        java.lang.String str7 = nextDate3.run(10, (int) 'a', (int) (short) 10);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, (int) (byte) 1, 10);
        java.lang.String str15 = nextDate3.run((int) (byte) 10, (-1), (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09458() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09458");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 10, (int) (short) 10);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09459() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09459");
        NextDate nextDate3 = new NextDate((-1), (int) '#', (int) (byte) 100);
    }

    @Test
    public void test09460() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09460");
        NextDate nextDate3 = new NextDate(1, (int) (byte) -1, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run(0, (int) (byte) -1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09461() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09461");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) ' ', 10);
        java.lang.String str7 = nextDate3.run(1, 10, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((int) '4', 1, (int) (byte) 100);
        java.lang.String str15 = nextDate3.run((-1), (int) (short) -1, 0);
        java.lang.String str19 = nextDate3.run((int) (byte) 1, (int) ' ', (int) ' ');
        java.lang.String str23 = nextDate3.run((int) (byte) 0, (int) (short) 10, (int) (short) 10);
        java.lang.String str27 = nextDate3.run((int) (short) -1, 1, (int) (byte) 100);
        java.lang.String str31 = nextDate3.run((-1), 100, (int) '4');
        java.lang.Class<?> wildcardClass32 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass32);
    }

    @Test
    public void test09462() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09462");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 1, 0);
        java.lang.String str7 = nextDate3.run(0, 0, 0);
        java.lang.String str11 = nextDate3.run((int) ' ', (int) 'a', (int) 'a');
        java.lang.String str15 = nextDate3.run(0, (int) (byte) 10, 0);
        java.lang.String str19 = nextDate3.run(0, (int) (byte) 0, 100);
        java.lang.String str23 = nextDate3.run(100, (int) (byte) 100, (int) (short) 100);
        java.lang.String str27 = nextDate3.run((int) (short) -1, (int) (short) 10, (int) (short) 0);
        java.lang.Class<?> wildcardClass28 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test09463() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09463");
        NextDate nextDate3 = new NextDate((int) (short) -1, (-1), (int) (byte) 100);
        java.lang.String str7 = nextDate3.run((int) (short) 10, 1, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, (int) '4', (int) 'a');
        java.lang.String str15 = nextDate3.run((int) '4', 1, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test09464() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09464");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) '#', (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (byte) 0, (-1));
        java.lang.String str11 = nextDate3.run((int) (byte) 100, 0, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09465() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09465");
        NextDate nextDate3 = new NextDate(100, (int) '4', 1);
    }

    @Test
    public void test09466() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09466");
        NextDate nextDate3 = new NextDate(10, 1, (int) 'a');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09467() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09467");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 1, (int) '4');
        java.lang.String str7 = nextDate3.run(1, (int) ' ', (-1));
        java.lang.String str11 = nextDate3.run((int) (short) 0, (-1), (int) (byte) 10);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09468() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09468");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) 100, (int) (short) 1);
        java.lang.String str7 = nextDate3.run(1, (int) (byte) 100, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09469() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09469");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 1, 0);
        java.lang.String str7 = nextDate3.run(0, 0, 0);
        java.lang.String str11 = nextDate3.run((int) ' ', (int) 'a', (int) 'a');
        java.lang.String str15 = nextDate3.run((-1), (int) ' ', (int) (byte) -1);
        java.lang.String str19 = nextDate3.run(1, (int) (byte) 100, 0);
        java.lang.String str23 = nextDate3.run((int) '4', (-1), (int) (short) 1);
        java.lang.String str27 = nextDate3.run((int) (short) 100, 1, 1);
        java.lang.String str31 = nextDate3.run((int) (short) 1, 1, (-1));
        java.lang.String str35 = nextDate3.run((int) (byte) -1, (int) (byte) 100, 10);
        java.lang.String str39 = nextDate3.run((int) (byte) 100, (int) (byte) 10, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
    }

    @Test
    public void test09470() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09470");
        NextDate nextDate3 = new NextDate((int) '4', (-1), (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) '4', (-1), 100);
        java.lang.String str11 = nextDate3.run((int) (short) 100, 10, (int) (byte) 1);
        java.lang.String str15 = nextDate3.run((int) (short) 100, (int) (byte) 1, (int) (short) 100);
        java.lang.String str19 = nextDate3.run(1, 0, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test09471() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09471");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) '4', (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (byte) 100, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run(0, (int) (byte) 100, (int) (short) 0);
        java.lang.String str15 = nextDate3.run((int) (byte) 10, 1, (-1));
        java.lang.String str19 = nextDate3.run((int) (short) 1, (int) '#', (int) (short) 100);
        java.lang.Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test09472() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09472");
        NextDate nextDate3 = new NextDate((int) '#', (-1), 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) ' ', (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((int) (byte) 10, 100, (int) (byte) 0);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09473() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09473");
        NextDate nextDate3 = new NextDate((int) 'a', 0, (int) (short) 10);
        java.lang.String str7 = nextDate3.run(10, (int) (byte) 0, (int) (byte) 100);
        java.lang.String str11 = nextDate3.run((int) (short) 100, 100, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09474() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09474");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) ' ', 10);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09475() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09475");
        NextDate nextDate3 = new NextDate((int) (short) 1, 10, (int) ' ');
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (byte) 0, (int) (short) 100);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09476() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09476");
        NextDate nextDate3 = new NextDate((int) '4', (int) '4', 0);
        java.lang.String str7 = nextDate3.run((int) (short) 0, (int) (short) 1, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09477() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09477");
        NextDate nextDate3 = new NextDate((-1), 10, 0);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (byte) -1, (int) 'a');
        java.lang.String str11 = nextDate3.run((int) (short) 100, (int) (short) 1, (-1));
        java.lang.String str15 = nextDate3.run(10, (int) (byte) 1, (int) (short) 100);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09478() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09478");
        NextDate nextDate3 = new NextDate((int) ' ', 1, (int) '4');
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) '4', 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09479() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09479");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 100, (int) '4');
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (short) -1, (int) (short) 100);
        java.lang.String str11 = nextDate3.run(1, 1, (int) ' ');
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09480() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09480");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (short) 1, (int) 'a');
        java.lang.String str7 = nextDate3.run((int) '4', (int) 'a', (int) ' ');
        java.lang.String str11 = nextDate3.run((int) ' ', 1, (int) ' ');
        java.lang.String str15 = nextDate3.run((int) '#', 0, (int) (short) 100);
        java.lang.String str19 = nextDate3.run((int) (short) 0, (int) (byte) 0, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test09481() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09481");
        NextDate nextDate3 = new NextDate((int) ' ', 0, 1);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) '4', (int) (byte) 10);
        java.lang.String str11 = nextDate3.run((int) (short) 0, (int) (byte) 10, (int) (byte) -1);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09482() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09482");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 1, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run(1, (int) (byte) 100, (int) '#');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test09483() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09483");
        NextDate nextDate3 = new NextDate((int) '4', 0, (int) '4');
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (byte) 0, (int) (short) 100);
        java.lang.String str11 = nextDate3.run((int) '#', 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09484() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09484");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) -1, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, 0, (-1));
        java.lang.String str11 = nextDate3.run((int) (byte) 0, 10, 100);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test09485() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09485");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 100, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (byte) 0, 0);
        java.lang.String str11 = nextDate3.run((int) (short) 1, 100, (int) (byte) 10);
        java.lang.String str15 = nextDate3.run(1, (int) (short) 10, (int) (short) 0);
        java.lang.String str19 = nextDate3.run((int) (short) 100, (int) (byte) 0, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test09486() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09486");
        NextDate nextDate3 = new NextDate(100, (int) '4', (-1));
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) (byte) 10, (int) (byte) 100);
        java.lang.String str11 = nextDate3.run(100, (int) (byte) 0, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09487() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09487");
        NextDate nextDate3 = new NextDate((-1), (int) ' ', 0);
        java.lang.String str7 = nextDate3.run(1, (int) (short) 0, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09488() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09488");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 0, (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) (byte) -1, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09489() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09489");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) ' ', 1);
    }

    @Test
    public void test09490() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09490");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) 10, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) '#', (int) (short) 10, (int) (short) 1);
        java.lang.String str11 = nextDate3.run((int) (short) 10, (int) (short) -1, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09491() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09491");
        NextDate nextDate3 = new NextDate((int) '4', (int) (byte) 10, (int) 'a');
        java.lang.String str7 = nextDate3.run((int) (byte) 1, 10, (int) ' ');
        java.lang.String str11 = nextDate3.run((int) (byte) 100, (int) (short) 0, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09492() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09492");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 0, (int) 'a');
    }

    @Test
    public void test09493() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09493");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) -1, (int) 'a');
    }

    @Test
    public void test09494() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09494");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) 100, (int) (short) 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09495() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09495");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 1, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) (short) 0, (int) ' ', (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test09496() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09496");
        NextDate nextDate3 = new NextDate(10, (int) '#', 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test09497() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09497");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (short) 100, 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) ' ', (int) (short) 100);
        java.lang.String str11 = nextDate3.run((int) ' ', (int) '4', (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09498() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09498");
        NextDate nextDate3 = new NextDate((int) '#', 0, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run(10, (int) (short) 0, (int) '#');
        java.lang.String str11 = nextDate3.run((int) '#', (int) (byte) 1, (int) (short) 100);
        java.lang.String str15 = nextDate3.run((int) (byte) 100, (int) (byte) 0, (int) '4');
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test09499() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09499");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) '#', 10);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (short) 10, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run(1, (int) (byte) 0, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test09500() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest18.test09500");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) 'a', (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) (short) 0, (int) (byte) -1, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((int) (short) 0, (int) (byte) 0, 10);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }
}

