import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest13 {

    public static boolean debug = false;

    @Test
    public void test6501() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6501");
        NextDate nextDate3 = new NextDate(1, 12, (int) 'a');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6502() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6502");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) '#', (int) ' ');
        java.lang.String str7 = nextDate3.run((-1), 1, (-1));
        java.lang.String str11 = nextDate3.run((int) (short) 100, (int) '4', 12);
        java.lang.String str15 = nextDate3.run(10, (int) ' ', (int) ' ');
        java.lang.String str19 = nextDate3.run(1, 30, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6503() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6503");
        NextDate nextDate3 = new NextDate(2001, 0, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (byte) -1, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6504() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6504");
        NextDate nextDate3 = new NextDate((int) (short) 1, (-1), (int) (short) 10);
    }

    @Test
    public void test6505() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6505");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (short) -1, (int) ' ');
    }

    @Test
    public void test6506() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6506");
        NextDate nextDate3 = new NextDate((-1), 10, (int) (short) 100);
        java.lang.String str7 = nextDate3.run(1, (int) (short) -1, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6507() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6507");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (short) 0, (int) (short) 1);
    }

    @Test
    public void test6508() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6508");
        NextDate nextDate3 = new NextDate(2001, 12, (-1));
        java.lang.String str7 = nextDate3.run((int) '4', 2020, (int) (byte) -1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6509() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6509");
        NextDate nextDate3 = new NextDate(12, (int) ' ', (int) ' ');
        java.lang.String str7 = nextDate3.run((-1), 7, (int) (short) 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6510() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6510");
        NextDate nextDate3 = new NextDate(30, (int) (byte) 10, 100);
        java.lang.String str7 = nextDate3.run(7, (int) (byte) 10, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6511() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6511");
        NextDate nextDate3 = new NextDate(7, 1, (int) (byte) -1);
    }

    @Test
    public void test6512() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6512");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) 'a', (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) '#', (int) (byte) 0, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6513() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6513");
        NextDate nextDate3 = new NextDate(2001, (int) (short) 0, 2020);
        java.lang.String str7 = nextDate3.run(2020, 30, (int) '#');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6514() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6514");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run(0, 2001, 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        java.lang.String str15 = nextDate3.run((int) (byte) 1, (int) (short) 0, (int) (short) 100);
        java.lang.String str19 = nextDate3.run((int) (short) 10, 0, 10);
        java.lang.String str23 = nextDate3.run((int) (short) 10, 12, 1);
        java.lang.String str27 = nextDate3.run(100, (int) ' ', (int) (byte) 0);
        java.lang.String str31 = nextDate3.run((int) (short) 100, 1, (int) (byte) -1);
        java.lang.String str35 = nextDate3.run((int) (byte) 1, 2020, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
    }

    @Test
    public void test6515() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6515");
        NextDate nextDate3 = new NextDate(0, (-1), (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, (int) (short) 10, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6516() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6516");
        NextDate nextDate3 = new NextDate(30, (int) (byte) 0, (int) '#');
    }

    @Test
    public void test6517() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6517");
        NextDate nextDate3 = new NextDate((-1), (int) (short) -1, (int) (short) 0);
    }

    @Test
    public void test6518() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6518");
        NextDate nextDate3 = new NextDate((-1), (int) '4', (int) (short) -1);
    }

    @Test
    public void test6519() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6519");
        NextDate nextDate3 = new NextDate(12, (int) (short) -1, (int) (byte) 10);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6520() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6520");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 1, (int) '4');
        java.lang.String str7 = nextDate3.run((int) '#', 0, (int) (byte) -1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6521() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6521");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) 'a', (int) (short) -1);
        java.lang.String str7 = nextDate3.run(100, (int) ' ', 1);
        java.lang.String str11 = nextDate3.run(0, (int) '4', (int) (byte) 1);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6522() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6522");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 10, (int) (short) 0);
    }

    @Test
    public void test6523() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6523");
        NextDate nextDate3 = new NextDate((int) '#', (-1), 7);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, 2001, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6524() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6524");
        NextDate nextDate3 = new NextDate(12, (int) (short) 10, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (short) 100, 2020);
        java.lang.String str11 = nextDate3.run((int) (short) 1, 0, (int) (short) 10);
        java.lang.String str15 = nextDate3.run((int) '#', (int) '#', (int) (short) 100);
        java.lang.String str19 = nextDate3.run((-1), (int) (byte) 0, (int) (byte) -1);
        java.lang.String str23 = nextDate3.run((int) (short) 10, 2020, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test6525() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6525");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) 'a', (int) ' ');
    }

    @Test
    public void test6526() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6526");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 10, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((-1), 12, (int) (short) 100);
        java.lang.String str11 = nextDate3.run((int) (byte) 10, (int) (byte) 100, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6527() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6527");
        NextDate nextDate3 = new NextDate(31, (int) (short) -1, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run(31, (int) (byte) 100, 10);
        java.lang.String str11 = nextDate3.run(1, 0, 10);
        java.lang.String str15 = nextDate3.run((int) '4', (int) '#', 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6528() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6528");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) -1, (int) '4');
    }

    @Test
    public void test6529() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6529");
        NextDate nextDate3 = new NextDate((int) (short) 0, 10, 2020);
    }

    @Test
    public void test6530() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6530");
        NextDate nextDate3 = new NextDate(1, 12, (-1));
        java.lang.String str7 = nextDate3.run(1, (int) (byte) 100, (int) (short) 100);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6531() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6531");
        NextDate nextDate3 = new NextDate(30, (-1), 31);
        java.lang.String str7 = nextDate3.run(7, (int) (short) 100, 1);
        java.lang.String str11 = nextDate3.run(12, 2001, (int) (short) 1);
        java.lang.String str15 = nextDate3.run((int) (short) 0, (int) ' ', 2020);
        java.lang.String str19 = nextDate3.run((int) (byte) 100, (int) ' ', 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6532() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6532");
        NextDate nextDate3 = new NextDate(30, (int) (short) -1, 10);
    }

    @Test
    public void test6533() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6533");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 1, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) ' ', (int) (byte) -1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6534() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6534");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) -1, 0);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (short) 1, 10);
        java.lang.String str11 = nextDate3.run((int) ' ', (int) (byte) -1, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6535() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6535");
        NextDate nextDate3 = new NextDate(12, 30, (int) (short) 1);
    }

    @Test
    public void test6536() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6536");
        NextDate nextDate3 = new NextDate(2001, (int) '4', (int) (byte) 1);
    }

    @Test
    public void test6537() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6537");
        NextDate nextDate3 = new NextDate(31, (int) (short) 100, 10);
    }

    @Test
    public void test6538() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6538");
        NextDate nextDate3 = new NextDate(0, (int) (short) 100, (-1));
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6539() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6539");
        NextDate nextDate3 = new NextDate(31, 0, 0);
        java.lang.String str7 = nextDate3.run((int) (short) 0, (-1), (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6540() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6540");
        NextDate nextDate3 = new NextDate(100, 31, 30);
    }

    @Test
    public void test6541() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6541");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 1, (int) (short) 100);
    }

    @Test
    public void test6542() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6542");
        NextDate nextDate3 = new NextDate(2020, 100, (int) (short) 100);
        java.lang.String str7 = nextDate3.run(1, (int) (short) 0, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6543() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6543");
        NextDate nextDate3 = new NextDate(1, 12, (int) '4');
    }

    @Test
    public void test6544() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6544");
        NextDate nextDate3 = new NextDate(12, 1, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) ' ', 7, 7);
        java.lang.String str11 = nextDate3.run((int) '4', (-1), (int) '4');
        java.lang.String str15 = nextDate3.run(0, (int) (byte) 100, (int) (byte) 10);
        java.lang.String str19 = nextDate3.run((int) 'a', (int) (byte) 1, (int) '#');
        java.lang.String str23 = nextDate3.run((int) ' ', 10, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test6545() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6545");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 100, (int) (short) 0);
        java.lang.String str7 = nextDate3.run(12, 2001, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "1/1/2021" + "'", str7, "1/1/2021");
    }

    @Test
    public void test6546() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6546");
        NextDate nextDate3 = new NextDate(2020, 1, 2020);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) (byte) 0, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6547() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6547");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 10, (int) (short) 100);
    }

    @Test
    public void test6548() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6548");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 12, 12);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6549() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6549");
        NextDate nextDate3 = new NextDate(100, 1, (int) (short) 0);
    }

    @Test
    public void test6550() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6550");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) -1, 10);
    }

    @Test
    public void test6551() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6551");
        NextDate nextDate3 = new NextDate((int) '4', 31, 2001);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (short) 10, (int) '4');
        java.lang.String str11 = nextDate3.run((int) (byte) 1, (int) '4', (int) (short) 0);
        java.lang.String str15 = nextDate3.run((int) '4', 10, (int) (byte) 100);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test6552() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6552");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) -1, (int) (byte) -1);
    }

    @Test
    public void test6553() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6553");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 10, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run(30, 2001, (int) '#');
        java.lang.String str11 = nextDate3.run((int) (byte) 1, (int) (byte) -1, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6554() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6554");
        NextDate nextDate3 = new NextDate(1, (int) (byte) 10, 2001);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) 'a', 100);
        java.lang.String str11 = nextDate3.run(12, (int) (byte) -1, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6555() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6555");
        NextDate nextDate3 = new NextDate(100, 7, (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (byte) 0, (int) ' ');
        java.lang.String str11 = nextDate3.run((int) (short) 100, 0, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6556() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6556");
        NextDate nextDate3 = new NextDate(12, (int) (short) -1, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, 100, (int) (short) -1);
        java.lang.String str11 = nextDate3.run((int) (byte) 100, 12, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6557() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6557");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (short) 10, 0);
        java.lang.String str7 = nextDate3.run((int) '#', (int) (short) 10, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run(0, (int) ' ', 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6558() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6558");
        NextDate nextDate3 = new NextDate(0, (int) (short) -1, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run(100, 10, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6559() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6559");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run(0, 2001, 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        java.lang.String str15 = nextDate3.run((int) (byte) 1, (int) (short) 0, (int) (short) 100);
        java.lang.String str19 = nextDate3.run((int) (short) 10, 0, 10);
        java.lang.String str23 = nextDate3.run((int) '#', 100, 100);
        java.lang.String str27 = nextDate3.run((int) (byte) 1, (int) (short) 10, (int) (short) 10);
        java.lang.String str31 = nextDate3.run((int) 'a', 1, 12);
        java.lang.String str35 = nextDate3.run((int) ' ', 30, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
    }

    @Test
    public void test6560() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6560");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 0, (int) '4');
        java.lang.String str7 = nextDate3.run(7, 0, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run(12, 10, (int) (byte) -1);
        java.lang.String str15 = nextDate3.run((int) '4', (int) (short) 100, (int) (byte) 1);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test6561() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6561");
        NextDate nextDate3 = new NextDate(100, 100, (int) (byte) 10);
    }

    @Test
    public void test6562() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6562");
        NextDate nextDate3 = new NextDate(1, 0, 0);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) (short) 0, (int) '4');
        java.lang.String str11 = nextDate3.run((int) (short) 0, (int) (byte) 10, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6563() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6563");
        NextDate nextDate3 = new NextDate(1, (int) (short) -1, 30);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (short) -1, 100);
        java.lang.String str11 = nextDate3.run((int) '#', 12, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6564() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6564");
        NextDate nextDate3 = new NextDate(2001, (int) (short) 10, (int) (short) -1);
        java.lang.String str7 = nextDate3.run(1, 31, 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6565() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6565");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) 'a', (int) (byte) 1);
    }

    @Test
    public void test6566() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6566");
        NextDate nextDate3 = new NextDate(31, 2020, 31);
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) 'a', (-1));
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6567() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6567");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) -1, 0);
        java.lang.String str7 = nextDate3.run((-1), 0, 12);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6568() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6568");
        NextDate nextDate3 = new NextDate((int) (short) -1, 10, 31);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (short) 100, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6569() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6569");
        NextDate nextDate3 = new NextDate(1, (int) (byte) 1, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (byte) 1, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run(10, 30, (int) ' ');
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6570() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6570");
        NextDate nextDate3 = new NextDate((int) ' ', (int) '4', (int) 'a');
        java.lang.String str7 = nextDate3.run(12, (-1), 31);
        java.lang.String str11 = nextDate3.run(30, (int) (short) 1, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6571() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6571");
        NextDate nextDate3 = new NextDate(2020, (int) (short) 0, 0);
    }

    @Test
    public void test6572() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6572");
        NextDate nextDate3 = new NextDate(0, (int) '4', (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) '#', (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6573() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6573");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) -1, (int) ' ');
    }

    @Test
    public void test6574() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6574");
        NextDate nextDate3 = new NextDate(0, (int) (short) -1, (int) '4');
        java.lang.String str7 = nextDate3.run((int) (short) 0, 7, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6575() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6575");
        NextDate nextDate3 = new NextDate((int) (short) -1, 0, 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) 'a', 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6576() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6576");
        NextDate nextDate3 = new NextDate(100, (int) '4', (int) '4');
        java.lang.String str7 = nextDate3.run(0, (-1), (int) ' ');
        java.lang.String str11 = nextDate3.run(10, 10, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6577() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6577");
        NextDate nextDate3 = new NextDate((int) '#', (int) '4', 2001);
    }

    @Test
    public void test6578() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6578");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run((int) 'a', 12, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run(31, (int) (byte) 1, (int) (byte) 10);
        java.lang.String str15 = nextDate3.run((int) (short) 10, (int) (byte) 0, (int) 'a');
        java.lang.String str19 = nextDate3.run((int) (short) 100, (int) (short) -1, (int) (short) 1);
        java.lang.String str23 = nextDate3.run(2001, (int) (byte) -1, (int) (short) 0);
        java.lang.String str27 = nextDate3.run(100, 12, 7);
        java.lang.Class<?> wildcardClass28 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test6579() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6579");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) -1, 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6580() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6580");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 1, 2020);
    }

    @Test
    public void test6581() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6581");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 0, 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6582() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6582");
        NextDate nextDate3 = new NextDate(7, (-1), (int) (byte) 1);
        java.lang.String str7 = nextDate3.run(0, (int) (short) 0, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6583() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6583");
        NextDate nextDate3 = new NextDate(100, (int) '#', 0);
    }

    @Test
    public void test6584() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6584");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run(0, 2001, 0);
        java.lang.String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        java.lang.String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        java.lang.String str19 = nextDate3.run((int) '4', 30, 0);
        java.lang.String str23 = nextDate3.run(7, (int) (short) 10, (int) (short) 1);
        java.lang.String str27 = nextDate3.run(2020, 7, (int) (short) 0);
        java.lang.String str31 = nextDate3.run(31, 2020, (int) ' ');
        java.lang.String str35 = nextDate3.run((int) (short) -1, 0, 7);
        java.lang.String str39 = nextDate3.run((int) (short) 10, (int) (short) 100, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
    }

    @Test
    public void test6585() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6585");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) -1, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, 10, 0);
        java.lang.String str11 = nextDate3.run((int) 'a', (int) (short) 100, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6586() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6586");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) -1, (-1));
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (short) 100, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6587() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6587");
        NextDate nextDate3 = new NextDate(0, 7, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) ' ', 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6588() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6588");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) 10, (int) '4');
        java.lang.String str7 = nextDate3.run(0, (int) (byte) 1, (int) (short) 1);
        java.lang.String str11 = nextDate3.run((int) 'a', (int) '4', 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6589() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6589");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) '#', (int) '#');
    }

    @Test
    public void test6590() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6590");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) 'a', 12);
        java.lang.String str7 = nextDate3.run((int) (short) 1, 31, (int) ' ');
        java.lang.String str11 = nextDate3.run((int) (byte) 0, 100, (int) (byte) -1);
        java.lang.String str15 = nextDate3.run((int) (short) 100, (int) (byte) -1, 31);
        java.lang.String str19 = nextDate3.run((int) (short) 1, (int) 'a', 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6591() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6591");
        NextDate nextDate3 = new NextDate(0, (int) (short) -1, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (short) 100, 0);
        java.lang.String str11 = nextDate3.run((int) (short) 1, 31, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6592() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6592");
        NextDate nextDate3 = new NextDate(2020, 2001, 10);
        java.lang.String str7 = nextDate3.run((int) (short) -1, 7, (int) (short) -1);
        java.lang.String str11 = nextDate3.run((int) ' ', (int) (byte) 100, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6593() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6593");
        NextDate nextDate3 = new NextDate((int) (short) 1, 0, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (byte) 1, 100);
        java.lang.String str11 = nextDate3.run((int) (byte) 0, (int) (short) -1, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6594() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6594");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 0, (int) ' ');
    }

    @Test
    public void test6595() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6595");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 10, 31);
    }

    @Test
    public void test6596() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6596");
        NextDate nextDate3 = new NextDate((int) (short) 10, 12, (int) (byte) -1);
    }

    @Test
    public void test6597() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6597");
        NextDate nextDate3 = new NextDate(30, 0, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (short) -1, (int) (short) -1);
        java.lang.String str11 = nextDate3.run((int) (short) 100, (-1), 0);
        java.lang.String str15 = nextDate3.run(2020, (int) 'a', (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6598() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6598");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 10, 30);
        java.lang.String str7 = nextDate3.run(2001, 100, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6599() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6599");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 0, 2001);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, 2020, (int) (short) 10);
        java.lang.String str11 = nextDate3.run((int) (byte) 100, 2001, 31);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6600() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6600");
        NextDate nextDate3 = new NextDate((int) ' ', (int) '#', (int) (byte) 1);
    }

    @Test
    public void test6601() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6601");
        NextDate nextDate3 = new NextDate((int) (short) 10, 12, 0);
        java.lang.String str7 = nextDate3.run(2020, (int) (byte) 1, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6602() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6602");
        NextDate nextDate3 = new NextDate(100, 30, 0);
    }

    @Test
    public void test6603() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6603");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) 'a', 2001);
    }

    @Test
    public void test6604() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6604");
        NextDate nextDate3 = new NextDate(0, (int) (short) 100, (int) (short) 100);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6605() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6605");
        NextDate nextDate3 = new NextDate((-1), (int) (short) -1, 31);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (byte) 0, 7);
        java.lang.String str11 = nextDate3.run(100, (int) (byte) 1, (int) ' ');
        java.lang.String str15 = nextDate3.run((int) '4', 1, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6606() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6606");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 10, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) '4', 2020, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6607() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6607");
        NextDate nextDate3 = new NextDate((int) ' ', (int) '#', (int) ' ');
        java.lang.String str7 = nextDate3.run(0, (int) (short) -1, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6608() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6608");
        NextDate nextDate3 = new NextDate((-1), (int) (short) -1, 31);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, 30, (int) '4');
        java.lang.String str11 = nextDate3.run(30, 10, (int) (byte) 1);
        java.lang.String str15 = nextDate3.run((int) (short) 100, (int) (byte) 100, (int) (short) 100);
        java.lang.String str19 = nextDate3.run((int) (short) 100, 0, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6609() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6609");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) 1, 12);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (byte) -1, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run((-1), 31, (int) (short) 1);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6610() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6610");
        NextDate nextDate3 = new NextDate(12, (-1), 7);
    }

    @Test
    public void test6611() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6611");
        NextDate nextDate3 = new NextDate((int) (short) -1, 2020, 12);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6612() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6612");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 31, (int) ' ');
    }

    @Test
    public void test6613() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6613");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) 1, 0);
    }

    @Test
    public void test6614() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6614");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 100, (int) '#');
        java.lang.String str7 = nextDate3.run(10, 1, (int) (short) 100);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, 7, (int) (byte) 100);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6615() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6615");
        NextDate nextDate3 = new NextDate(1, 7, 100);
    }

    @Test
    public void test6616() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6616");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 10, 0);
    }

    @Test
    public void test6617() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6617");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 10, (int) '#');
        java.lang.String str7 = nextDate3.run(30, 0, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6618() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6618");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 31, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, 7, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6619() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6619");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run(0, 2001, 0);
        java.lang.String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        java.lang.String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        java.lang.String str19 = nextDate3.run((int) '4', 30, 0);
        java.lang.String str23 = nextDate3.run((int) (byte) 10, (int) (short) 100, 100);
        java.lang.String str27 = nextDate3.run((int) (short) 100, (int) (short) 0, (int) (byte) 0);
        java.lang.String str31 = nextDate3.run(2020, 30, 7);
        java.lang.String str35 = nextDate3.run((int) (short) 10, (int) (byte) -1, 100);
        java.lang.String str39 = nextDate3.run((int) (byte) -1, (int) (byte) 1, (int) (byte) 100);
        java.lang.String str43 = nextDate3.run(31, (int) (byte) -1, (int) (byte) 1);
        java.lang.String str47 = nextDate3.run((int) (short) -1, 2001, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str43 + "' != '" + "invalid Input Date" + "'", str43, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str47 + "' != '" + "invalid Input Date" + "'", str47, "invalid Input Date");
    }

    @Test
    public void test6620() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6620");
        NextDate nextDate3 = new NextDate(0, 1, (int) (byte) -1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6621() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6621");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 10, 1);
    }

    @Test
    public void test6622() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6622");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 0, 30);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6623() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6623");
        NextDate nextDate3 = new NextDate(2020, (int) ' ', (int) ' ');
        java.lang.String str7 = nextDate3.run((int) ' ', 0, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6624() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6624");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) -1, 30);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6625() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6625");
        NextDate nextDate3 = new NextDate(7, (int) (byte) 1, (int) 'a');
    }

    @Test
    public void test6626() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6626");
        NextDate nextDate3 = new NextDate(12, 2001, 31);
    }

    @Test
    public void test6627() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6627");
        NextDate nextDate3 = new NextDate(10, 0, 2001);
        java.lang.String str7 = nextDate3.run(0, 1, 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 100, 10, (int) '4');
        java.lang.String str15 = nextDate3.run((int) '4', (int) (short) 0, (-1));
        java.lang.String str19 = nextDate3.run((int) (byte) 10, 2001, 7);
        java.lang.String str23 = nextDate3.run((int) (byte) 1, (int) (byte) 0, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test6628() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6628");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 31, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run(1, (int) (short) -1, 30);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6629() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6629");
        NextDate nextDate3 = new NextDate((int) '4', (int) ' ', (int) (byte) 1);
    }

    @Test
    public void test6630() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6630");
        NextDate nextDate3 = new NextDate((int) (short) 10, 1, (int) 'a');
        java.lang.String str7 = nextDate3.run(30, (int) (short) 1, (int) (short) 10);
        java.lang.String str11 = nextDate3.run(2020, (int) (byte) -1, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6631() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6631");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) '#', 10);
    }

    @Test
    public void test6632() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6632");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 100, (int) 'a');
    }

    @Test
    public void test6633() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6633");
        NextDate nextDate3 = new NextDate(12, (int) '#', 0);
        java.lang.String str7 = nextDate3.run(100, 30, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6634() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6634");
        NextDate nextDate3 = new NextDate(0, (int) ' ', (int) (byte) 1);
    }

    @Test
    public void test6635() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6635");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 1, (int) (short) 0);
        java.lang.String str7 = nextDate3.run(12, 100, (int) 'a');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6636() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6636");
        NextDate nextDate3 = new NextDate((int) ' ', 2020, (int) '4');
        java.lang.String str7 = nextDate3.run(2001, 30, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6637() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6637");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) 'a', (int) (byte) 100);
    }

    @Test
    public void test6638() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6638");
        NextDate nextDate3 = new NextDate(7, 100, (int) (byte) 100);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6639() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6639");
        NextDate nextDate3 = new NextDate((int) '4', 12, (int) (short) 0);
    }

    @Test
    public void test6640() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6640");
        NextDate nextDate3 = new NextDate((-1), 0, 30);
    }

    @Test
    public void test6641() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6641");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 0, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((-1), (int) (byte) 1, (int) '#');
        java.lang.String str11 = nextDate3.run((int) (byte) 10, 2020, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6642() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6642");
        NextDate nextDate3 = new NextDate(12, (int) (short) 10, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (short) 100, 2020);
        java.lang.String str11 = nextDate3.run((int) (short) 1, 0, (int) (short) 10);
        java.lang.String str15 = nextDate3.run((int) '#', (int) '#', (int) (short) 100);
        java.lang.String str19 = nextDate3.run(12, (int) (byte) 10, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6643() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6643");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 100, (int) (byte) 0);
    }

    @Test
    public void test6644() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6644");
        NextDate nextDate3 = new NextDate(1, (int) (byte) 0, (int) (byte) 1);
    }

    @Test
    public void test6645() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6645");
        NextDate nextDate3 = new NextDate(2001, 100, 2001);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6646() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6646");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 12, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run(10, (int) (short) 10, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6647() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6647");
        NextDate nextDate3 = new NextDate((int) '4', 100, (int) ' ');
    }

    @Test
    public void test6648() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6648");
        NextDate nextDate3 = new NextDate(31, (int) 'a', (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (short) 10, 1, 10);
        java.lang.String str11 = nextDate3.run(0, 100, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6649() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6649");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) 'a', (int) ' ');
        java.lang.String str7 = nextDate3.run(100, (int) (byte) -1, 12);
        java.lang.String str11 = nextDate3.run(30, 12, 0);
        java.lang.String str15 = nextDate3.run((int) (short) 0, 31, (int) '4');
        java.lang.String str19 = nextDate3.run((int) (short) 1, 12, (int) (byte) -1);
        java.lang.String str23 = nextDate3.run(31, 0, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test6650() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6650");
        NextDate nextDate3 = new NextDate(2001, 100, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run(100, 0, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6651() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6651");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run(0, 2001, 0);
        java.lang.String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        java.lang.String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        java.lang.String str19 = nextDate3.run((int) (byte) 0, (int) (byte) 10, 31);
        java.lang.String str23 = nextDate3.run(0, 2001, (int) '#');
        java.lang.String str27 = nextDate3.run(7, (int) (byte) 1, 100);
        java.lang.String str31 = nextDate3.run((int) (short) 1, 7, (int) (short) 10);
        java.lang.Class<?> wildcardClass32 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass32);
    }

    @Test
    public void test6652() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6652");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) 10, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (short) 100, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6653() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6653");
        NextDate nextDate3 = new NextDate(2001, 100, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (byte) -1, (int) (short) -1);
        java.lang.String str11 = nextDate3.run(2020, (-1), 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6654() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6654");
        NextDate nextDate3 = new NextDate(31, 1, 100);
    }

    @Test
    public void test6655() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6655");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) -1, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) ' ', (-1), (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6656() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6656");
        NextDate nextDate3 = new NextDate(31, (int) '#', (int) 'a');
        java.lang.String str7 = nextDate3.run(0, 0, (int) (short) 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 100, (int) (byte) 100, (int) 'a');
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6657() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6657");
        NextDate nextDate3 = new NextDate((int) (short) 1, 0, (-1));
        java.lang.String str7 = nextDate3.run(1, 0, 1);
        java.lang.String str11 = nextDate3.run(100, 1, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6658() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6658");
        NextDate nextDate3 = new NextDate(31, 2020, 31);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6659() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6659");
        NextDate nextDate3 = new NextDate((int) (short) 0, 0, 2001);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6660() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6660");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 10, (int) (short) -1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6661() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6661");
        NextDate nextDate3 = new NextDate(2020, (int) (short) 1, 1);
    }

    @Test
    public void test6662() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6662");
        NextDate nextDate3 = new NextDate(1, 10, 2001);
        java.lang.String str7 = nextDate3.run((int) '#', (int) (byte) 10, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6663() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6663");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 1, (int) ' ');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6664() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6664");
        NextDate nextDate3 = new NextDate((int) (short) 0, 0, (int) (short) 100);
        java.lang.String str7 = nextDate3.run(0, (int) (short) -1, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6665() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6665");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 100, (int) '#');
        java.lang.String str7 = nextDate3.run((int) '4', 30, 2001);
        java.lang.String str11 = nextDate3.run(10, (int) (byte) 100, 7);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6666() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6666");
        NextDate nextDate3 = new NextDate(2020, 30, 31);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (short) 10, (int) 'a');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6667() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6667");
        NextDate nextDate3 = new NextDate(0, 2020, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, 10, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6668() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6668");
        NextDate nextDate3 = new NextDate(30, (int) (byte) 10, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (short) 0, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6669() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6669");
        NextDate nextDate3 = new NextDate((int) (short) 100, 2020, 2020);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (-1), 7);
        java.lang.String str11 = nextDate3.run((-1), (int) 'a', (int) 'a');
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6670() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6670");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 0, 10);
        java.lang.String str7 = nextDate3.run((int) (short) 0, (int) 'a', 12);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6671() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6671");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 1, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run(10, 2001, (int) (short) 1);
        java.lang.String str11 = nextDate3.run((int) (short) -1, (int) 'a', (int) (short) 1);
        java.lang.String str15 = nextDate3.run(31, (int) (byte) 100, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6672() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6672");
        NextDate nextDate3 = new NextDate((int) 'a', 100, 0);
        java.lang.String str7 = nextDate3.run((int) ' ', 0, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6673() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6673");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) '#', 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6674() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6674");
        NextDate nextDate3 = new NextDate((int) (short) 10, 0, (int) (short) 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6675() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6675");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) -1, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run(12, 0, (int) (byte) 100);
        java.lang.String str11 = nextDate3.run(31, (int) (short) 0, 0);
        java.lang.String str15 = nextDate3.run(2001, 31, (int) (byte) 0);
        java.lang.String str19 = nextDate3.run((int) '#', 7, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6676() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6676");
        NextDate nextDate3 = new NextDate(100, 0, (int) (byte) 0);
    }

    @Test
    public void test6677() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6677");
        NextDate nextDate3 = new NextDate(0, 2020, (int) 'a');
        java.lang.String str7 = nextDate3.run((int) (byte) 1, (int) (short) 10, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6678() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6678");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run(0, 2001, 0);
        java.lang.String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        java.lang.String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        java.lang.String str19 = nextDate3.run((int) 'a', (int) (short) -1, 10);
        java.lang.String str23 = nextDate3.run((-1), (int) '#', (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test6679() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6679");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 1, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run(0, (int) (short) 100, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6680() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6680");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) '4', 0);
    }

    @Test
    public void test6681() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6681");
        NextDate nextDate3 = new NextDate(0, (int) (short) 1, (-1));
        java.lang.String str7 = nextDate3.run(7, 0, (int) '4');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6682() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6682");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 1, 0);
        java.lang.String str7 = nextDate3.run((-1), 100, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run(0, 100, 1);
        java.lang.String str15 = nextDate3.run(2001, 12, 7);
        java.lang.String str19 = nextDate3.run(2001, (int) '#', (int) (byte) 1);
        java.lang.String str23 = nextDate3.run((int) (byte) 10, (int) 'a', 31);
        java.lang.Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test6683() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6683");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) 'a', (int) (byte) -1);
    }

    @Test
    public void test6684() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6684");
        NextDate nextDate3 = new NextDate(12, 12, (int) (short) 10);
    }

    @Test
    public void test6685() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6685");
        NextDate nextDate3 = new NextDate(1, (int) ' ', (-1));
    }

    @Test
    public void test6686() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6686");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 2001, 2001);
    }

    @Test
    public void test6687() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6687");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 1, 30);
    }

    @Test
    public void test6688() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6688");
        NextDate nextDate3 = new NextDate(2001, (int) (short) 0, 1);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) '#', 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6689() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6689");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 31, (-1));
        java.lang.String str7 = nextDate3.run((int) '#', (-1), 100);
        java.lang.String str11 = nextDate3.run((int) ' ', (int) (short) -1, (int) (short) -1);
        java.lang.String str15 = nextDate3.run(12, (int) (short) 0, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6690() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6690");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) 'a', 30);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6691() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6691");
        NextDate nextDate3 = new NextDate(7, (int) (short) 0, (int) (short) 10);
    }

    @Test
    public void test6692() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6692");
        NextDate nextDate3 = new NextDate(7, (int) (short) 10, 12);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6693() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6693");
        NextDate nextDate3 = new NextDate(0, 1, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run(100, (int) (short) -1, 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, 100, (int) '4');
        java.lang.String str15 = nextDate3.run(31, (int) (short) 1, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6694() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6694");
        NextDate nextDate3 = new NextDate(2001, 30, 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6695() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6695");
        NextDate nextDate3 = new NextDate(10, 0, (int) '#');
        java.lang.String str7 = nextDate3.run((-1), 0, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6696() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6696");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) 100, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) '#', 31, 1);
        java.lang.String str11 = nextDate3.run(1, 100, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6697() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6697");
        NextDate nextDate3 = new NextDate(7, 10, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) ' ', (int) (short) 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6698() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6698");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) '#', 12);
        java.lang.String str7 = nextDate3.run(0, (int) (short) -1, 2001);
        java.lang.String str11 = nextDate3.run((int) (short) -1, (int) (byte) 1, (int) (short) -1);
        java.lang.String str15 = nextDate3.run(2001, 0, 2020);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test6699() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6699");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 0, (int) 'a');
    }

    @Test
    public void test6700() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6700");
        NextDate nextDate3 = new NextDate((int) (short) 10, 1, 2001);
    }

    @Test
    public void test6701() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6701");
        NextDate nextDate3 = new NextDate((int) '4', (int) (byte) 100, (-1));
        java.lang.String str7 = nextDate3.run(10, (int) (short) 1, (int) (short) 10);
        java.lang.String str11 = nextDate3.run(2020, (int) (byte) 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6702() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6702");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 10, (int) (short) 1);
    }

    @Test
    public void test6703() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6703");
        NextDate nextDate3 = new NextDate(2020, 30, (int) (short) -1);
    }

    @Test
    public void test6704() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6704");
        NextDate nextDate3 = new NextDate(10, 0, 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, 10, 7);
        java.lang.String str11 = nextDate3.run(2001, 31, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6705() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6705");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 1, 10);
        java.lang.String str7 = nextDate3.run((int) (short) 0, 30, (int) (byte) 1);
        java.lang.String str11 = nextDate3.run((int) ' ', (int) (short) 100, (int) (short) 100);
        java.lang.String str15 = nextDate3.run((int) (short) 10, 10, 10);
        java.lang.String str19 = nextDate3.run(2020, (int) (byte) 10, 31);
        java.lang.String str23 = nextDate3.run((int) '4', (int) (byte) 0, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test6706() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6706");
        NextDate nextDate3 = new NextDate((-1), 0, (int) (byte) -1);
    }

    @Test
    public void test6707() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6707");
        NextDate nextDate3 = new NextDate((int) ' ', 100, 2020);
    }

    @Test
    public void test6708() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6708");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) 1, 12);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (byte) -1, 30);
        java.lang.String str11 = nextDate3.run(0, (int) (byte) 0, 2001);
        java.lang.String str15 = nextDate3.run((int) (short) 100, 1, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6709() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6709");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 31, 0);
        java.lang.String str7 = nextDate3.run(10, (int) (byte) 1, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6710() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6710");
        NextDate nextDate3 = new NextDate(12, (int) '#', (int) (byte) 100);
    }

    @Test
    public void test6711() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6711");
        NextDate nextDate3 = new NextDate(0, 100, (int) 'a');
        java.lang.String str7 = nextDate3.run(31, 1, 31);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6712() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6712");
        NextDate nextDate3 = new NextDate((int) '#', 2020, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, 10, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6713() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6713");
        NextDate nextDate3 = new NextDate(2020, (int) ' ', (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) 'a', (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6714() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6714");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) ' ', (int) (byte) -1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6715() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6715");
        NextDate nextDate3 = new NextDate(12, (int) (short) 10, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (short) 100, 2020);
        java.lang.String str11 = nextDate3.run(1, (int) '#', 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6716() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6716");
        NextDate nextDate3 = new NextDate(12, 10, 2001);
    }

    @Test
    public void test6717() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6717");
        NextDate nextDate3 = new NextDate(31, (int) (byte) 1, 2020);
        java.lang.String str7 = nextDate3.run((int) '4', 2020, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6718() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6718");
        NextDate nextDate3 = new NextDate(31, 12, 12);
        java.lang.String str7 = nextDate3.run(100, (int) (short) 1, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6719() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6719");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 100, 2020);
        java.lang.String str7 = nextDate3.run((int) ' ', 10, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run(7, (int) (byte) 0, (int) 'a');
        java.lang.String str15 = nextDate3.run((-1), (int) (short) -1, (int) 'a');
        java.lang.String str19 = nextDate3.run(0, (int) (short) 100, (int) (short) 0);
        java.lang.Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test6720() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6720");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 100, 2020);
    }

    @Test
    public void test6721() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6721");
        NextDate nextDate3 = new NextDate(100, (int) (byte) -1, 2001);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (byte) 100, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6722() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6722");
        NextDate nextDate3 = new NextDate((int) (short) 100, 30, (int) (short) 0);
    }

    @Test
    public void test6723() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6723");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) '#', 7);
        java.lang.String str7 = nextDate3.run(100, (int) (byte) 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6724() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6724");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 0, 30);
        java.lang.String str7 = nextDate3.run(31, (int) (byte) 100, 30);
        java.lang.String str11 = nextDate3.run((int) 'a', 12, 1);
        java.lang.String str15 = nextDate3.run((int) (byte) 0, 30, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6725() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6725");
        NextDate nextDate3 = new NextDate(2001, (int) '#', 31);
        java.lang.String str7 = nextDate3.run(0, (int) (short) 0, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6726() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6726");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 10, (int) (byte) 0);
    }

    @Test
    public void test6727() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6727");
        NextDate nextDate3 = new NextDate(12, 1, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (short) 1, 10, 1);
        java.lang.String str11 = nextDate3.run(7, (int) 'a', 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "7/97/2020" + "'", str11, "7/97/2020");
    }

    @Test
    public void test6728() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6728");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) -1, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (short) 10, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6729() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6729");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) -1, 10);
        java.lang.String str7 = nextDate3.run(30, (int) (short) 100, 31);
        java.lang.String str11 = nextDate3.run((int) 'a', 31, (int) '#');
        java.lang.String str15 = nextDate3.run(12, 0, 100);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test6730() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6730");
        NextDate nextDate3 = new NextDate(30, (int) (short) 1, 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6731() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6731");
        NextDate nextDate3 = new NextDate(31, (int) (short) 100, (int) (short) 1);
    }

    @Test
    public void test6732() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6732");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 7, (int) '#');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6733() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6733");
        NextDate nextDate3 = new NextDate(10, (int) '#', 2020);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) ' ', 31);
        java.lang.String str11 = nextDate3.run((int) (short) 1, 2020, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6734() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6734");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 10, 1);
        java.lang.String str7 = nextDate3.run((int) ' ', (-1), (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6735() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6735");
        NextDate nextDate3 = new NextDate(7, 12, (int) (byte) 1);
    }

    @Test
    public void test6736() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6736");
        NextDate nextDate3 = new NextDate(100, 7, (int) '#');
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (byte) -1, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6737() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6737");
        NextDate nextDate3 = new NextDate((int) '#', (int) '#', (int) (short) 0);
    }

    @Test
    public void test6738() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6738");
        NextDate nextDate3 = new NextDate((int) ' ', (int) 'a', 7);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (byte) -1, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6739() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6739");
        NextDate nextDate3 = new NextDate(2020, 1, 30);
    }

    @Test
    public void test6740() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6740");
        NextDate nextDate3 = new NextDate(1, (int) '4', (int) 'a');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6741() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6741");
        NextDate nextDate3 = new NextDate(2001, 100, 10);
        java.lang.String str7 = nextDate3.run(0, 10, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6742() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6742");
        NextDate nextDate3 = new NextDate((int) (short) 10, 0, 31);
    }

    @Test
    public void test6743() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6743");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) -1, 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) ' ', (int) (byte) 100);
        java.lang.String str11 = nextDate3.run((int) '4', 0, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6744() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6744");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) 'a', 12);
        java.lang.String str7 = nextDate3.run((int) (short) 1, 31, (int) ' ');
        java.lang.String str11 = nextDate3.run((int) (byte) 0, 100, (int) (byte) -1);
        java.lang.String str15 = nextDate3.run((int) (short) -1, (int) (byte) -1, (int) (short) 0);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test6745() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6745");
        NextDate nextDate3 = new NextDate((int) ' ', 2001, 2001);
    }

    @Test
    public void test6746() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6746");
        NextDate nextDate3 = new NextDate(31, 2001, 2001);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (byte) -1, (-1));
        java.lang.String str11 = nextDate3.run(2020, (-1), 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6747() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6747");
        NextDate nextDate3 = new NextDate((int) (short) 100, 1, 2001);
        java.lang.String str7 = nextDate3.run(2020, (int) '#', (int) '#');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6748() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6748");
        NextDate nextDate3 = new NextDate(1, (int) (byte) 0, (int) (byte) 0);
    }

    @Test
    public void test6749() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6749");
        NextDate nextDate3 = new NextDate(31, 12, 12);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, (int) (short) 100, 100);
        java.lang.String str11 = nextDate3.run(2020, 100, (int) ' ');
        java.lang.String str15 = nextDate3.run((int) (byte) -1, 2020, (int) (byte) 0);
        java.lang.String str19 = nextDate3.run((int) (byte) 10, 0, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6750() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6750");
        NextDate nextDate3 = new NextDate(0, 0, (int) (short) 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6751() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6751");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) -1, 0);
    }

    @Test
    public void test6752() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6752");
        NextDate nextDate3 = new NextDate(31, (-1), (int) 'a');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6753() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6753");
        NextDate nextDate3 = new NextDate(0, (int) (short) 10, 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6754() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6754");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run(0, 2001, 0);
        java.lang.String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        java.lang.String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        java.lang.String str19 = nextDate3.run((int) '4', 30, 0);
        java.lang.String str23 = nextDate3.run(7, (int) (short) 10, (int) (short) 1);
        java.lang.String str27 = nextDate3.run((int) 'a', (int) (byte) 1, 0);
        java.lang.String str31 = nextDate3.run((int) (byte) 0, (int) ' ', (int) (short) 10);
        java.lang.String str35 = nextDate3.run((int) (short) -1, (int) '4', 7);
        java.lang.String str39 = nextDate3.run(10, 31, (int) (byte) 0);
        java.lang.String str43 = nextDate3.run((int) (short) 10, 0, (int) (byte) 1);
        java.lang.String str47 = nextDate3.run((int) (byte) -1, (int) (byte) -1, (int) (byte) -1);
        java.lang.String str51 = nextDate3.run((int) (short) 1, (int) (short) 100, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str43 + "' != '" + "invalid Input Date" + "'", str43, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str47 + "' != '" + "invalid Input Date" + "'", str47, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str51 + "' != '" + "invalid Input Date" + "'", str51, "invalid Input Date");
    }

    @Test
    public void test6755() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6755");
        NextDate nextDate3 = new NextDate((int) (short) 100, 0, 1);
        java.lang.String str7 = nextDate3.run(0, (int) ' ', 2001);
        java.lang.String str11 = nextDate3.run(7, 31, 0);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6756() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6756");
        NextDate nextDate3 = new NextDate(2001, 0, 31);
    }

    @Test
    public void test6757() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6757");
        NextDate nextDate3 = new NextDate(0, 12, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (short) 1, 100, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6758() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6758");
        NextDate nextDate3 = new NextDate(100, 1, 2001);
    }

    @Test
    public void test6759() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6759");
        NextDate nextDate3 = new NextDate(0, 1, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run(30, (int) '4', (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6760() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6760");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) 'a', 12);
        java.lang.String str7 = nextDate3.run((int) (short) 1, 31, (int) ' ');
        java.lang.String str11 = nextDate3.run((int) (byte) 0, 100, (int) (byte) -1);
        java.lang.String str15 = nextDate3.run((int) (short) 100, (int) (byte) -1, 31);
        java.lang.String str19 = nextDate3.run((int) (byte) 0, (int) (short) 1, (int) '#');
        java.lang.String str23 = nextDate3.run((int) 'a', (int) 'a', (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test6761() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6761");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 0, 12);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (short) 100, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6762() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6762");
        NextDate nextDate3 = new NextDate((int) '#', (int) 'a', 2001);
        java.lang.String str7 = nextDate3.run((int) (short) 0, (int) (short) 10, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6763() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6763");
        NextDate nextDate3 = new NextDate(2001, (-1), (int) (byte) -1);
    }

    @Test
    public void test6764() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6764");
        NextDate nextDate3 = new NextDate(30, (int) '4', 12);
        java.lang.String str7 = nextDate3.run(12, (-1), (int) (short) 10);
        java.lang.String str11 = nextDate3.run((int) (byte) 0, (int) (byte) 1, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6765() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6765");
        NextDate nextDate3 = new NextDate((int) '4', 30, (int) (byte) 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6766() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6766");
        NextDate nextDate3 = new NextDate(10, 7, (int) '4');
    }

    @Test
    public void test6767() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6767");
        NextDate nextDate3 = new NextDate((int) (short) -1, 2001, (int) (byte) 0);
    }

    @Test
    public void test6768() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6768");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 0, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (byte) 1, (int) '#');
        java.lang.String str11 = nextDate3.run(30, (int) 'a', 2001);
        java.lang.String str15 = nextDate3.run(31, (int) (byte) 100, (int) '4');
        java.lang.String str19 = nextDate3.run((int) (byte) -1, (int) (short) 10, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6769() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6769");
        NextDate nextDate3 = new NextDate(2001, 30, (int) (short) 10);
    }

    @Test
    public void test6770() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6770");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 10, 2001);
    }

    @Test
    public void test6771() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6771");
        NextDate nextDate3 = new NextDate(12, (int) '4', 1);
        java.lang.String str7 = nextDate3.run(7, (int) (byte) 100, (int) (short) -1);
        java.lang.String str11 = nextDate3.run(0, (int) (byte) 1, 10);
        java.lang.String str15 = nextDate3.run(1, (int) 'a', (int) 'a');
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test6772() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6772");
        NextDate nextDate3 = new NextDate(12, 7, 12);
        java.lang.String str7 = nextDate3.run((int) ' ', 2001, (int) ' ');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6773() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6773");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) 10, 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (byte) 10, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6774() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6774");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 0, (int) '#');
    }

    @Test
    public void test6775() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6775");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) ' ', 12);
        java.lang.String str7 = nextDate3.run(30, 12, 12);
        java.lang.String str11 = nextDate3.run((int) (short) 0, (int) '#', (int) '#');
        java.lang.String str15 = nextDate3.run(0, 0, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6776() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6776");
        NextDate nextDate3 = new NextDate(10, 30, (int) (byte) 100);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6777() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6777");
        NextDate nextDate3 = new NextDate(0, (int) 'a', 31);
        java.lang.String str7 = nextDate3.run(2020, (int) ' ', 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6778() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6778");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 0, (int) '#');
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (byte) 10, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6779() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6779");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 0, 1);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) ' ', (int) (byte) 100);
        java.lang.String str11 = nextDate3.run(10, 31, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "11/1/2020" + "'", str11, "11/1/2020");
    }

    @Test
    public void test6780() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6780");
        NextDate nextDate3 = new NextDate((int) 'a', (-1), 2001);
        java.lang.String str7 = nextDate3.run((int) '4', 2001, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run(2020, (int) (short) 1, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6781() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6781");
        NextDate nextDate3 = new NextDate(30, (int) (byte) 100, 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, 0, 30);
        java.lang.String str11 = nextDate3.run((int) (byte) -1, 30, (-1));
        java.lang.String str15 = nextDate3.run(12, 0, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6782() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6782");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) 1, (int) (short) 1);
    }

    @Test
    public void test6783() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6783");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) '#', 12);
        java.lang.String str7 = nextDate3.run((int) (short) 100, 100, 2020);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6784() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6784");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 0, 2020);
        java.lang.String str7 = nextDate3.run(7, 1, (int) '#');
        java.lang.String str11 = nextDate3.run((int) (short) 0, (int) '4', 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6785() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6785");
        NextDate nextDate3 = new NextDate((int) (short) -1, 100, (int) (short) 1);
    }

    @Test
    public void test6786() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6786");
        NextDate nextDate3 = new NextDate((int) ' ', (int) '4', 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6787() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6787");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 30, 30);
        java.lang.String str7 = nextDate3.run(0, (int) (byte) 100, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6788() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6788");
        NextDate nextDate3 = new NextDate(7, 0, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (byte) 10, (int) ' ');
        java.lang.String str11 = nextDate3.run((int) 'a', (int) (short) 100, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6789() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6789");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (short) 1, 0);
    }

    @Test
    public void test6790() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6790");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 0, (int) '4');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6791() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6791");
        NextDate nextDate3 = new NextDate((int) '#', 100, 7);
        java.lang.String str7 = nextDate3.run(31, 7, (int) '#');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6792() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6792");
        NextDate nextDate3 = new NextDate(10, (int) (short) 0, 100);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, 0, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6793() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6793");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 10, (int) '4');
    }

    @Test
    public void test6794() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6794");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 0, 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) ' ', 7);
        java.lang.String str11 = nextDate3.run(31, 7, 0);
        java.lang.String str15 = nextDate3.run(31, (int) (short) 0, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6795() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6795");
        NextDate nextDate3 = new NextDate((int) '4', 0, 12);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) ' ', (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6796() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6796");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 2020, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (short) 1, 31, 2001);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, 0, (int) (byte) 1);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "2/1/2001" + "'", str7, "2/1/2001");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6797() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6797");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 30, 31);
        java.lang.String str7 = nextDate3.run(30, (int) '4', (int) (short) 100);
        java.lang.String str11 = nextDate3.run(2001, (int) (short) 1, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6798() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6798");
        NextDate nextDate3 = new NextDate(12, 7, 100);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6799() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6799");
        NextDate nextDate3 = new NextDate((int) (short) 0, 100, 31);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6800() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6800");
        NextDate nextDate3 = new NextDate(12, (int) (byte) 1, (int) (short) 10);
        java.lang.String str7 = nextDate3.run((-1), (int) (short) 0, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run((int) (byte) -1, 7, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6801() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6801");
        NextDate nextDate3 = new NextDate((int) (short) -1, 2020, 31);
    }

    @Test
    public void test6802() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6802");
        NextDate nextDate3 = new NextDate(0, 2020, 0);
    }

    @Test
    public void test6803() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6803");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 10, (int) (short) 1);
    }

    @Test
    public void test6804() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6804");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) 10, 2020);
        java.lang.String str7 = nextDate3.run(100, (int) '4', 1);
        java.lang.String str11 = nextDate3.run(2020, (int) (byte) 10, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6805() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6805");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) 10, (int) (short) 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6806() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6806");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run((int) 'a', 12, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run(31, (int) (byte) 1, (int) (byte) 10);
        java.lang.String str15 = nextDate3.run((int) (short) 10, (int) (short) 100, (int) (short) 1);
        java.lang.String str19 = nextDate3.run((int) (byte) -1, (int) (short) 1, (int) (byte) 1);
        java.lang.String str23 = nextDate3.run((int) ' ', 100, (-1));
        java.lang.String str27 = nextDate3.run((int) '#', (int) ' ', 10);
        java.lang.String str31 = nextDate3.run((int) '#', (int) '#', (int) (short) 1);
        java.lang.String str35 = nextDate3.run((int) 'a', 10, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
    }

    @Test
    public void test6807() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6807");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) -1, 2001);
        java.lang.String str7 = nextDate3.run((-1), (int) ' ', (int) (byte) 100);
        java.lang.String str11 = nextDate3.run(31, (int) (short) 10, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6808() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6808");
        NextDate nextDate3 = new NextDate((int) (short) 0, 12, 2020);
        java.lang.String str7 = nextDate3.run((int) (short) 1, 10, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6809() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6809");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 2001, (int) ' ');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6810() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6810");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 10, (int) (byte) 1);
    }

    @Test
    public void test6811() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6811");
        NextDate nextDate3 = new NextDate(12, 12, (int) 'a');
        java.lang.String str7 = nextDate3.run(0, (int) (short) 1, 2020);
        java.lang.String str11 = nextDate3.run(100, 0, 100);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6812() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6812");
        NextDate nextDate3 = new NextDate((int) (short) -1, (-1), 2001);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (byte) -1, (-1));
        java.lang.String str11 = nextDate3.run((int) ' ', (int) (short) 10, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6813() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6813");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) -1, 12);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, 1, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6814() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6814");
        NextDate nextDate3 = new NextDate((-1), 100, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (short) 100, 1, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6815() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6815");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 1, 12);
    }

    @Test
    public void test6816() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6816");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) 1, 2001);
    }

    @Test
    public void test6817() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6817");
        NextDate nextDate3 = new NextDate((int) (short) 1, 0, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, (int) (byte) -1, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6818() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6818");
        NextDate nextDate3 = new NextDate(2020, 31, 30);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (short) -1, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6819() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6819");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 10, 12);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (short) -1, (int) (byte) 1);
        java.lang.String str11 = nextDate3.run((int) (short) 0, 1, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6820() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6820");
        NextDate nextDate3 = new NextDate(12, 10, 30);
    }

    @Test
    public void test6821() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6821");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 100, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run(12, (int) (short) -1, (int) (short) 100);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6822() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6822");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 100, 7);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (short) 1, 0);
        java.lang.String str11 = nextDate3.run((int) (short) 10, 1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6823() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6823");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 0, 2001);
        java.lang.String str7 = nextDate3.run((int) (short) 10, 1, (int) (short) 0);
        java.lang.String str11 = nextDate3.run(2001, 0, (int) ' ');
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6824() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6824");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) 'a', (int) (byte) 10);
    }

    @Test
    public void test6825() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6825");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 0, 100);
    }

    @Test
    public void test6826() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6826");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) ' ', (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (short) 0, (int) (short) 1);
        java.lang.String str11 = nextDate3.run(12, (int) ' ', 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6827() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6827");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 100, 2001);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, 30, 31);
        java.lang.String str11 = nextDate3.run((int) '4', 0, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6828() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6828");
        NextDate nextDate3 = new NextDate(0, (int) (byte) -1, (int) (byte) 10);
    }

    @Test
    public void test6829() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6829");
        NextDate nextDate3 = new NextDate((int) (short) 10, 31, (int) ' ');
        java.lang.String str7 = nextDate3.run(0, 30, (int) (byte) 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6830() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6830");
        NextDate nextDate3 = new NextDate(31, 1, (int) (byte) 0);
    }

    @Test
    public void test6831() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6831");
        NextDate nextDate3 = new NextDate(30, 1, 2020);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6832() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6832");
        NextDate nextDate3 = new NextDate((int) '4', 7, 30);
        java.lang.String str7 = nextDate3.run(1, (int) (byte) 100, 30);
        java.lang.String str11 = nextDate3.run((int) (byte) 0, (int) (byte) 1, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6833() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6833");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 0, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, 30, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run(2001, 10, (int) '#');
        java.lang.String str15 = nextDate3.run(2020, (int) (short) 1, (int) (byte) 100);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test6834() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6834");
        NextDate nextDate3 = new NextDate(0, (int) (short) 10, (-1));
    }

    @Test
    public void test6835() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6835");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) 0, (int) 'a');
    }

    @Test
    public void test6836() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6836");
        NextDate nextDate3 = new NextDate(100, 31, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (byte) 0, (int) (byte) -1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6837() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6837");
        NextDate nextDate3 = new NextDate(0, 0, 100);
        java.lang.String str7 = nextDate3.run(100, (int) '#', 7);
        java.lang.String str11 = nextDate3.run(12, (int) (byte) 100, (int) (byte) 100);
        java.lang.String str15 = nextDate3.run((int) (short) 100, 31, (int) (byte) 10);
        java.lang.String str19 = nextDate3.run((int) (byte) 0, 2020, (int) (short) 10);
        java.lang.Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test6838() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6838");
        NextDate nextDate3 = new NextDate(2020, (int) (short) 1, (int) (short) 100);
        java.lang.String str7 = nextDate3.run(10, 0, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6839() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6839");
        NextDate nextDate3 = new NextDate(2020, (int) 'a', (int) '4');
        java.lang.String str7 = nextDate3.run(0, 30, 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6840() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6840");
        NextDate nextDate3 = new NextDate(100, (int) (short) 10, (int) '#');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6841() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6841");
        NextDate nextDate3 = new NextDate(2020, (int) (short) 100, 0);
    }

    @Test
    public void test6842() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6842");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) 100, 2020);
        java.lang.String str7 = nextDate3.run((-1), (int) (short) 100, (int) (byte) 100);
        java.lang.String str11 = nextDate3.run(2020, 2020, (int) '#');
        java.lang.String str15 = nextDate3.run(30, (int) 'a', (-1));
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test6843() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6843");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) ' ', 100);
    }

    @Test
    public void test6844() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6844");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 0, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (byte) 1, (int) '#');
        java.lang.String str11 = nextDate3.run((int) (short) 0, (int) '#', 30);
        java.lang.String str15 = nextDate3.run((int) ' ', (int) (short) 100, 0);
        java.lang.String str19 = nextDate3.run(30, (int) 'a', 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6845() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6845");
        NextDate nextDate3 = new NextDate(0, 0, 100);
        java.lang.String str7 = nextDate3.run((int) (short) 10, 100, (int) (short) 1);
        java.lang.String str11 = nextDate3.run((int) (byte) 0, 1, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6846() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6846");
        NextDate nextDate3 = new NextDate(31, (int) 'a', (int) (byte) 0);
    }

    @Test
    public void test6847() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6847");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) -1, 0);
    }

    @Test
    public void test6848() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6848");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run(0, 2001, 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        java.lang.String str15 = nextDate3.run(7, (int) (short) 1, 30);
        java.lang.String str19 = nextDate3.run(12, 100, (int) (byte) 1);
        java.lang.String str23 = nextDate3.run(10, 2020, (int) (short) 1);
        java.lang.Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test6849() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6849");
        NextDate nextDate3 = new NextDate((int) (short) 100, 0, (int) (byte) 1);
    }

    @Test
    public void test6850() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6850");
        NextDate nextDate3 = new NextDate(1, 2020, (int) '4');
        java.lang.String str7 = nextDate3.run(31, 30, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6851() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6851");
        NextDate nextDate3 = new NextDate(1, 12, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) '4', 0, (int) (short) 10);
        java.lang.String str11 = nextDate3.run((int) (byte) 10, (int) (byte) 10, 1);
        java.lang.String str15 = nextDate3.run((int) (short) 100, 0, 1);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test6852() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6852");
        NextDate nextDate3 = new NextDate((int) 'a', 31, 2001);
    }

    @Test
    public void test6853() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6853");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 100, (-1));
    }

    @Test
    public void test6854() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6854");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 10, (int) (short) 1);
    }

    @Test
    public void test6855() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6855");
        NextDate nextDate3 = new NextDate(30, (int) '#', (int) ' ');
        java.lang.String str7 = nextDate3.run(2001, (int) (byte) 10, 31);
        java.lang.String str11 = nextDate3.run(30, (int) (byte) 100, 12);
        java.lang.String str15 = nextDate3.run((int) (byte) 100, 0, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6856() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6856");
        NextDate nextDate3 = new NextDate(30, 7, (int) (byte) -1);
    }

    @Test
    public void test6857() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6857");
        NextDate nextDate3 = new NextDate((-1), (int) (short) -1, 31);
        java.lang.String str7 = nextDate3.run(30, 12, 30);
        java.lang.String str11 = nextDate3.run((-1), 1, (int) (byte) 0);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6858() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6858");
        NextDate nextDate3 = new NextDate((int) 'a', 1, 7);
        java.lang.String str7 = nextDate3.run(10, 100, 0);
        java.lang.String str11 = nextDate3.run(31, (int) (byte) 1, (int) ' ');
        java.lang.String str15 = nextDate3.run((int) (short) 0, (int) (short) 0, 0);
        java.lang.String str19 = nextDate3.run((int) (byte) 10, 30, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6859() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6859");
        NextDate nextDate3 = new NextDate(31, 1, 12);
    }

    @Test
    public void test6860() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6860");
        NextDate nextDate3 = new NextDate(10, 7, 1);
        java.lang.String str7 = nextDate3.run((int) (short) 100, 0, 31);
        java.lang.String str11 = nextDate3.run(0, 0, (int) (byte) 1);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6861() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6861");
        NextDate nextDate3 = new NextDate((int) (short) 0, 12, (int) '#');
    }

    @Test
    public void test6862() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6862");
        NextDate nextDate3 = new NextDate((int) '#', 2020, (int) (short) 10);
    }

    @Test
    public void test6863() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6863");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) 0, (int) (byte) -1);
    }

    @Test
    public void test6864() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6864");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 100, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run(30, 10, (int) '4');
        java.lang.String str11 = nextDate3.run((int) (byte) 0, (-1), 31);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6865() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6865");
        NextDate nextDate3 = new NextDate(12, (-1), 100);
        java.lang.String str7 = nextDate3.run(0, (int) (byte) 1, (int) (short) 1);
        java.lang.String str11 = nextDate3.run(0, 2020, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6866() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6866");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) -1, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) '#', (int) ' ', 2020);
        java.lang.String str11 = nextDate3.run(12, 0, (int) (byte) 100);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6867() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6867");
        NextDate nextDate3 = new NextDate((int) (short) -1, 1, 1);
    }

    @Test
    public void test6868() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6868");
        NextDate nextDate3 = new NextDate(7, 7, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run(0, 12, 100);
        java.lang.String str11 = nextDate3.run(100, (int) (short) 0, (int) (short) -1);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6869() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6869");
        NextDate nextDate3 = new NextDate(2001, 31, (int) (short) 100);
    }

    @Test
    public void test6870() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6870");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 10, 0);
        java.lang.String str7 = nextDate3.run(1, (int) (byte) 100, 1);
        java.lang.String str11 = nextDate3.run((int) '4', 0, (int) (short) 10);
        java.lang.String str15 = nextDate3.run(0, (int) (short) 10, 30);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test6871() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6871");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (byte) 0, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (short) 10, 2020, (int) '4');
        java.lang.String str11 = nextDate3.run((-1), 0, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6872() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6872");
        NextDate nextDate3 = new NextDate(1, (int) (byte) 1, 1);
    }

    @Test
    public void test6873() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6873");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run(0, 2001, 0);
        java.lang.String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        java.lang.String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        java.lang.String str19 = nextDate3.run((int) '4', 30, 0);
        java.lang.String str23 = nextDate3.run(7, (int) (short) 10, (int) (short) 1);
        java.lang.String str27 = nextDate3.run(2020, 7, (int) (short) 0);
        java.lang.String str31 = nextDate3.run(0, 31, (int) (short) 0);
        java.lang.String str35 = nextDate3.run((int) (byte) -1, 12, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
    }

    @Test
    public void test6874() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6874");
        NextDate nextDate3 = new NextDate(2020, (int) (short) 1, 10);
        java.lang.String str7 = nextDate3.run((int) (short) 1, 12, 2001);
        java.lang.String str11 = nextDate3.run((int) '#', (int) (byte) 10, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "1/13/2001" + "'", str7, "1/13/2001");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6875() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6875");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) 10, (int) (short) 10);
        java.lang.String str7 = nextDate3.run(10, 0, (int) '#');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6876() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6876");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 100, 2020);
        java.lang.String str7 = nextDate3.run((int) (short) 0, (-1), 0);
        java.lang.String str11 = nextDate3.run(30, (int) (byte) 0, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6877() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6877");
        NextDate nextDate3 = new NextDate(1, (int) (byte) -1, (int) (short) -1);
        java.lang.String str7 = nextDate3.run(31, 1, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6878() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6878");
        NextDate nextDate3 = new NextDate(12, 12, 10);
        java.lang.String str7 = nextDate3.run(10, (int) (byte) 1, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6879() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6879");
        NextDate nextDate3 = new NextDate((int) ' ', 10, (int) (short) 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6880() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6880");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 1, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) ' ', (-1));
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6881() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6881");
        NextDate nextDate3 = new NextDate(31, (int) (byte) 0, (int) '#');
        java.lang.String str7 = nextDate3.run(12, (int) (short) 100, (int) ' ');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6882() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6882");
        NextDate nextDate3 = new NextDate(12, (int) (short) 10, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (short) 100, 2020);
        java.lang.String str11 = nextDate3.run((int) (short) 1, 0, (int) (short) 10);
        java.lang.String str15 = nextDate3.run((int) (short) 10, 2001, 0);
        java.lang.String str19 = nextDate3.run(10, (int) ' ', (int) (short) 0);
        java.lang.String str23 = nextDate3.run((int) (byte) 100, 10, 7);
        java.lang.String str27 = nextDate3.run((int) (byte) 0, 12, (int) (short) 100);
        java.lang.String str31 = nextDate3.run(10, (int) (byte) 100, (int) '#');
        java.lang.String str35 = nextDate3.run((int) '#', 2001, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
    }

    @Test
    public void test6883() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6883");
        NextDate nextDate3 = new NextDate(100, (int) '#', (int) (short) 10);
    }

    @Test
    public void test6884() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6884");
        NextDate nextDate3 = new NextDate(2001, (int) (short) 10, (int) (byte) 0);
    }

    @Test
    public void test6885() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6885");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) -1, (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (short) 10, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6886() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6886");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 0, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (byte) 1, 12);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6887() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6887");
        NextDate nextDate3 = new NextDate((int) (short) 10, 12, 0);
        java.lang.String str7 = nextDate3.run((int) '4', 1, 1);
        java.lang.String str11 = nextDate3.run(2001, (int) '#', (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6888() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6888");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) 100, 7);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6889() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6889");
        NextDate nextDate3 = new NextDate((int) (short) 100, 2001, (int) ' ');
    }

    @Test
    public void test6890() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6890");
        NextDate nextDate3 = new NextDate(10, 2020, 100);
    }

    @Test
    public void test6891() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6891");
        NextDate nextDate3 = new NextDate(2020, (-1), 100);
    }

    @Test
    public void test6892() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6892");
        NextDate nextDate3 = new NextDate(1, 2020, 10);
    }

    @Test
    public void test6893() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6893");
        NextDate nextDate3 = new NextDate((int) (short) -1, 2020, (int) (short) 100);
        java.lang.String str7 = nextDate3.run(1, (int) (short) 100, (int) (short) -1);
        java.lang.String str11 = nextDate3.run((-1), 30, (int) (byte) 10);
        java.lang.String str15 = nextDate3.run((int) (short) 0, (int) '4', (int) (short) 100);
        java.lang.String str19 = nextDate3.run((int) 'a', (int) '4', 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6894() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6894");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 1, (int) (byte) 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6895() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6895");
        NextDate nextDate3 = new NextDate(2020, (int) '4', (int) (byte) 100);
        java.lang.String str7 = nextDate3.run(0, 1, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6896() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6896");
        NextDate nextDate3 = new NextDate(2020, (-1), (int) '#');
    }

    @Test
    public void test6897() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6897");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) 100, 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6898() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6898");
        NextDate nextDate3 = new NextDate(2020, (int) (short) 100, 30);
    }

    @Test
    public void test6899() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6899");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) -1, 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (short) 10, 31);
        java.lang.String str11 = nextDate3.run(10, (int) (byte) 10, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6900() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6900");
        NextDate nextDate3 = new NextDate(10, 0, 2001);
        java.lang.String str7 = nextDate3.run(0, 1, 0);
        java.lang.String str11 = nextDate3.run((int) '#', (int) (short) 0, (int) 'a');
        java.lang.String str15 = nextDate3.run((int) (byte) 10, 10, 1);
        java.lang.String str19 = nextDate3.run((int) '4', (int) (short) -1, 0);
        java.lang.String str23 = nextDate3.run((int) 'a', (int) (short) 0, (int) (byte) 1);
        java.lang.String str27 = nextDate3.run(7, (int) (byte) 10, 2020);
        java.lang.String str31 = nextDate3.run((int) (byte) -1, (int) (byte) 1, (int) (byte) -1);
        java.lang.String str35 = nextDate3.run((int) (byte) -1, (int) 'a', (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "7/10/2020" + "'", str27, "7/10/2020");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
    }

    @Test
    public void test6901() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6901");
        NextDate nextDate3 = new NextDate(100, (int) '4', (int) (short) 100);
    }

    @Test
    public void test6902() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6902");
        NextDate nextDate3 = new NextDate(2001, 2001, (int) (short) -1);
        java.lang.String str7 = nextDate3.run(0, (int) (byte) 1, (int) (byte) 1);
        java.lang.String str11 = nextDate3.run((int) (short) 1, 2001, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6903() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6903");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 0, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (short) 10, (int) (short) -1);
        java.lang.String str11 = nextDate3.run(0, 2001, (int) (byte) 1);
        java.lang.String str15 = nextDate3.run(0, (int) (byte) -1, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6904() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6904");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 1, (int) '4');
    }

    @Test
    public void test6905() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6905");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) 1, 12);
        java.lang.String str7 = nextDate3.run(30, (int) ' ', 12);
        java.lang.String str11 = nextDate3.run((int) ' ', 12, 0);
        java.lang.String str15 = nextDate3.run(10, (int) '4', (int) (byte) 100);
        java.lang.String str19 = nextDate3.run(7, 0, (-1));
        java.lang.String str23 = nextDate3.run((int) (byte) 0, (int) (byte) 1, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test6906() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6906");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run(0, 2001, 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        java.lang.String str15 = nextDate3.run((int) (byte) 1, (int) (short) 0, (int) (short) 100);
        java.lang.String str19 = nextDate3.run((int) (short) 10, 0, 10);
        java.lang.String str23 = nextDate3.run((int) (byte) -1, 10, 30);
        java.lang.String str27 = nextDate3.run((int) (byte) 100, (int) (short) 0, (-1));
        java.lang.String str31 = nextDate3.run((int) (byte) -1, 10, (int) (short) 10);
        java.lang.String str35 = nextDate3.run((int) (short) 0, 7, (int) (byte) 100);
        java.lang.String str39 = nextDate3.run((int) (short) 1, (int) (short) 10, (int) (short) -1);
        java.lang.String str43 = nextDate3.run((int) (byte) 100, (int) (short) 100, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str43 + "' != '" + "invalid Input Date" + "'", str43, "invalid Input Date");
    }

    @Test
    public void test6907() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6907");
        NextDate nextDate3 = new NextDate(2001, 100, (int) (byte) 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6908() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6908");
        NextDate nextDate3 = new NextDate(30, (int) (short) 0, 0);
        java.lang.String str7 = nextDate3.run(12, (int) 'a', (int) (short) 100);
        java.lang.String str11 = nextDate3.run((int) '#', (int) (short) 1, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6909() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6909");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 0, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run(2001, 10, (int) (byte) 1);
        java.lang.String str11 = nextDate3.run(2020, 2020, 30);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6910() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6910");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) ' ', 1);
        java.lang.String str7 = nextDate3.run(31, 2001, 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6911() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6911");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 1, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) (byte) -1, 7);
        java.lang.String str11 = nextDate3.run((int) (byte) 0, 0, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6912() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6912");
        NextDate nextDate3 = new NextDate(7, (int) (byte) 0, 2020);
        java.lang.String str7 = nextDate3.run((int) (short) 0, 31, 2020);
        java.lang.String str11 = nextDate3.run((int) (short) 10, (int) (short) 10, (int) ' ');
        java.lang.String str15 = nextDate3.run((int) (short) 1, (int) (byte) 0, (int) (byte) 100);
        java.lang.String str19 = nextDate3.run((-1), (-1), 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6913() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6913");
        NextDate nextDate3 = new NextDate(1, (int) (short) 100, 7);
        java.lang.String str7 = nextDate3.run(100, (int) (byte) -1, (int) (byte) 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6914() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6914");
        NextDate nextDate3 = new NextDate((int) (short) 10, 2001, 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6915() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6915");
        NextDate nextDate3 = new NextDate((-1), 2001, 30);
    }

    @Test
    public void test6916() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6916");
        NextDate nextDate3 = new NextDate((int) (short) -1, 30, (int) (byte) 100);
    }

    @Test
    public void test6917() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6917");
        NextDate nextDate3 = new NextDate(7, (int) (byte) 1, (int) (byte) 100);
    }

    @Test
    public void test6918() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6918");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 31, (int) '4');
        java.lang.String str7 = nextDate3.run((int) (short) 0, (int) (byte) 0, 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 100, (int) (byte) 1, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6919() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6919");
        NextDate nextDate3 = new NextDate(100, (int) (byte) -1, (int) (short) -1);
    }

    @Test
    public void test6920() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6920");
        NextDate nextDate3 = new NextDate(100, (int) (short) 10, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, 12, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6921() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6921");
        NextDate nextDate3 = new NextDate(10, 30, (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) 'a', (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6922() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6922");
        NextDate nextDate3 = new NextDate((int) ' ', 12, (int) (short) 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6923() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6923");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 0, 7);
        java.lang.String str7 = nextDate3.run((int) ' ', 0, (int) (short) 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6924() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6924");
        NextDate nextDate3 = new NextDate((int) ' ', (int) '#', (int) ' ');
        java.lang.String str7 = nextDate3.run(31, (int) (byte) 1, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run(0, 100, (int) (byte) 10);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6925() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6925");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) '#', 31);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6926() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6926");
        NextDate nextDate3 = new NextDate(100, 30, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) ' ', 7);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6927() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6927");
        NextDate nextDate3 = new NextDate(1, (int) (byte) 1, 30);
    }

    @Test
    public void test6928() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6928");
        NextDate nextDate3 = new NextDate(2001, (int) (short) 0, (int) (short) 10);
        java.lang.String str7 = nextDate3.run(2020, (int) (byte) 1, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6929() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6929");
        NextDate nextDate3 = new NextDate(12, (int) (byte) -1, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (byte) -1, 2001);
        java.lang.String str11 = nextDate3.run(2001, (int) (byte) 100, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6930() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6930");
        NextDate nextDate3 = new NextDate(12, (int) 'a', 31);
    }

    @Test
    public void test6931() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6931");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 1, 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, (int) (short) 0, (int) (short) 100);
        java.lang.String str11 = nextDate3.run(31, 2001, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6932() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6932");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run(0, 2001, 0);
        java.lang.String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        java.lang.String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        java.lang.String str19 = nextDate3.run((int) '4', 30, 0);
        java.lang.String str23 = nextDate3.run((int) (byte) 10, (int) (short) 100, 100);
        java.lang.String str27 = nextDate3.run(0, 100, 31);
        java.lang.String str31 = nextDate3.run(31, 31, (int) (short) 1);
        java.lang.Class<?> wildcardClass32 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass32);
    }

    @Test
    public void test6933() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6933");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) -1, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, (int) ' ', 0);
        java.lang.String str11 = nextDate3.run(30, (int) (byte) 1, 12);
        java.lang.String str15 = nextDate3.run(0, 2001, (int) ' ');
        java.lang.String str19 = nextDate3.run((int) (byte) 100, (-1), 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6934() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6934");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 100, (int) (short) 10);
        java.lang.String str7 = nextDate3.run(7, 100, (int) (byte) 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6935() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6935");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 100, 2020);
        java.lang.String str7 = nextDate3.run((int) (short) 0, (-1), 0);
        java.lang.String str11 = nextDate3.run((int) (short) -1, 100, 2020);
        java.lang.String str15 = nextDate3.run(0, 2001, (int) (byte) 100);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test6936() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6936");
        NextDate nextDate3 = new NextDate((int) (short) 10, 30, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (byte) 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6937() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6937");
        NextDate nextDate3 = new NextDate(7, 12, 10);
    }

    @Test
    public void test6938() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6938");
        NextDate nextDate3 = new NextDate(30, 0, 100);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6939() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6939");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) ' ', (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) '4', (int) '#', (int) (short) 0);
        java.lang.String str11 = nextDate3.run(0, (int) (byte) -1, (int) (short) 0);
        java.lang.String str15 = nextDate3.run((int) (short) 0, 30, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6940() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6940");
        NextDate nextDate3 = new NextDate(0, 7, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) (byte) 100, (int) (short) 0);
        java.lang.String str11 = nextDate3.run(0, (int) (byte) 100, 30);
        java.lang.String str15 = nextDate3.run((int) (short) -1, 0, 0);
        java.lang.String str19 = nextDate3.run((int) (short) 100, (int) (short) 100, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6941() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6941");
        NextDate nextDate3 = new NextDate(10, (int) 'a', 30);
    }

    @Test
    public void test6942() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6942");
        NextDate nextDate3 = new NextDate(2020, (int) (short) 10, (-1));
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6943() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6943");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 0, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) (short) 100, 7, (int) '4');
        java.lang.String str11 = nextDate3.run(2020, (int) 'a', 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6944() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6944");
        NextDate nextDate3 = new NextDate(1, 31, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run(2020, (int) '#', (int) 'a');
        java.lang.String str11 = nextDate3.run((int) ' ', (int) '4', (int) (byte) 100);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6945() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6945");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (short) 10, 31);
        java.lang.String str7 = nextDate3.run((int) (short) 100, 1, 1);
        java.lang.String str11 = nextDate3.run((int) (byte) 10, 2001, (int) ' ');
        java.lang.String str15 = nextDate3.run((int) (short) 100, 7, (int) (byte) 10);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test6946() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6946");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 10, (-1));
    }

    @Test
    public void test6947() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6947");
        NextDate nextDate3 = new NextDate((int) (short) 100, 12, 12);
    }

    @Test
    public void test6948() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6948");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 1, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (short) 1, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run(0, (int) ' ', 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6949() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6949");
        NextDate nextDate3 = new NextDate(1, 100, (int) ' ');
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) (short) 0, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6950() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6950");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) -1, (int) (short) 10);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6951() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6951");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) -1, (int) (short) -1);
        java.lang.String str7 = nextDate3.run(0, 7, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run(100, (int) '#', 12);
        java.lang.String str15 = nextDate3.run(2020, (int) (byte) 10, (int) (byte) 0);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test6952() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6952");
        NextDate nextDate3 = new NextDate(100, (int) (short) 100, 2001);
        java.lang.String str7 = nextDate3.run(2020, (int) (short) 0, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6953() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6953");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) ' ', 0);
        java.lang.String str7 = nextDate3.run(0, (int) (byte) 100, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6954() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6954");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) 10, 12);
    }

    @Test
    public void test6955() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6955");
        NextDate nextDate3 = new NextDate(31, (int) (byte) 10, 100);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (short) 1, (int) 'a');
        java.lang.String str11 = nextDate3.run((int) (byte) -1, 12, (int) 'a');
        java.lang.String str15 = nextDate3.run(31, 0, (int) (byte) 100);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test6956() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6956");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) ' ', (int) (short) 0);
        java.lang.String str7 = nextDate3.run(1, (int) '4', 30);
        java.lang.String str11 = nextDate3.run((int) (short) 1, (-1), 10);
        java.lang.String str15 = nextDate3.run((int) (short) 10, 30, 2020);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "10/31/2020" + "'", str15, "10/31/2020");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test6957() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6957");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 100, 1);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (byte) -1, 0);
        java.lang.String str11 = nextDate3.run((int) (short) 0, 100, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6958() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6958");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 0, (-1));
        java.lang.String str7 = nextDate3.run(7, (int) (byte) 1, 31);
        java.lang.String str11 = nextDate3.run(2001, 0, 1);
        java.lang.String str15 = nextDate3.run(2020, (-1), (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6959() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6959");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 1, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run(0, 100, (int) (short) 10);
        java.lang.String str11 = nextDate3.run((int) '4', 1, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6960() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6960");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) 10, 2020);
    }

    @Test
    public void test6961() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6961");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 1, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (short) 0, 2001);
        java.lang.String str11 = nextDate3.run(12, (int) (short) 1, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6962() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6962");
        NextDate nextDate3 = new NextDate(10, 0, 2001);
        java.lang.String str7 = nextDate3.run(0, 1, 0);
        java.lang.String str11 = nextDate3.run((int) (short) 1, (-1), (int) (short) 0);
        java.lang.String str15 = nextDate3.run((int) ' ', 10, 7);
        java.lang.String str19 = nextDate3.run((int) (short) -1, 7, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6963() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6963");
        NextDate nextDate3 = new NextDate(0, 1, (int) ' ');
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (byte) 100, 31);
        java.lang.String str11 = nextDate3.run((int) '4', 100, (int) (byte) 10);
        java.lang.String str15 = nextDate3.run(0, (int) (byte) 100, (int) (byte) 100);
        java.lang.String str19 = nextDate3.run((int) 'a', 2020, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6964() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6964");
        NextDate nextDate3 = new NextDate(7, 1, (int) (byte) 10);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6965() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6965");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) 1, (-1));
    }

    @Test
    public void test6966() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6966");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 10, 30);
        java.lang.String str7 = nextDate3.run(10, (int) (byte) 1, (int) (short) 0);
        java.lang.String str11 = nextDate3.run(0, (int) '#', (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6967() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6967");
        NextDate nextDate3 = new NextDate((int) (short) 1, 2001, 1);
        java.lang.String str7 = nextDate3.run((-1), (int) (short) 100, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6968() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6968");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) -1, (int) (short) 0);
    }

    @Test
    public void test6969() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6969");
        NextDate nextDate3 = new NextDate(0, (int) (short) 10, (int) 'a');
        java.lang.String str7 = nextDate3.run((int) (byte) -1, 100, (int) 'a');
        java.lang.String str11 = nextDate3.run((int) ' ', 0, 10);
        java.lang.String str15 = nextDate3.run((int) (byte) -1, (int) '#', (int) (short) -1);
        java.lang.String str19 = nextDate3.run((int) (short) 10, (int) (byte) 0, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6970() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6970");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) '#', 2020);
        java.lang.String str7 = nextDate3.run((int) '#', (int) 'a', (int) '#');
        java.lang.String str11 = nextDate3.run(2001, (int) (byte) 1, (int) (byte) 0);
        java.lang.String str15 = nextDate3.run(10, 1, (int) (byte) 1);
        java.lang.String str19 = nextDate3.run((int) '#', 12, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6971() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6971");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) ' ', (int) (short) 10);
        java.lang.String str7 = nextDate3.run(7, (int) 'a', (int) (byte) 1);
        java.lang.String str11 = nextDate3.run((int) '4', 2020, (int) (short) 0);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6972() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6972");
        NextDate nextDate3 = new NextDate(30, (int) '#', (int) ' ');
        java.lang.String str7 = nextDate3.run(0, 100, (int) (short) 1);
        java.lang.String str11 = nextDate3.run(12, (int) (short) 1, 1);
        java.lang.String str15 = nextDate3.run(10, (int) (byte) 10, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6973() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6973");
        NextDate nextDate3 = new NextDate(31, 0, (-1));
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (byte) 100, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6974() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6974");
        NextDate nextDate3 = new NextDate((int) (short) 100, 0, 0);
        java.lang.String str7 = nextDate3.run(31, (int) (byte) -1, (int) (short) 100);
        java.lang.String str11 = nextDate3.run(0, (int) (short) 0, 0);
        java.lang.String str15 = nextDate3.run(30, 31, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6975() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6975");
        NextDate nextDate3 = new NextDate((int) ' ', 0, 31);
    }

    @Test
    public void test6976() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6976");
        NextDate nextDate3 = new NextDate((int) 'a', (int) ' ', 12);
    }

    @Test
    public void test6977() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6977");
        NextDate nextDate3 = new NextDate(0, (-1), 2020);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6978() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6978");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 0, 2020);
        java.lang.String str7 = nextDate3.run(0, (int) (byte) 1, (int) '4');
        java.lang.String str11 = nextDate3.run(31, 30, 30);
        java.lang.String str15 = nextDate3.run((int) (byte) 100, 1, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6979() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6979");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) '#', (int) (byte) 100);
        java.lang.String str7 = nextDate3.run(100, (int) (byte) 0, 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 100, 30, 0);
        java.lang.String str15 = nextDate3.run((int) (short) 1, 100, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6980() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6980");
        NextDate nextDate3 = new NextDate(100, 0, 10);
        java.lang.String str7 = nextDate3.run(12, 1, 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6981() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6981");
        NextDate nextDate3 = new NextDate((int) (short) 0, 10, (int) '#');
    }

    @Test
    public void test6982() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6982");
        NextDate nextDate3 = new NextDate(7, 1, (int) '#');
    }

    @Test
    public void test6983() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6983");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 1, 0);
        java.lang.String str7 = nextDate3.run((int) '4', 0, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run(31, 0, (int) (byte) -1);
        java.lang.String str15 = nextDate3.run(2001, 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6984() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6984");
        NextDate nextDate3 = new NextDate(10, 10, (int) ' ');
        java.lang.String str7 = nextDate3.run((int) '4', (int) 'a', (-1));
        java.lang.String str11 = nextDate3.run(100, 100, (int) '4');
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6985() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6985");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) '#', (int) ' ');
        java.lang.String str7 = nextDate3.run((-1), 1, (-1));
        java.lang.String str11 = nextDate3.run(100, 10, (int) (short) 100);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6986() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6986");
        NextDate nextDate3 = new NextDate((int) (short) 10, 2001, 100);
    }

    @Test
    public void test6987() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6987");
        NextDate nextDate3 = new NextDate((int) (short) 100, 1, (int) '4');
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) ' ', (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6988() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6988");
        NextDate nextDate3 = new NextDate((int) (short) 100, 2020, (-1));
    }

    @Test
    public void test6989() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6989");
        NextDate nextDate3 = new NextDate(1, (int) (short) 0, 2020);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, 0, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6990() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6990");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 10, (int) '#');
        java.lang.String str7 = nextDate3.run(10, (int) (byte) 100, 0);
        java.lang.String str11 = nextDate3.run(12, (int) (byte) 1, (int) (byte) 100);
        java.lang.String str15 = nextDate3.run((int) (short) 0, (int) ' ', (int) ' ');
        java.lang.String str19 = nextDate3.run((int) '4', 12, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6991() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6991");
        NextDate nextDate3 = new NextDate(2020, (int) (short) -1, (int) '#');
    }

    @Test
    public void test6992() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6992");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 1, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) (byte) -1, 7);
        java.lang.String str11 = nextDate3.run((int) (short) 10, 30, (int) '#');
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6993() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6993");
        NextDate nextDate3 = new NextDate(7, (int) '#', 7);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6994() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6994");
        NextDate nextDate3 = new NextDate(1, 30, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run(2001, (int) '#', (int) '4');
        java.lang.String str11 = nextDate3.run((int) (byte) 100, (int) '#', 2020);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6995() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6995");
        NextDate nextDate3 = new NextDate(1, (int) (byte) -1, 100);
    }

    @Test
    public void test6996() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6996");
        NextDate nextDate3 = new NextDate((int) '4', 30, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run(100, (int) (short) 100, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6997() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6997");
        NextDate nextDate3 = new NextDate(0, 1, (int) ' ');
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (byte) 100, 31);
        java.lang.String str11 = nextDate3.run((int) '4', (int) (byte) 1, (int) (byte) 10);
        java.lang.String str15 = nextDate3.run((int) (byte) 0, (int) (byte) -1, 30);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test6998() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6998");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) 100, (int) '#');
    }

    @Test
    public void test6999() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test6999");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 0, (-1));
    }

    @Test
    public void test7000() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest13.test7000");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 0, (-1));
        java.lang.String str7 = nextDate3.run((int) (short) 10, 30, (-1));
        java.lang.String str11 = nextDate3.run((int) '#', (int) (short) -1, (int) (byte) 10);
        java.lang.String str15 = nextDate3.run((int) (byte) 10, 1, (int) 'a');
        java.lang.String str19 = nextDate3.run(30, (int) (short) -1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }
}

