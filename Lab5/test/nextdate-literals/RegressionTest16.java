import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest16 {

    public static boolean debug = false;

    @Test
    public void test8001() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8001");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 0, (int) (short) -1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8002() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8002");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 10, 1);
        java.lang.String str7 = nextDate3.run((int) 'a', 2020, (int) (short) -1);
        java.lang.String str11 = nextDate3.run(2020, (int) (short) 1, (int) (byte) 100);
        java.lang.String str15 = nextDate3.run((int) (short) 0, (int) 'a', 31);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test8003() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8003");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) 0, 0);
    }

    @Test
    public void test8004() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8004");
        NextDate nextDate3 = new NextDate((-1), (int) 'a', (int) '4');
        java.lang.String str7 = nextDate3.run((int) (short) 10, 0, (int) (short) 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 100, (int) (short) -1, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8005() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8005");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) '4', (int) (short) 1);
    }

    @Test
    public void test8006() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8006");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 100, 0);
        java.lang.String str7 = nextDate3.run((-1), (int) (short) 1, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((int) (short) 1, (int) (byte) 100, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8007() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8007");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (byte) 1, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) '#', 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8008() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8008");
        NextDate nextDate3 = new NextDate(30, (int) (short) -1, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, 2020, (int) '4');
        java.lang.String str11 = nextDate3.run((int) (short) 1, (int) (short) 0, (int) (short) 100);
        java.lang.String str15 = nextDate3.run((int) ' ', 2001, (int) 'a');
        java.lang.String str19 = nextDate3.run((int) (short) 10, 0, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test8009() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8009");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 1, (int) 'a');
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (short) 0, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8010() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8010");
        NextDate nextDate3 = new NextDate(0, (int) (short) 0, 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8011() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8011");
        NextDate nextDate3 = new NextDate((int) (short) -1, 2020, (int) (short) 100);
        java.lang.String str7 = nextDate3.run(10, (int) 'a', (int) (byte) 0);
        java.lang.String str11 = nextDate3.run((int) (short) 10, (int) (short) -1, (int) 'a');
        java.lang.String str15 = nextDate3.run((int) 'a', 12, 2020);
        java.lang.String str19 = nextDate3.run((int) '#', 2020, (int) ' ');
        java.lang.String str23 = nextDate3.run((int) (short) 0, (int) '#', 1);
        java.lang.String str27 = nextDate3.run(1, 7, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test8012() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8012");
        NextDate nextDate3 = new NextDate(1, 7, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (short) 0, 2001, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8013() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8013");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 1, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run(1, (int) (short) 1, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "1/2/2020" + "'", str7, "1/2/2020");
    }

    @Test
    public void test8014() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8014");
        NextDate nextDate3 = new NextDate((int) (short) 10, 12, (int) ' ');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8015() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8015");
        NextDate nextDate3 = new NextDate((int) (short) 0, 0, (int) ' ');
        java.lang.String str7 = nextDate3.run(31, (int) (short) 100, (int) (short) 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test8016() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8016");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 100, (int) (byte) 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8017() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8017");
        NextDate nextDate3 = new NextDate(12, (int) (byte) 1, (int) ' ');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8018() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8018");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) 'a', (int) '#');
        java.lang.String str7 = nextDate3.run((int) (short) 1, 0, 1);
        java.lang.String str11 = nextDate3.run((int) (short) 100, (int) (byte) 10, 0);
        java.lang.String str15 = nextDate3.run(0, (int) '#', (int) '4');
        java.lang.String str19 = nextDate3.run((int) (short) 100, 12, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test8019() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8019");
        NextDate nextDate3 = new NextDate((int) '4', 31, (int) ' ');
    }

    @Test
    public void test8020() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8020");
        NextDate nextDate3 = new NextDate((int) (short) 0, 30, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run(0, 10, (-1));
        java.lang.String str11 = nextDate3.run((int) (short) 0, (int) (short) 1, (int) ' ');
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test8021() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8021");
        NextDate nextDate3 = new NextDate(1, 1, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, 0, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8022() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8022");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) 10, 10);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8023() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8023");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) '#', (int) ' ');
        java.lang.String str7 = nextDate3.run((-1), 1, (-1));
        java.lang.String str11 = nextDate3.run((int) (byte) 1, 12, (int) (short) 100);
        java.lang.String str15 = nextDate3.run(2020, 12, 10);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test8024() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8024");
        NextDate nextDate3 = new NextDate(0, (int) '4', (int) (short) 10);
        java.lang.String str7 = nextDate3.run(7, (int) (short) 1, (int) 'a');
        java.lang.String str11 = nextDate3.run(30, 10, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8025() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8025");
        NextDate nextDate3 = new NextDate(31, (-1), 0);
        java.lang.String str7 = nextDate3.run(12, 31, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8026() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8026");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) '4', 2001);
        java.lang.String str7 = nextDate3.run(7, 0, 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8027() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8027");
        NextDate nextDate3 = new NextDate(0, 2001, 0);
    }

    @Test
    public void test8028() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8028");
        NextDate nextDate3 = new NextDate((int) (short) 10, 2001, (int) (short) 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8029() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8029");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 2001, 0);
        java.lang.String str7 = nextDate3.run(7, (int) (short) 10, 31);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8030() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8030");
        NextDate nextDate3 = new NextDate(2020, (int) '4', (int) (short) 1);
    }

    @Test
    public void test8031() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8031");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 1, 2001);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) 'a', 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8032() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8032");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) 100, 12);
    }

    @Test
    public void test8033() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8033");
        NextDate nextDate3 = new NextDate((int) '4', (int) (byte) -1, 0);
    }

    @Test
    public void test8034() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8034");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) ' ', 2001);
        java.lang.String str7 = nextDate3.run((int) (short) 0, (int) 'a', (int) (byte) 100);
        java.lang.String str11 = nextDate3.run((int) (short) 1, 7, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8035() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8035");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 7, 12);
        java.lang.String str7 = nextDate3.run(0, (int) (short) 1, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8036() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8036");
        NextDate nextDate3 = new NextDate(2001, 100, 1);
        java.lang.String str7 = nextDate3.run(7, 0, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8037() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8037");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) 1, 30);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8038() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8038");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 0, 1);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, 1, (int) (short) 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8039() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8039");
        NextDate nextDate3 = new NextDate(12, 2020, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (byte) -1, (int) (byte) 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8040() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8040");
        NextDate nextDate3 = new NextDate((int) (short) 1, 0, (int) (short) 1);
    }

    @Test
    public void test8041() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8041");
        NextDate nextDate3 = new NextDate(0, 30, 100);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) '4', (int) 'a');
        java.lang.String str11 = nextDate3.run(2001, (int) (byte) 0, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8042() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8042");
        NextDate nextDate3 = new NextDate(30, 31, 12);
        java.lang.String str7 = nextDate3.run((int) (short) 1, 0, 2001);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8043() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8043");
        NextDate nextDate3 = new NextDate(0, (int) 'a', 31);
        java.lang.String str7 = nextDate3.run((int) 'a', 12, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8044() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8044");
        NextDate nextDate3 = new NextDate(0, 2020, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run(1, (int) '#', (int) (short) -1);
        java.lang.String str11 = nextDate3.run((int) (byte) 100, 0, (int) ' ');
        java.lang.String str15 = nextDate3.run(31, (int) (byte) 0, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8045() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8045");
        NextDate nextDate3 = new NextDate(31, (int) (byte) 1, (int) ' ');
        java.lang.String str7 = nextDate3.run(0, (int) (byte) -1, 1);
        java.lang.String str11 = nextDate3.run(0, (int) (short) 10, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8046() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8046");
        NextDate nextDate3 = new NextDate((int) (short) 0, 10, (int) (byte) 10);
    }

    @Test
    public void test8047() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8047");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) 'a', 2020);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8048() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8048");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run((int) ' ', 2020, (int) (short) 0);
        java.lang.String str11 = nextDate3.run((int) (byte) -1, (int) (byte) -1, (int) (short) 0);
        java.lang.String str15 = nextDate3.run((-1), (int) (byte) 10, 31);
        java.lang.String str19 = nextDate3.run((int) (short) 1, (int) (byte) 0, 7);
        java.lang.String str23 = nextDate3.run(2020, 100, 1);
        java.lang.String str27 = nextDate3.run(0, 2020, 100);
        java.lang.Class<?> wildcardClass28 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test8049() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8049");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) '4', 100);
    }

    @Test
    public void test8050() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8050");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) ' ', 10);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (byte) 100, (int) (byte) 1);
        java.lang.String str11 = nextDate3.run(0, 10, 2001);
        java.lang.String str15 = nextDate3.run((int) (short) 100, (int) (short) -1, (int) 'a');
        java.lang.String str19 = nextDate3.run((int) (short) 100, (int) (short) 100, (int) (short) 1);
        java.lang.Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test8051() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8051");
        NextDate nextDate3 = new NextDate((int) (short) 100, (-1), (int) (short) 1);
    }

    @Test
    public void test8052() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8052");
        NextDate nextDate3 = new NextDate((int) '4', (int) '4', 1);
        java.lang.String str7 = nextDate3.run(0, (int) (short) 10, 1);
        java.lang.String str11 = nextDate3.run((int) 'a', 31, 0);
        java.lang.String str15 = nextDate3.run((-1), 2020, (int) (short) 0);
        java.lang.String str19 = nextDate3.run((int) (short) -1, (-1), 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test8053() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8053");
        NextDate nextDate3 = new NextDate(7, 100, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) ' ', (int) (short) -1);
        java.lang.String str11 = nextDate3.run((int) (short) -1, (int) 'a', (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8054() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8054");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 31, (int) '#');
        java.lang.String str7 = nextDate3.run((int) (short) 100, 31, 10);
        java.lang.String str11 = nextDate3.run(0, (int) (short) 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8055() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8055");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run(0, 2001, 0);
        java.lang.String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        java.lang.String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        java.lang.String str19 = nextDate3.run(31, 31, 12);
        java.lang.String str23 = nextDate3.run((int) (short) 1, (int) 'a', (int) (short) 10);
        java.lang.String str27 = nextDate3.run(12, (int) (byte) 10, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test8056() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8056");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 100, (int) (short) 1);
    }

    @Test
    public void test8057() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8057");
        NextDate nextDate3 = new NextDate(7, 0, (int) (byte) 10);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8058() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8058");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 0, (-1));
    }

    @Test
    public void test8059() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8059");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) 1, (int) '#');
        java.lang.String str7 = nextDate3.run(1, (int) (byte) -1, (int) (short) 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8060() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8060");
        NextDate nextDate3 = new NextDate((int) (short) 0, 0, (int) (short) 0);
        java.lang.String str7 = nextDate3.run(2001, 100, (int) (short) -1);
        java.lang.String str11 = nextDate3.run(10, (int) 'a', (int) (byte) -1);
        java.lang.String str15 = nextDate3.run((int) (byte) 1, 31, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8061() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8061");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 10, (int) '4');
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (byte) 0, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8062() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8062");
        NextDate nextDate3 = new NextDate((int) '4', 2020, (int) (short) 100);
    }

    @Test
    public void test8063() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8063");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) '4', 30);
        java.lang.String str7 = nextDate3.run((int) 'a', 2001, 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8064() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8064");
        NextDate nextDate3 = new NextDate((int) '4', 2020, (int) (byte) 100);
    }

    @Test
    public void test8065() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8065");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 12, 10);
        java.lang.String str7 = nextDate3.run(2020, (int) (short) 1, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8066() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8066");
        NextDate nextDate3 = new NextDate(31, (int) (short) -1, (int) (short) -1);
    }

    @Test
    public void test8067() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8067");
        NextDate nextDate3 = new NextDate(31, (int) (short) -1, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, (int) (byte) 1, (int) '4');
        java.lang.String str11 = nextDate3.run((int) '4', (int) (byte) 1, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8068() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8068");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 1, 31);
        java.lang.String str7 = nextDate3.run(100, 0, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8069() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8069");
        NextDate nextDate3 = new NextDate(1, 2001, 30);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8070() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8070");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 10, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run(100, (int) (short) 10, (int) (byte) 100);
        java.lang.String str11 = nextDate3.run(12, 2001, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8071() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8071");
        NextDate nextDate3 = new NextDate((int) (short) -1, 30, 12);
        java.lang.String str7 = nextDate3.run(30, (int) (byte) 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8072() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8072");
        NextDate nextDate3 = new NextDate(2001, 100, 2020);
        java.lang.String str7 = nextDate3.run(0, 0, 12);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8073() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8073");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 100, (-1));
    }

    @Test
    public void test8074() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8074");
        NextDate nextDate3 = new NextDate(1, (int) (short) 100, 7);
        java.lang.String str7 = nextDate3.run(100, (int) (byte) -1, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run((int) (short) 0, (int) 'a', (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8075() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8075");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run(0, 2001, 0);
        java.lang.String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        java.lang.String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        java.lang.String str19 = nextDate3.run((int) '4', 30, 0);
        java.lang.String str23 = nextDate3.run(7, (int) (short) 10, (int) (short) 1);
        java.lang.String str27 = nextDate3.run((int) 'a', (int) (byte) 1, 0);
        java.lang.String str31 = nextDate3.run((int) (byte) 0, (int) ' ', (int) (short) 10);
        java.lang.String str35 = nextDate3.run((int) ' ', (int) (short) 1, (int) (byte) 1);
        java.lang.String str39 = nextDate3.run(100, 2020, 2020);
        java.lang.String str43 = nextDate3.run((int) (byte) 10, 30, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str43 + "' != '" + "invalid Input Date" + "'", str43, "invalid Input Date");
    }

    @Test
    public void test8076() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8076");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 10, 12);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (short) -1, (int) (byte) 1);
        java.lang.String str11 = nextDate3.run((int) (short) -1, (int) (short) 100, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8077() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8077");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 0, 2001);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, 2020, (int) (short) 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8078() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8078");
        NextDate nextDate3 = new NextDate(0, 2001, (-1));
    }

    @Test
    public void test8079() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8079");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (short) 10, 0);
        java.lang.String str7 = nextDate3.run(12, 100, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8080() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8080");
        NextDate nextDate3 = new NextDate(0, 1, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (byte) 100, (int) '4');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8081() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8081");
        NextDate nextDate3 = new NextDate(12, (int) (byte) 100, 31);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (short) 100, (int) (short) 1);
        java.lang.String str11 = nextDate3.run(12, (int) (byte) 0, (int) (short) 10);
        java.lang.String str15 = nextDate3.run(2020, (int) '4', (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8082() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8082");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 0, 0);
    }

    @Test
    public void test8083() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8083");
        NextDate nextDate3 = new NextDate((int) (short) 10, 1, (int) ' ');
        java.lang.String str7 = nextDate3.run((int) '#', 100, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8084() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8084");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) -1, 2001);
        java.lang.String str7 = nextDate3.run(0, 7, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8085() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8085");
        NextDate nextDate3 = new NextDate(0, (int) (short) -1, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) (short) 1, 30, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8086() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8086");
        NextDate nextDate3 = new NextDate(7, 12, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (byte) 100, 2020);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "11/1/2020" + "'", str7, "11/1/2020");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8087() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8087");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run(0, 2001, 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        java.lang.String str15 = nextDate3.run(12, (int) (byte) 0, (int) (byte) -1);
        java.lang.String str19 = nextDate3.run((int) (short) 10, (int) '4', 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test8088() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8088");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (byte) 100, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (byte) -1, 2020);
        java.lang.String str11 = nextDate3.run(7, (int) (byte) 10, 100);
        java.lang.String str15 = nextDate3.run((int) (byte) 0, (int) (short) 1, 10);
        java.lang.String str19 = nextDate3.run((int) 'a', (int) (short) 1, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test8089() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8089");
        NextDate nextDate3 = new NextDate((int) ' ', (int) ' ', (int) (byte) -1);
        java.lang.String str7 = nextDate3.run(10, (int) (short) 100, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8090() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8090");
        NextDate nextDate3 = new NextDate(10, 7, 7);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8091() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8091");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) -1, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) '#', (int) ' ', 2020);
        java.lang.String str11 = nextDate3.run(2001, 12, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8092() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8092");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 0, (int) (short) -1);
        java.lang.String str7 = nextDate3.run(2020, 10, (int) (short) 0);
        java.lang.String str11 = nextDate3.run((int) (short) 10, 1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8093() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8093");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) -1, (int) (short) 100);
        java.lang.String str7 = nextDate3.run(0, (int) (byte) 1, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8094() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8094");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) '#', (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) '#', (int) (byte) 100, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8095() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8095");
        NextDate nextDate3 = new NextDate((-1), 2001, 2020);
    }

    @Test
    public void test8096() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8096");
        NextDate nextDate3 = new NextDate((-1), 1, (-1));
        java.lang.String str7 = nextDate3.run(1, (int) (byte) 10, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "1/11/2001" + "'", str7, "1/11/2001");
    }

    @Test
    public void test8097() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8097");
        NextDate nextDate3 = new NextDate(12, 2001, 2020);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, 0, (int) (short) 100);
        java.lang.String str11 = nextDate3.run(0, 31, 12);
        java.lang.String str15 = nextDate3.run((int) (byte) 10, 30, (int) '#');
        java.lang.String str19 = nextDate3.run((int) (byte) 1, 10, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test8098() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8098");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) '#', (int) (short) 100);
    }

    @Test
    public void test8099() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8099");
        NextDate nextDate3 = new NextDate(12, (int) (short) 10, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (short) 100, 2020);
        java.lang.String str11 = nextDate3.run((int) (short) 1, 0, (int) (short) 10);
        java.lang.String str15 = nextDate3.run((int) (short) 10, 2001, 0);
        java.lang.String str19 = nextDate3.run(31, (int) '#', 0);
        java.lang.String str23 = nextDate3.run(0, 100, 0);
        java.lang.String str27 = nextDate3.run(10, (int) (short) 1, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test8100() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8100");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) 'a', 12);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8101() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8101");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) 1, (int) 'a');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8102() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8102");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) -1, 0);
    }

    @Test
    public void test8103() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8103");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) ' ', 31);
        java.lang.String str7 = nextDate3.run(10, (int) (short) -1, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8104() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8104");
        NextDate nextDate3 = new NextDate(1, 10, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) (byte) 0, 31);
        java.lang.String str11 = nextDate3.run(2020, 30, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8105() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8105");
        NextDate nextDate3 = new NextDate(7, (int) (short) 10, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, 100, (int) (short) -1);
        java.lang.String str11 = nextDate3.run(0, (int) (short) 10, 12);
        java.lang.String str15 = nextDate3.run(0, 100, (int) 'a');
        java.lang.String str19 = nextDate3.run((int) (short) 1, (int) (short) 0, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test8106() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8106");
        NextDate nextDate3 = new NextDate(100, 100, 2020);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8107() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8107");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 10, 12);
    }

    @Test
    public void test8108() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8108");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) 0, 10);
    }

    @Test
    public void test8109() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8109");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (short) 10, (int) (short) 0);
    }

    @Test
    public void test8110() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8110");
        NextDate nextDate3 = new NextDate(30, 2001, 2001);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, 2001, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8111() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8111");
        NextDate nextDate3 = new NextDate(12, (int) (short) 10, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (short) 100, 2020);
        java.lang.String str11 = nextDate3.run((int) (short) 1, 0, (int) (short) 10);
        java.lang.String str15 = nextDate3.run((int) (short) 10, 2001, 0);
        java.lang.String str19 = nextDate3.run((int) (byte) 10, (-1), (int) (byte) 0);
        java.lang.String str23 = nextDate3.run(7, 0, (int) (short) -1);
        java.lang.String str27 = nextDate3.run(7, 2020, (int) ' ');
        java.lang.String str31 = nextDate3.run(30, (int) (short) 1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
    }

    @Test
    public void test8112() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8112");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 100, 0);
    }

    @Test
    public void test8113() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8113");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) 100, (int) '#');
        java.lang.String str7 = nextDate3.run((int) (short) 100, 30, 1);
        java.lang.String str11 = nextDate3.run(2020, 7, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8114() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8114");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (short) 1, 100);
    }

    @Test
    public void test8115() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8115");
        NextDate nextDate3 = new NextDate(31, (int) (short) -1, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) (byte) 10, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8116() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8116");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) 10, (int) '#');
        java.lang.String str7 = nextDate3.run(2020, 31, (int) (short) 100);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8117() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8117");
        NextDate nextDate3 = new NextDate(30, (int) (short) 10, (int) (short) 10);
    }

    @Test
    public void test8118() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8118");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) 'a', 1);
        java.lang.String str7 = nextDate3.run(1, 100, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8119() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8119");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run((int) ' ', 2020, (int) (short) 0);
        java.lang.String str11 = nextDate3.run(31, (int) '4', (int) (byte) -1);
        java.lang.String str15 = nextDate3.run((int) (byte) 100, 7, (int) (short) 100);
        java.lang.String str19 = nextDate3.run(1, 31, 2001);
        java.lang.String str23 = nextDate3.run(0, 30, 30);
        java.lang.String str27 = nextDate3.run((int) (short) 10, (int) 'a', (int) (short) 0);
        java.lang.String str31 = nextDate3.run((int) 'a', (int) (byte) 100, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "2/1/2001" + "'", str19, "2/1/2001");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
    }

    @Test
    public void test8120() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8120");
        NextDate nextDate3 = new NextDate((int) ' ', 0, (int) 'a');
        java.lang.String str7 = nextDate3.run((-1), 10, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8121() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8121");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 100, 30);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, 30, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8122() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8122");
        NextDate nextDate3 = new NextDate(31, (int) (short) -1, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (byte) -1, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8123() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8123");
        NextDate nextDate3 = new NextDate(0, (int) (short) 0, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) (byte) 0, (int) (short) 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8124() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8124");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 0, 7);
        java.lang.String str7 = nextDate3.run((int) ' ', 0, (int) (short) 1);
        java.lang.String str11 = nextDate3.run(2001, (int) (short) -1, (int) ' ');
        java.lang.String str15 = nextDate3.run((int) ' ', (int) (short) 0, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8125() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8125");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 31, 10);
        java.lang.String str7 = nextDate3.run((-1), (int) (short) 1, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8126() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8126");
        NextDate nextDate3 = new NextDate((int) '#', 2001, (int) (short) 100);
        java.lang.String str7 = nextDate3.run(1, 7, 12);
        java.lang.String str11 = nextDate3.run((int) (short) 0, 12, 7);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test8127() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8127");
        NextDate nextDate3 = new NextDate((int) 'a', 1, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) ' ', (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8128() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8128");
        NextDate nextDate3 = new NextDate(2020, (int) (short) 100, (int) (short) 10);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8129() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8129");
        NextDate nextDate3 = new NextDate((int) (short) 100, 30, 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8130() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8130");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 1, 30);
        java.lang.String str7 = nextDate3.run((int) (short) 10, 0, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8131() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8131");
        NextDate nextDate3 = new NextDate((int) (short) 100, 0, 2020);
    }

    @Test
    public void test8132() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8132");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) '4', (int) (byte) 0);
        java.lang.String str7 = nextDate3.run(12, 0, (int) 'a');
        java.lang.String str11 = nextDate3.run((int) (short) 100, (int) (byte) -1, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8133() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8133");
        NextDate nextDate3 = new NextDate((int) (short) 0, 100, (int) '4');
        java.lang.String str7 = nextDate3.run(10, 1, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8134() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8134");
        NextDate nextDate3 = new NextDate((int) '#', (int) 'a', 2001);
        java.lang.String str7 = nextDate3.run((int) (short) -1, 100, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8135() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8135");
        NextDate nextDate3 = new NextDate(0, (int) (short) 1, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (short) 0, 100, (-1));
        java.lang.String str11 = nextDate3.run((int) (byte) 100, 7, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8136() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8136");
        NextDate nextDate3 = new NextDate((int) 'a', 100, 30);
    }

    @Test
    public void test8137() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8137");
        NextDate nextDate3 = new NextDate((int) '#', (int) '4', (int) '4');
    }

    @Test
    public void test8138() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8138");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) -1, 31);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (byte) -1, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8139() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8139");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 12, (int) '4');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8140() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8140");
        NextDate nextDate3 = new NextDate(1, (int) (short) -1, (int) (byte) 100);
    }

    @Test
    public void test8141() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8141");
        NextDate nextDate3 = new NextDate(100, 10, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) ' ', (int) '4');
        java.lang.String str11 = nextDate3.run((int) (byte) -1, (int) (byte) 0, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8142() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8142");
        NextDate nextDate3 = new NextDate(7, 0, (int) '#');
    }

    @Test
    public void test8143() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8143");
        NextDate nextDate3 = new NextDate((int) (short) -1, 100, (int) (short) -1);
    }

    @Test
    public void test8144() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8144");
        NextDate nextDate3 = new NextDate((int) (short) 0, 12, (int) (short) 10);
    }

    @Test
    public void test8145() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8145");
        NextDate nextDate3 = new NextDate(30, 10, (int) '4');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8146() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8146");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 2001, (int) ' ');
        java.lang.String str7 = nextDate3.run((int) (byte) 1, 0, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8147() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8147");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run((int) ' ', 2020, (int) (short) 0);
        java.lang.String str11 = nextDate3.run(30, (int) (byte) 100, (int) (byte) 100);
        java.lang.String str15 = nextDate3.run(12, (int) (byte) 1, 30);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test8148() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8148");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (short) -1, 2020);
    }

    @Test
    public void test8149() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8149");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run((int) ' ', 2020, (int) (short) 0);
        java.lang.String str11 = nextDate3.run(30, (int) (byte) 100, (int) (byte) 100);
        java.lang.String str15 = nextDate3.run((int) (byte) 100, 31, (int) (short) -1);
        java.lang.String str19 = nextDate3.run((int) (byte) 100, (-1), (int) (short) -1);
        java.lang.String str23 = nextDate3.run(2001, 31, 12);
        java.lang.Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test8150() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8150");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 1, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, 0, (int) (byte) -1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8151() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8151");
        NextDate nextDate3 = new NextDate(10, 0, 30);
        java.lang.String str7 = nextDate3.run(7, 10, 10);
        java.lang.String str11 = nextDate3.run((int) (short) 1, (int) (short) -1, (int) (short) 100);
        java.lang.String str15 = nextDate3.run(0, (int) (short) -1, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8152() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8152");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 2020, 12);
        java.lang.String str7 = nextDate3.run(0, 30, (int) (byte) 100);
        java.lang.String str11 = nextDate3.run(30, (int) ' ', (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8153() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8153");
        NextDate nextDate3 = new NextDate(1, 1, (int) '4');
    }

    @Test
    public void test8154() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8154");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) -1, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) (byte) 0, 2001);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8155() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8155");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) '#', (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (short) 0, 2001);
        java.lang.String str11 = nextDate3.run((int) (short) -1, 30, 2020);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test8156() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8156");
        NextDate nextDate3 = new NextDate(12, (int) 'a', (int) (short) 10);
    }

    @Test
    public void test8157() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8157");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 10, (int) (byte) 100);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8158() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8158");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) ' ', (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) '#', (int) (byte) -1, (int) (short) 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8159() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8159");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 1, (int) (byte) -1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8160() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8160");
        NextDate nextDate3 = new NextDate(100, 2001, 100);
    }

    @Test
    public void test8161() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8161");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 0, (int) (byte) 0);
    }

    @Test
    public void test8162() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8162");
        NextDate nextDate3 = new NextDate((int) '#', (int) ' ', (int) (byte) 1);
        java.lang.String str7 = nextDate3.run(0, 7, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8163() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8163");
        NextDate nextDate3 = new NextDate(12, 30, (int) '#');
    }

    @Test
    public void test8164() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8164");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) 1, (int) (byte) -1);
    }

    @Test
    public void test8165() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8165");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 1, (int) (short) 100);
    }

    @Test
    public void test8166() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8166");
        NextDate nextDate3 = new NextDate((int) (short) 1, 0, (int) ' ');
        java.lang.String str7 = nextDate3.run(100, 30, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run((int) (short) 100, (int) '#', (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8167() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8167");
        NextDate nextDate3 = new NextDate((int) (short) -1, 2020, 0);
    }

    @Test
    public void test8168() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8168");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 31, (-1));
    }

    @Test
    public void test8169() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8169");
        NextDate nextDate3 = new NextDate((int) (short) 100, 12, (int) (short) 0);
        java.lang.String str7 = nextDate3.run(2001, (int) 'a', 1);
        java.lang.String str11 = nextDate3.run((int) (short) 10, (int) (byte) 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8170() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8170");
        NextDate nextDate3 = new NextDate(0, 30, (int) (short) -1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8171() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8171");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) ' ', (int) '#');
    }

    @Test
    public void test8172() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8172");
        NextDate nextDate3 = new NextDate(10, (int) '#', 2020);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) ' ', 31);
        java.lang.String str11 = nextDate3.run(12, (int) '4', 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "1/1/2002" + "'", str11, "1/1/2002");
    }

    @Test
    public void test8173() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8173");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 1, (int) (short) 10);
        java.lang.String str7 = nextDate3.run(1, 30, (int) (byte) 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8174() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8174");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 100, 10);
    }

    @Test
    public void test8175() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8175");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 12, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (byte) 0, 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8176() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8176");
        NextDate nextDate3 = new NextDate(0, 30, (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, 1, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8177() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8177");
        NextDate nextDate3 = new NextDate(31, (-1), (int) '#');
    }

    @Test
    public void test8178() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8178");
        NextDate nextDate3 = new NextDate(2020, (int) (short) 0, (int) (byte) 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8179() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8179");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 2001, 31);
        java.lang.String str7 = nextDate3.run((-1), (int) '#', (int) '4');
        java.lang.String str11 = nextDate3.run((int) (short) 100, (int) (byte) 10, (int) (byte) -1);
        java.lang.String str15 = nextDate3.run((int) '#', (int) (short) 10, (int) (byte) 100);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test8180() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8180");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 10, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (byte) 100, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8181() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8181");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run(0, 2001, 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        java.lang.String str15 = nextDate3.run(7, (int) (short) 1, 30);
        java.lang.String str19 = nextDate3.run(30, (int) 'a', (int) (short) 0);
        java.lang.String str23 = nextDate3.run(31, 100, 12);
        java.lang.Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test8182() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8182");
        NextDate nextDate3 = new NextDate(10, 10, (int) (short) 100);
    }

    @Test
    public void test8183() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8183");
        NextDate nextDate3 = new NextDate(100, 12, 1);
    }

    @Test
    public void test8184() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8184");
        NextDate nextDate3 = new NextDate(31, (int) (byte) 10, (int) 'a');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8185() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8185");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 100, (-1));
    }

    @Test
    public void test8186() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8186");
        NextDate nextDate3 = new NextDate(31, (int) 'a', 12);
    }

    @Test
    public void test8187() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8187");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 7, 0);
    }

    @Test
    public void test8188() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8188");
        NextDate nextDate3 = new NextDate(100, 7, (int) 'a');
    }

    @Test
    public void test8189() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8189");
        NextDate nextDate3 = new NextDate(2020, 12, (-1));
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8190() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8190");
        NextDate nextDate3 = new NextDate(2001, (int) '4', (int) (short) 0);
        java.lang.String str7 = nextDate3.run(2001, 0, (int) (byte) 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8191() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8191");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) -1, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) '#', (int) '4');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8192() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8192");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 30, 7);
    }

    @Test
    public void test8193() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8193");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 0, 2001);
        java.lang.String str7 = nextDate3.run(10, (int) (short) 100, (int) '#');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8194() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8194");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 2020, (int) (short) 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8195() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8195");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 0, (int) (short) 0);
        java.lang.String str7 = nextDate3.run(100, (int) '4', (-1));
        java.lang.String str11 = nextDate3.run(100, (int) (byte) 10, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8196() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8196");
        NextDate nextDate3 = new NextDate(10, 10, 7);
    }

    @Test
    public void test8197() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8197");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 1, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run(30, (int) (short) 100, (int) 'a');
        java.lang.String str11 = nextDate3.run((int) (byte) -1, (-1), 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8198() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8198");
        NextDate nextDate3 = new NextDate(10, (int) (short) -1, 0);
        java.lang.String str7 = nextDate3.run(12, (int) '4', 2020);
        java.lang.String str11 = nextDate3.run(1, (int) (short) -1, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "1/1/2021" + "'", str7, "1/1/2021");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8199() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8199");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 0, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (short) 10, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run((int) (short) 10, 30, (int) (short) 10);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test8200() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8200");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 0, 1);
    }

    @Test
    public void test8201() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8201");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 1, (int) ' ');
        java.lang.String str7 = nextDate3.run(1, 7, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8202() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8202");
        NextDate nextDate3 = new NextDate(0, (int) '4', (int) ' ');
    }

    @Test
    public void test8203() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8203");
        NextDate nextDate3 = new NextDate(30, (int) (byte) 1, (int) (byte) 1);
    }

    @Test
    public void test8204() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8204");
        NextDate nextDate3 = new NextDate(12, 10, (int) (byte) 10);
    }

    @Test
    public void test8205() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8205");
        NextDate nextDate3 = new NextDate(10, (int) '#', 0);
        java.lang.String str7 = nextDate3.run((int) (short) -1, 0, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((int) 'a', 31, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8206() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8206");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 10, (int) (byte) 100);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8207() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8207");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 100, 31);
    }

    @Test
    public void test8208() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8208");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 10, (int) (short) 0);
        java.lang.String str7 = nextDate3.run(30, (int) 'a', (int) (short) -1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8209() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8209");
        NextDate nextDate3 = new NextDate((int) ' ', (int) ' ', (int) 'a');
        java.lang.String str7 = nextDate3.run((int) (short) 0, (int) (byte) 100, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8210() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8210");
        NextDate nextDate3 = new NextDate((-1), 7, (int) (byte) 0);
    }

    @Test
    public void test8211() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8211");
        NextDate nextDate3 = new NextDate((int) 'a', 31, (int) (byte) 10);
    }

    @Test
    public void test8212() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8212");
        NextDate nextDate3 = new NextDate(100, (int) (short) 1, (int) (short) 100);
    }

    @Test
    public void test8213() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8213");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) -1, 30);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8214() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8214");
        NextDate nextDate3 = new NextDate(100, (int) (byte) -1, 10);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (short) 10, 31);
        java.lang.String str11 = nextDate3.run((int) (byte) 100, (int) (byte) 0, (int) (byte) 10);
        java.lang.String str15 = nextDate3.run((int) '#', 1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8215() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8215");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run((int) 'a', 12, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run(31, (int) (byte) 1, (int) (byte) 10);
        java.lang.String str15 = nextDate3.run((int) (short) 10, (int) (byte) 0, (int) 'a');
        java.lang.String str19 = nextDate3.run((int) '4', (int) (byte) 0, 31);
        java.lang.String str23 = nextDate3.run((int) (byte) 0, (int) '#', 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test8216() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8216");
        NextDate nextDate3 = new NextDate(0, (int) (short) 0, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, 31, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8217() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8217");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 100, (int) (short) -1);
        java.lang.String str7 = nextDate3.run(12, 1, (int) (short) 100);
        java.lang.String str11 = nextDate3.run((int) '4', 100, 30);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test8218() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8218");
        NextDate nextDate3 = new NextDate(1, (int) (short) 100, 7);
        java.lang.String str7 = nextDate3.run((int) '#', (int) '#', 100);
        java.lang.String str11 = nextDate3.run(2020, 2001, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8219() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8219");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 0, 30);
        java.lang.String str7 = nextDate3.run(30, 1, (-1));
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8220() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8220");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 31, (int) (byte) 0);
    }

    @Test
    public void test8221() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8221");
        NextDate nextDate3 = new NextDate(12, (int) 'a', 1);
    }

    @Test
    public void test8222() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8222");
        NextDate nextDate3 = new NextDate(100, 0, (int) (short) 0);
        java.lang.String str7 = nextDate3.run(10, (int) (short) 0, 0);
        java.lang.String str11 = nextDate3.run(30, 31, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8223() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8223");
        NextDate nextDate3 = new NextDate(31, (int) 'a', (int) (byte) -1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8224() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8224");
        NextDate nextDate3 = new NextDate(1, (int) '#', (-1));
        java.lang.String str7 = nextDate3.run(31, (int) (byte) 10, 100);
        java.lang.String str11 = nextDate3.run((-1), 30, 7);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test8225() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8225");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (byte) 10, 100);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (short) 100, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8226() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8226");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 1, 30);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8227() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8227");
        NextDate nextDate3 = new NextDate(31, (int) '4', 0);
        java.lang.String str7 = nextDate3.run(0, (int) (byte) 1, (int) (byte) 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8228() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8228");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 0, 7);
        java.lang.String str7 = nextDate3.run(31, 0, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8229() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8229");
        NextDate nextDate3 = new NextDate(0, (int) '4', 0);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) '4', 100);
        java.lang.String str11 = nextDate3.run(30, 0, 2020);
        java.lang.String str15 = nextDate3.run(100, 2020, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8230() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8230");
        NextDate nextDate3 = new NextDate(2020, 0, 100);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (short) 100, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run((int) (byte) 10, (int) (short) 0, 0);
        java.lang.String str15 = nextDate3.run(7, 30, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8231() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8231");
        NextDate nextDate3 = new NextDate((int) (short) 0, 12, 31);
    }

    @Test
    public void test8232() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8232");
        NextDate nextDate3 = new NextDate(30, (int) (short) 10, (int) (byte) 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8233() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8233");
        NextDate nextDate3 = new NextDate(100, (int) (byte) -1, 10);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (short) 10, 31);
        java.lang.String str11 = nextDate3.run((int) (byte) 100, (int) (byte) 0, (int) (byte) 10);
        java.lang.String str15 = nextDate3.run((int) (short) 0, 30, 30);
        java.lang.String str19 = nextDate3.run((int) 'a', 7, (-1));
        java.lang.String str23 = nextDate3.run(0, (int) ' ', (int) (byte) 0);
        java.lang.String str27 = nextDate3.run(100, 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test8234() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8234");
        NextDate nextDate3 = new NextDate(2001, 100, 10);
        java.lang.String str7 = nextDate3.run(1, (-1), 1);
        java.lang.String str11 = nextDate3.run(7, (-1), 10);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test8235() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8235");
        NextDate nextDate3 = new NextDate(1, 12, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) ' ', 2001, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8236() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8236");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (-1), (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) '#', (int) (short) -1, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8237() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8237");
        NextDate nextDate3 = new NextDate(0, 2020, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) '4', 10, 12);
        java.lang.String str11 = nextDate3.run((int) (byte) 100, (int) (byte) 1, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8238() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8238");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run((int) ' ', 2020, (int) (short) 0);
        java.lang.String str11 = nextDate3.run((int) (byte) -1, (int) (byte) -1, (int) (short) 0);
        java.lang.String str15 = nextDate3.run(12, 0, 30);
        java.lang.String str19 = nextDate3.run((int) (short) -1, (int) (short) 1, 12);
        java.lang.String str23 = nextDate3.run((int) (short) 1, (-1), (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test8239() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8239");
        NextDate nextDate3 = new NextDate(100, (-1), 12);
    }

    @Test
    public void test8240() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8240");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 100, (int) (byte) 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8241() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8241");
        NextDate nextDate3 = new NextDate((int) (short) 1, 12, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, 100, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "11/1/2020" + "'", str7, "11/1/2020");
    }

    @Test
    public void test8242() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8242");
        NextDate nextDate3 = new NextDate((int) '4', (-1), (int) (short) -1);
    }

    @Test
    public void test8243() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8243");
        NextDate nextDate3 = new NextDate((int) (short) 10, 0, (int) (short) -1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8244() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8244");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) -1, 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (short) 10, 31);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8245() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8245");
        NextDate nextDate3 = new NextDate((int) (short) 10, 0, 10);
    }

    @Test
    public void test8246() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8246");
        NextDate nextDate3 = new NextDate((int) ' ', 7, 7);
    }

    @Test
    public void test8247() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8247");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 1, (int) '4');
    }

    @Test
    public void test8248() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8248");
        NextDate nextDate3 = new NextDate(7, (int) (short) 100, (int) (byte) 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8249() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8249");
        NextDate nextDate3 = new NextDate(7, 12, (int) '#');
    }

    @Test
    public void test8250() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8250");
        NextDate nextDate3 = new NextDate(31, (int) (byte) 100, 31);
        java.lang.String str7 = nextDate3.run(2001, (int) (short) 100, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8251() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8251");
        NextDate nextDate3 = new NextDate(30, (-1), (int) (byte) 100);
    }

    @Test
    public void test8252() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8252");
        NextDate nextDate3 = new NextDate(100, 1, (int) (short) 10);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8253() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8253");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) 100, 31);
        java.lang.String str7 = nextDate3.run(7, 2020, 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8254() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8254");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) 10, (int) '4');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8255() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8255");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) ' ', (int) 'a');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8256() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8256");
        NextDate nextDate3 = new NextDate(12, 10, 1);
    }

    @Test
    public void test8257() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8257");
        NextDate nextDate3 = new NextDate(12, (int) (byte) 100, 100);
        java.lang.String str7 = nextDate3.run((int) (short) 1, 30, 12);
        java.lang.String str11 = nextDate3.run((int) (short) 100, (int) '4', (int) (short) 100);
        java.lang.String str15 = nextDate3.run((int) '4', (int) (byte) 0, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8258() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8258");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) 100, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run(0, (int) (short) -1, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run(12, (int) (byte) -1, (int) (short) -1);
        java.lang.String str15 = nextDate3.run(12, (int) (byte) 1, 12);
        java.lang.String str19 = nextDate3.run(0, (int) (byte) 1, 1);
        java.lang.Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test8259() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8259");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (byte) 10, 7);
    }

    @Test
    public void test8260() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8260");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 100, (int) (byte) 1);
    }

    @Test
    public void test8261() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8261");
        NextDate nextDate3 = new NextDate((int) (short) 100, 0, 31);
        java.lang.String str7 = nextDate3.run(10, 10, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8262() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8262");
        NextDate nextDate3 = new NextDate(0, 7, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (byte) 1, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((int) (short) -1, 7, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8263() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8263");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 10, 2001);
    }

    @Test
    public void test8264() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8264");
        NextDate nextDate3 = new NextDate((int) '#', 0, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run(12, (int) (short) -1, 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8265() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8265");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run(0, 2001, 0);
        java.lang.String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        java.lang.String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        java.lang.String str19 = nextDate3.run((int) '4', 30, 0);
        java.lang.String str23 = nextDate3.run(7, (int) (short) 10, (int) (short) 1);
        java.lang.String str27 = nextDate3.run((int) 'a', (int) (byte) 1, 0);
        java.lang.String str31 = nextDate3.run((int) (byte) 0, (int) ' ', (int) (short) 10);
        java.lang.String str35 = nextDate3.run((int) (short) -1, (int) '4', 7);
        java.lang.String str39 = nextDate3.run(0, (int) (short) 10, 12);
        java.lang.String str43 = nextDate3.run((int) (byte) -1, (int) (short) 0, (int) '4');
        java.lang.String str47 = nextDate3.run((int) (byte) 0, (int) 'a', (int) 'a');
        java.lang.String str51 = nextDate3.run(2001, (int) '#', (int) (byte) 100);
        java.lang.Class<?> wildcardClass52 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str43 + "' != '" + "invalid Input Date" + "'", str43, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str47 + "' != '" + "invalid Input Date" + "'", str47, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str51 + "' != '" + "invalid Input Date" + "'", str51, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass52);
    }

    @Test
    public void test8266() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8266");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) -1, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) '#', (int) (short) -1);
        java.lang.String str11 = nextDate3.run((-1), (int) (byte) 1, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8267() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8267");
        NextDate nextDate3 = new NextDate((int) (short) 100, 12, 0);
    }

    @Test
    public void test8268() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8268");
        NextDate nextDate3 = new NextDate(30, 2020, 2001);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8269() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8269");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) -1, 2020);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8270() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8270");
        NextDate nextDate3 = new NextDate((int) (short) -1, 10, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (short) 10, 1, 12);
        java.lang.String str11 = nextDate3.run((int) (byte) 0, 0, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8271() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8271");
        NextDate nextDate3 = new NextDate((-1), 10, (int) 'a');
        java.lang.String str7 = nextDate3.run(10, (int) (short) 10, 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8272() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8272");
        NextDate nextDate3 = new NextDate(100, (int) (byte) -1, 0);
    }

    @Test
    public void test8273() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8273");
        NextDate nextDate3 = new NextDate(7, (int) (byte) -1, (int) 'a');
        java.lang.String str7 = nextDate3.run(30, 1, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8274() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8274");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 12, 30);
        java.lang.String str7 = nextDate3.run((int) (short) -1, 2001, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8275() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8275");
        NextDate nextDate3 = new NextDate(100, (int) (short) 1, 12);
    }

    @Test
    public void test8276() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8276");
        NextDate nextDate3 = new NextDate(10, (int) (short) -1, 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (short) 100, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8277() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8277");
        NextDate nextDate3 = new NextDate(1, (int) '4', (int) '4');
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (byte) -1, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run(0, (int) (short) 10, 0);
        java.lang.String str15 = nextDate3.run((int) (byte) 100, 31, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8278() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8278");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 0, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run(0, 1, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8279() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8279");
        NextDate nextDate3 = new NextDate(12, (int) (short) -1, (int) (short) -1);
        java.lang.String str7 = nextDate3.run(30, 30, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8280() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8280");
        NextDate nextDate3 = new NextDate(1, (int) '4', 100);
        java.lang.String str7 = nextDate3.run(2020, 30, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8281() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8281");
        NextDate nextDate3 = new NextDate((int) (short) 10, 0, (int) (byte) 10);
    }

    @Test
    public void test8282() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8282");
        NextDate nextDate3 = new NextDate(0, (int) (byte) -1, (int) (byte) 100);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8283() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8283");
        NextDate nextDate3 = new NextDate((int) (short) 10, 7, (int) '#');
        java.lang.String str7 = nextDate3.run(10, (int) (short) 100, (int) ' ');
        java.lang.String str11 = nextDate3.run(2020, (-1), (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8284() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8284");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) 10, 2020);
    }

    @Test
    public void test8285() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8285");
        NextDate nextDate3 = new NextDate((int) '4', 1, (int) (byte) 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8286() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8286");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 0, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (byte) 0, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8287() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8287");
        NextDate nextDate3 = new NextDate((int) '4', 2020, 31);
    }

    @Test
    public void test8288() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8288");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 1, 12);
        java.lang.String str7 = nextDate3.run(30, (int) (short) 1, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8289() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8289");
        NextDate nextDate3 = new NextDate(12, 10, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run(0, 2020, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8290() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8290");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 10, 2001);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8291() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8291");
        NextDate nextDate3 = new NextDate((int) ' ', 1, (int) '#');
        java.lang.String str7 = nextDate3.run(10, (int) (byte) 1, (int) (short) 0);
        java.lang.String str11 = nextDate3.run(0, 31, (int) (short) 100);
        java.lang.String str15 = nextDate3.run((int) (short) -1, (int) (byte) 10, (int) (byte) 1);
        java.lang.String str19 = nextDate3.run(10, 2020, (int) (short) 1);
        java.lang.Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test8292() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8292");
        NextDate nextDate3 = new NextDate(2001, 0, (-1));
        java.lang.String str7 = nextDate3.run(100, (int) (short) 0, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8293() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8293");
        NextDate nextDate3 = new NextDate(1, 12, (int) (short) 0);
    }

    @Test
    public void test8294() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8294");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 30, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run(100, (int) '4', 31);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8295() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8295");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 100, 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8296() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8296");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 1, (int) (short) -1);
        java.lang.String str7 = nextDate3.run(2001, (int) (byte) -1, (int) (byte) 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8297() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8297");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) -1, 12);
        java.lang.String str7 = nextDate3.run((int) (short) 100, 2020, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8298() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8298");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 100, (int) ' ');
        java.lang.String str7 = nextDate3.run((int) (byte) 1, 100, (int) 'a');
        java.lang.String str11 = nextDate3.run(31, (int) (byte) 10, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8299() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8299");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 10, (int) ' ');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8300() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8300");
        NextDate nextDate3 = new NextDate(12, (int) (short) 10, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (short) 100, 2020);
        java.lang.String str11 = nextDate3.run((int) (short) 1, 0, (int) (short) 10);
        java.lang.String str15 = nextDate3.run((int) (short) 10, 2001, 0);
        java.lang.String str19 = nextDate3.run(31, (int) '#', 0);
        java.lang.String str23 = nextDate3.run(7, (int) (byte) 0, (int) (short) 10);
        java.lang.String str27 = nextDate3.run(0, (int) (short) 1, 31);
        java.lang.String str31 = nextDate3.run(31, 7, (int) '4');
        java.lang.Class<?> wildcardClass32 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass32);
    }

    @Test
    public void test8301() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8301");
        NextDate nextDate3 = new NextDate((int) (short) 10, 1, (int) ' ');
        java.lang.String str7 = nextDate3.run(0, (int) (short) 100, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8302() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8302");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) '#', 2020);
        java.lang.String str7 = nextDate3.run((int) '#', (int) 'a', (int) '#');
        java.lang.String str11 = nextDate3.run(2001, (int) (byte) 1, (int) (byte) 0);
        java.lang.String str15 = nextDate3.run(12, (int) (byte) 0, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8303() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8303");
        NextDate nextDate3 = new NextDate(12, 31, 10);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8304() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8304");
        NextDate nextDate3 = new NextDate(12, 7, 10);
        java.lang.String str7 = nextDate3.run(2020, (int) (short) 10, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8305() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8305");
        NextDate nextDate3 = new NextDate(1, 100, (int) ' ');
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) (byte) 1, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((int) (byte) -1, 0, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8306() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8306");
        NextDate nextDate3 = new NextDate(100, (int) ' ', (int) 'a');
        java.lang.String str7 = nextDate3.run(7, (int) (byte) 10, (int) (byte) 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8307() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8307");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 7, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, 2001, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8308() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8308");
        NextDate nextDate3 = new NextDate(100, 0, 0);
        java.lang.String str7 = nextDate3.run(12, (int) (short) 0, (int) (byte) 1);
        java.lang.String str11 = nextDate3.run(100, (int) (short) 100, (int) (short) 10);
        java.lang.String str15 = nextDate3.run((int) (short) -1, (int) '#', 30);
        java.lang.String str19 = nextDate3.run((int) (byte) -1, (int) 'a', (int) (byte) 10);
        java.lang.String str23 = nextDate3.run((int) '4', (-1), (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test8309() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8309");
        NextDate nextDate3 = new NextDate(12, 2001, (-1));
        java.lang.String str7 = nextDate3.run(0, (-1), (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8310() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8310");
        NextDate nextDate3 = new NextDate(0, 0, 100);
        java.lang.String str7 = nextDate3.run(100, (int) '#', 7);
        java.lang.String str11 = nextDate3.run(12, (int) (byte) 100, (int) (byte) 100);
        java.lang.String str15 = nextDate3.run((int) '#', (int) (byte) 0, (int) (byte) -1);
        java.lang.String str19 = nextDate3.run((int) (short) 1, (int) (short) 1, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test8311() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8311");
        NextDate nextDate3 = new NextDate(30, (int) (byte) 0, (int) (byte) 10);
    }

    @Test
    public void test8312() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8312");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 0, 1);
        java.lang.String str7 = nextDate3.run(2020, 2001, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8313() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8313");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 100, 2020);
        java.lang.String str7 = nextDate3.run((int) ' ', 10, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run(7, (int) (byte) 0, (int) 'a');
        java.lang.String str15 = nextDate3.run((-1), (int) (short) -1, (int) 'a');
        java.lang.String str19 = nextDate3.run(0, (int) (short) 100, (int) (short) 0);
        java.lang.String str23 = nextDate3.run(2001, (int) (byte) 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test8314() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8314");
        NextDate nextDate3 = new NextDate(31, (int) (short) 1, 10);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8315() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8315");
        NextDate nextDate3 = new NextDate(7, (int) 'a', (int) (short) -1);
        java.lang.String str7 = nextDate3.run(31, 0, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run((int) '#', (int) (byte) 100, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8316() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8316");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) -1, 10);
    }

    @Test
    public void test8317() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8317");
        NextDate nextDate3 = new NextDate(12, (int) (short) 1, (int) '4');
        java.lang.String str7 = nextDate3.run((int) (byte) 10, 0, (int) ' ');
        java.lang.String str11 = nextDate3.run((int) (byte) 10, 10, (int) '#');
        java.lang.String str15 = nextDate3.run(12, (int) (byte) 1, (int) (short) 1);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test8318() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8318");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (-1), (int) '#');
        java.lang.String str7 = nextDate3.run(7, 12, (int) '4');
        java.lang.String str11 = nextDate3.run(2020, (int) (short) 10, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8319() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8319");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 2001, (int) 'a');
    }

    @Test
    public void test8320() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8320");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (-1), 10);
    }

    @Test
    public void test8321() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8321");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 100, 0);
        java.lang.String str7 = nextDate3.run((-1), (int) (short) 1, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((int) '4', (int) (byte) 10, 2001);
        java.lang.String str15 = nextDate3.run(31, 0, (int) (byte) 1);
        java.lang.String str19 = nextDate3.run(1, (int) '#', (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test8322() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8322");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) 1, 7);
    }

    @Test
    public void test8323() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8323");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (short) 10, 31);
        java.lang.String str7 = nextDate3.run((int) (short) 100, 1, 1);
        java.lang.String str11 = nextDate3.run((int) (byte) -1, 31, (int) '#');
        java.lang.String str15 = nextDate3.run(30, (int) (short) 1, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8324() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8324");
        NextDate nextDate3 = new NextDate(31, 12, 12);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, (int) (short) 100, 100);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8325() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8325");
        NextDate nextDate3 = new NextDate(1, (-1), (-1));
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (short) 0, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run(10, 0, (int) (byte) 0);
        java.lang.String str15 = nextDate3.run((int) (short) -1, (int) '#', 12);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test8326() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8326");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 0, (int) (byte) 10);
    }

    @Test
    public void test8327() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8327");
        NextDate nextDate3 = new NextDate(31, (int) ' ', (int) '#');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8328() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8328");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) -1, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run(1, 10, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8329() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8329");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 0, 1);
        java.lang.String str7 = nextDate3.run(2020, (int) (byte) 0, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run((-1), (int) (byte) 100, (int) ' ');
        java.lang.String str15 = nextDate3.run(100, 1, 0);
        java.lang.String str19 = nextDate3.run(2020, (int) (byte) 100, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test8330() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8330");
        NextDate nextDate3 = new NextDate(10, 0, 1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8331() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8331");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 2001, (int) ' ');
        java.lang.String str7 = nextDate3.run((int) (byte) 1, (int) (byte) 10, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8332() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8332");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) ' ', 2001);
        java.lang.String str7 = nextDate3.run(2001, 1, (int) (short) 10);
        java.lang.String str11 = nextDate3.run(0, (int) '4', (int) (byte) 1);
        java.lang.String str15 = nextDate3.run(7, 30, 2001);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "7/30/2001" + "'", str15, "7/30/2001");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test8333() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8333");
        NextDate nextDate3 = new NextDate((int) ' ', 30, (int) '4');
    }

    @Test
    public void test8334() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8334");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) '#', 31);
        java.lang.String str11 = nextDate3.run((int) (short) 1, (int) 'a', (int) (byte) -1);
        java.lang.String str15 = nextDate3.run(2001, (int) (short) 10, 12);
        java.lang.String str19 = nextDate3.run((int) (byte) 0, (int) (byte) 0, (-1));
        java.lang.String str23 = nextDate3.run(2020, 10, (int) 'a');
        java.lang.String str27 = nextDate3.run((int) (short) 100, 2020, (int) (byte) 1);
        java.lang.Class<?> wildcardClass28 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test8335() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8335");
        NextDate nextDate3 = new NextDate(12, (int) (short) 10, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (short) 100, 2020);
        java.lang.String str11 = nextDate3.run((int) (short) 0, 2001, (int) (short) -1);
        java.lang.String str15 = nextDate3.run((int) (short) 100, (int) (byte) 0, (int) (short) 10);
        java.lang.String str19 = nextDate3.run((int) (short) 10, (int) '4', 10);
        java.lang.String str23 = nextDate3.run((int) (short) 0, (int) (short) -1, (int) (byte) 100);
        java.lang.Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test8336() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8336");
        NextDate nextDate3 = new NextDate(2020, 0, 0);
        java.lang.String str7 = nextDate3.run((int) (short) 10, 2020, (int) (byte) 1);
        java.lang.String str11 = nextDate3.run((int) (short) 100, (int) (byte) 10, (int) (short) 0);
        java.lang.String str15 = nextDate3.run(0, (-1), 12);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test8337() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8337");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 1, 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8338() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8338");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) 100, 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8339() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8339");
        NextDate nextDate3 = new NextDate(2001, (int) (short) 1, (int) (short) 1);
    }

    @Test
    public void test8340() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8340");
        NextDate nextDate3 = new NextDate(12, 100, 2001);
    }

    @Test
    public void test8341() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8341");
        NextDate nextDate3 = new NextDate(100, (int) '4', (int) (short) -1);
    }

    @Test
    public void test8342() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8342");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) '4', 2001);
    }

    @Test
    public void test8343() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8343");
        NextDate nextDate3 = new NextDate(12, (int) (byte) 100, (int) (byte) 10);
    }

    @Test
    public void test8344() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8344");
        NextDate nextDate3 = new NextDate((-1), 10, (int) (short) 100);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8345() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8345");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 0, 7);
    }

    @Test
    public void test8346() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8346");
        NextDate nextDate3 = new NextDate(31, (int) (short) 1, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (short) 0, (int) 'a', 0);
        java.lang.String str11 = nextDate3.run((int) ' ', 1, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8347() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8347");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 0, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (short) 0, 1, 12);
        java.lang.String str11 = nextDate3.run(12, (int) (byte) 100, (int) '#');
        java.lang.String str15 = nextDate3.run((int) 'a', (-1), 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8348() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8348");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) ' ', 10);
        java.lang.String str7 = nextDate3.run(2020, 12, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8349() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8349");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 2001, 100);
        java.lang.String str7 = nextDate3.run(30, (int) (byte) 10, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8350() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8350");
        NextDate nextDate3 = new NextDate((int) (short) 100, 0, 0);
        java.lang.String str7 = nextDate3.run(31, (int) (byte) -1, (int) (short) 100);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8351() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8351");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 2020, (int) (byte) 0);
    }

    @Test
    public void test8352() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8352");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) 10, (int) ' ');
    }

    @Test
    public void test8353() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8353");
        NextDate nextDate3 = new NextDate(30, 1, (-1));
    }

    @Test
    public void test8354() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8354");
        NextDate nextDate3 = new NextDate(10, (int) 'a', (int) (short) 0);
        java.lang.String str7 = nextDate3.run(0, 7, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8355() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8355");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 1, (int) 'a');
    }

    @Test
    public void test8356() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8356");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) -1, (int) (short) 10);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8357() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8357");
        NextDate nextDate3 = new NextDate((int) '#', (int) 'a', (-1));
        java.lang.String str7 = nextDate3.run(0, (int) (short) 10, (int) 'a');
        java.lang.String str11 = nextDate3.run((int) (short) 10, 2001, (int) (short) 0);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test8358() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8358");
        NextDate nextDate3 = new NextDate(100, 31, (int) ' ');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8359() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8359");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 100, (int) (byte) -1);
    }

    @Test
    public void test8360() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8360");
        NextDate nextDate3 = new NextDate(10, (int) ' ', (int) (short) -1);
    }

    @Test
    public void test8361() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8361");
        NextDate nextDate3 = new NextDate((int) (short) 100, 2020, 7);
        java.lang.String str7 = nextDate3.run(7, (int) ' ', 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8362() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8362");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 0, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) (byte) 1, (int) '#');
        java.lang.String str11 = nextDate3.run(30, (int) 'a', 2001);
        java.lang.String str15 = nextDate3.run(31, (int) (byte) 100, (int) '4');
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test8363() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8363");
        NextDate nextDate3 = new NextDate(31, 31, 31);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8364() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8364");
        NextDate nextDate3 = new NextDate(2020, (int) '#', 31);
    }

    @Test
    public void test8365() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8365");
        NextDate nextDate3 = new NextDate(7, (int) (short) 0, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) '#', (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8366() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8366");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 100, (int) '#');
    }

    @Test
    public void test8367() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8367");
        NextDate nextDate3 = new NextDate(10, 10, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, (int) 'a', 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8368() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8368");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) ' ', 1);
        java.lang.String str7 = nextDate3.run(2001, (int) '#', 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, 2001, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8369() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8369");
        NextDate nextDate3 = new NextDate(1, 0, 2001);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8370() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8370");
        NextDate nextDate3 = new NextDate(0, (int) 'a', (int) 'a');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8371() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8371");
        NextDate nextDate3 = new NextDate(2001, 7, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run(0, 10, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8372() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8372");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) 0, (int) (byte) 10);
    }

    @Test
    public void test8373() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8373");
        NextDate nextDate3 = new NextDate(12, (int) (short) 10, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (short) 100, 2020);
        java.lang.String str11 = nextDate3.run((int) (short) 1, 0, (int) (short) 10);
        java.lang.String str15 = nextDate3.run((int) (short) 10, 2001, 0);
        java.lang.String str19 = nextDate3.run(10, (int) ' ', (int) (short) 0);
        java.lang.String str23 = nextDate3.run((int) '#', 12, (int) (byte) 10);
        java.lang.String str27 = nextDate3.run(31, (int) ' ', (int) 'a');
        java.lang.Class<?> wildcardClass28 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test8374() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8374");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) -1, 2020);
    }

    @Test
    public void test8375() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8375");
        NextDate nextDate3 = new NextDate(2020, 0, 10);
        java.lang.String str7 = nextDate3.run(0, (int) (byte) 0, (int) (byte) 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8376() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8376");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 0, (int) '4');
        java.lang.String str7 = nextDate3.run((int) (short) 100, (int) '4', (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8377() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8377");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) -1, (int) (byte) 1);
    }

    @Test
    public void test8378() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8378");
        NextDate nextDate3 = new NextDate(12, 2001, (-1));
        java.lang.String str7 = nextDate3.run(0, (int) '4', (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8379() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8379");
        NextDate nextDate3 = new NextDate(0, 0, 100);
        java.lang.String str7 = nextDate3.run(100, (int) '#', 7);
        java.lang.String str11 = nextDate3.run(100, (int) (short) 1, (int) (byte) 0);
        java.lang.String str15 = nextDate3.run(100, (int) (short) 0, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8380() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8380");
        NextDate nextDate3 = new NextDate(31, 2001, 2001);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (byte) -1, (-1));
        java.lang.String str11 = nextDate3.run(1, 2020, 0);
        java.lang.String str15 = nextDate3.run(0, (-1), 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8381() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8381");
        NextDate nextDate3 = new NextDate(30, (int) (short) 100, (int) (short) 100);
        java.lang.String str7 = nextDate3.run((int) (short) 0, 7, 2020);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8382() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8382");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 2001, 31);
        java.lang.String str7 = nextDate3.run((-1), (int) '#', (int) '4');
        java.lang.String str11 = nextDate3.run(1, (int) (short) -1, 0);
        java.lang.String str15 = nextDate3.run((int) (byte) 0, (int) 'a', (int) (short) 1);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test8383() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8383");
        NextDate nextDate3 = new NextDate((int) '#', 100, 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (short) 10, (int) 'a');
        java.lang.String str11 = nextDate3.run((int) '#', (int) (short) -1, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8384() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8384");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) -1, 100);
        java.lang.String str7 = nextDate3.run(7, 10, (int) '4');
        java.lang.String str11 = nextDate3.run((int) (byte) 100, (-1), 31);
        java.lang.String str15 = nextDate3.run((int) (short) 100, (int) (byte) -1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8385() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8385");
        NextDate nextDate3 = new NextDate(12, (int) (byte) 100, (-1));
    }

    @Test
    public void test8386() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8386");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 31, (int) '#');
        java.lang.String str7 = nextDate3.run(7, (int) (short) -1, (int) (short) 1);
        java.lang.String str11 = nextDate3.run((int) (byte) -1, (int) (short) 1, (int) (byte) -1);
        java.lang.String str15 = nextDate3.run(2020, (int) 'a', 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8387() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8387");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 100, 2020);
        java.lang.String str7 = nextDate3.run((int) ' ', 10, (int) (byte) 10);
        java.lang.String str11 = nextDate3.run(7, (int) (byte) 0, (int) 'a');
        java.lang.String str15 = nextDate3.run((-1), (int) (short) -1, (int) 'a');
        java.lang.String str19 = nextDate3.run((int) (short) 0, (int) (byte) 100, (int) (byte) 100);
        java.lang.String str23 = nextDate3.run((int) '#', 2001, (int) (byte) 1);
        java.lang.Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test8388() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8388");
        NextDate nextDate3 = new NextDate((int) '4', 7, (int) (short) 10);
    }

    @Test
    public void test8389() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8389");
        NextDate nextDate3 = new NextDate(100, (int) (short) 0, (int) (byte) 0);
    }

    @Test
    public void test8390() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8390");
        NextDate nextDate3 = new NextDate((int) '4', (int) ' ', (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (short) 0, (int) (short) 0, 10);
        java.lang.String str11 = nextDate3.run(12, (int) '4', 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8391() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8391");
        NextDate nextDate3 = new NextDate((-1), 30, 1);
    }

    @Test
    public void test8392() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8392");
        NextDate nextDate3 = new NextDate(1, (-1), (-1));
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (short) 0, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run((int) ' ', (int) (short) -1, (int) ' ');
        java.lang.String str15 = nextDate3.run((int) (short) 1, 12, (int) '4');
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test8393() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8393");
        NextDate nextDate3 = new NextDate(1, (int) (short) 0, 12);
        java.lang.String str7 = nextDate3.run(0, (int) (short) 0, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8394() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8394");
        NextDate nextDate3 = new NextDate(0, 1, 7);
        java.lang.String str7 = nextDate3.run((int) (short) 100, 31, (int) (short) 100);
        java.lang.String str11 = nextDate3.run(10, (int) (byte) 0, (-1));
        java.lang.String str15 = nextDate3.run(31, (int) '#', 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8395() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8395");
        NextDate nextDate3 = new NextDate((int) 'a', 100, (int) (byte) 1);
    }

    @Test
    public void test8396() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8396");
        NextDate nextDate3 = new NextDate(12, (-1), 31);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8397() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8397");
        NextDate nextDate3 = new NextDate((int) (short) -1, 0, 12);
        java.lang.String str7 = nextDate3.run(12, 2020, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8398() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8398");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) '4', (int) (short) 0);
    }

    @Test
    public void test8399() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8399");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 0, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, (int) (short) 10, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8400() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8400");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 100, (-1));
        java.lang.String str7 = nextDate3.run(2020, 7, 31);
        java.lang.String str11 = nextDate3.run((int) ' ', 0, (int) (short) 0);
        java.lang.String str15 = nextDate3.run(0, 100, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8401() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8401");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 7, 2020);
        java.lang.String str7 = nextDate3.run((int) (short) 100, 10, 2001);
        java.lang.String str11 = nextDate3.run((int) (short) 10, (int) (short) 1, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8402() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8402");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) 100, (int) (short) 1);
    }

    @Test
    public void test8403() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8403");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 10, (int) (short) 1);
    }

    @Test
    public void test8404() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8404");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 0, 1);
        java.lang.String str7 = nextDate3.run((int) ' ', 31, (int) '4');
        java.lang.String str11 = nextDate3.run((int) (byte) 0, (int) (byte) 1, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8405() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8405");
        NextDate nextDate3 = new NextDate((int) '#', (int) '#', 10);
    }

    @Test
    public void test8406() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8406");
        NextDate nextDate3 = new NextDate((int) '4', (int) (byte) -1, (int) (short) 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8407() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8407");
        NextDate nextDate3 = new NextDate((-1), (int) 'a', (int) (byte) 10);
        java.lang.String str7 = nextDate3.run(12, 12, 12);
        java.lang.String str11 = nextDate3.run((int) (byte) 0, (int) (short) 100, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8408() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8408");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) '#', (-1));
        java.lang.String str7 = nextDate3.run((int) ' ', 31, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8409() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8409");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) '4', 1);
        java.lang.String str7 = nextDate3.run(2001, (int) (short) -1, (int) (short) 10);
        java.lang.String str11 = nextDate3.run(0, (int) (byte) 100, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8410() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8410");
        NextDate nextDate3 = new NextDate((int) 'a', 12, (int) ' ');
    }

    @Test
    public void test8411() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8411");
        NextDate nextDate3 = new NextDate(0, 31, 0);
    }

    @Test
    public void test8412() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8412");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) ' ', 2020);
        java.lang.String str7 = nextDate3.run((int) '#', 12, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8413() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8413");
        NextDate nextDate3 = new NextDate((int) '#', 2001, (int) (byte) 1);
    }

    @Test
    public void test8414() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8414");
        NextDate nextDate3 = new NextDate((int) '4', (int) ' ', (int) (short) 0);
        java.lang.String str7 = nextDate3.run(100, (-1), 0);
        java.lang.String str11 = nextDate3.run(100, (int) '4', (int) (byte) 10);
        java.lang.String str15 = nextDate3.run((int) (short) 100, (int) (byte) 10, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8415() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8415");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) -1, (int) (short) 10);
    }

    @Test
    public void test8416() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8416");
        NextDate nextDate3 = new NextDate((int) (short) 100, 2020, (int) (short) 0);
    }

    @Test
    public void test8417() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8417");
        NextDate nextDate3 = new NextDate(2020, 2001, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((-1), (int) (byte) 10, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8418() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8418");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) 1, (int) (short) 0);
    }

    @Test
    public void test8419() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8419");
        NextDate nextDate3 = new NextDate((-1), (int) '4', 1);
    }

    @Test
    public void test8420() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8420");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) '4', 100);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) '#', (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((int) (short) 0, (-1), 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8421() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8421");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 0, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (short) -1, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8422() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8422");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) 100, 2001);
    }

    @Test
    public void test8423() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8423");
        NextDate nextDate3 = new NextDate(7, 31, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, 31, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8424() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8424");
        NextDate nextDate3 = new NextDate(30, 31, 12);
        java.lang.String str7 = nextDate3.run((-1), 0, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8425() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8425");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) 100, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run((-1), (int) (byte) 1, 0);
        java.lang.String str11 = nextDate3.run(0, (int) (byte) 1, 10);
        java.lang.String str15 = nextDate3.run(10, (int) '4', (-1));
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test8426() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8426");
        NextDate nextDate3 = new NextDate((int) (short) -1, 2001, 100);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8427() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8427");
        NextDate nextDate3 = new NextDate(2001, (int) (short) 100, 7);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (byte) 10, (int) '#');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8428() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8428");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 31, 2001);
    }

    @Test
    public void test8429() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8429");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 100, (int) (short) -1);
        java.lang.String str7 = nextDate3.run(12, 12, (int) (byte) 1);
        java.lang.String str11 = nextDate3.run(12, (int) (short) 100, 0);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test8430() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8430");
        NextDate nextDate3 = new NextDate((int) '#', 12, (int) (short) 0);
    }

    @Test
    public void test8431() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8431");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 0, 0);
    }

    @Test
    public void test8432() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8432");
        NextDate nextDate3 = new NextDate(0, (int) ' ', 2001);
    }

    @Test
    public void test8433() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8433");
        NextDate nextDate3 = new NextDate(0, (int) 'a', 2020);
    }

    @Test
    public void test8434() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8434");
        NextDate nextDate3 = new NextDate((int) '#', 10, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, 10, (int) ' ');
        java.lang.String str11 = nextDate3.run(2001, (-1), 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8435() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8435");
        NextDate nextDate3 = new NextDate((int) ' ', (-1), (int) 'a');
    }

    @Test
    public void test8436() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8436");
        NextDate nextDate3 = new NextDate(100, (int) (short) 100, (int) (byte) 0);
    }

    @Test
    public void test8437() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8437");
        NextDate nextDate3 = new NextDate((int) '#', 0, (int) (byte) 0);
        java.lang.String str7 = nextDate3.run(1, (int) (byte) -1, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8438() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8438");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 100, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, (int) (short) -1, 10);
        java.lang.String str11 = nextDate3.run(10, 10, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8439() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8439");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) ' ', (int) '4');
        java.lang.String str7 = nextDate3.run((int) (short) 0, (int) (byte) -1, (int) (short) 10);
        java.lang.String str11 = nextDate3.run((int) (short) 0, 30, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8440() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8440");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 100, (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) '4', 100, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8441() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8441");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) ' ', (int) (short) 0);
        java.lang.String str7 = nextDate3.run(1, (int) (short) 10, 30);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8442() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8442");
        NextDate nextDate3 = new NextDate((int) (short) 1, 100, 12);
    }

    @Test
    public void test8443() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8443");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) 'a', 2020);
        java.lang.String str7 = nextDate3.run((int) 'a', 1, (int) (short) 0);
        java.lang.String str11 = nextDate3.run((int) '4', (int) (short) 100, (int) (byte) 1);
        java.lang.String str15 = nextDate3.run(0, (int) ' ', (int) (short) 1);
        java.lang.String str19 = nextDate3.run((int) (short) 100, 1, 12);
        java.lang.String str23 = nextDate3.run(31, 12, (int) (byte) 1);
        java.lang.String str27 = nextDate3.run((int) (byte) 10, (int) (byte) 1, 0);
        java.lang.Class<?> wildcardClass28 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test8444() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8444");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 2020, (int) (byte) 10);
    }

    @Test
    public void test8445() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8445");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 0, 30);
        java.lang.String str7 = nextDate3.run(10, 100, (-1));
        java.lang.String str11 = nextDate3.run(100, 30, 2020);
        java.lang.String str15 = nextDate3.run(7, (int) (short) -1, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8446() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8446");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 10, 0);
    }

    @Test
    public void test8447() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8447");
        NextDate nextDate3 = new NextDate((int) ' ', 100, 31);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (short) 0, (int) '4');
        java.lang.String str11 = nextDate3.run((int) '4', 100, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8448() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8448");
        NextDate nextDate3 = new NextDate(1, (int) '#', (-1));
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (byte) 0, (int) (short) 1);
        java.lang.String str11 = nextDate3.run((int) (short) 1, 7, 7);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test8449() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8449");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 2020, (int) '#');
        java.lang.String str7 = nextDate3.run(100, (int) '#', (int) (byte) 10);
        java.lang.String str11 = nextDate3.run((int) (short) 100, (int) 'a', (-1));
        java.lang.String str15 = nextDate3.run((int) (short) 1, (int) '#', (int) (short) 0);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test8450() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8450");
        NextDate nextDate3 = new NextDate(0, (int) '4', 0);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (short) 0, (int) (short) 100);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, (int) '4', 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8451() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8451");
        NextDate nextDate3 = new NextDate(7, 12, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 10, (int) (byte) 100, 2020);
        java.lang.String str11 = nextDate3.run(7, 0, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "11/1/2020" + "'", str7, "11/1/2020");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8452() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8452");
        NextDate nextDate3 = new NextDate(2001, (int) '4', (int) (short) 1);
    }

    @Test
    public void test8453() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8453");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) '#', (int) (byte) 1);
    }

    @Test
    public void test8454() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8454");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) '#', 12);
        java.lang.String str7 = nextDate3.run(0, (int) (short) -1, 2001);
        java.lang.String str11 = nextDate3.run(12, (int) ' ', (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8455() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8455");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 7, 12);
    }

    @Test
    public void test8456() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8456");
        NextDate nextDate3 = new NextDate((int) (short) 10, 1, (int) 'a');
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (byte) 10, 100);
        java.lang.String str11 = nextDate3.run(12, 12, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8457() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8457");
        NextDate nextDate3 = new NextDate(10, (int) (short) 10, (-1));
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8458() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8458");
        NextDate nextDate3 = new NextDate(7, (-1), (-1));
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8459() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8459");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 0, (int) (short) 1);
        java.lang.String str7 = nextDate3.run(2001, (int) ' ', (-1));
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8460() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8460");
        NextDate nextDate3 = new NextDate(0, 30, 7);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8461() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8461");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 0, 10);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8462() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8462");
        NextDate nextDate3 = new NextDate(10, (int) (short) 10, 2001);
    }

    @Test
    public void test8463() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8463");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 100, (int) (byte) 0);
    }

    @Test
    public void test8464() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8464");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 1, 31);
    }

    @Test
    public void test8465() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8465");
        NextDate nextDate3 = new NextDate(10, (-1), 31);
    }

    @Test
    public void test8466() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8466");
        NextDate nextDate3 = new NextDate((int) (short) 1, 0, (int) ' ');
        java.lang.String str7 = nextDate3.run(100, 30, (int) (byte) 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8467() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8467");
        NextDate nextDate3 = new NextDate(0, (int) '4', 0);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) '4', 100);
        java.lang.String str11 = nextDate3.run(30, 0, 2020);
        java.lang.String str15 = nextDate3.run(100, 10, (int) ' ');
        java.lang.String str19 = nextDate3.run(1, (int) (short) -1, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test8468() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8468");
        NextDate nextDate3 = new NextDate(1, (int) (byte) 0, 31);
    }

    @Test
    public void test8469() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8469");
        NextDate nextDate3 = new NextDate(7, (int) (short) 100, (int) ' ');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8470() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8470");
        NextDate nextDate3 = new NextDate((int) 'a', 7, 30);
        java.lang.String str7 = nextDate3.run((int) ' ', 2020, (-1));
        java.lang.String str11 = nextDate3.run((-1), 2001, (int) (short) 0);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test8471() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8471");
        NextDate nextDate3 = new NextDate(0, 7, (int) (short) 10);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) ' ', 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8472() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8472");
        NextDate nextDate3 = new NextDate(7, 2020, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (byte) 10, (int) '4');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8473() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8473");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) 'a', 12);
        java.lang.String str7 = nextDate3.run(0, 0, 7);
        java.lang.String str11 = nextDate3.run((int) (short) -1, (int) '4', (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8474() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8474");
        NextDate nextDate3 = new NextDate((int) (short) 1, 0, 10);
        java.lang.String str7 = nextDate3.run(31, (-1), 31);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8475() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8475");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (-1), (int) (byte) -1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8476() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8476");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 2020, 2001);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) (short) 10, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run(31, (int) (byte) 1, 12);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test8477() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8477");
        NextDate nextDate3 = new NextDate(0, 2020, (int) (short) 0);
        java.lang.String str7 = nextDate3.run(12, (-1), 2020);
        java.lang.String str11 = nextDate3.run((int) (short) -1, 0, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8478() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8478");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) -1, 7);
    }

    @Test
    public void test8479() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8479");
        NextDate nextDate3 = new NextDate(2001, (int) (short) 1, 100);
    }

    @Test
    public void test8480() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8480");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 0, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (short) 1, (int) (byte) -1);
        java.lang.String str11 = nextDate3.run((int) (byte) 0, (int) 'a', 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8481() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8481");
        NextDate nextDate3 = new NextDate(31, 1, (int) 'a');
    }

    @Test
    public void test8482() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8482");
        NextDate nextDate3 = new NextDate(100, (int) '#', (int) ' ');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8483() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8483");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 10, 0);
        java.lang.String str7 = nextDate3.run(0, (int) (byte) 1, 7);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, (int) (byte) 100, (int) (short) 0);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test8484() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8484");
        NextDate nextDate3 = new NextDate((int) ' ', 2001, (int) (short) 10);
    }

    @Test
    public void test8485() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8485");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 10, 0);
    }

    @Test
    public void test8486() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8486");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) 10, (int) (short) 100);
    }

    @Test
    public void test8487() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8487");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 10, (-1));
    }

    @Test
    public void test8488() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8488");
        NextDate nextDate3 = new NextDate(7, (int) (byte) 10, 31);
        java.lang.String str7 = nextDate3.run((-1), (int) '4', (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8489() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8489");
        NextDate nextDate3 = new NextDate(10, (int) (short) 100, (int) (short) -1);
    }

    @Test
    public void test8490() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8490");
        NextDate nextDate3 = new NextDate((int) '4', (int) ' ', (-1));
    }

    @Test
    public void test8491() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8491");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) 1, 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, 30, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8492() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8492");
        NextDate nextDate3 = new NextDate(31, (int) 'a', 0);
        java.lang.String str7 = nextDate3.run((int) ' ', 30, (int) '4');
        java.lang.String str11 = nextDate3.run((int) (byte) 10, (int) (short) 0, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8493() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8493");
        NextDate nextDate3 = new NextDate(30, (int) ' ', 31);
    }

    @Test
    public void test8494() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8494");
        NextDate nextDate3 = new NextDate(10, 0, (int) (byte) 10);
    }

    @Test
    public void test8495() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8495");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) '#', 30);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, (int) 'a', (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8496() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8496");
        NextDate nextDate3 = new NextDate(12, (int) (byte) 100, 31);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (short) 100, (int) (short) 1);
        java.lang.String str11 = nextDate3.run(12, (int) (byte) 0, (int) (short) 10);
        java.lang.String str15 = nextDate3.run((int) (short) 10, 12, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8497() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8497");
        NextDate nextDate3 = new NextDate((int) (short) 1, 0, 31);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, (int) (short) 100, (int) (short) 1);
        java.lang.String str11 = nextDate3.run(2020, (int) 'a', 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8498() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8498");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 1, (int) (byte) 0);
    }

    @Test
    public void test8499() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8499");
        NextDate nextDate3 = new NextDate((int) 'a', 30, (int) (byte) 1);
    }

    @Test
    public void test8500() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest16.test8500");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 0, 30);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }
}

