import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest17 {

    public static boolean debug = false;

    @Test
    public void test8501() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8501");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 0, 100);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8502() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8502");
        NextDate nextDate3 = new NextDate((int) '#', 2001, (int) (short) 0);
    }

    @Test
    public void test8503() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8503");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 0, 31);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8504() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8504");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) 100, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run((int) '#', (int) (byte) 0, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8505() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8505");
        NextDate nextDate3 = new NextDate((int) (short) 0, (-1), (int) '#');
        java.lang.String str7 = nextDate3.run((int) (byte) 1, 7, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8506() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8506");
        NextDate nextDate3 = new NextDate((int) ' ', 0, (int) (byte) 1);
    }

    @Test
    public void test8507() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8507");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 10, 7);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (short) 1, (-1));
        java.lang.String str11 = nextDate3.run((int) (short) 0, 100, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8508() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8508");
        NextDate nextDate3 = new NextDate((int) '4', (int) (byte) -1, (int) 'a');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8509() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8509");
        NextDate nextDate3 = new NextDate((int) '#', 0, 0);
        java.lang.String str7 = nextDate3.run((int) (short) -1, 2001, 30);
        java.lang.String str11 = nextDate3.run(12, (int) ' ', 0);
        java.lang.String str15 = nextDate3.run(1, (int) (byte) 100, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8510() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8510");
        NextDate nextDate3 = new NextDate((int) 'a', 2001, 0);
    }

    @Test
    public void test8511() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8511");
        NextDate nextDate3 = new NextDate((int) '#', 7, (int) (short) 100);
    }

    @Test
    public void test8512() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8512");
        NextDate nextDate3 = new NextDate(10, (int) (short) 10, 30);
    }

    @Test
    public void test8513() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8513");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) 10, 31);
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (short) 100, 12);
        java.lang.String str11 = nextDate3.run((int) '4', (int) (byte) -1, 30);
        java.lang.String str15 = nextDate3.run((int) (byte) 10, 100, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8514() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8514");
        NextDate nextDate3 = new NextDate(0, (int) (short) 100, (-1));
        java.lang.String str7 = nextDate3.run(0, 30, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8515() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8515");
        NextDate nextDate3 = new NextDate(100, 1, (int) (byte) 100);
    }

    @Test
    public void test8516() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8516");
        NextDate nextDate3 = new NextDate(12, 30, (int) (byte) 100);
        java.lang.String str7 = nextDate3.run(30, 1, 30);
        java.lang.String str11 = nextDate3.run(0, (int) (short) 100, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8517() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8517");
        NextDate nextDate3 = new NextDate((int) ' ', 7, (int) (short) 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8518() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8518");
        NextDate nextDate3 = new NextDate((int) (short) 1, (-1), (int) 'a');
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (byte) -1, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8519() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8519");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) 100, (int) 'a');
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) (short) 10, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8520() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8520");
        NextDate nextDate3 = new NextDate(10, (int) (byte) -1, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) ' ', 1, 31);
        java.lang.String str11 = nextDate3.run((int) '#', (int) '#', 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8521() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8521");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) -1, 30);
    }

    @Test
    public void test8522() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8522");
        NextDate nextDate3 = new NextDate(100, (int) (byte) -1, 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (short) 1, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8523() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8523");
        NextDate nextDate3 = new NextDate((-1), 30, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run(0, (int) (byte) -1, (int) (byte) 1);
        java.lang.String str11 = nextDate3.run(0, (int) '#', 31);
        java.lang.String str15 = nextDate3.run((int) (short) -1, 0, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8524() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8524");
        NextDate nextDate3 = new NextDate((int) (short) 1, 0, 31);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) (byte) 0, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8525() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8525");
        NextDate nextDate3 = new NextDate((int) (short) 0, (-1), (int) 'a');
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (byte) 100, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8526() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8526");
        NextDate nextDate3 = new NextDate((int) (short) 1, 0, (int) (short) -1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8527() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8527");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 100, (int) (short) 100);
        java.lang.String str7 = nextDate3.run(12, (-1), (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8528() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8528");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 10, 1);
        java.lang.String str7 = nextDate3.run((int) 'a', 2020, (int) (short) -1);
        java.lang.String str11 = nextDate3.run(31, (int) (byte) 1, (int) (byte) 0);
        java.lang.String str15 = nextDate3.run(100, 31, 2001);
        java.lang.String str19 = nextDate3.run((int) (byte) -1, (int) '4', 2001);
        java.lang.Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test8529() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8529");
        NextDate nextDate3 = new NextDate((int) (short) 1, 30, 2001);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8530() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8530");
        NextDate nextDate3 = new NextDate((int) (short) 100, 30, 12);
        java.lang.String str7 = nextDate3.run(100, (int) (byte) 100, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8531() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8531");
        NextDate nextDate3 = new NextDate((int) 'a', 1, (int) (byte) 0);
    }

    @Test
    public void test8532() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8532");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 1, (-1));
        java.lang.String str7 = nextDate3.run((-1), 2001, 2020);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8533() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8533");
        NextDate nextDate3 = new NextDate(0, (int) (short) 100, 0);
        java.lang.String str7 = nextDate3.run(7, 0, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8534() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8534");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) -1, (int) (byte) 1);
    }

    @Test
    public void test8535() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8535");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) 1, 2020);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8536() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8536");
        NextDate nextDate3 = new NextDate((int) 'a', 1, 7);
        java.lang.String str7 = nextDate3.run(10, 100, 0);
        java.lang.String str11 = nextDate3.run(31, (int) (byte) 1, (int) ' ');
        java.lang.String str15 = nextDate3.run((int) (short) 0, (int) (short) 0, 0);
        java.lang.String str19 = nextDate3.run((-1), (int) 'a', (int) (short) 0);
        java.lang.String str23 = nextDate3.run(100, 10, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test8537() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8537");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 1, 31);
    }

    @Test
    public void test8538() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8538");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) 10, 12);
    }

    @Test
    public void test8539() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8539");
        NextDate nextDate3 = new NextDate((int) ' ', 31, (int) (short) 1);
    }

    @Test
    public void test8540() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8540");
        NextDate nextDate3 = new NextDate(30, (-1), (int) '#');
        java.lang.String str7 = nextDate3.run((int) (byte) 100, (int) '#', (int) 'a');
        java.lang.String str11 = nextDate3.run((int) (short) 1, (int) '4', 7);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test8541() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8541");
        NextDate nextDate3 = new NextDate(1, (int) (short) 0, (int) (byte) 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8542() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8542");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) -1, (int) (short) -1);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8543() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8543");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) -1, 30);
        java.lang.String str7 = nextDate3.run((int) ' ', (-1), (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8544() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8544");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) -1, 12);
    }

    @Test
    public void test8545() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8545");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) '4', (int) (short) 100);
        java.lang.String str7 = nextDate3.run(0, (int) (byte) -1, 100);
        java.lang.String str11 = nextDate3.run(2001, 0, 0);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test8546() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8546");
        NextDate nextDate3 = new NextDate(7, (int) (short) 0, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (short) 0, (int) (short) 0);
        java.lang.String str11 = nextDate3.run((int) '#', 2020, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8547() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8547");
        NextDate nextDate3 = new NextDate((int) (short) -1, 30, 12);
        java.lang.String str7 = nextDate3.run(0, (int) (byte) -1, (int) '4');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8548() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8548");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) '#', (int) ' ');
        java.lang.String str7 = nextDate3.run((int) ' ', (int) (short) 1, (int) (short) 1);
        java.lang.String str11 = nextDate3.run((-1), (int) (byte) -1, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8549() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8549");
        NextDate nextDate3 = new NextDate(0, 2001, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run(2020, (int) '4', (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8550() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8550");
        NextDate nextDate3 = new NextDate(12, (int) 'a', (int) (byte) -1);
        java.lang.String str7 = nextDate3.run(31, 0, 10);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, (-1), (-1));
        java.lang.String str15 = nextDate3.run((int) (byte) 1, (int) (short) 10, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8551() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8551");
        NextDate nextDate3 = new NextDate(7, 0, (int) 'a');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8552() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8552");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (short) 100, (int) (byte) 1);
    }

    @Test
    public void test8553() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8553");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) '#', 0);
        java.lang.String str7 = nextDate3.run(10, 0, (int) ' ');
        java.lang.String str11 = nextDate3.run((int) (short) -1, (int) ' ', (int) '#');
        java.lang.String str15 = nextDate3.run(100, (int) '#', 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8554() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8554");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 0, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) (byte) 0, (int) 'a');
        java.lang.String str11 = nextDate3.run((int) (short) 0, 2020, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8555() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8555");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 100, (int) '4');
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) 'a', (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8556() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8556");
        NextDate nextDate3 = new NextDate(7, (int) (short) -1, 12);
        java.lang.String str7 = nextDate3.run(30, 31, (int) (byte) 1);
        java.lang.String str11 = nextDate3.run((int) (byte) 100, (int) (short) -1, 1);
        java.lang.String str15 = nextDate3.run(12, (int) (short) 100, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8557() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8557");
        NextDate nextDate3 = new NextDate(1, 2001, (-1));
    }

    @Test
    public void test8558() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8558");
        NextDate nextDate3 = new NextDate(30, (int) (short) 1, (int) (short) 10);
    }

    @Test
    public void test8559() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8559");
        NextDate nextDate3 = new NextDate(31, (int) (byte) 1, (int) (short) 0);
        java.lang.String str7 = nextDate3.run(31, (int) (short) 100, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8560() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8560");
        NextDate nextDate3 = new NextDate(0, (int) (short) 1, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run((-1), (int) (byte) 0, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8561() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8561");
        NextDate nextDate3 = new NextDate((int) (short) 10, 1, 30);
    }

    @Test
    public void test8562() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8562");
        NextDate nextDate3 = new NextDate(2020, (int) (short) 1, 10);
        java.lang.String str7 = nextDate3.run((int) (short) 1, 12, 2001);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "1/13/2001" + "'", str7, "1/13/2001");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8563() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8563");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) 100, 2020);
        java.lang.String str7 = nextDate3.run((-1), (int) (short) 100, (int) (byte) 100);
        java.lang.String str11 = nextDate3.run((int) (byte) 0, (int) (short) 10, 2001);
        java.lang.String str15 = nextDate3.run((int) (byte) 1, 100, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8564() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8564");
        NextDate nextDate3 = new NextDate((int) ' ', 100, 31);
        java.lang.String str7 = nextDate3.run(30, (int) (short) 0, 2001);
        java.lang.String str11 = nextDate3.run(30, 1, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8565() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8565");
        NextDate nextDate3 = new NextDate(100, 7, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run((int) (short) 100, 12, 10);
        java.lang.String str11 = nextDate3.run(100, (int) 'a', (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8566() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8566");
        NextDate nextDate3 = new NextDate((-1), 12, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) ' ', 10);
        java.lang.String str11 = nextDate3.run(10, 0, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8567() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8567");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) -1, 0);
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) (byte) 100, 2020);
        java.lang.String str11 = nextDate3.run(10, (int) 'a', (int) (byte) 10);
        java.lang.String str15 = nextDate3.run(31, 0, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8568() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8568");
        NextDate nextDate3 = new NextDate((int) ' ', (int) '4', (int) 'a');
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8569() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8569");
        NextDate nextDate3 = new NextDate(31, 100, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run(1, (int) (byte) 1, (int) '4');
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8570() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8570");
        NextDate nextDate3 = new NextDate((int) '4', 31, 2001);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (short) 10, (int) '4');
        java.lang.String str11 = nextDate3.run(0, 0, 31);
        java.lang.String str15 = nextDate3.run((int) (short) 100, (int) (byte) 100, 7);
        java.lang.String str19 = nextDate3.run((int) (byte) 100, (int) (byte) 1, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test8571() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8571");
        NextDate nextDate3 = new NextDate((int) '#', (int) ' ', (int) (byte) 100);
    }

    @Test
    public void test8572() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8572");
        NextDate nextDate3 = new NextDate(2020, 2001, (-1));
        java.lang.String str7 = nextDate3.run((-1), 2001, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8573() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8573");
        NextDate nextDate3 = new NextDate(0, 100, 100);
        java.lang.String str7 = nextDate3.run((int) '#', 10, 7);
        java.lang.String str11 = nextDate3.run(1, (int) (byte) -1, (int) (byte) 100);
        java.lang.String str15 = nextDate3.run((int) (short) 0, 2001, (-1));
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test8574() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8574");
        NextDate nextDate3 = new NextDate(31, 100, 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8575() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8575");
        NextDate nextDate3 = new NextDate(10, 0, 2001);
        java.lang.String str7 = nextDate3.run(0, 1, 0);
        java.lang.String str11 = nextDate3.run((int) '#', (int) (short) 0, (int) 'a');
        java.lang.String str15 = nextDate3.run((int) '4', 7, 100);
        java.lang.String str19 = nextDate3.run(12, 10, 100);
        java.lang.String str23 = nextDate3.run(2020, 12, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test8576() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8576");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 7, 1);
    }

    @Test
    public void test8577() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8577");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 10, (int) (byte) 0);
    }

    @Test
    public void test8578() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8578");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 1, (int) '#');
    }

    @Test
    public void test8579() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8579");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 0, (int) (byte) 10);
    }

    @Test
    public void test8580() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8580");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) ' ', (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) '4', (int) '#', (int) (short) 0);
        java.lang.String str11 = nextDate3.run(0, (int) (byte) -1, (int) (short) 0);
        java.lang.String str15 = nextDate3.run((int) (byte) -1, (int) (byte) 1, 10);
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test8581() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8581");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 1, 10);
        java.lang.String str7 = nextDate3.run(2020, 7, 100);
        java.lang.String str11 = nextDate3.run(7, 30, (int) (byte) 10);
        java.lang.String str15 = nextDate3.run((int) (byte) -1, 2001, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8582() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8582");
        NextDate nextDate3 = new NextDate((int) (short) 0, 30, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run(0, 10, (-1));
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8583() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8583");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (short) 0, 10);
        java.lang.String str7 = nextDate3.run((int) (byte) -1, (int) '#', (int) 'a');
        java.lang.String str11 = nextDate3.run((int) (byte) 10, 30, 10);
        java.lang.String str15 = nextDate3.run(7, 1, 0);
        java.lang.String str19 = nextDate3.run(100, 2020, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test8584() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8584");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 100, 12);
        java.lang.String str7 = nextDate3.run((int) (short) 10, 100, (int) '4');
        java.lang.String str11 = nextDate3.run(100, (int) (byte) 1, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8585() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8585");
        NextDate nextDate3 = new NextDate(2001, (int) (short) 100, 7);
        java.lang.String str7 = nextDate3.run(2001, (int) (short) 100, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8586() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8586");
        NextDate nextDate3 = new NextDate(100, 100, (int) (short) 100);
    }

    @Test
    public void test8587() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8587");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) -1, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (short) 0, 31, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8588() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8588");
        NextDate nextDate3 = new NextDate((int) (short) 10, 12, 0);
        java.lang.String str7 = nextDate3.run((int) '4', 1, 1);
        java.lang.String str11 = nextDate3.run(1, 1, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8589() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8589");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 30, 2020);
    }

    @Test
    public void test8590() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8590");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 10, 12);
    }

    @Test
    public void test8591() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8591");
        NextDate nextDate3 = new NextDate(12, (int) (short) 10, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, (int) '4', 31);
        java.lang.String str11 = nextDate3.run(7, 12, (-1));
        java.lang.String str15 = nextDate3.run((-1), (int) (short) 10, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8592() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8592");
        NextDate nextDate3 = new NextDate(100, (int) (short) 1, 2020);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) ' ', (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8593() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8593");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 100, (int) '#');
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (byte) 1, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8594() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8594");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) 100, (int) (short) -1);
        java.lang.String str7 = nextDate3.run(31, 10, 7);
        java.lang.String str11 = nextDate3.run(2020, 2001, (int) ' ');
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test8595() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8595");
        NextDate nextDate3 = new NextDate((int) '#', (int) '4', 12);
        java.lang.String str7 = nextDate3.run(0, 2001, 7);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8596() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8596");
        NextDate nextDate3 = new NextDate(7, 30, (int) (short) 0);
        java.lang.String str7 = nextDate3.run((int) (byte) 1, (int) (short) 10, 2020);
        java.lang.String str11 = nextDate3.run(0, 2001, (int) (short) -1);
        java.lang.String str15 = nextDate3.run((int) (byte) 10, (int) ' ', 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "1/11/2020" + "'", str7, "1/11/2020");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8597() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8597");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) 100, (int) (byte) 10);
    }

    @Test
    public void test8598() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8598");
        NextDate nextDate3 = new NextDate(2020, 100, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run(31, (int) (byte) 100, 30);
        java.lang.String str11 = nextDate3.run(10, 0, (int) (short) 1);
        java.lang.String str15 = nextDate3.run((int) 'a', (int) '#', (int) (short) 1);
        java.lang.String str19 = nextDate3.run((int) (short) 0, (int) (byte) -1, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test8599() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8599");
        NextDate nextDate3 = new NextDate(7, (int) (byte) 0, (int) 'a');
        java.lang.String str7 = nextDate3.run((int) (short) 1, 7, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8600() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8600");
        NextDate nextDate3 = new NextDate((int) (short) 10, 10, (int) (short) 1);
    }

    @Test
    public void test8601() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8601");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) 10, 7);
    }

    @Test
    public void test8602() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8602");
        NextDate nextDate3 = new NextDate(1, 12, (int) (short) 1);
        java.lang.String str7 = nextDate3.run(10, (int) '#', 0);
        java.lang.String str11 = nextDate3.run(2020, (-1), (int) (short) -1);
        java.lang.String str15 = nextDate3.run(0, 2020, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8603() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8603");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 7, 0);
    }

    @Test
    public void test8604() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8604");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) -1, (int) (short) 100);
    }

    @Test
    public void test8605() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8605");
        NextDate nextDate3 = new NextDate((-1), (int) ' ', (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) '#', (int) (short) 1, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8606() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8606");
        NextDate nextDate3 = new NextDate(0, 7, 12);
    }

    @Test
    public void test8607() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8607");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 31, (-1));
        java.lang.String str7 = nextDate3.run((int) (short) 0, 31, (int) (byte) 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 100, (int) (byte) -1, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8608() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8608");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 0, 0);
    }

    @Test
    public void test8609() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8609");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 10, 100);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8610() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8610");
        NextDate nextDate3 = new NextDate((-1), 7, (int) (byte) -1);
    }

    @Test
    public void test8611() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8611");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) '#', (int) ' ');
        java.lang.String str7 = nextDate3.run((-1), 1, (-1));
        java.lang.String str11 = nextDate3.run(100, 10, (int) (short) 100);
        java.lang.String str15 = nextDate3.run((int) '4', 0, (int) (byte) 1);
        java.lang.String str19 = nextDate3.run(30, 0, 30);
        java.lang.String str23 = nextDate3.run(0, (int) (short) -1, 1);
        java.lang.String str27 = nextDate3.run(2020, 31, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test8612() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8612");
        NextDate nextDate3 = new NextDate(7, (int) (byte) 100, (-1));
    }

    @Test
    public void test8613() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8613");
        NextDate nextDate3 = new NextDate(31, 30, (int) (byte) 0);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8614() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8614");
        NextDate nextDate3 = new NextDate(2020, (int) 'a', 12);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (short) 0, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8615() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8615");
        NextDate nextDate3 = new NextDate((-1), (int) (short) -1, 2001);
        java.lang.Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test8616() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8616");
        NextDate nextDate3 = new NextDate(2020, (int) (short) 0, (int) (byte) 1);
        java.lang.String str7 = nextDate3.run(7, (int) (byte) 100, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8617() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8617");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) 'a', 12);
        java.lang.String str7 = nextDate3.run(0, 0, 7);
        java.lang.String str11 = nextDate3.run(1, 0, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8618() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8618");
        NextDate nextDate3 = new NextDate(0, (int) (short) 1, 2001);
        java.lang.String str7 = nextDate3.run(100, 2020, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8619() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8619");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 2001, 12);
        java.lang.String str7 = nextDate3.run((int) (short) -1, 100, (int) '4');
        java.lang.String str11 = nextDate3.run(12, 2020, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test8620() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8620");
        NextDate nextDate3 = new NextDate((int) ' ', 7, 100);
    }

    @Test
    public void test8621() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8621");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 100, 100);
    }

    @Test
    public void test8622() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8622");
        NextDate nextDate3 = new NextDate((int) ' ', 31, 30);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (short) 1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8623() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8623");
        NextDate nextDate3 = new NextDate(100, (-1), 0);
        java.lang.String str7 = nextDate3.run(100, (int) (short) 100, (int) (byte) 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8624() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8624");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) 'a', 10);
        java.lang.String str7 = nextDate3.run((int) (short) 0, 31, (int) (short) 1);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8625() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8625");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) 'a', (int) (byte) 100);
        java.lang.String str7 = nextDate3.run(0, (int) '4', 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8626() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8626");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 0, (int) (short) 0);
        java.lang.String str7 = nextDate3.run(100, (int) '4', (-1));
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8627() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8627");
        NextDate nextDate3 = new NextDate((int) ' ', 0, (int) (short) 1);
        java.lang.String str7 = nextDate3.run((int) '4', (int) (short) 100, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8628() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8628");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 10, 30);
        java.lang.String str7 = nextDate3.run((int) (short) 1, (int) (short) 1, (int) (byte) 1);
        java.lang.String str11 = nextDate3.run((int) '#', (int) ' ', (int) '4');
        java.lang.String str15 = nextDate3.run((int) '#', 0, 7);
        java.lang.String str19 = nextDate3.run(2020, (-1), (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test8629() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8629");
        NextDate nextDate3 = new NextDate((-1), 100, 2001);
        java.lang.String str7 = nextDate3.run((int) 'a', (int) (short) 1, (-1));
        java.lang.String str11 = nextDate3.run(1, 100, 0);
        java.lang.String str15 = nextDate3.run(100, (int) (short) 1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test8630() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8630");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (-1), 12);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) ' ', (int) (byte) -1);
        java.lang.String str11 = nextDate3.run(12, (int) '#', 0);
        java.lang.Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test8631() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8631");
        NextDate nextDate3 = new NextDate((int) (short) 0, (-1), (int) (byte) -1);
    }

    @Test
    public void test8632() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8632");
        NextDate nextDate3 = new NextDate((int) '4', 10, (int) (byte) -1);
    }

    @Test
    public void test8633() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8633");
        NextDate nextDate3 = new NextDate(31, 31, (int) (short) 100);
    }

    @Test
    public void test8634() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8634");
        NextDate nextDate3 = new NextDate(7, (int) (byte) -1, (int) 'a');
        java.lang.String str7 = nextDate3.run((int) (short) 1, 2020, (int) (byte) 0);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8635() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8635");
        NextDate nextDate3 = new NextDate((int) (short) 100, 31, (int) ' ');
        java.lang.String str7 = nextDate3.run((int) (short) -1, (int) '#', 1);
        java.lang.String str11 = nextDate3.run((int) '4', 10, (int) (byte) 1);
        java.lang.String str15 = nextDate3.run(30, (int) (byte) 0, (-1));
        java.lang.Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test8636() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8636");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 10, (int) (byte) -1);
        java.lang.String str7 = nextDate3.run(10, 100, (int) (byte) 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8637() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8637");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 2001, 100);
        java.lang.String str7 = nextDate3.run((int) (byte) 0, (int) (short) 10, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8638() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8638");
        NextDate nextDate3 = new NextDate((-1), 2020, 0);
        java.lang.String str7 = nextDate3.run(12, 1, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8639() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8639");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) 'a', (int) (byte) 0);
    }

    @Test
    public void test8640() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8640");
        NextDate nextDate3 = new NextDate((int) (short) -1, (-1), (int) '4');
        java.lang.String str7 = nextDate3.run((int) (byte) -1, 2020, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8641() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8641");
        NextDate nextDate3 = new NextDate(1, (int) ' ', 2020);
        java.lang.String str7 = nextDate3.run(0, (int) 'a', (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8642() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8642");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        java.lang.String str7 = nextDate3.run(0, 2001, 0);
        java.lang.String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        java.lang.String str15 = nextDate3.run((int) (byte) 1, (int) (short) 0, (int) (short) 100);
        java.lang.String str19 = nextDate3.run((int) (short) 10, 0, 10);
        java.lang.String str23 = nextDate3.run((int) (byte) -1, 10, 30);
        java.lang.String str27 = nextDate3.run((int) (byte) 100, (int) (short) 0, (-1));
        java.lang.String str31 = nextDate3.run((int) (byte) -1, 10, (int) (short) 10);
        java.lang.String str35 = nextDate3.run((int) (byte) -1, (-1), 12);
        java.lang.String str39 = nextDate3.run(7, (int) (short) 1, (int) (short) 1);
        java.lang.String str43 = nextDate3.run((int) (byte) -1, (int) '4', (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str43 + "' != '" + "invalid Input Date" + "'", str43, "invalid Input Date");
    }

    @Test
    public void test8643() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8643");
        NextDate nextDate3 = new NextDate(12, 1, (int) (byte) 10);
        java.lang.String str7 = nextDate3.run((int) ' ', 7, 7);
        java.lang.String str11 = nextDate3.run((int) '4', (-1), (int) '4');
        java.lang.String str15 = nextDate3.run(0, (int) (byte) 100, (int) (byte) 10);
        java.lang.String str19 = nextDate3.run(30, (int) ' ', (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test8644() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8644");
        NextDate nextDate3 = new NextDate((int) '#', 30, (int) (short) -1);
        java.lang.String str7 = nextDate3.run((int) '4', 12, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8645() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8645");
        NextDate nextDate3 = new NextDate(31, 12, (int) (short) 0);
    }

    @Test
    public void test8646() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8646");
        NextDate nextDate3 = new NextDate((int) ' ', (int) 'a', (int) (short) 0);
        java.lang.String str7 = nextDate3.run(2001, 0, (int) (short) 10);
        java.lang.Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test8647() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8647");
        NextDate nextDate3 = new NextDate((int) (short) 100, (-1), (int) (byte) 0);
        java.lang.String str7 = nextDate3.run((int) (short) 10, 0, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8648() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8648");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) 100, (int) (short) 1);
    }

    @Test
    public void test8649() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8649");
        NextDate nextDate3 = new NextDate((-1), (int) (short) -1, 7);
    }

    @Test
    public void test8650() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8650");
        NextDate nextDate3 = new NextDate(7, 31, (int) ' ');
        java.lang.String str7 = nextDate3.run(0, 100, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test8651() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8651");
        NextDate nextDate3 = new NextDate(0, 1, 2001);
    }

    @Test
    public void test8652() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8652");
        NextDate nextDate3 = new NextDate(2020, 12, (int) (byte) -1);
    }

    @Test
    public void test8653() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest17.test8653");
        NextDate nextDate3 = new NextDate(10, (-1), 12);
        java.lang.String str7 = nextDate3.run((int) (short) 10, (int) (short) 10, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }
}

