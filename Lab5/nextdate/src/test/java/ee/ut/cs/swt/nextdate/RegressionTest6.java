package ee.ut.cs.swt.nextdate;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest6 {

    public static boolean debug = false;

    @Test
    public void test3001() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3001");
        NextDate nextDate3 = new NextDate(1, 12, (-1));
        String str7 = nextDate3.run(1, (int) (byte) 100, (int) (short) 100);
        String str11 = nextDate3.run((int) '4', (int) '#', (int) (byte) 1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3002() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3002");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) -1, 12);
    }

    @Test
    public void test3003() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3003");
        NextDate nextDate3 = new NextDate((int) '4', 31, 0);
        String str7 = nextDate3.run(0, 2001, (int) '4');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3004() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3004");
        NextDate nextDate3 = new NextDate(2001, 31, (int) (byte) -1);
        String str7 = nextDate3.run(0, 2020, 2001);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3005() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3005");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) 1, (int) (byte) 100);
        String str7 = nextDate3.run((int) (short) -1, (int) (byte) -1, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3006() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3006");
        NextDate nextDate3 = new NextDate(0, 1, 12);
    }

    @Test
    public void test3007() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3007");
        NextDate nextDate3 = new NextDate(2001, 0, (int) '4');
    }

    @Test
    public void test3008() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3008");
        NextDate nextDate3 = new NextDate((int) ' ', 1, (-1));
        String str7 = nextDate3.run((int) (short) -1, (int) ' ', 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3009() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3009");
        NextDate nextDate3 = new NextDate((int) (short) 100, 7, 10);
    }

    @Test
    public void test3010() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3010");
        NextDate nextDate3 = new NextDate(7, 2020, 12);
        String str7 = nextDate3.run((int) (short) 0, 31, (int) (short) 100);
        String str11 = nextDate3.run(1, 30, (-1));
        String str15 = nextDate3.run(10, (int) '#', (int) '4');
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3011() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3011");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) '4', 0);
        String str7 = nextDate3.run(0, (int) 'a', 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3012() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3012");
        NextDate nextDate3 = new NextDate(31, (int) (short) 0, 31);
        String str7 = nextDate3.run((int) (byte) 10, (-1), (int) (byte) -1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3013() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3013");
        NextDate nextDate3 = new NextDate(100, 7, (int) '#');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3014() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3014");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) -1, 0);
        String str7 = nextDate3.run((int) (short) -1, (int) (byte) 100, 2020);
        String str11 = nextDate3.run((int) (short) -1, (int) (short) 10, (int) (byte) 10);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3015() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3015");
        NextDate nextDate3 = new NextDate((int) (short) 100, 12, (int) '4');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3016() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3016");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) 'a', 12);
        String str7 = nextDate3.run((int) (short) 1, 31, (int) ' ');
        String str11 = nextDate3.run(0, (int) (byte) -1, (-1));
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3017() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3017");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (short) 100, (int) (short) 0);
    }

    @Test
    public void test3018() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3018");
        NextDate nextDate3 = new NextDate(12, (int) 'a', (int) (byte) -1);
        String str7 = nextDate3.run((int) (short) 1, (int) (byte) 10, 2020);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "1/11/2020" + "'", str7, "1/11/2020");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3019() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3019");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 1, (int) (byte) 10);
        String str7 = nextDate3.run(1, 100, 0);
        String str11 = nextDate3.run((int) (byte) 0, 0, 10);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3020() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3020");
        NextDate nextDate3 = new NextDate((-1), 30, (int) (byte) 100);
    }

    @Test
    public void test3021() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3021");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) 100, 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3022() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3022");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) '#', (int) ' ');
        String str7 = nextDate3.run((-1), 1, (-1));
        String str11 = nextDate3.run((int) (short) 100, (int) '4', 12);
        String str15 = nextDate3.run(7, (int) (byte) 100, (int) '4');
        String str19 = nextDate3.run((int) ' ', (int) (byte) -1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test3023() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3023");
        NextDate nextDate3 = new NextDate(10, 7, 1);
        String str7 = nextDate3.run((int) (byte) 0, (int) '#', (int) (byte) 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3024() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3024");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 0, (int) (byte) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3025() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3025");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        String str15 = nextDate3.run((int) (byte) 1, (int) (short) 0, (int) (short) 100);
        String str19 = nextDate3.run((int) (short) 10, 0, 10);
        String str23 = nextDate3.run((int) (byte) -1, 10, 30);
        String str27 = nextDate3.run((int) (byte) 100, (int) (short) 0, (-1));
        String str31 = nextDate3.run((int) (byte) -1, 10, (int) (short) 10);
        String str35 = nextDate3.run((int) (short) 0, 7, (int) (byte) 100);
        String str39 = nextDate3.run((int) 'a', (int) (byte) 10, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
    }

    @Test
    public void test3026() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3026");
        NextDate nextDate3 = new NextDate(0, (int) (short) 1, (int) (byte) 100);
        String str7 = nextDate3.run((int) (byte) -1, (int) (short) -1, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3027() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3027");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) 1, (int) (short) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3028() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3028");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        String str15 = nextDate3.run(0, (int) (short) -1, (int) 'a');
        String str19 = nextDate3.run(7, (int) (short) 1, 100);
        String str23 = nextDate3.run(7, 31, (int) ' ');
        Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test3029() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3029");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) 100, (int) (short) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3030() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3030");
        NextDate nextDate3 = new NextDate(10, (int) (byte) -1, 12);
    }

    @Test
    public void test3031() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3031");
        NextDate nextDate3 = new NextDate((int) (short) 100, 31, (int) (short) 1);
    }

    @Test
    public void test3032() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3032");
        NextDate nextDate3 = new NextDate(10, (-1), (int) (short) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3033() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3033");
        NextDate nextDate3 = new NextDate(100, 7, 2001);
    }

    @Test
    public void test3034() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3034");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 30, (int) '4');
        String str7 = nextDate3.run(0, 0, 0);
        String str11 = nextDate3.run(10, (int) (short) 100, (int) (byte) 0);
        String str15 = nextDate3.run(31, 7, (int) '#');
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3035() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3035");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) '#', (int) (short) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3036() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3036");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) 100, (int) (short) -1);
    }

    @Test
    public void test3037() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3037");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) -1, (int) (short) 1);
        String str7 = nextDate3.run((int) (byte) -1, 0, 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3038() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3038");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) '#', (int) ' ');
    }

    @Test
    public void test3039() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3039");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) (byte) 0, (int) (byte) 10, 31);
        String str23 = nextDate3.run((-1), 0, (int) (byte) -1);
        String str27 = nextDate3.run((int) '4', (int) (short) -1, (int) '4');
        Class<?> wildcardClass28 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test3040() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3040");
        NextDate nextDate3 = new NextDate(7, (int) (short) 1, 2001);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3041() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3041");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) 100, 10);
    }

    @Test
    public void test3042() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3042");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (short) 10, 2020);
    }

    @Test
    public void test3043() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3043");
        NextDate nextDate3 = new NextDate((int) (short) 100, 1, (int) (byte) 0);
    }

    @Test
    public void test3044() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3044");
        NextDate nextDate3 = new NextDate(0, (int) (short) 1, (int) (short) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3045() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3045");
        NextDate nextDate3 = new NextDate(0, 1, (int) (short) 100);
        String str7 = nextDate3.run((int) (byte) 10, 0, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3046() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3046");
        NextDate nextDate3 = new NextDate(2020, 0, 2020);
        String str7 = nextDate3.run((int) ' ', 7, (int) (byte) 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3047() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3047");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 1, (-1));
        String str7 = nextDate3.run(10, 2001, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3048() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3048");
        NextDate nextDate3 = new NextDate((int) '4', 30, (int) (short) 100);
        String str7 = nextDate3.run(2020, 0, (int) (byte) 1);
        String str11 = nextDate3.run((int) 'a', 10, (int) (byte) 1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3049() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3049");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) '#', (int) (byte) 0);
    }

    @Test
    public void test3050() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3050");
        NextDate nextDate3 = new NextDate(31, (int) (byte) 1, (int) ' ');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3051() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3051");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 2020, 7);
    }

    @Test
    public void test3052() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3052");
        NextDate nextDate3 = new NextDate(30, (int) '4', (int) (short) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3053() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3053");
        NextDate nextDate3 = new NextDate(31, (int) (byte) -1, (int) '4');
        String str7 = nextDate3.run((int) '#', 31, 7);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3054() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3054");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) '#', 0);
        String str7 = nextDate3.run(2020, 0, 30);
        String str11 = nextDate3.run((int) (byte) 10, (int) (byte) 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3055() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3055");
        NextDate nextDate3 = new NextDate(7, (int) 'a', 10);
    }

    @Test
    public void test3056() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3056");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 0, 0);
    }

    @Test
    public void test3057() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3057");
        NextDate nextDate3 = new NextDate(0, (int) (short) 1, (int) 'a');
        String str7 = nextDate3.run(0, (int) (byte) 10, 12);
        String str11 = nextDate3.run((int) 'a', (int) (byte) -1, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3058() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3058");
        NextDate nextDate3 = new NextDate((int) ' ', (int) '4', (int) 'a');
        String str7 = nextDate3.run(12, (-1), 31);
        String str11 = nextDate3.run(30, 10, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3059() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3059");
        NextDate nextDate3 = new NextDate(31, 2001, 10);
    }

    @Test
    public void test3060() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3060");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 1, 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3061() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3061");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 100, (int) (short) -1);
        String str7 = nextDate3.run(12, 12, (int) (byte) 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3062() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3062");
        NextDate nextDate3 = new NextDate((int) (short) 0, 12, 0);
    }

    @Test
    public void test3063() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3063");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 0, (int) (short) 1);
        String str7 = nextDate3.run((int) (byte) 10, (int) (byte) 10, (int) 'a');
        String str11 = nextDate3.run(7, 100, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3064() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3064");
        NextDate nextDate3 = new NextDate(0, 10, (int) '#');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3065() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3065");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 12, (int) (byte) 1);
        String str7 = nextDate3.run(2001, 7, (int) (short) 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3066() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3066");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run((int) (byte) 10, (int) (short) 100, 100);
        String str27 = nextDate3.run((int) (short) 100, (int) (short) 0, (int) (byte) 0);
        String str31 = nextDate3.run(2020, 30, 7);
        String str35 = nextDate3.run((int) (short) -1, (int) (short) 10, 2020);
        String str39 = nextDate3.run((int) (byte) 0, (int) (short) 10, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
    }

    @Test
    public void test3067() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3067");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) 'a', 31);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3068() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3068");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 100, 2020);
        String str7 = nextDate3.run((int) ' ', 10, (int) (byte) 10);
        String str11 = nextDate3.run(7, (int) (byte) 0, (int) 'a');
        String str15 = nextDate3.run(0, (int) (byte) 0, 0);
        String str19 = nextDate3.run((int) (short) 1, (-1), 100);
        String str23 = nextDate3.run((int) (short) 10, (int) ' ', 31);
        Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test3069() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3069");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 10, (int) '#');
    }

    @Test
    public void test3070() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3070");
        NextDate nextDate3 = new NextDate(10, 31, 2020);
    }

    @Test
    public void test3071() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3071");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) ' ', 30);
    }

    @Test
    public void test3072() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3072");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) -1, 30);
    }

    @Test
    public void test3073() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3073");
        NextDate nextDate3 = new NextDate(31, (-1), (int) (short) 0);
        String str7 = nextDate3.run(0, 0, (int) (short) 100);
        String str11 = nextDate3.run(10, 30, (int) (byte) 1);
        String str15 = nextDate3.run(0, (int) (short) -1, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test3074() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3074");
        NextDate nextDate3 = new NextDate((int) (short) 10, 12, 10);
        String str7 = nextDate3.run((int) (short) 100, 0, (int) ' ');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3075() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3075");
        NextDate nextDate3 = new NextDate((int) (short) 0, 100, (int) (byte) 10);
        String str7 = nextDate3.run((int) ' ', (int) (byte) 0, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3076() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3076");
        NextDate nextDate3 = new NextDate((int) ' ', 12, (int) (short) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3077() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3077");
        NextDate nextDate3 = new NextDate((int) (short) 1, (-1), (int) (byte) -1);
    }

    @Test
    public void test3078() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3078");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 10, (-1));
        String str7 = nextDate3.run((-1), (int) '#', (int) (byte) 100);
        String str11 = nextDate3.run((int) (short) 100, 2001, (int) '#');
        String str15 = nextDate3.run((int) (byte) 10, (int) (short) 1, 100);
        String str19 = nextDate3.run((int) (short) 1, (-1), (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test3079() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3079");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run((int) (byte) 10, (int) (short) 100, 100);
        String str27 = nextDate3.run(1, (int) (short) 10, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test3080() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3080");
        NextDate nextDate3 = new NextDate(12, (int) (byte) -1, (int) (byte) 0);
        String str7 = nextDate3.run((int) (short) 100, (int) (short) 1, 0);
        String str11 = nextDate3.run(0, (int) (short) 10, 10);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3081() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3081");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) '#', (int) (short) -1);
        String str7 = nextDate3.run(12, 2001, (int) (short) 100);
        String str11 = nextDate3.run((int) '4', 2001, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3082() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3082");
        NextDate nextDate3 = new NextDate(2020, 0, (-1));
        String str7 = nextDate3.run(30, (int) (byte) 100, (-1));
        String str11 = nextDate3.run((int) (byte) 100, 12, 2001);
        String str15 = nextDate3.run((int) 'a', (-1), 0);
        String str19 = nextDate3.run(1, 0, (-1));
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test3083() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3083");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        String str15 = nextDate3.run((int) (byte) 1, (int) (short) 0, (int) (short) 100);
        String str19 = nextDate3.run((int) (byte) 1, (int) '#', (int) (short) 10);
        String str23 = nextDate3.run((-1), 10, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test3084() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3084");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 1, 0);
        String str7 = nextDate3.run((-1), 100, (int) (byte) -1);
        String str11 = nextDate3.run(0, 100, 1);
        String str15 = nextDate3.run(2001, 12, 7);
        String str19 = nextDate3.run((int) (short) 10, 12, 12);
        String str23 = nextDate3.run(0, 0, 7);
        Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test3085() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3085");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 7, (int) (byte) 1);
        String str7 = nextDate3.run(30, (int) (short) 100, 100);
        String str11 = nextDate3.run(10, 1, (int) '4');
        String str15 = nextDate3.run(31, (int) (byte) 10, 1);
        String str19 = nextDate3.run(30, (int) (byte) 10, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test3086() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3086");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 10, 2001);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3087() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3087");
        NextDate nextDate3 = new NextDate((int) 'a', (int) '4', (int) (short) -1);
        String str7 = nextDate3.run(10, 31, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3088() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3088");
        NextDate nextDate3 = new NextDate(0, 1, (int) ' ');
        String str7 = nextDate3.run(30, (int) (byte) -1, (int) (byte) 0);
        String str11 = nextDate3.run((int) (byte) -1, (int) (short) 0, 30);
        String str15 = nextDate3.run((int) (byte) 0, (int) (short) -1, (int) '#');
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3089() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3089");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) -1, 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3090() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3090");
        NextDate nextDate3 = new NextDate(30, (int) '#', 0);
        String str7 = nextDate3.run((int) '4', 30, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3091() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3091");
        NextDate nextDate3 = new NextDate(31, (int) (byte) 100, (int) (byte) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3092() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3092");
        NextDate nextDate3 = new NextDate((int) (short) 1, 0, (int) (short) 0);
        String str7 = nextDate3.run((int) '4', (int) (byte) 1, 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3093() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3093");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) 100, (int) (short) -1);
    }

    @Test
    public void test3094() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3094");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) '4', 2001);
        String str7 = nextDate3.run((int) (byte) -1, 0, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3095() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3095");
        NextDate nextDate3 = new NextDate(12, 31, 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3096() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3096");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) -1, 2001);
    }

    @Test
    public void test3097() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3097");
        NextDate nextDate3 = new NextDate(100, (int) (short) 10, (int) (byte) 1);
    }

    @Test
    public void test3098() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3098");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 31, 0);
        String str7 = nextDate3.run((int) '4', (int) '4', 7);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3099() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3099");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 7, (int) (byte) 1);
        String str7 = nextDate3.run(30, (int) (short) 100, 100);
        String str11 = nextDate3.run(10, 1, (int) '4');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3100() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3100");
        NextDate nextDate3 = new NextDate((-1), 0, (int) '#');
        String str7 = nextDate3.run((int) (byte) 100, 0, (int) (short) 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3101() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3101");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) -1, (int) ' ');
        String str7 = nextDate3.run((int) (byte) 0, 2001, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3102() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3102");
        NextDate nextDate3 = new NextDate(2001, (int) (short) 0, (int) '4');
        String str7 = nextDate3.run((int) (short) -1, (int) ' ', (int) ' ');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3103() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3103");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) '4', (int) (byte) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3104() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3104");
        NextDate nextDate3 = new NextDate(2001, 12, (int) (byte) -1);
        String str7 = nextDate3.run((int) (short) 1, (int) (byte) 1, (int) ' ');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3105() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3105");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) -1, (int) '4');
    }

    @Test
    public void test3106() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3106");
        NextDate nextDate3 = new NextDate((int) ' ', 2020, 2001);
    }

    @Test
    public void test3107() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3107");
        NextDate nextDate3 = new NextDate(100, (int) (short) 100, (int) (byte) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3108() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3108");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (-1), 31);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3109() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3109");
        NextDate nextDate3 = new NextDate(2001, 10, (int) (short) 0);
    }

    @Test
    public void test3110() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3110");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) 'a', 2020);
        String str7 = nextDate3.run((int) 'a', 1, (int) (short) 0);
        String str11 = nextDate3.run((int) '4', (int) (short) 100, (int) (byte) 1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3111() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3111");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 100, (int) (short) 100);
    }

    @Test
    public void test3112() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3112");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) 'a', (int) '4');
        String str7 = nextDate3.run((int) (short) 1, (int) (byte) 100, (int) (short) 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3113() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3113");
        NextDate nextDate3 = new NextDate((int) (short) 10, 7, 10);
        String str7 = nextDate3.run((int) (short) 0, 31, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3114() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3114");
        NextDate nextDate3 = new NextDate(12, 2020, (int) 'a');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3115() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3115");
        NextDate nextDate3 = new NextDate((int) ' ', 10, (int) (short) 10);
    }

    @Test
    public void test3116() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3116");
        NextDate nextDate3 = new NextDate((-1), 2001, 10);
    }

    @Test
    public void test3117() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3117");
        NextDate nextDate3 = new NextDate((int) '4', 30, (int) (short) 100);
        String str7 = nextDate3.run((int) (byte) -1, 30, 7);
        String str11 = nextDate3.run(10, 10, (int) ' ');
        String str15 = nextDate3.run(10, (int) (short) 1, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test3118() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3118");
        NextDate nextDate3 = new NextDate(1, 0, (int) (byte) 100);
    }

    @Test
    public void test3119() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3119");
        NextDate nextDate3 = new NextDate(1, 7, (int) (byte) 1);
        String str7 = nextDate3.run((int) (short) -1, 0, 1);
        String str11 = nextDate3.run((int) '#', (int) (byte) 100, 30);
        String str15 = nextDate3.run((int) (short) -1, (int) (short) 1, 2020);
        String str19 = nextDate3.run((int) (short) 10, (int) (byte) 10, 2001);
        String str23 = nextDate3.run(31, 30, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "10/11/2001" + "'", str19, "10/11/2001");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test3120() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3120");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 0, 2001);
        String str7 = nextDate3.run(10, (int) (short) 100, (int) '#');
        String str11 = nextDate3.run((int) (byte) 10, (int) (byte) 100, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3121() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3121");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run(31, 31, 12);
        String str23 = nextDate3.run(10, 2020, (int) (byte) -1);
        String str27 = nextDate3.run(2001, (int) (short) 100, 7);
        String str31 = nextDate3.run((int) (short) 100, (int) (byte) 0, (int) (short) 10);
        String str35 = nextDate3.run(7, (-1), (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
    }

    @Test
    public void test3122() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3122");
        NextDate nextDate3 = new NextDate(10, 7, (int) (short) 100);
    }

    @Test
    public void test3123() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3123");
        NextDate nextDate3 = new NextDate((int) ' ', 10, (int) 'a');
        String str7 = nextDate3.run((int) 'a', 7, 12);
        String str11 = nextDate3.run(31, (int) 'a', 31);
        String str15 = nextDate3.run((-1), 12, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test3124() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3124");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 10, 30);
        String str7 = nextDate3.run(12, (-1), (int) (byte) 100);
        String str11 = nextDate3.run(7, 10, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3125() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3125");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 12, (-1));
    }

    @Test
    public void test3126() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3126");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) ' ', 0);
        String str7 = nextDate3.run((int) '#', (int) (short) -1, (int) (byte) 10);
        String str11 = nextDate3.run(31, (int) '4', (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3127() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3127");
        NextDate nextDate3 = new NextDate(10, 0, 2001);
        String str7 = nextDate3.run(0, 1, 0);
        String str11 = nextDate3.run(12, (int) (short) 1, (int) (short) 100);
        String str15 = nextDate3.run((-1), (int) (byte) -1, 1);
        String str19 = nextDate3.run(0, (int) '4', (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test3128() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3128");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 2001, (int) (short) 10);
        String str7 = nextDate3.run(30, (int) '#', 12);
        String str11 = nextDate3.run((int) (byte) 10, 0, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3129() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3129");
        NextDate nextDate3 = new NextDate(0, 2001, (int) (short) -1);
        String str7 = nextDate3.run(0, (int) (short) 10, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3130() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3130");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 10, (int) (short) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3131() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3131");
        NextDate nextDate3 = new NextDate(31, 2020, 31);
        String str7 = nextDate3.run((int) (short) -1, (int) 'a', (-1));
        String str11 = nextDate3.run(0, 1, 0);
        String str15 = nextDate3.run((int) ' ', (int) ' ', (int) (short) 1);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3132() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3132");
        NextDate nextDate3 = new NextDate(1, (int) (short) 1, 0);
        String str7 = nextDate3.run(0, (int) (byte) 10, (int) (byte) -1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3133() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3133");
        NextDate nextDate3 = new NextDate((int) (short) 100, 0, (int) (short) -1);
        String str7 = nextDate3.run((int) 'a', (int) (byte) 0, 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3134() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3134");
        NextDate nextDate3 = new NextDate((int) '#', (int) ' ', (int) ' ');
    }

    @Test
    public void test3135() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3135");
        NextDate nextDate3 = new NextDate(30, 0, 7);
        String str7 = nextDate3.run(2001, (int) (short) -1, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3136() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3136");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) 'a', (int) (short) -1);
        String str7 = nextDate3.run(100, (int) ' ', 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3137() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3137");
        NextDate nextDate3 = new NextDate((int) (short) 10, 100, (int) (short) 0);
    }

    @Test
    public void test3138() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3138");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 1, (int) (short) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3139() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3139");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) '#', 12);
    }

    @Test
    public void test3140() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3140");
        NextDate nextDate3 = new NextDate((int) (short) 1, 7, 2001);
        String str7 = nextDate3.run((int) (short) 10, (int) ' ', 0);
        String str11 = nextDate3.run((int) (short) 1, (int) ' ', 7);
        String str15 = nextDate3.run((int) 'a', 1, 2020);
        String str19 = nextDate3.run(31, (int) (short) -1, (int) (byte) 100);
        String str23 = nextDate3.run((int) ' ', (int) (short) -1, 1);
        Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test3141() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3141");
        NextDate nextDate3 = new NextDate((int) ' ', 31, 2001);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3142() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3142");
        NextDate nextDate3 = new NextDate((int) '#', (-1), (int) 'a');
        String str7 = nextDate3.run((int) (byte) 10, (int) 'a', (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3143() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3143");
        NextDate nextDate3 = new NextDate(0, (int) (short) 100, (int) (short) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3144() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3144");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) 10, (int) ' ');
        String str7 = nextDate3.run((int) (byte) 100, (-1), (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3145() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3145");
        NextDate nextDate3 = new NextDate(10, 0, 100);
    }

    @Test
    public void test3146() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3146");
        NextDate nextDate3 = new NextDate(30, (int) (short) 10, 10);
        String str7 = nextDate3.run((int) (short) 100, 12, (int) (byte) -1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3147() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3147");
        NextDate nextDate3 = new NextDate(31, (int) 'a', 2001);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3148() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3148");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 10, 7);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3149() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3149");
        NextDate nextDate3 = new NextDate(0, (int) (short) -1, (int) (short) 10);
    }

    @Test
    public void test3150() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3150");
        NextDate nextDate3 = new NextDate((int) (short) 100, 10, (int) '#');
    }

    @Test
    public void test3151() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3151");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) 'a', 12, (int) (byte) 10);
        String str11 = nextDate3.run(31, (int) (byte) 1, (int) (byte) 10);
        String str15 = nextDate3.run((int) (short) 10, (int) (byte) 0, (int) 'a');
        String str19 = nextDate3.run(2001, (-1), 1);
        String str23 = nextDate3.run(2020, (int) '#', (int) (short) 10);
        String str27 = nextDate3.run((int) (short) 100, (int) (byte) 0, 0);
        String str31 = nextDate3.run(0, 30, (int) (short) -1);
        Class<?> wildcardClass32 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass32);
    }

    @Test
    public void test3152() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3152");
        NextDate nextDate3 = new NextDate(1, 7, (int) (byte) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3153() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3153");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) -1, 7);
    }

    @Test
    public void test3154() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3154");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) ' ', 12);
        String str7 = nextDate3.run(12, 100, 2001);
        String str11 = nextDate3.run(10, (int) (short) 100, 1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "1/1/2002" + "'", str7, "1/1/2002");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3155() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3155");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 0, 1);
        String str7 = nextDate3.run(2020, (int) (byte) 0, (int) (byte) 0);
        String str11 = nextDate3.run((int) (byte) 10, 0, (int) '#');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3156() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3156");
        NextDate nextDate3 = new NextDate(100, (int) (short) 0, 10);
        String str7 = nextDate3.run(2001, 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3157() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3157");
        NextDate nextDate3 = new NextDate(12, 100, (int) (short) -1);
        String str7 = nextDate3.run(0, 100, 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3158() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3158");
        NextDate nextDate3 = new NextDate((int) (short) 10, 0, (int) (byte) 1);
        String str7 = nextDate3.run(100, (int) '#', (int) (short) 1);
        String str11 = nextDate3.run(100, (int) (byte) 100, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3159() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3159");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 10, (int) (short) 0);
    }

    @Test
    public void test3160() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3160");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (short) 0, (int) ' ');
        String str7 = nextDate3.run((int) 'a', 1, (int) 'a');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3161() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3161");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 100, (int) (short) 1);
    }

    @Test
    public void test3162() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3162");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 1, 10);
        String str7 = nextDate3.run((int) (short) 0, 30, (int) (byte) 1);
        String str11 = nextDate3.run((int) (short) 100, 12, (int) (short) -1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3163() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3163");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 0, (int) (short) 0);
        String str7 = nextDate3.run(2001, 2001, (int) (short) 0);
        String str11 = nextDate3.run((-1), (int) (byte) 0, (int) (short) 100);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3164() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3164");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 100, (int) (short) 100);
        String str7 = nextDate3.run((int) (byte) 0, 0, 30);
        String str11 = nextDate3.run((int) (byte) 100, (int) (byte) 0, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3165() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3165");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) -1, (int) (short) 100);
        String str7 = nextDate3.run(2020, (int) (short) 10, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3166() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3166");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 31, (int) (byte) 100);
        String str7 = nextDate3.run((int) '4', (int) ' ', (int) (byte) 100);
        String str11 = nextDate3.run((-1), 2001, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3167() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3167");
        NextDate nextDate3 = new NextDate(7, (int) (byte) 100, (int) 'a');
    }

    @Test
    public void test3168() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3168");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) 100, 2020);
        String str7 = nextDate3.run(2001, (int) (byte) 10, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3169() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3169");
        NextDate nextDate3 = new NextDate(7, 10, (int) (short) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3170() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3170");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 30, (int) (byte) 0);
    }

    @Test
    public void test3171() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3171");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 10, (int) (byte) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3172() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3172");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) -1, 0);
        String str7 = nextDate3.run(2001, 0, (int) (short) 10);
        String str11 = nextDate3.run((int) ' ', (int) '4', (int) '#');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3173() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3173");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 10, (int) (short) 1);
        String str7 = nextDate3.run(30, (int) (byte) 1, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3174() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3174");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 1, (int) '#');
        String str7 = nextDate3.run((int) (short) -1, (int) '4', (int) (short) 100);
        String str11 = nextDate3.run((int) (short) 10, 7, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3175() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3175");
        NextDate nextDate3 = new NextDate((int) 'a', (int) 'a', 0);
        String str7 = nextDate3.run(30, (int) '#', (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3176() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3176");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 10, 0);
    }

    @Test
    public void test3177() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3177");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) 'a', 12, (int) (byte) 10);
        String str11 = nextDate3.run(31, (int) (byte) 1, (int) (byte) 10);
        String str15 = nextDate3.run((int) (short) 10, (int) (byte) 0, (int) 'a');
        String str19 = nextDate3.run(2001, (-1), 1);
        String str23 = nextDate3.run(2020, (int) '#', (int) (short) 10);
        String str27 = nextDate3.run((int) (byte) -1, (int) (byte) 0, (int) (byte) 1);
        Class<?> wildcardClass28 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test3178() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3178");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 10, (int) '4');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3179() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3179");
        NextDate nextDate3 = new NextDate((-1), (int) (short) -1, 31);
        String str7 = nextDate3.run(30, 12, 30);
        String str11 = nextDate3.run((-1), 1, (int) (byte) 0);
        String str15 = nextDate3.run((int) '4', (int) 'a', (int) (byte) 0);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3180() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3180");
        NextDate nextDate3 = new NextDate((-1), 0, 31);
        String str7 = nextDate3.run(10, (-1), (int) (short) -1);
        String str11 = nextDate3.run((int) (short) 10, 31, (int) 'a');
        String str15 = nextDate3.run((-1), (int) (short) 0, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test3181() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3181");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) 1, 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3182() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3182");
        NextDate nextDate3 = new NextDate(100, (int) (short) 100, (int) (short) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3183() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3183");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 1, (int) (byte) 10);
        String str7 = nextDate3.run((int) ' ', 10, (int) 'a');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3184() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3184");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (short) 100, (int) ' ');
    }

    @Test
    public void test3185() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3185");
        NextDate nextDate3 = new NextDate(2020, 0, 2001);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3186() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3186");
        NextDate nextDate3 = new NextDate(12, 2001, 2020);
        String str7 = nextDate3.run((int) (byte) 10, 0, (int) (short) 100);
        String str11 = nextDate3.run(2001, (int) 'a', (int) (short) 1);
        String str15 = nextDate3.run((int) ' ', (int) (short) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) (byte) 1, 100, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test3187() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3187");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        String str15 = nextDate3.run(7, (int) (short) 1, 30);
        String str19 = nextDate3.run((int) 'a', (int) '#', 1);
        String str23 = nextDate3.run((int) (byte) 0, (int) (short) 0, 1);
        String str27 = nextDate3.run((int) (short) 100, (int) ' ', (-1));
        String str31 = nextDate3.run(10, 100, (int) (short) 100);
        String str35 = nextDate3.run((int) (short) 0, (int) (byte) 10, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
    }

    @Test
    public void test3188() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3188");
        NextDate nextDate3 = new NextDate(31, (int) (short) 10, 2001);
    }

    @Test
    public void test3189() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3189");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 100, (int) (short) 100);
    }

    @Test
    public void test3190() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3190");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) -1, 0);
        String str7 = nextDate3.run((-1), 0, 12);
        String str11 = nextDate3.run((int) (short) 10, (int) '#', (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3191() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3191");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 100, 7);
    }

    @Test
    public void test3192() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3192");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 1, (int) (short) 0);
        String str7 = nextDate3.run(12, 100, (int) 'a');
        String str11 = nextDate3.run(0, (int) (byte) 100, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3193() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3193");
        NextDate nextDate3 = new NextDate((int) ' ', 30, 30);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3194() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3194");
        NextDate nextDate3 = new NextDate(0, (int) (byte) -1, 2020);
    }

    @Test
    public void test3195() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3195");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) '4', (-1));
        String str7 = nextDate3.run(7, 1, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3196() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3196");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 31, (int) '#');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3197() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3197");
        NextDate nextDate3 = new NextDate((int) (short) 1, 2020, (int) '4');
        String str7 = nextDate3.run((int) (short) 10, 0, 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3198() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3198");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 1, 30);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3199() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3199");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) -1, (int) (byte) 0);
        String str7 = nextDate3.run(2001, 30, (int) (short) 10);
        String str11 = nextDate3.run((int) (byte) 100, 30, (-1));
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3200() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3200");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) ' ', 2020);
    }

    @Test
    public void test3201() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3201");
        NextDate nextDate3 = new NextDate((int) (short) 1, 1, (int) (short) 10);
        String str7 = nextDate3.run((int) '#', (int) (short) 0, (int) (byte) 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3202() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3202");
        NextDate nextDate3 = new NextDate((int) 'a', 31, (int) (byte) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3203() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3203");
        NextDate nextDate3 = new NextDate(0, 100, (int) (short) 0);
        String str7 = nextDate3.run((int) 'a', (int) (byte) 10, (int) (byte) 0);
        String str11 = nextDate3.run(7, 100, (int) 'a');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3204() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3204");
        NextDate nextDate3 = new NextDate(2020, 10, (int) 'a');
    }

    @Test
    public void test3205() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3205");
        NextDate nextDate3 = new NextDate(2020, (int) (short) -1, 0);
        String str7 = nextDate3.run((int) 'a', 2020, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3206() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3206");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) 100, (int) (byte) 1);
    }

    @Test
    public void test3207() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3207");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 100, (int) (short) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3208() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3208");
        NextDate nextDate3 = new NextDate(7, (int) (byte) 0, 2020);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3209() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3209");
        NextDate nextDate3 = new NextDate(2001, 2020, (int) 'a');
    }

    @Test
    public void test3210() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3210");
        NextDate nextDate3 = new NextDate(0, 2020, 7);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3211() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3211");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) 0, (int) (byte) 0);
    }

    @Test
    public void test3212() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3212");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 0, (int) (short) 1);
        String str7 = nextDate3.run((int) (byte) 10, (int) (byte) 10, (int) 'a');
        String str11 = nextDate3.run((int) (byte) 0, 0, 100);
        String str15 = nextDate3.run((int) (byte) 10, (int) (byte) -1, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test3213() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3213");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 0, 7);
        String str7 = nextDate3.run((int) ' ', 0, (int) (short) 1);
        String str11 = nextDate3.run((int) (short) 10, (int) (byte) 100, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3214() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3214");
        NextDate nextDate3 = new NextDate((int) '4', (int) 'a', (-1));
    }

    @Test
    public void test3215() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3215");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) 0, (int) (byte) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3216() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3216");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(0, 2020, (int) (byte) 10);
        String str19 = nextDate3.run((int) (short) 0, 2020, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test3217() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3217");
        NextDate nextDate3 = new NextDate((int) (short) 0, 2001, 12);
    }

    @Test
    public void test3218() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3218");
        NextDate nextDate3 = new NextDate(31, 2001, 2001);
        String str7 = nextDate3.run((int) ' ', (int) (byte) -1, (-1));
        String str11 = nextDate3.run((int) (byte) 1, (int) '4', (int) (byte) -1);
        String str15 = nextDate3.run((int) ' ', (int) (byte) 0, (int) (short) 10);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3219() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3219");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) -1, (int) (short) 0);
    }

    @Test
    public void test3220() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3220");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 0, (int) (short) 1);
        String str7 = nextDate3.run((int) (short) 0, (int) (byte) 0, (int) (byte) 0);
        String str11 = nextDate3.run((int) ' ', (int) (byte) 0, (int) (short) 10);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3221() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3221");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 30, (int) (short) 1);
        String str7 = nextDate3.run((int) (byte) -1, 2001, 10);
        String str11 = nextDate3.run(100, (int) (byte) 10, 2001);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3222() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3222");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 100, 12);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3223() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3223");
        NextDate nextDate3 = new NextDate((int) '4', 31, 0);
        String str7 = nextDate3.run((int) (byte) -1, (int) 'a', 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3224() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3224");
        NextDate nextDate3 = new NextDate(0, 7, 10);
    }

    @Test
    public void test3225() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3225");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) 0, (int) (short) -1);
    }

    @Test
    public void test3226() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3226");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 100, (int) '#');
        String str7 = nextDate3.run(2020, (int) (byte) -1, (int) (short) 1);
        String str11 = nextDate3.run(0, (int) (byte) -1, (int) (byte) 100);
        String str15 = nextDate3.run((int) (short) 1, (int) (byte) -1, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test3227() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3227");
        NextDate nextDate3 = new NextDate(0, (int) '#', (int) (byte) 10);
    }

    @Test
    public void test3228() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3228");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) 'a', 10);
        String str7 = nextDate3.run((int) (short) 10, (int) (short) 1, 2020);
        String str11 = nextDate3.run((int) (short) 0, (int) (byte) -1, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "10/2/2020" + "'", str7, "10/2/2020");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3229() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3229");
        NextDate nextDate3 = new NextDate(7, (int) 'a', 30);
    }

    @Test
    public void test3230() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3230");
        NextDate nextDate3 = new NextDate(2001, 31, (int) (byte) -1);
        String str7 = nextDate3.run((int) ' ', 1, 100);
        String str11 = nextDate3.run((int) (byte) 10, (int) ' ', 31);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3231() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3231");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 0, 1);
        String str7 = nextDate3.run(2020, (int) (byte) 0, (int) (byte) 0);
        String str11 = nextDate3.run((-1), (int) (byte) 100, (int) ' ');
        String str15 = nextDate3.run(100, 1, 0);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3232() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3232");
        NextDate nextDate3 = new NextDate(1, 2001, (int) (byte) 100);
        String str7 = nextDate3.run((int) (byte) 100, (int) (byte) 10, (int) (short) 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3233() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3233");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 0, (int) (short) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3234() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3234");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 31, 0);
        String str7 = nextDate3.run((int) (byte) 10, (int) (short) -1, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3235() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3235");
        NextDate nextDate3 = new NextDate((int) (short) -1, 100, 0);
        String str7 = nextDate3.run((int) (short) -1, (int) (short) 100, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3236() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3236");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (byte) 100, 30);
        String str7 = nextDate3.run((int) (byte) 10, (-1), (int) (byte) -1);
        String str11 = nextDate3.run((int) (byte) 1, 10, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3237() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3237");
        NextDate nextDate3 = new NextDate(100, 30, (int) (short) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3238() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3238");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 7, 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3239() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3239");
        NextDate nextDate3 = new NextDate(0, 2020, (int) (short) 0);
        String str7 = nextDate3.run(1, (int) (byte) 0, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3240() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3240");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) '#', (-1));
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3241() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3241");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 100, 31);
    }

    @Test
    public void test3242() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3242");
        NextDate nextDate3 = new NextDate(31, (int) (short) 10, (int) ' ');
        String str7 = nextDate3.run((int) (short) 1, 0, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3243() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3243");
        NextDate nextDate3 = new NextDate(31, 2001, (int) '#');
    }

    @Test
    public void test3244() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3244");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 1, (int) (byte) 10);
        String str7 = nextDate3.run(1, 100, 0);
        String str11 = nextDate3.run((int) (byte) 0, 0, 10);
        String str15 = nextDate3.run((int) (short) 0, (int) (short) 0, 10);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3245() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3245");
        NextDate nextDate3 = new NextDate((int) (short) 10, 100, (int) (byte) 100);
        String str7 = nextDate3.run((int) (byte) 10, (int) (byte) 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3246() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3246");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 10, (int) (short) -1);
        String str7 = nextDate3.run((int) (byte) 100, (int) '#', 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3247() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3247");
        NextDate nextDate3 = new NextDate((int) (short) 100, 2001, (int) (byte) 0);
        String str7 = nextDate3.run(0, (int) ' ', 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3248() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3248");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 0, (int) (byte) 10);
        String str7 = nextDate3.run(10, (int) '#', 30);
        String str11 = nextDate3.run(31, (int) (short) 10, (int) (byte) 1);
        String str15 = nextDate3.run((int) (byte) 100, (int) (byte) 10, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test3249() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3249");
        NextDate nextDate3 = new NextDate(7, (-1), 2020);
    }

    @Test
    public void test3250() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3250");
        NextDate nextDate3 = new NextDate(10, (int) '#', 12);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3251() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3251");
        NextDate nextDate3 = new NextDate(1, (int) '4', (int) 'a');
        String str7 = nextDate3.run((int) '#', 7, (int) (short) 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3252() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3252");
        NextDate nextDate3 = new NextDate((int) (short) 0, 30, (int) (byte) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3253() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3253");
        NextDate nextDate3 = new NextDate((int) '#', 0, (-1));
        String str7 = nextDate3.run(0, 0, 0);
        String str11 = nextDate3.run((int) (byte) 100, (int) (byte) 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3254() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3254");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 2020, (int) '4');
    }

    @Test
    public void test3255() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3255");
        NextDate nextDate3 = new NextDate(0, (int) (byte) -1, 100);
    }

    @Test
    public void test3256() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3256");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run((int) (byte) 10, (int) (short) 100, 100);
        String str27 = nextDate3.run((int) (short) 100, (int) (short) 0, (int) (byte) 0);
        String str31 = nextDate3.run(2020, 30, 7);
        String str35 = nextDate3.run((int) (short) 10, (int) (byte) -1, 100);
        String str39 = nextDate3.run((int) (byte) 100, (int) (byte) 10, (int) '4');
        Class<?> wildcardClass40 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass40);
    }

    @Test
    public void test3257() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3257");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) 100, (int) 'a');
    }

    @Test
    public void test3258() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3258");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) 'a', (int) '#');
        String str7 = nextDate3.run(12, 30, (int) (byte) 10);
        String str11 = nextDate3.run((int) (short) -1, (int) ' ', (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3259() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3259");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) -1, (int) '4');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3260() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3260");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) -1, 2001);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3261() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3261");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 0, (int) (byte) 1);
        String str7 = nextDate3.run(0, (int) (byte) 10, (int) (byte) 0);
        String str11 = nextDate3.run(10, (int) (short) -1, (int) (byte) -1);
        String str15 = nextDate3.run(7, 7, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test3262() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3262");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 100, 10);
        String str7 = nextDate3.run((int) (short) 100, 0, (int) 'a');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3263() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3263");
        NextDate nextDate3 = new NextDate((int) '4', (int) '4', 1);
        String str7 = nextDate3.run((int) '4', 0, 100);
        String str11 = nextDate3.run((int) (short) -1, 100, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3264() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3264");
        NextDate nextDate3 = new NextDate(31, (int) 'a', (-1));
        String str7 = nextDate3.run((int) (byte) 1, 100, (int) (byte) -1);
        String str11 = nextDate3.run(7, (int) (short) 1, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "7/1/2001" + "'", str11, "7/1/2001");
    }

    @Test
    public void test3265() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3265");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) 100, (int) (short) 0);
        String str7 = nextDate3.run(10, (int) (short) 10, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3266() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3266");
        NextDate nextDate3 = new NextDate(7, (int) (short) -1, (int) (byte) 10);
        String str7 = nextDate3.run(10, 30, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3267() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3267");
        NextDate nextDate3 = new NextDate(0, (int) (short) -1, 0);
    }

    @Test
    public void test3268() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3268");
        NextDate nextDate3 = new NextDate(0, 12, (int) (byte) 1);
    }

    @Test
    public void test3269() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3269");
        NextDate nextDate3 = new NextDate(1, 100, (int) (byte) 10);
        String str7 = nextDate3.run(12, (int) (short) 0, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3270() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3270");
        NextDate nextDate3 = new NextDate(1, (int) ' ', (int) (byte) 100);
        String str7 = nextDate3.run((int) (byte) 100, (int) '#', (int) (short) -1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3271() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3271");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run(7, (int) (short) 10, (int) (short) 1);
        String str27 = nextDate3.run((int) 'a', (int) (byte) 1, 0);
        String str31 = nextDate3.run((int) (byte) 0, (int) ' ', (int) (short) 10);
        String str35 = nextDate3.run((int) (short) -1, (int) '4', 7);
        String str39 = nextDate3.run(0, (int) (byte) 0, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
    }

    @Test
    public void test3272() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3272");
        NextDate nextDate3 = new NextDate((-1), 100, 0);
    }

    @Test
    public void test3273() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3273");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 10, 7);
    }

    @Test
    public void test3274() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3274");
        NextDate nextDate3 = new NextDate(30, (int) (short) 0, 2020);
        String str7 = nextDate3.run(31, 100, (int) '4');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3275() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3275");
        NextDate nextDate3 = new NextDate((-1), 100, 100);
        String str7 = nextDate3.run((int) (short) 10, (int) (short) 0, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3276() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3276");
        NextDate nextDate3 = new NextDate((int) '#', 31, (int) (byte) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3277() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3277");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 100, (int) '4');
        String str7 = nextDate3.run(100, 0, 10);
        String str11 = nextDate3.run(0, (int) (short) -1, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3278() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3278");
        NextDate nextDate3 = new NextDate(0, (int) '4', 0);
        String str7 = nextDate3.run((int) (byte) -1, 0, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3279() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3279");
        NextDate nextDate3 = new NextDate((int) (short) 1, 30, (int) (byte) 100);
        String str7 = nextDate3.run((int) (byte) 0, (int) (short) -1, 7);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3280() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3280");
        NextDate nextDate3 = new NextDate((int) (short) 10, 100, (-1));
    }

    @Test
    public void test3281() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3281");
        NextDate nextDate3 = new NextDate(7, 7, (int) ' ');
        String str7 = nextDate3.run((int) (short) 0, (int) 'a', (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3282() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3282");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (-1), (int) '#');
        String str7 = nextDate3.run(7, 12, (int) '4');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3283() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3283");
        NextDate nextDate3 = new NextDate(10, 30, (int) (short) -1);
    }

    @Test
    public void test3284() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3284");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) '#', 7);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3285() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3285");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 1, (int) (short) -1);
    }

    @Test
    public void test3286() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3286");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 10, (int) '#');
        String str7 = nextDate3.run(10, (int) (byte) 100, 0);
        String str11 = nextDate3.run((int) '4', (int) (byte) 100, 12);
        String str15 = nextDate3.run((int) (byte) 100, 2001, (int) 'a');
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3287() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3287");
        NextDate nextDate3 = new NextDate(2020, 0, 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3288() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3288");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        String str15 = nextDate3.run((int) (byte) 1, (int) (short) 0, (int) (short) 100);
        String str19 = nextDate3.run((int) (byte) 0, (int) ' ', 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test3289() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3289");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 0, (-1));
        String str7 = nextDate3.run(10, (int) '4', (int) (short) 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3290() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3290");
        NextDate nextDate3 = new NextDate(100, 100, (int) (short) 1);
        String str7 = nextDate3.run((int) '4', 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3291() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3291");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) 'a', (int) ' ');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3292() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3292");
        NextDate nextDate3 = new NextDate(2001, 0, (int) (byte) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3293() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3293");
        NextDate nextDate3 = new NextDate((int) (short) 1, 2020, 0);
        String str7 = nextDate3.run(12, (int) (byte) 0, (int) 'a');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3294() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3294");
        NextDate nextDate3 = new NextDate(12, (int) (short) 1, 2001);
        String str7 = nextDate3.run(2001, 100, 2020);
        String str11 = nextDate3.run((int) (short) 100, (int) 'a', (int) (short) 10);
        String str15 = nextDate3.run((int) 'a', (int) (byte) 10, (int) ' ');
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3295() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3295");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) '4', (-1));
    }

    @Test
    public void test3296() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3296");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) '4', 12, 10);
        String str15 = nextDate3.run((int) (byte) 10, (int) '4', 7);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3297() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3297");
        NextDate nextDate3 = new NextDate(30, (-1), 31);
        String str7 = nextDate3.run(7, (int) (short) 100, 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3298() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3298");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 100, (-1));
    }

    @Test
    public void test3299() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3299");
        NextDate nextDate3 = new NextDate(31, (int) (byte) 10, 100);
        String str7 = nextDate3.run(2020, 12, 2020);
        String str11 = nextDate3.run((int) '4', 100, (-1));
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3300() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3300");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 100, (int) (short) -1);
    }

    @Test
    public void test3301() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3301");
        NextDate nextDate3 = new NextDate(12, 2020, 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3302() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3302");
        NextDate nextDate3 = new NextDate((int) (short) 100, 0, 12);
        String str7 = nextDate3.run((int) (short) 100, (int) (short) 0, (int) (byte) 1);
        String str11 = nextDate3.run((int) (byte) 1, (int) ' ', (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3303() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3303");
        NextDate nextDate3 = new NextDate((int) (short) 10, 30, 12);
    }

    @Test
    public void test3304() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3304");
        NextDate nextDate3 = new NextDate(30, 12, 2001);
    }

    @Test
    public void test3305() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3305");
        NextDate nextDate3 = new NextDate((int) (short) -1, 10, (int) (byte) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3306() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3306");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 2001, (int) (byte) 1);
    }

    @Test
    public void test3307() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3307");
        NextDate nextDate3 = new NextDate(0, 31, 2020);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3308() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3308");
        NextDate nextDate3 = new NextDate(10, (-1), 12);
        String str7 = nextDate3.run((int) (short) 1, 31, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3309() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3309");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 10, 30);
        String str7 = nextDate3.run((int) '#', 100, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3310() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3310");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 10, (int) (byte) 0);
    }

    @Test
    public void test3311() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3311");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) '4', 1);
        String str7 = nextDate3.run((int) 'a', 7, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3312() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3312");
        NextDate nextDate3 = new NextDate(31, 30, (int) (short) 0);
        String str7 = nextDate3.run(0, (int) (byte) -1, (int) (byte) 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3313() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3313");
        NextDate nextDate3 = new NextDate(2020, (int) ' ', (int) (byte) 100);
    }

    @Test
    public void test3314() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3314");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) -1, 100);
        String str7 = nextDate3.run(7, 10, (int) '4');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3315() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3315");
        NextDate nextDate3 = new NextDate((int) (short) -1, 7, (int) (byte) -1);
    }

    @Test
    public void test3316() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3316");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 1, (int) (short) 100);
        String str7 = nextDate3.run(100, (int) 'a', (int) (short) -1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3317() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3317");
        NextDate nextDate3 = new NextDate(7, (int) (short) 10, (int) 'a');
    }

    @Test
    public void test3318() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3318");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 1, (int) (short) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3319() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3319");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) 1, (int) (short) 10);
        String str7 = nextDate3.run((int) '#', (int) (byte) 1, 0);
        String str11 = nextDate3.run((int) 'a', 100, (int) 'a');
        String str15 = nextDate3.run((int) (short) 100, (int) (short) 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test3320() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3320");
        NextDate nextDate3 = new NextDate((int) (short) 0, 0, (int) ' ');
        String str7 = nextDate3.run(31, (int) (short) 100, (int) (short) 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3321() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3321");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 100, 7);
        String str7 = nextDate3.run(1, (int) (byte) 0, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3322() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3322");
        NextDate nextDate3 = new NextDate((int) '4', 100, 100);
        String str7 = nextDate3.run((int) (short) 1, (int) (byte) -1, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3323() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3323");
        NextDate nextDate3 = new NextDate(7, 100, (int) (short) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3324() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3324");
        NextDate nextDate3 = new NextDate(10, (int) (short) 0, 10);
        String str7 = nextDate3.run((int) (short) 10, 100, 1);
        String str11 = nextDate3.run(0, 2020, (int) (short) 10);
        String str15 = nextDate3.run((int) (short) 0, 12, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test3325() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3325");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) -1, (int) 'a');
    }

    @Test
    public void test3326() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3326");
        NextDate nextDate3 = new NextDate(100, (int) (short) 100, (-1));
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3327() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3327");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) -1, (int) (short) 100);
        String str7 = nextDate3.run((int) (byte) 1, 1, 12);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3328() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3328");
        NextDate nextDate3 = new NextDate(0, 12, (int) (byte) 0);
    }

    @Test
    public void test3329() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3329");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 10, (int) (short) 10);
    }

    @Test
    public void test3330() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3330");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) 100, 2020);
        String str7 = nextDate3.run(30, (int) ' ', 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3331() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3331");
        NextDate nextDate3 = new NextDate(0, (int) '4', 100);
    }

    @Test
    public void test3332() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3332");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) 1, (int) (byte) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3333() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3333");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 1, 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3334() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3334");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 100, (int) '4');
        String str7 = nextDate3.run((int) 'a', 2020, 31);
        String str11 = nextDate3.run((int) (short) -1, 2020, (int) (byte) 10);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3335() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3335");
        NextDate nextDate3 = new NextDate(100, (int) (byte) -1, 2001);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3336() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3336");
        NextDate nextDate3 = new NextDate(30, (int) (short) 10, 12);
    }

    @Test
    public void test3337() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3337");
        NextDate nextDate3 = new NextDate(30, (int) 'a', 0);
        String str7 = nextDate3.run((int) 'a', (int) (short) -1, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3338() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3338");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 7, (int) 'a');
    }

    @Test
    public void test3339() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3339");
        NextDate nextDate3 = new NextDate((-1), (int) (short) -1, 31);
        String str7 = nextDate3.run((int) (short) 1, (int) (byte) 0, 7);
        String str11 = nextDate3.run(100, (int) (byte) 1, (int) ' ');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3340() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3340");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (short) 100, 0);
        String str7 = nextDate3.run((int) '#', (int) (short) -1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3341() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3341");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 10, (int) (byte) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3342() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3342");
        NextDate nextDate3 = new NextDate((int) ' ', (-1), 0);
        String str7 = nextDate3.run((int) '#', 1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3343() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3343");
        NextDate nextDate3 = new NextDate((int) (short) 10, 2001, 10);
    }

    @Test
    public void test3344() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3344");
        NextDate nextDate3 = new NextDate(100, (int) (byte) -1, 10);
        String str7 = nextDate3.run((int) (short) 1, (int) (short) 10, 31);
        String str11 = nextDate3.run((int) (byte) 100, (int) (byte) 0, (int) (byte) 10);
        String str15 = nextDate3.run((int) (byte) 100, (int) (byte) -1, (int) '4');
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3345() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3345");
        NextDate nextDate3 = new NextDate(100, 12, 31);
    }

    @Test
    public void test3346() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3346");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 2020, 100);
        String str7 = nextDate3.run((int) (byte) 10, (int) 'a', (int) '#');
        String str11 = nextDate3.run((int) (byte) 0, (int) (short) 1, (int) '4');
        String str15 = nextDate3.run((int) '#', (int) '4', (int) (byte) 100);
        String str19 = nextDate3.run((int) (byte) 1, 2001, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test3347() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3347");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) ' ', 10);
        String str7 = nextDate3.run((int) ' ', (int) (byte) 100, (int) (byte) 1);
        String str11 = nextDate3.run(0, 10, 2001);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3348() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3348");
        NextDate nextDate3 = new NextDate(10, (int) '#', 31);
    }

    @Test
    public void test3349() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3349");
        NextDate nextDate3 = new NextDate((int) ' ', 1, (-1));
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3350() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3350");
        NextDate nextDate3 = new NextDate(0, 1, (int) ' ');
        String str7 = nextDate3.run((int) (byte) 100, (int) (byte) 100, 31);
        String str11 = nextDate3.run((int) '4', 100, (int) (byte) 10);
        String str15 = nextDate3.run((int) (short) 1, 31, (int) (short) 100);
        String str19 = nextDate3.run((int) (short) -1, (int) '4', 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test3351() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3351");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) 100, 31);
    }

    @Test
    public void test3352() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3352");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) ' ', 2020, (int) (short) 0);
        String str11 = nextDate3.run(31, (int) '4', (int) (byte) -1);
        String str15 = nextDate3.run((int) (byte) 100, 7, (int) (short) 100);
        String str19 = nextDate3.run((int) (short) 1, (int) (byte) 100, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test3353() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3353");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) 100, (int) (short) 10);
    }

    @Test
    public void test3354() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3354");
        NextDate nextDate3 = new NextDate(2020, 0, (int) (byte) 100);
        String str7 = nextDate3.run(100, 1, (int) '4');
        String str11 = nextDate3.run((int) (byte) 100, 1, (int) (byte) 1);
        String str15 = nextDate3.run(0, 0, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test3355() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3355");
        NextDate nextDate3 = new NextDate(2001, (int) (short) 0, 10);
        String str7 = nextDate3.run(31, (int) (byte) 100, (int) (byte) -1);
        String str11 = nextDate3.run(7, 10, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3356() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3356");
        NextDate nextDate3 = new NextDate(1, 31, (int) (short) 10);
    }

    @Test
    public void test3357() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3357");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run(31, 31, 12);
        String str23 = nextDate3.run(10, 2020, (int) (byte) -1);
        String str27 = nextDate3.run(2001, (int) (short) 100, 7);
        String str31 = nextDate3.run((int) (short) 100, (int) (byte) 0, (int) (short) 10);
        Class<?> wildcardClass32 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass32);
    }

    @Test
    public void test3358() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3358");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 1, 10);
        String str7 = nextDate3.run(2020, 7, 100);
        String str11 = nextDate3.run(7, 30, (int) (byte) 10);
        String str15 = nextDate3.run((int) '4', (int) (short) 100, 12);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3359() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3359");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) 10, 1);
        String str7 = nextDate3.run(12, (int) (short) -1, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3360() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3360");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 30, (int) (byte) -1);
        String str7 = nextDate3.run((int) (byte) -1, (int) '#', 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3361() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3361");
        NextDate nextDate3 = new NextDate(10, (int) (short) -1, (int) (short) 100);
    }

    @Test
    public void test3362() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3362");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) -1, 7);
        String str7 = nextDate3.run((int) (short) 100, 10, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3363() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3363");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) 1, (int) (short) 10);
    }

    @Test
    public void test3364() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3364");
        NextDate nextDate3 = new NextDate((int) (short) 100, 10, (int) (byte) 1);
    }

    @Test
    public void test3365() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3365");
        NextDate nextDate3 = new NextDate((int) '#', (int) 'a', (int) (byte) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3366() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3366");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) -1, (int) (byte) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3367() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3367");
        NextDate nextDate3 = new NextDate(100, (int) (byte) -1, 10);
        String str7 = nextDate3.run((int) (short) 1, (int) (short) 10, 31);
        String str11 = nextDate3.run((int) (short) 10, (int) '#', 7);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3368() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3368");
        NextDate nextDate3 = new NextDate(2001, 10, 2001);
        String str7 = nextDate3.run((int) '4', (int) (byte) 1, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3369() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3369");
        NextDate nextDate3 = new NextDate(7, (int) 'a', (int) (byte) 0);
        String str7 = nextDate3.run(30, 7, (int) (short) 100);
        String str11 = nextDate3.run(10, (int) (short) 1, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3370() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3370");
        NextDate nextDate3 = new NextDate(2020, 2001, (-1));
        String str7 = nextDate3.run(10, (int) (short) 10, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "10/11/2001" + "'", str7, "10/11/2001");
    }

    @Test
    public void test3371() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3371");
        NextDate nextDate3 = new NextDate(2020, 0, 100);
        String str7 = nextDate3.run((int) '#', (int) (short) 100, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3372() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3372");
        NextDate nextDate3 = new NextDate(2020, 2001, 0);
        String str7 = nextDate3.run(2001, (-1), (int) (byte) -1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3373() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3373");
        NextDate nextDate3 = new NextDate(10, (int) (short) 10, 12);
    }

    @Test
    public void test3374() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3374");
        NextDate nextDate3 = new NextDate(7, (int) '4', 31);
    }

    @Test
    public void test3375() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3375");
        NextDate nextDate3 = new NextDate(0, (int) (short) -1, (int) '4');
        String str7 = nextDate3.run((-1), (int) (byte) 1, 30);
        String str11 = nextDate3.run(2020, (-1), (int) (short) 0);
        String str15 = nextDate3.run(0, 2020, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test3376() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3376");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run(7, (int) (short) 10, (int) (short) 1);
        String str27 = nextDate3.run((int) 'a', (int) (byte) 1, 0);
        String str31 = nextDate3.run((int) (byte) 0, (int) ' ', (int) (short) 10);
        String str35 = nextDate3.run((int) (short) -1, (int) '4', 7);
        String str39 = nextDate3.run(0, (int) (short) 10, 12);
        String str43 = nextDate3.run((int) (byte) -1, (int) (short) 0, (int) '4');
        String str47 = nextDate3.run((int) (byte) 0, (int) 'a', (int) 'a');
        String str51 = nextDate3.run(2001, (int) '#', (int) (byte) 100);
        String str55 = nextDate3.run((int) ' ', 2020, 30);
        Class<?> wildcardClass56 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str43 + "' != '" + "invalid Input Date" + "'", str43, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str47 + "' != '" + "invalid Input Date" + "'", str47, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str51 + "' != '" + "invalid Input Date" + "'", str51, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str55 + "' != '" + "invalid Input Date" + "'", str55, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass56);
    }

    @Test
    public void test3377() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3377");
        NextDate nextDate3 = new NextDate(7, 2020, 12);
        String str7 = nextDate3.run((int) (short) 0, 31, (int) (short) 100);
        String str11 = nextDate3.run((int) (byte) 100, 0, (int) 'a');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3378() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3378");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) -1, 0);
        String str7 = nextDate3.run((int) (short) -1, (int) (byte) 100, 2020);
        String str11 = nextDate3.run((int) (short) -1, (int) (short) 10, (int) (byte) 10);
        String str15 = nextDate3.run(100, 0, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test3379() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3379");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 0, 30);
        String str7 = nextDate3.run(31, (int) (byte) 100, 30);
        String str11 = nextDate3.run((int) 'a', 12, 1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3380() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3380");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 12, 10);
        String str7 = nextDate3.run(0, (int) (byte) 10, (int) (byte) 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3381() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3381");
        NextDate nextDate3 = new NextDate(12, 2001, 2020);
        String str7 = nextDate3.run(31, (int) (short) 10, 2020);
        String str11 = nextDate3.run((int) 'a', 30, 100);
        String str15 = nextDate3.run((int) 'a', 2001, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test3382() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3382");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) -1, 100);
    }

    @Test
    public void test3383() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3383");
        NextDate nextDate3 = new NextDate((int) ' ', 100, 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3384() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3384");
        NextDate nextDate3 = new NextDate(0, 10, (int) (byte) 100);
    }

    @Test
    public void test3385() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3385");
        NextDate nextDate3 = new NextDate(31, 10, (-1));
        String str7 = nextDate3.run(2001, (int) (byte) -1, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3386() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3386");
        NextDate nextDate3 = new NextDate(0, (-1), (int) (short) 100);
        String str7 = nextDate3.run((int) (short) 1, 30, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3387() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3387");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) ' ', 12);
        String str7 = nextDate3.run(12, 100, 2001);
        String str11 = nextDate3.run((int) (byte) 1, 100, (int) (short) 0);
        String str15 = nextDate3.run((int) (byte) 10, (int) '4', (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "1/1/2002" + "'", str7, "1/1/2002");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test3388() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3388");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 1, (-1));
        String str7 = nextDate3.run(7, (int) (byte) 100, 12);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3389() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3389");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 100, (int) 'a');
        String str7 = nextDate3.run(0, (int) (short) 10, (int) '4');
        String str11 = nextDate3.run((int) (short) 10, (int) (short) -1, 7);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3390() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3390");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 100, 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3391() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3391");
        NextDate nextDate3 = new NextDate((int) (short) 100, 31, 2020);
        String str7 = nextDate3.run((int) (short) 0, (int) (short) -1, (int) '4');
        String str11 = nextDate3.run(31, (int) (short) -1, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3392() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3392");
        NextDate nextDate3 = new NextDate(12, (-1), (int) (short) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3393() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3393");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) '4', 10);
    }

    @Test
    public void test3394() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3394");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 10, (int) (byte) 0);
        String str7 = nextDate3.run((int) (short) 0, 0, (int) '4');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3395() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3395");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) 10, (int) (byte) -1);
    }

    @Test
    public void test3396() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3396");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        String str15 = nextDate3.run((int) (byte) 1, (int) (short) 0, (int) (short) 100);
        String str19 = nextDate3.run((int) (short) 10, 0, 10);
        String str23 = nextDate3.run(2020, (-1), (int) (byte) 10);
        String str27 = nextDate3.run(7, 31, 100);
        String str31 = nextDate3.run((int) (byte) -1, (int) (byte) 1, (int) ' ');
        String str35 = nextDate3.run(0, (int) '#', 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
    }

    @Test
    public void test3397() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3397");
        NextDate nextDate3 = new NextDate(100, 2020, 2001);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3398() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3398");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 100, (int) '4');
    }

    @Test
    public void test3399() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3399");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) ' ', (int) (short) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3400() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3400");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) 'a', 12, (int) (byte) 10);
        String str11 = nextDate3.run(31, (int) (byte) 1, (int) (byte) 10);
        String str15 = nextDate3.run((int) (short) 10, (int) (byte) 0, (int) 'a');
        String str19 = nextDate3.run(2001, (-1), 1);
        String str23 = nextDate3.run((int) (byte) 100, (int) (short) 1, (int) 'a');
        String str27 = nextDate3.run(0, 30, 10);
        String str31 = nextDate3.run((-1), 10, 0);
        Class<?> wildcardClass32 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass32);
    }

    @Test
    public void test3401() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3401");
        NextDate nextDate3 = new NextDate(10, (int) (short) 1, 31);
        String str7 = nextDate3.run(0, (int) (short) 10, 0);
        String str11 = nextDate3.run((int) (short) -1, 1, 0);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3402() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3402");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) 'a', (int) (short) 100);
        String str7 = nextDate3.run((int) (byte) 100, 1, 31);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3403() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3403");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 30, 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3404() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3404");
        NextDate nextDate3 = new NextDate((int) (short) 100, 0, 1);
        String str7 = nextDate3.run(0, (int) ' ', 2001);
        String str11 = nextDate3.run(7, 31, 0);
        String str15 = nextDate3.run((int) (byte) 0, (int) (byte) -1, 1);
        String str19 = nextDate3.run(2001, (int) (byte) 0, 30);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test3405() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3405");
        NextDate nextDate3 = new NextDate(12, (int) (byte) 1, 2001);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3406() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3406");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 100, 0);
        String str7 = nextDate3.run((int) (byte) 100, 10, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3407() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3407");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 0, 31);
    }

    @Test
    public void test3408() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3408");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 1, (int) (byte) -1);
    }

    @Test
    public void test3409() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3409");
        NextDate nextDate3 = new NextDate(1, (int) (short) 100, (int) (short) 1);
    }

    @Test
    public void test3410() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3410");
        NextDate nextDate3 = new NextDate(30, (int) (byte) 0, 2001);
        String str7 = nextDate3.run((int) (byte) 10, 12, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3411() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3411");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) 0, 2020);
    }

    @Test
    public void test3412() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3412");
        NextDate nextDate3 = new NextDate((int) (short) 100, 10, 0);
    }

    @Test
    public void test3413() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3413");
        NextDate nextDate3 = new NextDate((-1), 30, (int) (byte) 0);
    }

    @Test
    public void test3414() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3414");
        NextDate nextDate3 = new NextDate(7, (int) (byte) 0, 2020);
        String str7 = nextDate3.run((int) (short) 0, 31, 2020);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3415() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3415");
        NextDate nextDate3 = new NextDate((int) (short) 100, 0, 1);
        String str7 = nextDate3.run(0, (int) ' ', 2001);
        String str11 = nextDate3.run(7, 31, 0);
        String str15 = nextDate3.run((int) (byte) 0, (int) (byte) -1, 1);
        String str19 = nextDate3.run((int) (byte) 1, (int) (short) -1, 30);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test3416() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3416");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) ' ', (int) (short) 1);
        String str7 = nextDate3.run((int) (short) 100, (int) 'a', (int) (byte) -1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3417() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3417");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) '4', 2020);
        String str7 = nextDate3.run(0, 31, 2020);
        String str11 = nextDate3.run((int) 'a', 0, (int) (short) 10);
        String str15 = nextDate3.run((int) (short) 0, 30, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test3418() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3418");
        NextDate nextDate3 = new NextDate(0, 7, (int) (byte) 10);
        String str7 = nextDate3.run((int) '4', (int) '4', 10);
        String str11 = nextDate3.run((int) (byte) 10, 0, 2020);
        String str15 = nextDate3.run((int) (byte) -1, (int) (short) -1, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test3419() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3419");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (byte) 0, (int) (short) 100);
        String str7 = nextDate3.run((int) (short) 10, 2020, (int) '4');
        String str11 = nextDate3.run((int) (byte) 1, 0, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3420() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3420");
        NextDate nextDate3 = new NextDate(30, 0, 10);
        String str7 = nextDate3.run((int) (short) 100, (int) ' ', (-1));
        String str11 = nextDate3.run(12, (-1), 12);
        String str15 = nextDate3.run((-1), (int) (byte) 100, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test3421() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3421");
        NextDate nextDate3 = new NextDate((int) '#', 0, (int) (byte) 10);
        String str7 = nextDate3.run((int) (short) 100, 100, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3422() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3422");
        NextDate nextDate3 = new NextDate((int) '#', 2001, (int) ' ');
    }

    @Test
    public void test3423() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3423");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) -1, 31);
        String str7 = nextDate3.run(0, 0, 31);
        String str11 = nextDate3.run((int) (short) 1, (int) (byte) 1, (int) '4');
        String str15 = nextDate3.run(30, (int) (short) -1, (int) (short) 1);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3424() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3424");
        NextDate nextDate3 = new NextDate(2001, (int) ' ', (int) (byte) 1);
    }

    @Test
    public void test3425() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3425");
        NextDate nextDate3 = new NextDate(1, 2020, (int) (short) 100);
    }

    @Test
    public void test3426() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3426");
        NextDate nextDate3 = new NextDate((int) ' ', 1, (int) (short) -1);
    }

    @Test
    public void test3427() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3427");
        NextDate nextDate3 = new NextDate(0, (int) (short) -1, (int) (byte) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3428() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3428");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 100, (int) (short) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3429() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3429");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) '#', 30);
        String str7 = nextDate3.run((int) '4', (int) (byte) 1, (-1));
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3430() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3430");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 0, 30);
        String str7 = nextDate3.run((int) (short) 10, 0, (int) (byte) -1);
        String str11 = nextDate3.run((int) (short) -1, (int) (byte) 0, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3431() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3431");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) -1, (int) (byte) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3432() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3432");
        NextDate nextDate3 = new NextDate(12, (int) (short) -1, (int) (short) -1);
        String str7 = nextDate3.run((int) (byte) 10, 100, (int) (short) -1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3433() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3433");
        NextDate nextDate3 = new NextDate((int) '#', 100, 0);
        String str7 = nextDate3.run((int) (byte) 10, (int) (short) 10, (int) 'a');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3434() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3434");
        NextDate nextDate3 = new NextDate((int) 'a', (int) ' ', (-1));
    }

    @Test
    public void test3435() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3435");
        NextDate nextDate3 = new NextDate(30, (int) (short) 100, (int) (byte) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3436() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3436");
        NextDate nextDate3 = new NextDate(100, 100, 1);
        String str7 = nextDate3.run(10, (int) (byte) 10, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3437() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3437");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) 1, 12);
        String str7 = nextDate3.run(30, (int) ' ', 12);
        String str11 = nextDate3.run((int) ' ', 12, 0);
        String str15 = nextDate3.run(1, (int) (byte) -1, (int) '4');
        String str19 = nextDate3.run(0, (int) '#', (int) ' ');
        String str23 = nextDate3.run((int) (short) 0, 2001, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test3438() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3438");
        NextDate nextDate3 = new NextDate((-1), 7, 100);
        String str7 = nextDate3.run((int) (byte) 100, (int) '4', 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3439() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3439");
        NextDate nextDate3 = new NextDate(31, 30, 31);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3440() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3440");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) 10, 10);
        String str7 = nextDate3.run(30, 12, (int) (byte) 10);
        String str11 = nextDate3.run((int) (byte) -1, (int) (short) 0, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3441() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3441");
        NextDate nextDate3 = new NextDate((int) '4', (int) ' ', 30);
    }

    @Test
    public void test3442() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3442");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) 'a', 7);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3443() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3443");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 100, (int) (short) 0);
        String str7 = nextDate3.run((int) (short) 10, (int) (short) 1, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3444() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3444");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 0, 0);
    }

    @Test
    public void test3445() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3445");
        NextDate nextDate3 = new NextDate(12, (int) 'a', (int) (short) 0);
        String str7 = nextDate3.run((int) (short) 10, (int) (byte) -1, 2020);
        String str11 = nextDate3.run((int) (byte) 0, 30, 7);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3446() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3446");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) 100, (int) (short) 10);
        String str7 = nextDate3.run(31, 12, (int) (byte) 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3447() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3447");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) 'a', 2020);
        String str7 = nextDate3.run(12, (int) (byte) 1, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3448() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3448");
        NextDate nextDate3 = new NextDate(12, (int) '4', 1);
        String str7 = nextDate3.run(7, (int) (byte) 100, (int) (short) -1);
        String str11 = nextDate3.run(0, (int) (byte) 1, 10);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3449() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3449");
        NextDate nextDate3 = new NextDate(0, 0, (-1));
        String str7 = nextDate3.run((int) ' ', (int) (byte) 0, 1);
        String str11 = nextDate3.run((int) 'a', 0, (int) (short) 100);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3450() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3450");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) -1, 2020);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3451() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3451");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (byte) -1, (int) (short) 1);
        String str7 = nextDate3.run(2001, 12, 30);
        String str11 = nextDate3.run((int) (short) 10, (int) (short) 0, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3452() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3452");
        NextDate nextDate3 = new NextDate(12, 0, (int) (byte) 0);
        String str7 = nextDate3.run(0, 30, (int) (byte) -1);
        String str11 = nextDate3.run(0, 7, 100);
        String str15 = nextDate3.run(1, (int) 'a', (int) '4');
        String str19 = nextDate3.run(0, 0, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test3453() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3453");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 100, 2001);
        String str7 = nextDate3.run((int) (byte) 1, (int) (byte) 10, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3454() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3454");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 1, 2020);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3455() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3455");
        NextDate nextDate3 = new NextDate((-1), 2001, (int) (short) 100);
        String str7 = nextDate3.run(10, 2001, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3456() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3456");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) -1, 0);
        String str7 = nextDate3.run((int) (short) 100, (int) (short) -1, (int) (short) 10);
        String str11 = nextDate3.run((int) (byte) -1, 0, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3457() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3457");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 100, (int) (byte) -1);
        String str7 = nextDate3.run(2020, (int) (byte) 10, 0);
        String str11 = nextDate3.run(31, 30, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3458() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3458");
        NextDate nextDate3 = new NextDate(7, (int) (byte) 10, (int) ' ');
    }

    @Test
    public void test3459() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3459");
        NextDate nextDate3 = new NextDate((int) (short) 100, 2020, 2020);
        String str7 = nextDate3.run((int) (byte) -1, (-1), 7);
        String str11 = nextDate3.run((int) ' ', (int) (short) 10, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3460() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3460");
        NextDate nextDate3 = new NextDate((int) '4', 2001, 30);
        String str7 = nextDate3.run(31, (-1), (-1));
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3461() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3461");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) -1, 2001);
        String str7 = nextDate3.run((-1), (int) ' ', (int) (byte) 100);
        String str11 = nextDate3.run((int) '#', 0, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3462() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3462");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 7, 2001);
    }

    @Test
    public void test3463() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3463");
        NextDate nextDate3 = new NextDate(10, 0, 2001);
        String str7 = nextDate3.run(0, 1, 0);
        String str11 = nextDate3.run((int) '#', (int) (short) 0, (int) 'a');
        String str15 = nextDate3.run((int) (byte) 10, (int) '4', (int) '4');
        String str19 = nextDate3.run(7, 12, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test3464() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3464");
        NextDate nextDate3 = new NextDate((int) ' ', 1, (int) '#');
        String str7 = nextDate3.run(10, (int) (byte) 1, (int) (short) 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3465() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3465");
        NextDate nextDate3 = new NextDate(100, (int) ' ', 7);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3466() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3466");
        NextDate nextDate3 = new NextDate(1, 0, 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3467() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3467");
        NextDate nextDate3 = new NextDate(31, (int) '4', (int) (short) -1);
        String str7 = nextDate3.run((int) (short) -1, (int) (byte) 10, 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3468() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3468");
        NextDate nextDate3 = new NextDate((int) (short) 0, 100, 30);
        String str7 = nextDate3.run((int) (short) 1, 0, (int) (short) 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3469() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3469");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) -1, (int) (short) 1);
        String str7 = nextDate3.run((int) (byte) -1, 0, 0);
        String str11 = nextDate3.run(100, (-1), 2020);
        String str15 = nextDate3.run((int) (short) 0, (int) (short) 1, 0);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test3470() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3470");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 10, (int) (byte) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3471() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3471");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) '#', (int) ' ');
        String str7 = nextDate3.run((-1), 1, (-1));
        String str11 = nextDate3.run((int) (byte) 1, 12, (int) (short) 100);
        String str15 = nextDate3.run(1, (int) (short) 0, (int) (short) 10);
        String str19 = nextDate3.run(12, (int) (short) -1, 31);
        String str23 = nextDate3.run((int) (byte) 0, 0, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test3472() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3472");
        NextDate nextDate3 = new NextDate(30, (int) (short) 1, (int) (short) 0);
        String str7 = nextDate3.run((int) (byte) 0, (int) (byte) -1, 7);
        String str11 = nextDate3.run((int) (byte) 1, (int) '#', (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3473() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3473");
        NextDate nextDate3 = new NextDate((int) (short) 0, 0, (int) (byte) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3474() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3474");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (byte) 10, (int) '#');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3475() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3475");
        NextDate nextDate3 = new NextDate(2001, 2001, 12);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3476() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3476");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (-1), 12);
        String str7 = nextDate3.run((int) (byte) 1, (int) (byte) 0, (int) (byte) 0);
        String str11 = nextDate3.run((int) (byte) 10, 31, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3477() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3477");
        NextDate nextDate3 = new NextDate((int) (short) -1, 12, (-1));
    }

    @Test
    public void test3478() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3478");
        NextDate nextDate3 = new NextDate(0, 30, 2001);
        String str7 = nextDate3.run(10, (int) (byte) 0, (int) (short) 0);
        String str11 = nextDate3.run(31, (int) ' ', (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3479() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3479");
        NextDate nextDate3 = new NextDate((int) (short) 1, 7, 0);
        String str7 = nextDate3.run((int) (byte) -1, 31, 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3480() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3480");
        NextDate nextDate3 = new NextDate((int) '#', 0, (-1));
        String str7 = nextDate3.run(0, 0, 0);
        String str11 = nextDate3.run((int) (short) -1, (int) (short) 10, (int) (byte) 0);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3481() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3481");
        NextDate nextDate3 = new NextDate((int) (short) 10, 100, (int) 'a');
        String str7 = nextDate3.run(10, (int) ' ', 1);
        String str11 = nextDate3.run((int) (byte) -1, (int) (short) -1, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test3482() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3482");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) 1, (int) '4');
        String str7 = nextDate3.run((int) (short) -1, 31, (int) '#');
        String str11 = nextDate3.run((int) (byte) 1, 30, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "1/31/2020" + "'", str11, "1/31/2020");
    }

    @Test
    public void test3483() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3483");
        NextDate nextDate3 = new NextDate(31, (int) 'a', (int) '4');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3484() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3484");
        NextDate nextDate3 = new NextDate(1, 2020, (int) (byte) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3485() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3485");
        NextDate nextDate3 = new NextDate(2020, (int) (short) 0, (int) (short) 100);
    }

    @Test
    public void test3486() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3486");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (short) 100, (int) (byte) 10);
        String str7 = nextDate3.run((int) '#', 10, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3487() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3487");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) 10, 12);
        String str7 = nextDate3.run(100, 30, (int) ' ');
        String str11 = nextDate3.run((-1), 2001, (int) ' ');
        String str15 = nextDate3.run((int) (short) 1, (int) (short) 100, 10);
        String str19 = nextDate3.run((int) (short) 0, 2020, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test3488() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3488");
        NextDate nextDate3 = new NextDate((int) (short) 1, (-1), 2020);
        String str7 = nextDate3.run(31, (int) '4', 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test3489() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3489");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 0, (int) (byte) 1);
        String str7 = nextDate3.run(1, 12, (int) ' ');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3490() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3490");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) '4', (int) (short) -1);
        String str7 = nextDate3.run((int) (byte) 0, (int) (short) 1, (int) (byte) 10);
        String str11 = nextDate3.run(2001, 0, (int) (short) 10);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3491() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3491");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 0, 7);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3492() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3492");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) 10, 12);
        String str7 = nextDate3.run(100, 30, (int) ' ');
        String str11 = nextDate3.run((-1), 2001, (int) ' ');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3493() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3493");
        NextDate nextDate3 = new NextDate(0, 0, (int) (short) -1);
        String str7 = nextDate3.run(2001, (int) (short) 1, (int) ' ');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test3494() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3494");
        NextDate nextDate3 = new NextDate(30, (int) (byte) 10, 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3495() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3495");
        NextDate nextDate3 = new NextDate(2001, 2001, 100);
    }

    @Test
    public void test3496() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3496");
        NextDate nextDate3 = new NextDate(12, 10, (-1));
    }

    @Test
    public void test3497() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3497");
        NextDate nextDate3 = new NextDate(1, (int) (short) -1, 12);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test3498() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3498");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) 'a', (int) (byte) 100);
    }

    @Test
    public void test3499() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3499");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 0, (int) (byte) 1);
        String str7 = nextDate3.run((int) (short) 100, (int) (byte) 1, (int) '#');
        String str11 = nextDate3.run(30, (int) 'a', 2001);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test3500() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest6.test3500");
        NextDate nextDate3 = new NextDate((int) (short) 100, 0, 1);
        String str7 = nextDate3.run(0, (int) ' ', 2001);
        String str11 = nextDate3.run(7, 31, 0);
        String str15 = nextDate3.run((int) (byte) 0, (int) (byte) -1, 1);
        String str19 = nextDate3.run((int) (short) 100, (int) (byte) -1, (int) ' ');
        String str23 = nextDate3.run(1, (int) (short) 1, (-1));
        String str27 = nextDate3.run(30, (int) (byte) 1, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }
}

