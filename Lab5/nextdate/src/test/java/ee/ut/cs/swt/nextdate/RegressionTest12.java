package ee.ut.cs.swt.nextdate;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest12 {

    public static boolean debug = false;

    @Test
    public void test6001() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6001");
        NextDate nextDate3 = new NextDate(10, (int) 'a', 2001);
    }

    @Test
    public void test6002() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6002");
        NextDate nextDate3 = new NextDate(10, 0, (int) (short) 1);
        String str7 = nextDate3.run((int) '#', 7, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6003() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6003");
        NextDate nextDate3 = new NextDate(2001, 0, 1);
    }

    @Test
    public void test6004() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6004");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 1, (int) (short) -1);
        String str7 = nextDate3.run(0, (int) (byte) 100, (int) (short) 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6005() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6005");
        NextDate nextDate3 = new NextDate(31, (int) (byte) 10, (int) (byte) 100);
    }

    @Test
    public void test6006() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6006");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 30, (int) (short) 100);
        String str7 = nextDate3.run((int) ' ', 2020, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6007() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6007");
        NextDate nextDate3 = new NextDate(10, (int) (byte) -1, (int) (short) 10);
    }

    @Test
    public void test6008() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6008");
        NextDate nextDate3 = new NextDate(7, (int) (byte) 0, 7);
        String str7 = nextDate3.run((int) (short) 10, 12, 31);
        String str11 = nextDate3.run(7, (-1), (int) (short) 0);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6009() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6009");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) '4', (int) (byte) 10);
    }

    @Test
    public void test6010() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6010");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) -1, 0);
        String str7 = nextDate3.run((int) (short) -1, (int) (byte) 100, 2020);
        String str11 = nextDate3.run((int) (short) 1, 2020, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6011() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6011");
        NextDate nextDate3 = new NextDate(2001, 12, 2020);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6012() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6012");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 0, (int) 'a');
    }

    @Test
    public void test6013() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6013");
        NextDate nextDate3 = new NextDate((int) 'a', 31, 2020);
    }

    @Test
    public void test6014() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6014");
        NextDate nextDate3 = new NextDate((int) (short) 0, 100, 2020);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6015() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6015");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 100, 10);
    }

    @Test
    public void test6016() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6016");
        NextDate nextDate3 = new NextDate(1, 31, (int) (short) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6017() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6017");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) -1, (int) (byte) -1);
    }

    @Test
    public void test6018() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6018");
        NextDate nextDate3 = new NextDate((int) ' ', 1, 10);
    }

    @Test
    public void test6019() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6019");
        NextDate nextDate3 = new NextDate(30, 1, 2020);
        String str7 = nextDate3.run(31, (int) (short) -1, (int) (byte) -1);
        String str11 = nextDate3.run(30, (int) (byte) 100, (int) ' ');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6020() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6020");
        NextDate nextDate3 = new NextDate((int) (short) 0, 1, (int) (byte) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6021() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6021");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 0, (int) (short) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6022() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6022");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) 'a', (int) (short) -1);
        String str7 = nextDate3.run((int) (short) 1, 2001, 2020);
        String str11 = nextDate3.run(10, 2020, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "2/1/2020" + "'", str7, "2/1/2020");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6023() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6023");
        NextDate nextDate3 = new NextDate(2001, 30, 12);
    }

    @Test
    public void test6024() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6024");
        NextDate nextDate3 = new NextDate(0, 2001, (int) (byte) 10);
    }

    @Test
    public void test6025() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6025");
        NextDate nextDate3 = new NextDate(0, 12, (int) '4');
    }

    @Test
    public void test6026() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6026");
        NextDate nextDate3 = new NextDate((int) (short) -1, 100, (int) ' ');
        String str7 = nextDate3.run(0, 1, (int) '#');
        String str11 = nextDate3.run(0, (int) (byte) -1, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6027() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6027");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 100, 31);
        String str7 = nextDate3.run(30, (int) '4', (int) (byte) 0);
        String str11 = nextDate3.run(1, 12, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6028() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6028");
        NextDate nextDate3 = new NextDate(100, 2020, (int) ' ');
        String str7 = nextDate3.run(100, 7, 30);
        String str11 = nextDate3.run(2001, (int) (byte) -1, (int) (short) 100);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6029() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6029");
        NextDate nextDate3 = new NextDate((int) (short) 100, 0, 0);
        String str7 = nextDate3.run(31, (int) (byte) -1, (int) (short) 100);
        String str11 = nextDate3.run(10, (int) (short) 10, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6030() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6030");
        NextDate nextDate3 = new NextDate((-1), 1, 100);
        String str7 = nextDate3.run(7, 0, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6031() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6031");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 100, (int) '#');
        String str7 = nextDate3.run((int) '#', (int) 'a', (-1));
        String str11 = nextDate3.run(12, 31, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6032() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6032");
        NextDate nextDate3 = new NextDate(2001, (int) (short) 10, (int) (short) 10);
        String str7 = nextDate3.run((int) (byte) 1, 0, 0);
        String str11 = nextDate3.run((int) '4', 12, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6033() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6033");
        NextDate nextDate3 = new NextDate((int) '4', 2020, (-1));
    }

    @Test
    public void test6034() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6034");
        NextDate nextDate3 = new NextDate(30, 0, (int) 'a');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6035() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6035");
        NextDate nextDate3 = new NextDate(2020, (int) (short) 100, (int) (short) 0);
    }

    @Test
    public void test6036() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6036");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 2020, (-1));
        String str7 = nextDate3.run(10, (int) (byte) 10, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6037() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6037");
        NextDate nextDate3 = new NextDate(0, 2020, (int) (short) 0);
        String str7 = nextDate3.run(12, (-1), 2020);
        String str11 = nextDate3.run((int) ' ', (int) ' ', 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6038() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6038");
        NextDate nextDate3 = new NextDate((int) (short) 100, 0, (-1));
        String str7 = nextDate3.run(0, (int) (byte) 1, (int) (short) 0);
        String str11 = nextDate3.run((int) (short) 10, 0, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6039() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6039");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) 100, (int) '4');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6040() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6040");
        NextDate nextDate3 = new NextDate(7, (int) (short) 1, (int) (byte) 0);
        String str7 = nextDate3.run((int) (byte) 10, (int) (byte) 100, 1);
        String str11 = nextDate3.run(2001, (int) (short) -1, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6041() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6041");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (-1), (int) '#');
    }

    @Test
    public void test6042() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6042");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) '#', (int) '#');
        String str7 = nextDate3.run(1, (int) ' ', (int) (short) 100);
        String str11 = nextDate3.run((int) (short) -1, 0, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6043() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6043");
        NextDate nextDate3 = new NextDate(10, (int) (short) 0, 10);
        String str7 = nextDate3.run((int) (short) 10, 100, 1);
        String str11 = nextDate3.run(30, 12, (int) '#');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6044() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6044");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 100, 2020);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6045() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6045");
        NextDate nextDate3 = new NextDate(12, (int) (byte) 100, 100);
        String str7 = nextDate3.run((int) '4', (int) (short) 1, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6046() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6046");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) '4', 2020);
        String str7 = nextDate3.run(1, (int) (short) 10, 31);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6047() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6047");
        NextDate nextDate3 = new NextDate(7, (int) (byte) 10, (int) (short) 1);
    }

    @Test
    public void test6048() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6048");
        NextDate nextDate3 = new NextDate(2001, 0, 30);
    }

    @Test
    public void test6049() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6049");
        NextDate nextDate3 = new NextDate(1, (int) (short) 100, 7);
        String str7 = nextDate3.run(12, (int) '#', 1);
        String str11 = nextDate3.run(30, 31, 0);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6050() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6050");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 31, (int) (byte) 100);
        String str7 = nextDate3.run((int) '4', (int) ' ', (int) (byte) 100);
        String str11 = nextDate3.run(100, (int) (byte) -1, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6051() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6051");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 0, (int) (byte) 10);
        String str7 = nextDate3.run((int) (short) 1, (int) (short) 10, (int) (byte) 10);
        String str11 = nextDate3.run((int) (byte) 100, (int) ' ', (int) '#');
        String str15 = nextDate3.run(10, (int) (short) -1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6052() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6052");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) '#', (int) (short) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6053() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6053");
        NextDate nextDate3 = new NextDate((int) (short) 100, 30, (int) (short) -1);
        String str7 = nextDate3.run((int) '4', (int) (byte) 100, 100);
        String str11 = nextDate3.run(0, 0, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6054() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6054");
        NextDate nextDate3 = new NextDate((int) '#', (int) ' ', 30);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6055() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6055");
        NextDate nextDate3 = new NextDate(31, 7, (int) ' ');
    }

    @Test
    public void test6056() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6056");
        NextDate nextDate3 = new NextDate(31, (int) (byte) -1, 10);
        String str7 = nextDate3.run((int) '4', 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6057() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6057");
        NextDate nextDate3 = new NextDate(1, (int) (byte) 1, (int) (short) 100);
    }

    @Test
    public void test6058() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6058");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) -1, (int) ' ');
        String str7 = nextDate3.run(12, (int) ' ', (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6059() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6059");
        NextDate nextDate3 = new NextDate(10, (int) (short) -1, 0);
        String str7 = nextDate3.run(30, (int) (byte) -1, 2020);
        String str11 = nextDate3.run((int) (byte) 0, 12, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6060() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6060");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) -1, (-1));
        String str7 = nextDate3.run((-1), 2001, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6061() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6061");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) '#', 30);
        String str7 = nextDate3.run(7, (-1), 0);
        String str11 = nextDate3.run(12, (int) (byte) 1, (int) (short) 100);
        String str15 = nextDate3.run((int) (short) 0, 12, 2020);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test6062() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6062");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 10, (int) (short) 0);
        String str7 = nextDate3.run(7, 30, 0);
        String str11 = nextDate3.run(10, (int) (short) 1, (int) (byte) 100);
        String str15 = nextDate3.run(2020, 10, (int) 'a');
        String str19 = nextDate3.run((int) (byte) 0, (int) '#', (int) (byte) 100);
        String str23 = nextDate3.run(31, (-1), (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test6063() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6063");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) '#', 0);
        String str7 = nextDate3.run(2020, 0, 30);
        String str11 = nextDate3.run((int) '#', 100, (int) 'a');
        String str15 = nextDate3.run(100, 2001, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6064() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6064");
        NextDate nextDate3 = new NextDate((-1), (int) 'a', (int) '4');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6065() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6065");
        NextDate nextDate3 = new NextDate(2001, (int) '4', (int) '#');
        String str7 = nextDate3.run((int) '#', (int) (short) 100, (int) (short) 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6066() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6066");
        NextDate nextDate3 = new NextDate(2020, (int) 'a', 2020);
    }

    @Test
    public void test6067() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6067");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 2001, (int) (byte) -1);
        String str7 = nextDate3.run((int) '4', (int) (short) -1, (int) (short) -1);
        String str11 = nextDate3.run((-1), 2001, 100);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6068() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6068");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 1, 2020);
        String str7 = nextDate3.run(0, (int) '4', 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6069() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6069");
        NextDate nextDate3 = new NextDate((int) (short) 0, 7, (-1));
        String str7 = nextDate3.run((int) (short) 10, 100, (int) ' ');
        String str11 = nextDate3.run(100, 12, (int) (short) -1);
        String str15 = nextDate3.run((int) '4', (int) '4', 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6070() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6070");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 12, 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6071() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6071");
        NextDate nextDate3 = new NextDate(31, 10, 2001);
        String str7 = nextDate3.run(10, 31, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6072() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6072");
        NextDate nextDate3 = new NextDate(30, 10, 31);
    }

    @Test
    public void test6073() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6073");
        NextDate nextDate3 = new NextDate(12, (int) (short) 10, (int) (byte) 10);
        String str7 = nextDate3.run((int) (byte) 1, (int) '4', 31);
        String str11 = nextDate3.run(2020, 0, 10);
        String str15 = nextDate3.run((int) (short) 10, (int) (short) 100, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6074() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6074");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) ' ', (int) (byte) 0);
    }

    @Test
    public void test6075() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6075");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 10, (int) (short) 0);
        String str7 = nextDate3.run((int) '4', 0, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6076() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6076");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 0, (int) (byte) 10);
        String str7 = nextDate3.run(10, (int) '#', 30);
        String str11 = nextDate3.run(31, (int) (short) 10, (int) (byte) 1);
        String str15 = nextDate3.run((int) '4', 2020, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6077() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6077");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 0, 1);
        String str7 = nextDate3.run(2020, (int) (byte) 0, (int) (byte) 0);
        String str11 = nextDate3.run((-1), (int) (byte) 100, (int) ' ');
        String str15 = nextDate3.run(100, 1, 0);
        String str19 = nextDate3.run((int) (short) -1, (int) (short) 1, (int) (short) 1);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test6078() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6078");
        NextDate nextDate3 = new NextDate(0, 7, (int) (short) 1);
        String str7 = nextDate3.run((int) (byte) -1, (int) (byte) 100, (int) (short) 0);
        String str11 = nextDate3.run(0, (int) (byte) 100, 30);
        String str15 = nextDate3.run(100, (int) '#', (int) 'a');
        String str19 = nextDate3.run((int) (byte) -1, 0, 31);
        String str23 = nextDate3.run((int) (byte) 100, (int) (byte) 1, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test6079() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6079");
        NextDate nextDate3 = new NextDate((int) (short) -1, 2020, (int) (short) 1);
        String str7 = nextDate3.run((int) (short) 0, (int) '4', (int) (short) -1);
        String str11 = nextDate3.run((int) (short) 100, 1, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6080() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6080");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) '4', (int) '4');
        String str7 = nextDate3.run((int) (byte) 1, 31, (int) (byte) 1);
        String str11 = nextDate3.run((int) (byte) -1, 0, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6081() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6081");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) 0, 0);
    }

    @Test
    public void test6082() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6082");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 0, 1);
        String str7 = nextDate3.run((int) (byte) -1, 10, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6083() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6083");
        NextDate nextDate3 = new NextDate((int) (short) 1, 100, 7);
    }

    @Test
    public void test6084() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6084");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 10, 0);
        String str7 = nextDate3.run(1, (int) (byte) 100, 1);
        String str11 = nextDate3.run((int) (short) 1, (-1), (int) (short) -1);
        String str15 = nextDate3.run(100, (int) (byte) 10, (int) (byte) 100);
        String str19 = nextDate3.run((int) (short) -1, (int) (byte) 100, (int) (byte) 1);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test6085() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6085");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 0, (int) '#');
    }

    @Test
    public void test6086() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6086");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 10, 12);
    }

    @Test
    public void test6087() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6087");
        NextDate nextDate3 = new NextDate(1, 0, (int) (byte) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6088() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6088");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run((int) (byte) 10, (int) (short) 100, 100);
        String str27 = nextDate3.run((int) (short) 100, (int) (short) 0, (int) (byte) 0);
        String str31 = nextDate3.run((int) 'a', 100, (int) (byte) 0);
        String str35 = nextDate3.run((int) (short) 10, (int) (short) 10, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
    }

    @Test
    public void test6089() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6089");
        NextDate nextDate3 = new NextDate(0, (int) (short) 100, (int) '#');
        String str7 = nextDate3.run((int) 'a', 100, (int) (byte) 1);
        String str11 = nextDate3.run(7, 1, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6090() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6090");
        NextDate nextDate3 = new NextDate(2001, (int) '#', (int) '#');
        String str7 = nextDate3.run((int) (byte) 0, (-1), 0);
        String str11 = nextDate3.run((-1), 12, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6091() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6091");
        NextDate nextDate3 = new NextDate(0, 10, (int) (short) 100);
    }

    @Test
    public void test6092() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6092");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) -1, 2020);
        String str7 = nextDate3.run((int) (short) 1, 10, (-1));
        String str11 = nextDate3.run(31, 12, (int) 'a');
        String str15 = nextDate3.run(30, 31, 7);
        String str19 = nextDate3.run((int) (short) 10, (int) '4', (int) (byte) -1);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test6093() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6093");
        NextDate nextDate3 = new NextDate(10, (int) 'a', 2020);
        String str7 = nextDate3.run((int) (short) 1, 1, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6094() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6094");
        NextDate nextDate3 = new NextDate(100, 1, 100);
        String str7 = nextDate3.run(10, (int) (short) 1, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6095() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6095");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) -1, (int) (short) 100);
        String str7 = nextDate3.run((int) ' ', 31, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6096() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6096");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 0, 30);
    }

    @Test
    public void test6097() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6097");
        NextDate nextDate3 = new NextDate((int) (short) 100, (-1), 10);
    }

    @Test
    public void test6098() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6098");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) '4', 30);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6099() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6099");
        NextDate nextDate3 = new NextDate((int) 'a', 2001, (int) (byte) 1);
    }

    @Test
    public void test6100() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6100");
        NextDate nextDate3 = new NextDate((int) 'a', 0, 12);
        String str7 = nextDate3.run((int) (byte) 10, (int) '#', (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6101() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6101");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) -1, (int) (short) -1);
        String str7 = nextDate3.run(0, 7, (int) (byte) 10);
        String str11 = nextDate3.run(100, (int) '#', 12);
        String str15 = nextDate3.run((int) 'a', (int) ' ', 0);
        String str19 = nextDate3.run((int) (byte) 100, (int) (byte) 100, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6102() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6102");
        NextDate nextDate3 = new NextDate((-1), 2001, (int) (byte) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6103() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6103");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (-1), (int) (short) 1);
    }

    @Test
    public void test6104() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6104");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) '#', 2001);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6105() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6105");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) 0, (int) '#');
    }

    @Test
    public void test6106() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6106");
        NextDate nextDate3 = new NextDate(12, 12, (int) (short) 100);
        String str7 = nextDate3.run(10, 1, (int) (short) -1);
        String str11 = nextDate3.run(31, (int) (short) -1, 0);
        String str15 = nextDate3.run(2001, (int) (byte) 1, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6107() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6107");
        NextDate nextDate3 = new NextDate(0, 1, (int) '#');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6108() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6108");
        NextDate nextDate3 = new NextDate((int) ' ', (int) '#', 12);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6109() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6109");
        NextDate nextDate3 = new NextDate(0, 12, (int) '#');
    }

    @Test
    public void test6110() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6110");
        NextDate nextDate3 = new NextDate((int) '4', (int) (byte) 1, 2020);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6111() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6111");
        NextDate nextDate3 = new NextDate((int) '4', (int) (byte) 10, 0);
        String str7 = nextDate3.run(1, (int) (byte) 1, (int) (short) -1);
        String str11 = nextDate3.run((int) '4', (int) (short) 1, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6112() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6112");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) 10, (int) (byte) 100);
        String str7 = nextDate3.run((int) ' ', (int) ' ', (int) (short) 10);
        String str11 = nextDate3.run((int) '4', (int) (byte) 0, (int) 'a');
        String str15 = nextDate3.run(10, 2020, 2020);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "11/1/2020" + "'", str15, "11/1/2020");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test6113() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6113");
        NextDate nextDate3 = new NextDate((int) 'a', (int) '#', (int) (byte) -1);
        String str7 = nextDate3.run((int) ' ', (int) (byte) 100, 2001);
        String str11 = nextDate3.run((int) (short) 0, (int) (byte) -1, (int) (short) 10);
        String str15 = nextDate3.run(100, 12, 0);
        String str19 = nextDate3.run(0, 100, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6114() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6114");
        NextDate nextDate3 = new NextDate((int) (short) -1, 10, (int) '#');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6115() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6115");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) -1, (int) '4');
        String str7 = nextDate3.run((int) (short) 100, 2001, 100);
        String str11 = nextDate3.run((int) (short) 0, 2020, (int) (short) 10);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6116() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6116");
        NextDate nextDate3 = new NextDate(0, 1, 7);
        String str7 = nextDate3.run((int) (short) 100, 31, (int) (short) 100);
        String str11 = nextDate3.run((int) (byte) 100, (int) (short) 1, (int) (byte) -1);
        String str15 = nextDate3.run((int) '#', (int) (byte) 1, (int) (byte) 100);
        String str19 = nextDate3.run(31, 12, (int) (short) 10);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test6117() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6117");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) -1, 7);
        String str7 = nextDate3.run((int) (short) 10, (int) (byte) 0, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6118() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6118");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run(31, 31, 12);
        String str23 = nextDate3.run(10, 2020, (int) (byte) -1);
        String str27 = nextDate3.run(2001, (int) (short) 100, 7);
        String str31 = nextDate3.run(2020, (int) '#', (int) (short) 100);
        String str35 = nextDate3.run(31, 0, (int) (byte) 100);
        String str39 = nextDate3.run(30, 10, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
    }

    @Test
    public void test6119() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6119");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) ' ', 2020, (int) (short) 0);
        String str11 = nextDate3.run(31, (int) '4', (int) (byte) -1);
        String str15 = nextDate3.run((int) (byte) 100, 7, (int) (short) 100);
        String str19 = nextDate3.run(1, 31, 2001);
        String str23 = nextDate3.run(0, 30, 30);
        String str27 = nextDate3.run(7, (int) (short) -1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "2/1/2001" + "'", str19, "2/1/2001");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test6120() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6120");
        NextDate nextDate3 = new NextDate(10, (int) '4', (int) (short) 10);
        String str7 = nextDate3.run(2001, (int) (byte) 100, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6121() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6121");
        NextDate nextDate3 = new NextDate(2020, (int) (short) 10, (int) 'a');
    }

    @Test
    public void test6122() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6122");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 0, 2020);
        String str7 = nextDate3.run((int) (byte) 100, 31, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6123() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6123");
        NextDate nextDate3 = new NextDate((int) (short) 10, 0, 7);
        String str7 = nextDate3.run(100, 12, (int) (short) 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6124() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6124");
        NextDate nextDate3 = new NextDate(100, 0, 0);
        String str7 = nextDate3.run(12, (int) (short) 0, (int) (byte) 1);
        String str11 = nextDate3.run(100, (int) (short) 100, (int) (short) 10);
        String str15 = nextDate3.run((int) (short) -1, (int) '#', 30);
        String str19 = nextDate3.run((int) 'a', 7, (int) (byte) -1);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test6125() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6125");
        NextDate nextDate3 = new NextDate(10, 7, 1);
        String str7 = nextDate3.run((int) (byte) 0, (int) (byte) 10, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6126() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6126");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) '#', (int) '#');
        String str7 = nextDate3.run(1, (int) ' ', (int) (short) 100);
        String str11 = nextDate3.run(1, (int) (byte) 100, 0);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6127() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6127");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) 'a', (-1));
        String str7 = nextDate3.run(100, 10, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6128() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6128");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) 0, 30);
        String str7 = nextDate3.run((int) (short) 100, (int) (short) -1, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6129() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6129");
        NextDate nextDate3 = new NextDate(2020, (int) ' ', (int) (byte) 0);
        String str7 = nextDate3.run(1, 12, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "1/13/2001" + "'", str7, "1/13/2001");
    }

    @Test
    public void test6130() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6130");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 100, 2020);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6131() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6131");
        NextDate nextDate3 = new NextDate(31, (int) ' ', 30);
    }

    @Test
    public void test6132() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6132");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 0, (-1));
        String str7 = nextDate3.run((int) (short) 10, 30, (-1));
        String str11 = nextDate3.run((int) '#', (int) (short) -1, (int) (byte) 10);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6133() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6133");
        NextDate nextDate3 = new NextDate(1, (int) '#', (-1));
        String str7 = nextDate3.run(31, (int) (byte) 10, 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6134() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6134");
        NextDate nextDate3 = new NextDate((int) '#', (int) 'a', 2001);
        String str7 = nextDate3.run(0, (int) '4', (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6135() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6135");
        NextDate nextDate3 = new NextDate(31, 100, 2020);
    }

    @Test
    public void test6136() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6136");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) 10, 100);
    }

    @Test
    public void test6137() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6137");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 1, 1);
        String str7 = nextDate3.run((int) (short) 1, (int) (short) 10, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6138() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6138");
        NextDate nextDate3 = new NextDate(12, 12, (int) 'a');
        String str7 = nextDate3.run((int) ' ', 2001, (int) (byte) 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6139() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6139");
        NextDate nextDate3 = new NextDate((int) (short) 10, 12, (int) (short) 0);
        String str7 = nextDate3.run(31, (int) (short) 0, (int) (byte) 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6140() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6140");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 10, (int) (byte) 100);
        String str7 = nextDate3.run((int) (short) 100, 2020, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6141() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6141");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) '#', (int) (short) 0);
    }

    @Test
    public void test6142() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6142");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 100, (int) (byte) -1);
    }

    @Test
    public void test6143() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6143");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) '4', (int) (byte) 100);
    }

    @Test
    public void test6144() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6144");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run(7, (int) (short) 10, (int) (short) 1);
        String str27 = nextDate3.run((int) 'a', (int) (byte) 1, 0);
        String str31 = nextDate3.run((int) (byte) 0, (int) ' ', (int) (short) 10);
        String str35 = nextDate3.run(7, 10, 0);
        String str39 = nextDate3.run(30, (int) (byte) 100, 7);
        String str43 = nextDate3.run((int) (short) 10, (int) (short) 100, 0);
        String str47 = nextDate3.run(100, (-1), (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str43 + "' != '" + "invalid Input Date" + "'", str43, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str47 + "' != '" + "invalid Input Date" + "'", str47, "invalid Input Date");
    }

    @Test
    public void test6145() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6145");
        NextDate nextDate3 = new NextDate(30, (int) (byte) 10, 2020);
        String str7 = nextDate3.run(2001, 2001, 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6146() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6146");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 12, 31);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6147() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6147");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (short) 0, 10);
        String str7 = nextDate3.run((int) (byte) -1, (int) '#', (int) 'a');
        String str11 = nextDate3.run((int) (byte) 10, 30, 10);
        String str15 = nextDate3.run(7, 1, 0);
        String str19 = nextDate3.run((int) (short) 1, (int) (byte) 100, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6148() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6148");
        NextDate nextDate3 = new NextDate(30, (int) '#', (int) ' ');
        String str7 = nextDate3.run((int) ' ', 100, 12);
        String str11 = nextDate3.run((int) (byte) -1, (int) (short) 1, (int) 'a');
        String str15 = nextDate3.run(100, (int) (byte) 0, (int) (byte) 100);
        String str19 = nextDate3.run((int) (byte) 100, 10, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6149() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6149");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 0, 7);
        String str7 = nextDate3.run((int) ' ', 0, (int) (short) 1);
        String str11 = nextDate3.run(2001, (int) (short) -1, (int) ' ');
        String str15 = nextDate3.run(0, (int) (byte) 1, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6150() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6150");
        NextDate nextDate3 = new NextDate(0, (int) (byte) -1, 10);
        String str7 = nextDate3.run((int) (short) 0, 30, (int) (short) 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6151() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6151");
        NextDate nextDate3 = new NextDate((int) 'a', 2020, (int) (short) 10);
    }

    @Test
    public void test6152() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6152");
        NextDate nextDate3 = new NextDate(10, (int) '#', (int) (short) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6153() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6153");
        NextDate nextDate3 = new NextDate(1, (int) (byte) -1, (int) 'a');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6154() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6154");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) -1, 31);
    }

    @Test
    public void test6155() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6155");
        NextDate nextDate3 = new NextDate(1, (int) (short) 0, (int) (byte) 100);
    }

    @Test
    public void test6156() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6156");
        NextDate nextDate3 = new NextDate(30, (int) (short) 10, 10);
        String str7 = nextDate3.run((int) (short) 100, 12, (int) (byte) -1);
        String str11 = nextDate3.run((-1), 12, 100);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6157() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6157");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) -1, 31);
        String str7 = nextDate3.run(0, 0, 31);
        String str11 = nextDate3.run((int) (short) 1, (int) (byte) 1, (int) '4');
        String str15 = nextDate3.run(30, (int) (short) -1, (int) (short) 1);
        String str19 = nextDate3.run(10, (int) (short) 10, 1);
        String str23 = nextDate3.run(31, (int) (byte) -1, (-1));
        Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test6158() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6158");
        NextDate nextDate3 = new NextDate(30, (int) '4', (int) (byte) 100);
        String str7 = nextDate3.run((int) (short) -1, (int) (byte) 10, (int) (byte) 1);
        String str11 = nextDate3.run((int) (short) 10, (int) (short) 10, (int) (short) 1);
        String str15 = nextDate3.run(30, 10, 2020);
        String str19 = nextDate3.run((int) (byte) 1, (int) (short) -1, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6159() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6159");
        NextDate nextDate3 = new NextDate((-1), (int) '#', 1);
    }

    @Test
    public void test6160() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6160");
        NextDate nextDate3 = new NextDate((int) (short) 0, 31, (int) (byte) 10);
        String str7 = nextDate3.run(0, 0, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6161() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6161");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 1, (int) (short) -1);
        String str7 = nextDate3.run((int) (byte) -1, (int) (byte) -1, 7);
        String str11 = nextDate3.run((int) '#', 0, 7);
        String str15 = nextDate3.run(31, (int) (short) -1, (int) (byte) 10);
        String str19 = nextDate3.run(12, (int) (short) 0, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6162() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6162");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) -1, (int) (short) -1);
        String str7 = nextDate3.run(0, 7, (int) (byte) 10);
        String str11 = nextDate3.run(100, (int) '#', 12);
        String str15 = nextDate3.run(2020, 100, 30);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test6163() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6163");
        NextDate nextDate3 = new NextDate(2020, (-1), 2001);
    }

    @Test
    public void test6164() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6164");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) 10, (int) (short) 100);
        String str7 = nextDate3.run(10, (int) (byte) -1, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6165() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6165");
        NextDate nextDate3 = new NextDate(10, 12, (int) '4');
    }

    @Test
    public void test6166() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6166");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) 0, 0);
    }

    @Test
    public void test6167() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6167");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (short) -1, 0);
    }

    @Test
    public void test6168() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6168");
        NextDate nextDate3 = new NextDate((int) (short) 10, 100, (int) (byte) 10);
    }

    @Test
    public void test6169() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6169");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) -1, (int) '4');
        String str7 = nextDate3.run((int) (byte) -1, 100, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6170() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6170");
        NextDate nextDate3 = new NextDate(1, 0, (int) '4');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6171() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6171");
        NextDate nextDate3 = new NextDate((int) ' ', 0, 7);
        String str7 = nextDate3.run((int) (short) 0, 12, 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6172() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6172");
        NextDate nextDate3 = new NextDate(2001, 0, (int) (byte) 1);
    }

    @Test
    public void test6173() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6173");
        NextDate nextDate3 = new NextDate((int) (short) 1, 0, (int) (short) 0);
        String str7 = nextDate3.run((int) '4', (int) (byte) 1, 100);
        String str11 = nextDate3.run((int) 'a', 30, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6174() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6174");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 100, (-1));
        String str7 = nextDate3.run((int) (byte) 1, 100, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6175() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6175");
        NextDate nextDate3 = new NextDate((int) (short) -1, (-1), (int) (short) 1);
    }

    @Test
    public void test6176() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6176");
        NextDate nextDate3 = new NextDate(100, (int) (short) -1, (int) (byte) -1);
        String str7 = nextDate3.run(1, (int) (short) 100, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6177() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6177");
        NextDate nextDate3 = new NextDate((int) ' ', (-1), 10);
    }

    @Test
    public void test6178() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6178");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 10, (int) (short) 1);
        String str7 = nextDate3.run(12, 0, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6179() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6179");
        NextDate nextDate3 = new NextDate(12, (int) (byte) 10, (int) (short) 0);
        String str7 = nextDate3.run(1, 100, (int) (byte) 1);
        String str11 = nextDate3.run(7, (int) (short) -1, (int) ' ');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6180() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6180");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) -1, 0);
        String str7 = nextDate3.run((int) (short) 10, (-1), 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6181() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6181");
        NextDate nextDate3 = new NextDate(100, 2020, 2001);
        String str7 = nextDate3.run((int) '4', (int) (short) 1, 0);
        String str11 = nextDate3.run((int) ' ', (int) (byte) 1, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6182() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6182");
        NextDate nextDate3 = new NextDate(2001, 10, 7);
    }

    @Test
    public void test6183() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6183");
        NextDate nextDate3 = new NextDate(1, (int) ' ', (int) (byte) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6184() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6184");
        NextDate nextDate3 = new NextDate(1, 7, (int) (byte) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6185() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6185");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 0, (int) (short) 1);
        String str7 = nextDate3.run(2001, (int) ' ', (-1));
        String str11 = nextDate3.run(0, (int) 'a', 10);
        String str15 = nextDate3.run((int) (byte) 0, (int) (short) -1, (int) (byte) 10);
        String str19 = nextDate3.run((int) (short) 1, (int) (short) 1, 1);
        String str23 = nextDate3.run(1, 1, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test6186() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6186");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 10, 2001);
        String str7 = nextDate3.run((int) (short) 0, 1, 0);
        String str11 = nextDate3.run(31, (int) (short) -1, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6187() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6187");
        NextDate nextDate3 = new NextDate((int) (short) -1, 0, (int) (byte) 0);
    }

    @Test
    public void test6188() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6188");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 100);
    }

    @Test
    public void test6189() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6189");
        NextDate nextDate3 = new NextDate(0, (int) (short) 0, 12);
    }

    @Test
    public void test6190() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6190");
        NextDate nextDate3 = new NextDate(31, (int) '#', (int) (byte) 100);
        String str7 = nextDate3.run((int) '#', 10, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6191() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6191");
        NextDate nextDate3 = new NextDate(0, (-1), 2001);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6192() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6192");
        NextDate nextDate3 = new NextDate(12, (int) (short) 10, (int) (byte) 10);
        String str7 = nextDate3.run((int) 'a', (int) (short) 100, 2020);
        String str11 = nextDate3.run((int) (short) 1, 0, (int) (short) 10);
        String str15 = nextDate3.run((int) (short) 10, 2001, 0);
        String str19 = nextDate3.run(31, (int) '#', 0);
        String str23 = nextDate3.run((int) (byte) 100, 2001, 30);
        String str27 = nextDate3.run((int) (short) 10, (int) ' ', 0);
        Class<?> wildcardClass28 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test6193() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6193");
        NextDate nextDate3 = new NextDate(10, (int) '4', (int) (short) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6194() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6194");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) ' ', 2020, (int) (short) 0);
        String str11 = nextDate3.run(31, (int) '4', (int) (byte) -1);
        String str15 = nextDate3.run((int) (byte) 100, 7, (int) (short) 100);
        String str19 = nextDate3.run(1, 31, 2001);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "2/1/2001" + "'", str19, "2/1/2001");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test6195() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6195");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) -1, (int) 'a');
        String str7 = nextDate3.run(7, (int) (short) 0, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6196() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6196");
        NextDate nextDate3 = new NextDate((int) ' ', 1, (int) '#');
        String str7 = nextDate3.run((int) '#', (int) (short) 0, (int) '4');
        String str11 = nextDate3.run(10, (int) ' ', (int) 'a');
        String str15 = nextDate3.run(30, (int) '4', 10);
        String str19 = nextDate3.run((int) (short) -1, (int) (byte) 0, (int) (byte) 0);
        String str23 = nextDate3.run(31, 1, (int) (byte) 1);
        String str27 = nextDate3.run((int) (short) -1, 1, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test6197() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6197");
        NextDate nextDate3 = new NextDate(7, 31, (int) (short) 10);
        String str7 = nextDate3.run(2020, 2001, (int) '#');
        String str11 = nextDate3.run(100, 12, 12);
        String str15 = nextDate3.run((int) '#', 1, (-1));
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test6198() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6198");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) -1, (int) (byte) 10);
        String str7 = nextDate3.run((int) (short) 10, (int) '#', (int) (short) -1);
        String str11 = nextDate3.run((int) (byte) 0, (int) (short) 10, (int) (byte) 10);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6199() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6199");
        NextDate nextDate3 = new NextDate(31, (int) (short) 10, (int) '4');
    }

    @Test
    public void test6200() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6200");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) 10, (int) (short) 100);
        String str7 = nextDate3.run((int) ' ', (int) 'a', (int) '#');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6201() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6201");
        NextDate nextDate3 = new NextDate(31, 31, 10);
        String str7 = nextDate3.run((int) (byte) 10, 1, 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6202() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6202");
        NextDate nextDate3 = new NextDate(7, (int) 'a', 7);
        String str7 = nextDate3.run((int) (short) -1, (int) (short) 0, (int) (byte) 1);
        String str11 = nextDate3.run(2020, 31, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6203() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6203");
        NextDate nextDate3 = new NextDate((int) '#', (int) ' ', (int) (short) 100);
        String str7 = nextDate3.run(0, 12, 2020);
        String str11 = nextDate3.run((int) ' ', (int) (short) 1, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6204() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6204");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 10, 2001);
        String str7 = nextDate3.run((-1), 2001, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6205() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6205");
        NextDate nextDate3 = new NextDate((int) ' ', 2020, (int) (byte) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6206() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6206");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 12, 31);
        String str7 = nextDate3.run((int) (short) 100, (int) (short) -1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6207() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6207");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 10, (int) '#');
        String str7 = nextDate3.run((int) (byte) 100, 31, (int) 'a');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6208() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6208");
        NextDate nextDate3 = new NextDate(0, 7, (int) (short) 1);
        String str7 = nextDate3.run((int) (byte) -1, (int) (byte) 100, (int) (short) 0);
        String str11 = nextDate3.run(0, (int) (byte) 100, 30);
        String str15 = nextDate3.run((int) (short) -1, 0, 0);
        String str19 = nextDate3.run(30, 12, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6209() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6209");
        NextDate nextDate3 = new NextDate(31, 2001, 31);
    }

    @Test
    public void test6210() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6210");
        NextDate nextDate3 = new NextDate((int) (short) 100, 2001, (int) (byte) -1);
    }

    @Test
    public void test6211() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6211");
        NextDate nextDate3 = new NextDate((int) (short) 10, 7, (int) '4');
    }

    @Test
    public void test6212() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6212");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 100, 2020);
        String str7 = nextDate3.run((int) (byte) 0, 0, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6213() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6213");
        NextDate nextDate3 = new NextDate((-1), 10, (int) (byte) 1);
        String str7 = nextDate3.run(100, 100, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6214() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6214");
        NextDate nextDate3 = new NextDate(100, 1, 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6215() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6215");
        NextDate nextDate3 = new NextDate(2001, (-1), 2020);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6216() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6216");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) 10, (int) (byte) 0);
        String str7 = nextDate3.run(0, (-1), 31);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6217() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6217");
        NextDate nextDate3 = new NextDate(30, 0, (int) ' ');
    }

    @Test
    public void test6218() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6218");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) -1, (int) (short) -1);
        String str7 = nextDate3.run(7, (int) (short) 0, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6219() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6219");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 1, (int) (short) -1);
        String str7 = nextDate3.run((int) (byte) 10, (int) (short) 0, 2001);
        String str11 = nextDate3.run((int) (byte) -1, (int) (byte) 0, 31);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6220() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6220");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 1, (int) (byte) 0);
        String str7 = nextDate3.run(12, (int) (short) 1, 2020);
        String str11 = nextDate3.run(2001, (int) (byte) 0, 10);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "12/2/2020" + "'", str7, "12/2/2020");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6221() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6221");
        NextDate nextDate3 = new NextDate(0, (int) ' ', (int) (byte) -1);
    }

    @Test
    public void test6222() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6222");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) 1, 31);
    }

    @Test
    public void test6223() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6223");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) 'a', 12);
        String str7 = nextDate3.run(12, (int) (byte) 10, (int) (short) 0);
        String str11 = nextDate3.run((int) (short) -1, (int) (byte) -1, (int) '#');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6224() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6224");
        NextDate nextDate3 = new NextDate(7, (int) ' ', 0);
    }

    @Test
    public void test6225() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6225");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) 10, (int) (byte) 1);
        String str7 = nextDate3.run(1, 30, (int) (byte) 0);
        String str11 = nextDate3.run((int) '4', 2001, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6226() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6226");
        NextDate nextDate3 = new NextDate((int) '4', (int) '4', 1);
        String str7 = nextDate3.run((int) '4', 0, 100);
        String str11 = nextDate3.run((int) (byte) -1, (int) (byte) 1, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6227() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6227");
        NextDate nextDate3 = new NextDate(1, 7, (int) (short) -1);
        String str7 = nextDate3.run(2020, 0, (int) '4');
        String str11 = nextDate3.run((-1), 100, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6228() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6228");
        NextDate nextDate3 = new NextDate(0, 0, 2001);
        String str7 = nextDate3.run((int) (short) -1, (int) (short) -1, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6229() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6229");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, (int) (byte) 1, (int) (short) 100);
        String str11 = nextDate3.run((int) (byte) -1, (int) (byte) 100, (int) (byte) 10);
        String str15 = nextDate3.run((int) ' ', (int) ' ', (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6230() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6230");
        NextDate nextDate3 = new NextDate(10, 30, 0);
    }

    @Test
    public void test6231() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6231");
        NextDate nextDate3 = new NextDate(2001, 31, (int) (byte) -1);
        String str7 = nextDate3.run(0, 2020, 2001);
        String str11 = nextDate3.run((int) (byte) 10, (int) (byte) -1, (int) (short) 0);
        String str15 = nextDate3.run((int) ' ', (int) (byte) -1, (int) (byte) 1);
        String str19 = nextDate3.run(0, 7, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6232() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6232");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) '4', 30);
    }

    @Test
    public void test6233() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6233");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) ' ', 2020, (int) (short) 0);
        String str11 = nextDate3.run(31, (int) '4', (int) (byte) -1);
        String str15 = nextDate3.run((int) (byte) 100, 7, (int) (short) 100);
        String str19 = nextDate3.run(1, 31, 2001);
        String str23 = nextDate3.run(0, 30, 30);
        String str27 = nextDate3.run((int) (short) 10, (int) 'a', (int) (short) 0);
        String str31 = nextDate3.run((int) (byte) 1, (int) (short) 10, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "2/1/2001" + "'", str19, "2/1/2001");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
    }

    @Test
    public void test6234() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6234");
        NextDate nextDate3 = new NextDate((int) (short) 100, 12, (int) '4');
        String str7 = nextDate3.run(0, (int) (short) 1, (int) '4');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6235() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6235");
        NextDate nextDate3 = new NextDate(31, (int) (short) 100, (int) (byte) 100);
        String str7 = nextDate3.run((int) 'a', (int) 'a', (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6236() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6236");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (short) -1, (int) (short) 10);
        String str7 = nextDate3.run(30, 10, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6237() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6237");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) '4', (int) (byte) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6238() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6238");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) 0, (int) (byte) 0);
    }

    @Test
    public void test6239() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6239");
        NextDate nextDate3 = new NextDate(0, 1, (int) (byte) 1);
        String str7 = nextDate3.run((int) (byte) 100, (int) (byte) 100, (int) '4');
        String str11 = nextDate3.run((int) (short) 10, (int) (short) 0, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6240() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6240");
        NextDate nextDate3 = new NextDate(12, 100, 7);
    }

    @Test
    public void test6241() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6241");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) '4', (int) (byte) 1);
        String str7 = nextDate3.run(0, (int) (byte) 100, (int) (short) 100);
        String str11 = nextDate3.run(10, 100, (int) (short) 100);
        String str15 = nextDate3.run((int) (byte) 100, (int) (short) 10, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6242() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6242");
        NextDate nextDate3 = new NextDate(10, (int) '#', 7);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6243() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6243");
        NextDate nextDate3 = new NextDate(2001, (int) (short) 1, 2001);
        String str7 = nextDate3.run((int) (short) 1, (int) '#', (int) (byte) -1);
        String str11 = nextDate3.run((int) 'a', (int) 'a', 7);
        String str15 = nextDate3.run((int) (short) -1, 0, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6244() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6244");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 10, 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6245() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6245");
        NextDate nextDate3 = new NextDate(12, (int) (short) 10, (int) (byte) 10);
        String str7 = nextDate3.run((int) 'a', (int) (short) 100, 2020);
        String str11 = nextDate3.run((int) (short) 1, 0, (int) (short) 10);
        String str15 = nextDate3.run((int) (short) 10, 2001, 0);
        String str19 = nextDate3.run(31, (int) '#', 0);
        String str23 = nextDate3.run(0, 100, 0);
        String str27 = nextDate3.run((int) (byte) 1, 2001, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test6246() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6246");
        NextDate nextDate3 = new NextDate((int) '#', (int) '4', (int) (short) -1);
        String str7 = nextDate3.run((int) '4', (int) (byte) -1, (int) '4');
        String str11 = nextDate3.run((int) 'a', (int) '4', (int) (short) 0);
        String str15 = nextDate3.run(2020, 2020, 30);
        String str19 = nextDate3.run(100, 10, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6247() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6247");
        NextDate nextDate3 = new NextDate(31, (int) 'a', (-1));
        String str7 = nextDate3.run((int) (byte) 1, 100, (int) (byte) -1);
        String str11 = nextDate3.run((int) 'a', 2001, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6248() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6248");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 10, (int) (byte) 1);
    }

    @Test
    public void test6249() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6249");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) 10, 0);
        String str7 = nextDate3.run(12, (int) '#', (int) (short) -1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6250() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6250");
        NextDate nextDate3 = new NextDate(30, (int) (short) 1, 1);
        String str7 = nextDate3.run(2020, 2020, (-1));
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6251() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6251");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 0, (int) (short) 1);
        String str7 = nextDate3.run((int) '4', (int) (short) 1, 10);
        String str11 = nextDate3.run((int) 'a', (int) (short) 10, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6252() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6252");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) 0, (int) ' ');
        String str7 = nextDate3.run((int) (byte) 0, 2001, (int) (short) 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6253() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6253");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 1, (int) '4');
    }

    @Test
    public void test6254() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6254");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) -1, (int) 'a');
        String str7 = nextDate3.run(2001, (-1), 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6255() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6255");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 0, (int) (byte) 0);
    }

    @Test
    public void test6256() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6256");
        NextDate nextDate3 = new NextDate(2001, (int) (short) 1, 2001);
        String str7 = nextDate3.run((int) (short) 1, (int) '#', (int) (byte) -1);
        String str11 = nextDate3.run((int) 'a', (int) 'a', 7);
        String str15 = nextDate3.run(100, (-1), (int) (short) 10);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test6257() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6257");
        NextDate nextDate3 = new NextDate(0, 2001, (int) (short) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6258() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6258");
        NextDate nextDate3 = new NextDate(7, 31, (int) '4');
    }

    @Test
    public void test6259() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6259");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 10, (int) (short) 0);
    }

    @Test
    public void test6260() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6260");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 0, (-1));
    }

    @Test
    public void test6261() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6261");
        NextDate nextDate3 = new NextDate((int) (short) 1, 30, (int) (byte) 100);
        String str7 = nextDate3.run((int) 'a', 0, (int) (byte) 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6262() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6262");
        NextDate nextDate3 = new NextDate(31, 30, (int) (byte) 0);
        String str7 = nextDate3.run((int) (short) 0, (int) (short) 10, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6263() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6263");
        NextDate nextDate3 = new NextDate(10, (int) (short) 100, 0);
        String str7 = nextDate3.run(2001, 2001, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6264() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6264");
        NextDate nextDate3 = new NextDate(2020, 100, (int) (byte) 1);
        String str7 = nextDate3.run((int) '4', 12, 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6265() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6265");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) '4', 1);
    }

    @Test
    public void test6266() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6266");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 1, 10);
        String str7 = nextDate3.run((int) (short) 0, 30, (int) (byte) 1);
        String str11 = nextDate3.run((int) (short) 100, 12, (int) (short) -1);
        String str15 = nextDate3.run((int) (short) 10, (int) (short) 0, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6267() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6267");
        NextDate nextDate3 = new NextDate(31, (int) (short) 10, 100);
    }

    @Test
    public void test6268() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6268");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 100, 31);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6269() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6269");
        NextDate nextDate3 = new NextDate(0, 100, 2020);
    }

    @Test
    public void test6270() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6270");
        NextDate nextDate3 = new NextDate((int) (short) 10, 12, 100);
        String str7 = nextDate3.run(1, (int) '#', 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6271() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6271");
        NextDate nextDate3 = new NextDate(1, (-1), (int) (short) 0);
    }

    @Test
    public void test6272() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6272");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 10, 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6273() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6273");
        NextDate nextDate3 = new NextDate(2001, (int) (short) -1, (int) (short) -1);
    }

    @Test
    public void test6274() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6274");
        NextDate nextDate3 = new NextDate(30, 2001, 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6275() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6275");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 12, (int) '4');
    }

    @Test
    public void test6276() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6276");
        NextDate nextDate3 = new NextDate((int) 'a', 7, (int) (short) 10);
        String str7 = nextDate3.run((int) (short) 100, (int) (short) -1, (int) (byte) 0);
        String str11 = nextDate3.run((int) (short) 100, (int) (short) 0, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6277() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6277");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) '#', 12);
        String str7 = nextDate3.run(30, (int) (byte) 10, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6278() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6278");
        NextDate nextDate3 = new NextDate((int) (short) 10, 2020, 1);
    }

    @Test
    public void test6279() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6279");
        NextDate nextDate3 = new NextDate(2020, (int) (short) 10, 1);
        String str7 = nextDate3.run((int) '4', (int) '4', 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6280() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6280");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 1, (int) '4');
    }

    @Test
    public void test6281() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6281");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 0, 10);
        String str7 = nextDate3.run((int) (short) 0, (int) 'a', 12);
        String str11 = nextDate3.run(2001, (int) ' ', 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6282() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6282");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) 100, 31);
    }

    @Test
    public void test6283() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6283");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 30, (int) (short) 1);
        String str7 = nextDate3.run(2001, (int) (short) 10, (int) (byte) 1);
        String str11 = nextDate3.run((int) (byte) 10, 30, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6284() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6284");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(7, 10, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6285() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6285");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 0, (int) '#');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6286() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6286");
        NextDate nextDate3 = new NextDate(0, 1, 30);
    }

    @Test
    public void test6287() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6287");
        NextDate nextDate3 = new NextDate((int) (short) 10, 1, 31);
    }

    @Test
    public void test6288() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6288");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 2001, (int) (byte) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6289() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6289");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) -1, 100);
        String str7 = nextDate3.run(7, (int) (short) 0, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6290() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6290");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (short) 1, 100);
    }

    @Test
    public void test6291() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6291");
        NextDate nextDate3 = new NextDate(1, 2020, 2001);
        String str7 = nextDate3.run((int) (short) 100, 2020, 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6292() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6292");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 0, 2001);
        String str7 = nextDate3.run((int) (short) -1, 0, (int) (byte) 0);
        String str11 = nextDate3.run((int) (byte) 1, 31, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6293() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6293");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 100, 10);
        String str7 = nextDate3.run((int) (short) 10, (int) (short) 100, (int) (byte) 0);
        String str11 = nextDate3.run((int) (byte) 0, 30, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6294() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6294");
        NextDate nextDate3 = new NextDate((int) '4', 100, (int) '#');
        String str7 = nextDate3.run(2020, (int) 'a', 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6295() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6295");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 100, 12);
        String str7 = nextDate3.run((int) (short) -1, 10, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6296() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6296");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 100, 2020);
        String str7 = nextDate3.run((int) '4', (int) (byte) -1, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6297() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6297");
        NextDate nextDate3 = new NextDate((int) (short) 10, 7, (int) (short) 100);
    }

    @Test
    public void test6298() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6298");
        NextDate nextDate3 = new NextDate(30, 0, 0);
    }

    @Test
    public void test6299() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6299");
        NextDate nextDate3 = new NextDate((-1), 30, (int) (byte) -1);
        String str7 = nextDate3.run((int) (short) 10, (int) (short) 1, 2020);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "10/2/2020" + "'", str7, "10/2/2020");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6300() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6300");
        NextDate nextDate3 = new NextDate((-1), (int) ' ', 2001);
    }

    @Test
    public void test6301() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6301");
        NextDate nextDate3 = new NextDate(2020, 0, (-1));
        String str7 = nextDate3.run((int) (byte) 1, (int) (short) -1, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6302() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6302");
        NextDate nextDate3 = new NextDate(2020, 0, (-1));
        String str7 = nextDate3.run((int) (byte) 100, 2020, (int) 'a');
        String str11 = nextDate3.run(2020, 12, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6303() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6303");
        NextDate nextDate3 = new NextDate(2020, 12, (-1));
        String str7 = nextDate3.run((int) (byte) -1, 2001, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6304() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6304");
        NextDate nextDate3 = new NextDate(2020, (int) 'a', (int) (short) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6305() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6305");
        NextDate nextDate3 = new NextDate((int) (short) 1, 31, (int) (short) -1);
        String str7 = nextDate3.run(2020, (int) (byte) -1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6306() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6306");
        NextDate nextDate3 = new NextDate((int) (short) 100, (-1), (int) 'a');
        String str7 = nextDate3.run((int) 'a', (int) '#', (int) (byte) -1);
        String str11 = nextDate3.run((int) 'a', (int) ' ', 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6307() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6307");
        NextDate nextDate3 = new NextDate(31, 10, 31);
    }

    @Test
    public void test6308() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6308");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 0, (int) 'a');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6309() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6309");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) '4', (int) (short) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6310() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6310");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (byte) 100, (int) (short) 100);
        String str7 = nextDate3.run((-1), 2001, 31);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6311() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6311");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) -1, (int) '#');
        String str7 = nextDate3.run(100, 2001, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6312() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6312");
        NextDate nextDate3 = new NextDate((int) (short) 1, 7, 2001);
        String str7 = nextDate3.run((int) (short) 10, (int) ' ', 0);
        String str11 = nextDate3.run((int) (short) 1, (int) ' ', 7);
        String str15 = nextDate3.run((int) 'a', 1, 2020);
        String str19 = nextDate3.run(31, (int) (short) -1, (int) (byte) 100);
        String str23 = nextDate3.run(100, (int) ' ', (int) (short) -1);
        String str27 = nextDate3.run((int) (short) 10, (int) (byte) -1, 0);
        Class<?> wildcardClass28 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test6313() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6313");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 100, 2020);
        String str7 = nextDate3.run(100, (int) (short) -1, (int) (short) 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6314() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6314");
        NextDate nextDate3 = new NextDate((int) '4', (-1), (int) (byte) 100);
        String str7 = nextDate3.run(31, (int) (byte) 1, (int) '#');
        String str11 = nextDate3.run(31, (int) (byte) 0, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6315() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6315");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) 100, (int) (short) 0);
    }

    @Test
    public void test6316() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6316");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 2001, 31);
        String str7 = nextDate3.run((-1), (int) '#', (int) '4');
        String str11 = nextDate3.run(1, (int) (short) -1, 0);
        String str15 = nextDate3.run((int) (byte) 10, (int) ' ', (int) '#');
        String str19 = nextDate3.run(2001, 2001, 100);
        String str23 = nextDate3.run((int) (byte) 10, 2020, 1);
        Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test6317() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6317");
        NextDate nextDate3 = new NextDate((int) '4', 10, 2001);
    }

    @Test
    public void test6318() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6318");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 1, (int) (short) 100);
    }

    @Test
    public void test6319() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6319");
        NextDate nextDate3 = new NextDate(30, (-1), 31);
        String str7 = nextDate3.run(7, (int) (short) 100, 1);
        String str11 = nextDate3.run(12, 2001, (int) (short) 1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6320() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6320");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) 1, (int) (byte) 1);
    }

    @Test
    public void test6321() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6321");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) -1, 2001);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6322() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6322");
        NextDate nextDate3 = new NextDate((int) '4', (-1), (int) (byte) 100);
        String str7 = nextDate3.run(31, (int) (byte) 1, (int) '#');
        String str11 = nextDate3.run((-1), (int) '#', (int) (short) -1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6323() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6323");
        NextDate nextDate3 = new NextDate((int) (short) 1, 1, (int) (short) 10);
        String str7 = nextDate3.run(31, (int) ' ', (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6324() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6324");
        NextDate nextDate3 = new NextDate(0, 7, (int) (byte) 10);
        String str7 = nextDate3.run(0, (int) (byte) 10, 10);
        String str11 = nextDate3.run((int) (short) 1, 1, (int) (short) 10);
        String str15 = nextDate3.run((int) ' ', 2020, (int) (short) 0);
        String str19 = nextDate3.run((int) ' ', (int) '#', (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6325() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6325");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 30, (int) '4');
        String str7 = nextDate3.run(0, 2020, (int) ' ');
        String str11 = nextDate3.run((int) (short) 1, (int) (short) 100, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6326() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6326");
        NextDate nextDate3 = new NextDate((int) (short) -1, 7, (int) (short) -1);
    }

    @Test
    public void test6327() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6327");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 0, 2020);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6328() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6328");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 100, (-1));
    }

    @Test
    public void test6329() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6329");
        NextDate nextDate3 = new NextDate((int) (short) 10, 100, 7);
        String str7 = nextDate3.run(30, 2020, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6330() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6330");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) -1, 10);
        String str7 = nextDate3.run((int) (byte) -1, (-1), 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6331() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6331");
        NextDate nextDate3 = new NextDate(30, (int) (byte) -1, 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6332() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6332");
        NextDate nextDate3 = new NextDate(2001, 7, (int) (byte) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6333() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6333");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) ' ', 2020, (int) (short) 0);
        String str11 = nextDate3.run(0, (int) '4', (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6334() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6334");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 1, (-1));
        String str7 = nextDate3.run((-1), (int) '4', (int) (short) 10);
        String str11 = nextDate3.run((int) (byte) 10, 2001, (int) (short) 1);
        String str15 = nextDate3.run((int) 'a', (int) (byte) -1, (int) (byte) 10);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test6335() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6335");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) (byte) 0, (int) (byte) 10, 31);
        String str23 = nextDate3.run((-1), 0, (int) (byte) -1);
        String str27 = nextDate3.run((int) (byte) 1, (int) (byte) 1, (int) 'a');
        String str31 = nextDate3.run((int) (short) 1, 30, (int) (byte) 100);
        String str35 = nextDate3.run((int) '4', (int) (byte) -1, (int) (short) 10);
        String str39 = nextDate3.run(0, 2001, 7);
        String str43 = nextDate3.run((int) (short) 100, (int) (byte) -1, (int) '#');
        Class<?> wildcardClass44 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str43 + "' != '" + "invalid Input Date" + "'", str43, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass44);
    }

    @Test
    public void test6336() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6336");
        NextDate nextDate3 = new NextDate((int) '#', 1, 31);
        String str7 = nextDate3.run((int) (byte) 100, (int) (byte) 10, 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6337() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6337");
        NextDate nextDate3 = new NextDate(1, 12, (int) (short) 1);
        String str7 = nextDate3.run((int) '4', 0, (int) (short) 10);
        String str11 = nextDate3.run((int) (byte) 0, 0, (int) '4');
        String str15 = nextDate3.run((int) ' ', (int) (byte) 10, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6338() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6338");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        String str15 = nextDate3.run(7, (int) (short) 1, 30);
        String str19 = nextDate3.run(10, 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6339() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6339");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 0, 12);
        String str7 = nextDate3.run((int) ' ', 31, (int) (short) 100);
        String str11 = nextDate3.run((int) (byte) -1, (int) (short) 1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6340() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6340");
        NextDate nextDate3 = new NextDate(1, 12, (-1));
        String str7 = nextDate3.run(1, (int) (byte) 100, (int) (short) 100);
        String str11 = nextDate3.run((int) '4', (int) '#', (int) (byte) 1);
        String str15 = nextDate3.run((int) '#', (int) '4', 12);
        String str19 = nextDate3.run((int) (byte) 100, (int) (short) 0, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6341() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6341");
        NextDate nextDate3 = new NextDate(7, (int) (byte) 0, (int) 'a');
        String str7 = nextDate3.run(2020, 12, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6342() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6342");
        NextDate nextDate3 = new NextDate(10, 31, 2001);
    }

    @Test
    public void test6343() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6343");
        NextDate nextDate3 = new NextDate(1, 0, 0);
        String str7 = nextDate3.run((int) (byte) -1, (int) (short) 0, (int) '4');
        String str11 = nextDate3.run((int) (short) -1, (int) (short) 1, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6344() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6344");
        NextDate nextDate3 = new NextDate(10, 0, (int) (short) 100);
    }

    @Test
    public void test6345() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6345");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 100, 0);
        String str7 = nextDate3.run(12, (int) '4', (int) '4');
        String str11 = nextDate3.run(30, (int) (byte) 1, 30);
        String str15 = nextDate3.run((-1), (int) '#', (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6346() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6346");
        NextDate nextDate3 = new NextDate(31, 10, 2001);
        String str7 = nextDate3.run((-1), (int) (byte) 10, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6347() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6347");
        NextDate nextDate3 = new NextDate(12, (int) (byte) 1, (int) '#');
        String str7 = nextDate3.run(12, (int) (short) 0, 0);
        String str11 = nextDate3.run((int) 'a', (-1), (int) (short) 100);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6348() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6348");
        NextDate nextDate3 = new NextDate((-1), 0, (int) (byte) 1);
    }

    @Test
    public void test6349() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6349");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 0, 7);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6350() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6350");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 0, 7);
    }

    @Test
    public void test6351() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6351");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 10, (int) ' ');
        String str7 = nextDate3.run(7, 0, (int) (short) 10);
        String str11 = nextDate3.run(1, 10, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6352() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6352");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 1, 0);
        String str7 = nextDate3.run((int) '4', 0, (int) (byte) 10);
        String str11 = nextDate3.run(31, 0, (int) (byte) -1);
        String str15 = nextDate3.run(2020, 1, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6353() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6353");
        NextDate nextDate3 = new NextDate(0, (int) ' ', (int) '4');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6354() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6354");
        NextDate nextDate3 = new NextDate(7, (int) (byte) 0, (int) ' ');
    }

    @Test
    public void test6355() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6355");
        NextDate nextDate3 = new NextDate((int) (short) -1, 0, (int) (short) 1);
    }

    @Test
    public void test6356() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6356");
        NextDate nextDate3 = new NextDate((int) (short) 10, 1, (int) '#');
    }

    @Test
    public void test6357() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6357");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 10, (int) (short) -1);
        String str7 = nextDate3.run((int) (short) 1, 0, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6358() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6358");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        String str15 = nextDate3.run((int) (byte) 1, (int) (short) 0, (int) (short) 100);
        String str19 = nextDate3.run((int) (short) 10, 0, 10);
        String str23 = nextDate3.run((int) (byte) -1, 10, 30);
        String str27 = nextDate3.run((int) (byte) 100, (int) (short) 0, (-1));
        String str31 = nextDate3.run((int) (byte) -1, 10, (int) (short) 10);
        String str35 = nextDate3.run((int) (short) 0, 7, (int) (byte) 100);
        String str39 = nextDate3.run((int) (short) 1, (int) (short) 10, (int) (short) -1);
        String str43 = nextDate3.run(1, 10, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str43 + "' != '" + "invalid Input Date" + "'", str43, "invalid Input Date");
    }

    @Test
    public void test6359() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6359");
        NextDate nextDate3 = new NextDate((int) (short) 1, 2020, (int) '4');
        String str7 = nextDate3.run((int) (short) 10, 0, 10);
        String str11 = nextDate3.run(0, (int) 'a', (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6360() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6360");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) 'a', 12, (int) (byte) 10);
        String str11 = nextDate3.run(31, (int) (byte) 1, (int) (byte) 10);
        String str15 = nextDate3.run((int) (short) 10, (int) (byte) 0, (int) 'a');
        String str19 = nextDate3.run(2001, (-1), 1);
        String str23 = nextDate3.run((int) (byte) 100, (int) (short) 1, (int) 'a');
        String str27 = nextDate3.run(0, 30, 10);
        Class<?> wildcardClass28 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test6361() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6361");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 1, (int) (byte) 100);
        String str7 = nextDate3.run(10, 2001, (int) (short) 1);
        String str11 = nextDate3.run((int) (short) -1, (int) 'a', (int) (short) 1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6362() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6362");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (-1), (int) (byte) -1);
        String str7 = nextDate3.run(100, 2020, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6363() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6363");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) 1, (int) '4');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6364() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6364");
        NextDate nextDate3 = new NextDate((int) 'a', 2020, 2001);
        String str7 = nextDate3.run((int) (short) 1, (int) '4', 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "2/1/2020" + "'", str7, "2/1/2020");
    }

    @Test
    public void test6365() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6365");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 10, (int) (short) -1);
    }

    @Test
    public void test6366() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6366");
        NextDate nextDate3 = new NextDate(30, 0, (int) (byte) -1);
    }

    @Test
    public void test6367() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6367");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 1, 31);
        String str7 = nextDate3.run((int) '#', (int) (byte) 10, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6368() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6368");
        NextDate nextDate3 = new NextDate((int) '#', 2001, (int) (short) -1);
    }

    @Test
    public void test6369() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6369");
        NextDate nextDate3 = new NextDate(0, 2020, (int) (short) 0);
        String str7 = nextDate3.run((int) (byte) 10, 2020, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6370() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6370");
        NextDate nextDate3 = new NextDate(0, 7, (int) '#');
    }

    @Test
    public void test6371() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6371");
        NextDate nextDate3 = new NextDate((int) '#', 1, (int) (short) -1);
    }

    @Test
    public void test6372() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6372");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 30, 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6373() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6373");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (short) 1, (int) (byte) 0);
    }

    @Test
    public void test6374() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6374");
        NextDate nextDate3 = new NextDate(30, (int) (byte) 100, 2001);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6375() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6375");
        NextDate nextDate3 = new NextDate(0, 1, 0);
    }

    @Test
    public void test6376() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6376");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        String str15 = nextDate3.run((int) (byte) 1, (int) (short) 0, (int) (short) 100);
        String str19 = nextDate3.run((int) (short) 10, 0, 10);
        String str23 = nextDate3.run((int) (byte) -1, 10, 30);
        String str27 = nextDate3.run((int) (byte) 100, 0, 12);
        String str31 = nextDate3.run((int) (short) 0, (int) (byte) 10, 0);
        String str35 = nextDate3.run((int) (byte) 1, (int) (short) 1, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
    }

    @Test
    public void test6377() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6377");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) '#', (int) (byte) 1);
    }

    @Test
    public void test6378() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6378");
        NextDate nextDate3 = new NextDate(0, (int) 'a', (int) '4');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6379() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6379");
        NextDate nextDate3 = new NextDate(12, (-1), 1);
        String str7 = nextDate3.run((int) '#', 0, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6380() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6380");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 31, (int) (short) 0);
        String str7 = nextDate3.run((int) '4', (int) (byte) 10, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6381() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6381");
        NextDate nextDate3 = new NextDate((int) (short) 0, 30, 30);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6382() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6382");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) 0, (int) 'a');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6383() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6383");
        NextDate nextDate3 = new NextDate((-1), 0, 31);
        String str7 = nextDate3.run(10, (-1), (int) (short) -1);
        String str11 = nextDate3.run((int) (short) -1, 2001, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6384() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6384");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) 1, (int) (byte) -1);
    }

    @Test
    public void test6385() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6385");
        NextDate nextDate3 = new NextDate(100, 2020, 2020);
    }

    @Test
    public void test6386() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6386");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 10, (int) (short) 0);
        String str7 = nextDate3.run((-1), 12, (int) (short) 100);
        String str11 = nextDate3.run((int) (short) -1, 0, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6387() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6387");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 12, (int) ' ');
        String str7 = nextDate3.run((int) (short) 0, (int) (byte) -1, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6388() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6388");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 100, (int) (short) 100);
        String str7 = nextDate3.run(0, 10, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6389() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6389");
        NextDate nextDate3 = new NextDate((int) '4', (int) ' ', (int) (byte) 10);
        String str7 = nextDate3.run(1, (int) ' ', (int) (short) 100);
        String str11 = nextDate3.run(100, (int) (short) 0, (int) ' ');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6390() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6390");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) 10, 2020);
        String str7 = nextDate3.run(100, (int) '4', 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6391() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6391");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) -1, (int) 'a');
        String str7 = nextDate3.run(7, (int) (short) 10, 1);
        String str11 = nextDate3.run((int) '4', 7, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6392() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6392");
        NextDate nextDate3 = new NextDate(12, (int) (byte) 0, 100);
    }

    @Test
    public void test6393() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6393");
        NextDate nextDate3 = new NextDate((int) (short) 100, 31, 2020);
        String str7 = nextDate3.run((int) (short) 0, (int) (short) -1, (int) '4');
        String str11 = nextDate3.run((int) (short) 1, 12, (int) (short) 0);
        String str15 = nextDate3.run((int) (byte) 10, (int) (byte) 100, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6394() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6394");
        NextDate nextDate3 = new NextDate(31, (int) (byte) 100, (int) (byte) 0);
    }

    @Test
    public void test6395() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6395");
        NextDate nextDate3 = new NextDate(100, 2020, 2001);
        String str7 = nextDate3.run((int) '4', (int) (short) 1, 0);
        String str11 = nextDate3.run((int) (byte) 0, 0, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6396() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6396");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) '#', 31);
        String str7 = nextDate3.run(0, (-1), (int) 'a');
        String str11 = nextDate3.run((int) ' ', 7, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6397() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6397");
        NextDate nextDate3 = new NextDate(7, (int) (short) 100, 1);
        String str7 = nextDate3.run((-1), (int) (byte) 10, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6398() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6398");
        NextDate nextDate3 = new NextDate(31, 2020, (int) (byte) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6399() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6399");
        NextDate nextDate3 = new NextDate((int) ' ', 30, 12);
    }

    @Test
    public void test6400() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6400");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) 0, (int) ' ');
        String str7 = nextDate3.run((int) (byte) 0, 2001, (int) (short) 10);
        String str11 = nextDate3.run((int) (byte) 100, (int) 'a', 31);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6401() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6401");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 12, (int) ' ');
        String str7 = nextDate3.run((int) (byte) 100, (int) (short) 100, 30);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6402() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6402");
        NextDate nextDate3 = new NextDate((int) (short) 100, 10, 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6403() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6403");
        NextDate nextDate3 = new NextDate((int) (short) 0, 0, (int) (short) 100);
        String str7 = nextDate3.run((int) ' ', 2001, 10);
        String str11 = nextDate3.run((int) (byte) 1, 100, (int) (byte) 1);
        String str15 = nextDate3.run(100, (int) (short) 10, (int) (byte) -1);
        String str19 = nextDate3.run((int) (short) 10, (-1), 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6404() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6404");
        NextDate nextDate3 = new NextDate(31, 30, (int) (short) 1);
        String str7 = nextDate3.run(100, 0, (int) '4');
        String str11 = nextDate3.run(0, (int) '4', (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6405() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6405");
        NextDate nextDate3 = new NextDate((int) '4', 31, 2001);
        String str7 = nextDate3.run((int) 'a', (int) (short) 10, (int) '4');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6406() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6406");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) ' ', 31);
        String str7 = nextDate3.run(10, (int) (short) 0, 10);
        String str11 = nextDate3.run((int) ' ', (int) (short) 100, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6407() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6407");
        NextDate nextDate3 = new NextDate(30, 0, (int) '#');
        String str7 = nextDate3.run((int) (byte) 10, (int) (short) 100, 2001);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "11/1/2001" + "'", str7, "11/1/2001");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6408() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6408");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 1, (int) (short) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6409() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6409");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) ' ', 100);
    }

    @Test
    public void test6410() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6410");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 1, (int) (byte) 100);
        String str7 = nextDate3.run((int) (byte) 0, 1, (int) (byte) 100);
        String str11 = nextDate3.run(0, (int) (byte) 10, (int) ' ');
        String str15 = nextDate3.run((int) (short) -1, (int) (short) -1, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6411() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6411");
        NextDate nextDate3 = new NextDate(30, (int) 'a', 2001);
        String str7 = nextDate3.run((int) (byte) 100, (-1), 31);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, (int) (short) 0);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6412() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6412");
        NextDate nextDate3 = new NextDate(7, 10, (int) (short) -1);
        String str7 = nextDate3.run((int) (byte) 100, (int) ' ', (int) (short) 10);
        String str11 = nextDate3.run(10, 0, (int) (byte) 0);
        String str15 = nextDate3.run(1, (int) (byte) -1, 0);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test6413() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6413");
        NextDate nextDate3 = new NextDate((int) (short) 10, 0, 100);
        String str7 = nextDate3.run(7, (int) (short) 1, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6414() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6414");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run((int) (short) 10, (int) (byte) 1, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test6415() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6415");
        NextDate nextDate3 = new NextDate(2020, (int) 'a', 30);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6416() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6416");
        NextDate nextDate3 = new NextDate(2020, 30, 0);
    }

    @Test
    public void test6417() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6417");
        NextDate nextDate3 = new NextDate(12, (-1), 100);
        String str7 = nextDate3.run(0, (int) (byte) 1, (int) (short) 1);
        String str11 = nextDate3.run(2020, (int) (byte) 100, 1);
        String str15 = nextDate3.run((int) (byte) 0, (int) (byte) 100, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6418() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6418");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) (byte) 0, (int) (byte) 10, 31);
        String str23 = nextDate3.run(0, 2001, (int) '#');
        String str27 = nextDate3.run((int) (short) -1, 12, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test6419() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6419");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 10, (int) (short) -1);
        String str7 = nextDate3.run((int) (byte) 100, (int) '#', 100);
        String str11 = nextDate3.run((int) 'a', (int) (short) -1, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6420() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6420");
        NextDate nextDate3 = new NextDate(10, 30, 31);
    }

    @Test
    public void test6421() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6421");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 10, (-1));
        String str7 = nextDate3.run((-1), (int) '#', (int) (byte) 100);
        String str11 = nextDate3.run((int) (short) 100, 2001, (int) '#');
        String str15 = nextDate3.run((int) (byte) 10, (int) (short) 1, 100);
        String str19 = nextDate3.run((int) (byte) 10, (int) (short) 1, (int) (short) 10);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test6422() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6422");
        NextDate nextDate3 = new NextDate((int) (short) 0, 0, (int) (byte) 100);
    }

    @Test
    public void test6423() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6423");
        NextDate nextDate3 = new NextDate(31, (int) '#', (int) 'a');
        String str7 = nextDate3.run((int) (short) 100, 0, (int) '#');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6424() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6424");
        NextDate nextDate3 = new NextDate(0, 31, (int) (short) 10);
        String str7 = nextDate3.run(12, 30, (int) (byte) 100);
        String str11 = nextDate3.run((int) (short) 10, 0, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6425() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6425");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) 10, (int) (byte) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6426() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6426");
        NextDate nextDate3 = new NextDate(100, 100, (int) (short) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6427() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6427");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 100, (int) (short) 100);
    }

    @Test
    public void test6428() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6428");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 10, (int) 'a');
    }

    @Test
    public void test6429() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6429");
        NextDate nextDate3 = new NextDate((int) (short) 0, 7, (-1));
        String str7 = nextDate3.run((int) (short) 10, 100, (int) ' ');
        String str11 = nextDate3.run(100, 12, (int) (short) -1);
        String str15 = nextDate3.run(7, (int) '4', (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6430() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6430");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 0, 30);
        String str7 = nextDate3.run(2020, 0, 31);
        String str11 = nextDate3.run((int) (short) 1, (int) (byte) 100, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6431() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6431");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 1, 2020);
    }

    @Test
    public void test6432() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6432");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) '#', 2001);
        String str7 = nextDate3.run(10, 31, (int) '#');
        String str11 = nextDate3.run(10, 30, (int) ' ');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6433() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6433");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 0, 0);
        String str7 = nextDate3.run((int) (short) 1, (int) 'a', 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6434() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6434");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) -1, 1);
        String str7 = nextDate3.run((int) ' ', (int) (short) -1, 0);
        String str11 = nextDate3.run((int) '#', 10, (int) ' ');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6435() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6435");
        NextDate nextDate3 = new NextDate((int) (short) 10, 0, (int) (byte) 1);
        String str7 = nextDate3.run(100, (int) '#', (int) (short) 1);
        String str11 = nextDate3.run((int) (short) -1, 10, 10);
        String str15 = nextDate3.run((int) (short) 10, (int) (short) 100, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6436() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6436");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, (int) '4');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6437() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6437");
        NextDate nextDate3 = new NextDate((int) (short) 10, 1, (int) 'a');
        String str7 = nextDate3.run((int) (short) 1, (int) (byte) 10, 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6438() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6438");
        NextDate nextDate3 = new NextDate(31, (int) 'a', (int) (byte) 100);
    }

    @Test
    public void test6439() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6439");
        NextDate nextDate3 = new NextDate((int) '4', 0, (int) '4');
        String str7 = nextDate3.run(1, (int) (short) 100, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6440() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6440");
        NextDate nextDate3 = new NextDate(10, (int) (short) 0, 10);
        String str7 = nextDate3.run((int) (short) 10, 100, 1);
        String str11 = nextDate3.run(1, 10, (int) '4');
        String str15 = nextDate3.run((int) (byte) -1, (int) (short) -1, 10);
        String str19 = nextDate3.run((int) (short) 1, 30, (int) (byte) -1);
        String str23 = nextDate3.run(2020, 2001, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test6441() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6441");
        NextDate nextDate3 = new NextDate(12, (int) (byte) 0, 0);
    }

    @Test
    public void test6442() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6442");
        NextDate nextDate3 = new NextDate(0, (int) '4', 10);
        String str7 = nextDate3.run((int) '4', 0, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6443() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6443");
        NextDate nextDate3 = new NextDate(7, 0, 31);
        String str7 = nextDate3.run((int) (short) -1, (-1), (int) (byte) 1);
        String str11 = nextDate3.run((int) (short) -1, (int) 'a', (int) (byte) 10);
        String str15 = nextDate3.run((int) (short) -1, (int) (byte) 100, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6444() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6444");
        NextDate nextDate3 = new NextDate(0, (int) (short) 100, (int) (short) 10);
    }

    @Test
    public void test6445() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6445");
        NextDate nextDate3 = new NextDate(2020, 0, (int) (byte) 1);
    }

    @Test
    public void test6446() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6446");
        NextDate nextDate3 = new NextDate(12, (int) (short) -1, (int) (byte) 0);
        String str7 = nextDate3.run((int) '#', (int) (byte) 100, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6447() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6447");
        NextDate nextDate3 = new NextDate((int) (short) 1, 100, 0);
        String str7 = nextDate3.run(0, (int) ' ', 31);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6448() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6448");
        NextDate nextDate3 = new NextDate(31, (int) 'a', (-1));
        String str7 = nextDate3.run((int) (byte) 1, 100, (int) (byte) -1);
        String str11 = nextDate3.run((int) (byte) 10, (int) (byte) -1, 10);
        String str15 = nextDate3.run((int) (short) 1, 31, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test6449() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6449");
        NextDate nextDate3 = new NextDate(12, (int) '#', 30);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6450() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6450");
        NextDate nextDate3 = new NextDate(1, 7, 12);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6451() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6451");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run(2001, 7, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6452() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6452");
        NextDate nextDate3 = new NextDate(10, 31, (int) (short) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6453() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6453");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) ' ', 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6454() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6454");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) 1, 30);
    }

    @Test
    public void test6455() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6455");
        NextDate nextDate3 = new NextDate(0, 1, (int) ' ');
        String str7 = nextDate3.run((int) (byte) 100, (int) (byte) 100, 31);
        String str11 = nextDate3.run((int) '4', 100, (int) (byte) 10);
        String str15 = nextDate3.run((int) '4', (int) (byte) 0, (int) (byte) 1);
        String str19 = nextDate3.run(0, 31, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6456() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6456");
        NextDate nextDate3 = new NextDate(0, 10, (int) '#');
        String str7 = nextDate3.run((int) (byte) 0, (int) (byte) -1, (int) '#');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6457() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6457");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) -1, 30);
        String str7 = nextDate3.run((int) (byte) -1, 2001, (int) (byte) 10);
        String str11 = nextDate3.run((int) '4', (int) (byte) 0, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6458() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6458");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 31, 10);
    }

    @Test
    public void test6459() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6459");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) 0, (int) (short) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6460() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6460");
        NextDate nextDate3 = new NextDate(30, (int) (short) 100, (int) (short) 100);
        String str7 = nextDate3.run((int) (short) 0, 7, 2020);
        String str11 = nextDate3.run((int) (short) 10, 1, (int) ' ');
        String str15 = nextDate3.run((int) '#', (int) '#', 100);
        String str19 = nextDate3.run(1, (int) (byte) 0, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6461() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6461");
        NextDate nextDate3 = new NextDate(31, 100, (int) (byte) 10);
        String str7 = nextDate3.run(0, (int) (short) 10, (int) (byte) -1);
        String str11 = nextDate3.run(10, (int) 'a', (int) (short) 0);
        String str15 = nextDate3.run((int) (short) 1, (int) (byte) 0, (int) (short) 100);
        String str19 = nextDate3.run((int) (short) 100, 31, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6462() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6462");
        NextDate nextDate3 = new NextDate(30, (int) (short) 100, (int) (short) 100);
        String str7 = nextDate3.run((int) (short) 0, 7, 2020);
        String str11 = nextDate3.run(100, (int) (byte) -1, (int) (short) 1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6463() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6463");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 10, 0);
        String str7 = nextDate3.run(1, (int) (byte) 100, 1);
        String str11 = nextDate3.run((int) (short) 1, (-1), (int) (short) -1);
        String str15 = nextDate3.run((int) (byte) 10, (int) (short) 10, (int) ' ');
        String str19 = nextDate3.run((int) (byte) 10, 0, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test6464() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6464");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) '4', 2001);
        String str7 = nextDate3.run((int) (short) -1, 100, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6465() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6465");
        NextDate nextDate3 = new NextDate((int) (short) 10, (-1), (int) (byte) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6466() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6466");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 100, (int) (byte) -1);
        String str7 = nextDate3.run(12, 0, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6467() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6467");
        NextDate nextDate3 = new NextDate((int) ' ', 1, (int) '#');
        String str7 = nextDate3.run((int) '#', (int) (short) 0, (int) '4');
        String str11 = nextDate3.run(10, (int) ' ', (int) 'a');
        String str15 = nextDate3.run(30, (int) '4', 10);
        String str19 = nextDate3.run((int) (short) -1, (int) (byte) 0, (int) (byte) 0);
        String str23 = nextDate3.run(31, 1, (int) (byte) 1);
        Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test6468() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6468");
        NextDate nextDate3 = new NextDate(12, (int) (byte) 1, (int) (short) 100);
    }

    @Test
    public void test6469() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6469");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 12, (int) ' ');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6470() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6470");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 1, (int) (byte) 10);
        String str7 = nextDate3.run((int) '4', 2001, (int) (byte) 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6471() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6471");
        NextDate nextDate3 = new NextDate((int) (short) 1, 31, (int) (byte) -1);
    }

    @Test
    public void test6472() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6472");
        NextDate nextDate3 = new NextDate(30, (int) (short) 100, (int) (short) 100);
        String str7 = nextDate3.run((int) (short) 0, 7, 2020);
        String str11 = nextDate3.run((int) (short) 10, 1, (int) ' ');
        String str15 = nextDate3.run(100, 0, 100);
        String str19 = nextDate3.run((int) (byte) 1, 1, 12);
        String str23 = nextDate3.run(100, 30, 10);
        String str27 = nextDate3.run((int) (short) 0, (int) (byte) 1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test6473() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6473");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) 10, 7);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6474() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6474");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (short) 0, 30);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6475() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6475");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 100, (int) (short) -1);
    }

    @Test
    public void test6476() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6476");
        NextDate nextDate3 = new NextDate(31, 1, 2001);
        String str7 = nextDate3.run((int) (short) 1, (int) ' ', (int) 'a');
        String str11 = nextDate3.run(30, 0, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6477() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6477");
        NextDate nextDate3 = new NextDate(10, (int) (short) 1, (int) (short) -1);
        String str7 = nextDate3.run((int) (short) 0, (int) (byte) 0, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6478() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6478");
        NextDate nextDate3 = new NextDate(0, (int) '4', 2001);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6479() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6479");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) -1, 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6480() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6480");
        NextDate nextDate3 = new NextDate(2020, 0, (-1));
        String str7 = nextDate3.run(30, (int) (byte) 100, (-1));
        String str11 = nextDate3.run((int) (byte) 100, 12, 2001);
        String str15 = nextDate3.run((int) 'a', (-1), 0);
        String str19 = nextDate3.run(1, 0, (-1));
        String str23 = nextDate3.run(0, (int) (byte) 1, (int) (byte) 1);
        String str27 = nextDate3.run((int) 'a', 0, (int) (byte) 1);
        String str31 = nextDate3.run(7, 12, (int) (byte) 100);
        String str35 = nextDate3.run((int) (short) -1, (-1), 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
    }

    @Test
    public void test6481() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6481");
        NextDate nextDate3 = new NextDate(1, (int) (short) -1, (int) '4');
        String str7 = nextDate3.run(12, 31, 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6482() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6482");
        NextDate nextDate3 = new NextDate(10, (int) '#', (int) ' ');
    }

    @Test
    public void test6483() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6483");
        NextDate nextDate3 = new NextDate(100, (int) (short) 0, (int) ' ');
    }

    @Test
    public void test6484() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6484");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 12, (int) (byte) -1);
        String str7 = nextDate3.run(100, (int) (byte) -1, 12);
        String str11 = nextDate3.run((int) (byte) -1, (int) '#', 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6485() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6485");
        NextDate nextDate3 = new NextDate(0, 30, (int) (byte) 10);
        String str7 = nextDate3.run((int) (byte) 0, 10, (int) '4');
        String str11 = nextDate3.run((int) (byte) -1, (int) (short) 10, (int) (short) 1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6486() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6486");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) 1, 12);
        String str7 = nextDate3.run(30, (int) ' ', 12);
        String str11 = nextDate3.run((int) (short) 100, (int) (byte) 10, 7);
        String str15 = nextDate3.run((int) (byte) 100, (int) '#', 2001);
        String str19 = nextDate3.run((int) (short) 1, 10, (-1));
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test6487() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6487");
        NextDate nextDate3 = new NextDate(31, 2020, 2001);
    }

    @Test
    public void test6488() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6488");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 10, (int) '#');
        String str7 = nextDate3.run((int) (byte) 100, (int) ' ', 2001);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6489() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6489");
        NextDate nextDate3 = new NextDate(0, 2020, 7);
        String str7 = nextDate3.run(31, 2001, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test6490() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6490");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 100, 7);
        String str7 = nextDate3.run((int) (short) 1, (int) (short) 10, 7);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test6491() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6491");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 0, (int) (byte) 0);
    }

    @Test
    public void test6492() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6492");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 100, (int) '#');
        String str7 = nextDate3.run(10, 1, (int) (short) 100);
        String str11 = nextDate3.run(12, 0, (int) (short) 0);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6493() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6493");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 1, 0);
        String str7 = nextDate3.run(10, 31, (int) (short) -1);
        String str11 = nextDate3.run(0, (int) '4', 12);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test6494() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6494");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 1, 2001);
    }

    @Test
    public void test6495() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6495");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) '#', (int) (short) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test6496() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6496");
        NextDate nextDate3 = new NextDate(2001, (int) (short) 1, 31);
        String str7 = nextDate3.run((int) (byte) 0, (int) (byte) 0, (int) (short) 100);
        String str11 = nextDate3.run((int) (short) -1, (int) (short) 1, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test6497() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6497");
        NextDate nextDate3 = new NextDate(2020, (int) (short) 100, (-1));
    }

    @Test
    public void test6498() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6498");
        NextDate nextDate3 = new NextDate(0, 0, (int) '4');
    }

    @Test
    public void test6499() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6499");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) 1, (int) (byte) 1);
    }

    @Test
    public void test6500() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest12.test6500");
        NextDate nextDate3 = new NextDate(2001, (int) (short) -1, 2020);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }
}

