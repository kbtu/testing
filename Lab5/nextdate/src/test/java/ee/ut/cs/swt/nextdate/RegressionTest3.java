package ee.ut.cs.swt.nextdate;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest3 {

    public static boolean debug = false;

    @Test
    public void test1501() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1501");
        NextDate nextDate3 = new NextDate(7, (int) (byte) 0, (int) (short) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1502() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1502");
        NextDate nextDate3 = new NextDate((int) '#', (int) ' ', 30);
        String str7 = nextDate3.run((int) (byte) 10, 10, (int) (byte) 1);
        String str11 = nextDate3.run(30, 2001, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1503() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1503");
        NextDate nextDate3 = new NextDate(31, (int) '4', (int) (short) -1);
        String str7 = nextDate3.run(10, 12, (int) 'a');
        String str11 = nextDate3.run((-1), 10, (int) (short) 0);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1504() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1504");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 1, (int) (byte) 100);
        String str7 = nextDate3.run(30, (int) (short) 100, (int) 'a');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1505() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1505");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 100, (int) (byte) -1);
        String str7 = nextDate3.run(2020, (int) (byte) 10, 0);
        String str11 = nextDate3.run(30, 2001, (int) (short) 1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1506() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1506");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) 0, (int) 'a');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1507() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1507");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 0, 7);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1508() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1508");
        NextDate nextDate3 = new NextDate(100, 0, (int) (byte) 1);
    }

    @Test
    public void test1509() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1509");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) 100, (int) (byte) 100);
        String str7 = nextDate3.run((-1), (int) (byte) 1, 0);
        String str11 = nextDate3.run((int) (short) 10, 31, 2001);
        String str15 = nextDate3.run(10, 31, 31);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "11/1/2001" + "'", str11, "11/1/2001");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1510() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1510");
        NextDate nextDate3 = new NextDate((int) 'a', (int) '4', (int) (short) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1511() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1511");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 12, 31);
        String str7 = nextDate3.run((int) (short) 10, (int) (short) 1, 31);
        String str11 = nextDate3.run((int) '#', 12, 2020);
        String str15 = nextDate3.run((int) 'a', 0, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test1512() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1512");
        NextDate nextDate3 = new NextDate(7, (int) 'a', (int) (byte) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1513() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1513");
        NextDate nextDate3 = new NextDate(0, 0, (int) ' ');
    }

    @Test
    public void test1514() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1514");
        NextDate nextDate3 = new NextDate((int) 'a', 1, 7);
        String str7 = nextDate3.run(10, 100, 0);
        String str11 = nextDate3.run(2001, (int) (short) -1, 30);
        String str15 = nextDate3.run((int) (short) 1, 0, 0);
        String str19 = nextDate3.run(31, (int) (short) 100, (int) '4');
        String str23 = nextDate3.run((int) (byte) 0, 7, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test1515() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1515");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 1, 0);
        String str7 = nextDate3.run(2020, (int) '4', 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1516() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1516");
        NextDate nextDate3 = new NextDate(30, (int) (short) 100, (int) '4');
    }

    @Test
    public void test1517() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1517");
        NextDate nextDate3 = new NextDate((-1), 10, (int) (short) -1);
    }

    @Test
    public void test1518() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1518");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) -1, 0);
        String str7 = nextDate3.run(2001, 0, (int) (short) 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1519() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1519");
        NextDate nextDate3 = new NextDate(0, 30, 0);
        String str7 = nextDate3.run(12, 1, 7);
        String str11 = nextDate3.run((int) (byte) -1, 0, 12);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1520() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1520");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 100, (int) (byte) -1);
    }

    @Test
    public void test1521() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1521");
        NextDate nextDate3 = new NextDate(7, 2020, (int) (short) 1);
        String str7 = nextDate3.run((int) 'a', (int) (byte) 100, 1);
        String str11 = nextDate3.run(1, 31, 100);
        String str15 = nextDate3.run((int) (byte) 10, (int) (byte) -1, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test1522() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1522");
        NextDate nextDate3 = new NextDate(2001, (int) (short) 10, (int) (short) 10);
        String str7 = nextDate3.run((int) (byte) 10, (-1), 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1523() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1523");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run(31, 31, 12);
        String str23 = nextDate3.run((int) (short) 0, (int) (byte) 100, (int) 'a');
        Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test1524() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1524");
        NextDate nextDate3 = new NextDate((int) 'a', (int) '#', 12);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1525() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1525");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run((int) (byte) 10, (int) (short) 100, 100);
        String str27 = nextDate3.run((int) (short) 100, (int) (short) 0, (int) (byte) 0);
        String str31 = nextDate3.run(2020, 30, 7);
        String str35 = nextDate3.run((int) (short) 10, (int) (byte) -1, 100);
        String str39 = nextDate3.run(31, (int) '#', (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
    }

    @Test
    public void test1526() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1526");
        NextDate nextDate3 = new NextDate((-1), 2001, (int) 'a');
    }

    @Test
    public void test1527() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1527");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) -1, (int) (byte) 0);
    }

    @Test
    public void test1528() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1528");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 1, 10);
        String str7 = nextDate3.run((int) (short) 0, 30, (int) (byte) 1);
        String str11 = nextDate3.run((int) (short) 1, 1, 7);
        String str15 = nextDate3.run(10, (int) (short) -1, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test1529() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1529");
        NextDate nextDate3 = new NextDate(12, 0, (int) (short) 100);
    }

    @Test
    public void test1530() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1530");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) ' ', 2020);
        String str7 = nextDate3.run(1, (int) (short) 100, (int) 'a');
        String str11 = nextDate3.run(0, (int) (byte) 10, 30);
        String str15 = nextDate3.run(7, (int) (byte) 0, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test1531() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1531");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        String str15 = nextDate3.run((int) (byte) 1, (int) (short) 0, (int) (short) 100);
        String str19 = nextDate3.run((int) (short) 10, 0, 10);
        String str23 = nextDate3.run((int) '#', 100, 100);
        String str27 = nextDate3.run((int) (byte) 1, (int) (short) 10, (int) (short) 10);
        String str31 = nextDate3.run((int) 'a', 1, 12);
        String str35 = nextDate3.run((int) (byte) 0, (int) (short) 0, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
    }

    @Test
    public void test1532() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1532");
        NextDate nextDate3 = new NextDate((int) (short) 1, 100, (int) '4');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1533() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1533");
        NextDate nextDate3 = new NextDate((int) ' ', (int) '#', (int) ' ');
        String str7 = nextDate3.run((int) '#', (int) (byte) 10, (int) 'a');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1534() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1534");
        NextDate nextDate3 = new NextDate(100, (int) (short) 0, 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1535() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1535");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run(7, (int) (short) 10, (int) (short) 1);
        String str27 = nextDate3.run((int) (short) 10, 7, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test1536() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1536");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 10, (int) (byte) 100);
    }

    @Test
    public void test1537() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1537");
        NextDate nextDate3 = new NextDate(10, (int) '#', (int) (byte) -1);
        String str7 = nextDate3.run((int) '#', (int) (byte) 0, 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1538() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1538");
        NextDate nextDate3 = new NextDate(0, (int) (byte) -1, 7);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1539() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1539");
        NextDate nextDate3 = new NextDate(2020, 30, 1);
        String str7 = nextDate3.run((int) '4', (int) (byte) 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1540() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1540");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) -1, (int) (byte) -1);
        String str7 = nextDate3.run(100, (int) 'a', 30);
        String str11 = nextDate3.run(0, (int) (byte) 1, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1541() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1541");
        NextDate nextDate3 = new NextDate((int) 'a', (int) '#', 2001);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1542() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1542");
        NextDate nextDate3 = new NextDate(2001, 7, (int) (short) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1543() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1543");
        NextDate nextDate3 = new NextDate(0, 7, (int) (short) 1);
        String str7 = nextDate3.run((int) (byte) -1, (int) (byte) 100, (int) (short) 0);
        String str11 = nextDate3.run(0, (int) (byte) 100, 30);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1544() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1544");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) '4', (int) '4');
    }

    @Test
    public void test1545() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1545");
        NextDate nextDate3 = new NextDate(2020, 0, (int) ' ');
    }

    @Test
    public void test1546() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1546");
        NextDate nextDate3 = new NextDate((int) (short) 0, 0, (int) (short) 100);
        String str7 = nextDate3.run(12, 0, (int) ' ');
        String str11 = nextDate3.run((int) (short) 0, (int) '4', (int) (short) 10);
        String str15 = nextDate3.run((int) '#', (int) (short) 10, (int) (short) 0);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1547() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1547");
        NextDate nextDate3 = new NextDate(30, 100, (int) (short) -1);
    }

    @Test
    public void test1548() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1548");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) (byte) 10, (int) '#', 31);
        String str11 = nextDate3.run((int) (short) 1, (int) 'a', (int) (byte) -1);
        String str15 = nextDate3.run((int) (byte) 0, (int) ' ', (int) (byte) 1);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1549() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1549");
        NextDate nextDate3 = new NextDate((int) '4', (int) ' ', (int) (byte) 10);
        String str7 = nextDate3.run(1, (int) ' ', (int) (short) 100);
        String str11 = nextDate3.run((int) 'a', (int) (byte) 1, (int) (byte) 0);
        String str15 = nextDate3.run(100, (int) (byte) 1, (int) (short) 10);
        String str19 = nextDate3.run(12, (int) (short) 1, (int) (byte) 10);
        String str23 = nextDate3.run(31, 1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test1550() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1550");
        NextDate nextDate3 = new NextDate(10, (int) (short) 100, (int) (byte) 10);
    }

    @Test
    public void test1551() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1551");
        NextDate nextDate3 = new NextDate((int) (short) 10, 1, (int) (short) -1);
        String str7 = nextDate3.run((int) ' ', 30, (int) (byte) 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1552() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1552");
        NextDate nextDate3 = new NextDate((int) (short) 10, 7, 2020);
    }

    @Test
    public void test1553() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1553");
        NextDate nextDate3 = new NextDate(2020, (int) 'a', (int) (byte) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1554() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1554");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 31, (int) '#');
        String str7 = nextDate3.run((int) (short) 100, 31, 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1555() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1555");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) -1, (-1));
        String str7 = nextDate3.run((int) (byte) 100, 2020, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1556() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1556");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 1, 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1557() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1557");
        NextDate nextDate3 = new NextDate(7, (int) (short) 10, (int) (byte) 100);
    }

    @Test
    public void test1558() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1558");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run(7, (int) (short) 10, (int) (short) 1);
        String str27 = nextDate3.run((int) 'a', (int) (byte) 1, 0);
        String str31 = nextDate3.run((int) (byte) 0, (int) ' ', (int) (short) 10);
        String str35 = nextDate3.run((int) (short) -1, (int) '4', 7);
        String str39 = nextDate3.run(10, 31, (int) (byte) 0);
        String str43 = nextDate3.run((int) (short) 10, 0, (int) (byte) 1);
        Class<?> wildcardClass44 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str43 + "' != '" + "invalid Input Date" + "'", str43, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass44);
    }

    @Test
    public void test1559() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1559");
        NextDate nextDate3 = new NextDate(0, 7, (int) (short) 10);
        String str7 = nextDate3.run((int) (short) 10, (-1), (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1560() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1560");
        NextDate nextDate3 = new NextDate((int) ' ', 1, (-1));
        String str7 = nextDate3.run((int) '4', 0, 30);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1561() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1561");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 100, 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1562() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1562");
        NextDate nextDate3 = new NextDate(0, (int) '4', 0);
        String str7 = nextDate3.run((int) (byte) 100, (int) (short) 10, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1563() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1563");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) 'a', 12, (int) (byte) 10);
        String str11 = nextDate3.run(31, (int) (byte) 1, (int) (byte) 10);
        String str15 = nextDate3.run((int) (short) 10, (int) (short) 100, (int) (short) 1);
        String str19 = nextDate3.run((int) (byte) -1, (int) (short) 1, (int) (byte) 1);
        String str23 = nextDate3.run((int) ' ', 100, (-1));
        String str27 = nextDate3.run((int) '#', (int) ' ', 10);
        String str31 = nextDate3.run(31, (int) '4', 1);
        Class<?> wildcardClass32 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass32);
    }

    @Test
    public void test1564() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1564");
        NextDate nextDate3 = new NextDate((int) (short) -1, 2020, (int) (short) 100);
        String str7 = nextDate3.run(1, (int) (short) 100, (int) (short) -1);
        String str11 = nextDate3.run(31, (int) (byte) 100, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1565() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1565");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 2020, 100);
        String str7 = nextDate3.run(1, 2001, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1566() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1566");
        NextDate nextDate3 = new NextDate((-1), 0, (int) '#');
        String str7 = nextDate3.run((int) (short) 1, 7, 10);
        String str11 = nextDate3.run((int) (byte) 10, 0, (int) '#');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1567() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1567");
        NextDate nextDate3 = new NextDate(10, (int) (short) -1, (int) '4');
    }

    @Test
    public void test1568() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1568");
        NextDate nextDate3 = new NextDate(2020, 0, 0);
        String str7 = nextDate3.run((int) '4', 2001, 2020);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1569() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1569");
        NextDate nextDate3 = new NextDate((int) (short) 1, 7, 2001);
        String str7 = nextDate3.run((int) (short) 10, (int) ' ', 0);
        String str11 = nextDate3.run((int) (short) 1, (int) ' ', 7);
        String str15 = nextDate3.run((int) ' ', (int) 'a', (int) (short) 0);
        String str19 = nextDate3.run(2020, (int) (byte) 10, (int) (short) -1);
        String str23 = nextDate3.run(0, (int) ' ', (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test1570() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1570");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 10, (int) '4');
        String str7 = nextDate3.run((int) (byte) 100, (int) (short) 1, (int) (byte) -1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1571() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1571");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 12, 10);
        String str7 = nextDate3.run(7, (int) 'a', 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1572() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1572");
        NextDate nextDate3 = new NextDate((int) ' ', 30, (int) (byte) 100);
    }

    @Test
    public void test1573() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1573");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 0, (int) ' ');
        String str7 = nextDate3.run(7, 30, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1574() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1574");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 10, (int) (short) 0);
        String str7 = nextDate3.run(2020, 30, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1575() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1575");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) '#', (int) ' ');
        String str7 = nextDate3.run((-1), 1, (-1));
        String str11 = nextDate3.run((int) (byte) 1, 12, (int) (short) 100);
        String str15 = nextDate3.run(1, (int) (short) 0, (int) (short) 10);
        String str19 = nextDate3.run((int) (short) 1, (int) (byte) -1, (int) (byte) 10);
        String str23 = nextDate3.run(2001, 7, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test1576() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1576");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) '4', (int) (byte) 1);
    }

    @Test
    public void test1577() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1577");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 10, (int) (byte) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1578() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1578");
        NextDate nextDate3 = new NextDate((int) (short) 100, 100, (int) (short) 1);
    }

    @Test
    public void test1579() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1579");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 10, 7);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1580() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1580");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 7, (int) (short) 10);
        String str7 = nextDate3.run((int) (byte) 100, (int) (short) -1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1581() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1581");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 1, (int) 'a');
        String str7 = nextDate3.run(0, (int) (short) 0, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1582() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1582");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 0, (int) (byte) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1583() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1583");
        NextDate nextDate3 = new NextDate(1, (int) (byte) 10, 2020);
    }

    @Test
    public void test1584() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1584");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run((int) (byte) 10, (int) (short) 100, 100);
        Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test1585() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1585");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) 'a', 12, (int) (byte) 10);
        String str11 = nextDate3.run(31, (int) (byte) 1, (int) (byte) 10);
        String str15 = nextDate3.run((int) (short) 10, (int) (byte) 0, (int) 'a');
        String str19 = nextDate3.run(2001, (-1), 1);
        String str23 = nextDate3.run(2020, (int) '#', (int) (short) 10);
        String str27 = nextDate3.run((int) (short) 100, (int) (byte) 0, 0);
        Class<?> wildcardClass28 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test1586() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1586");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 7, 0);
        String str7 = nextDate3.run(0, (int) ' ', (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1587() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1587");
        NextDate nextDate3 = new NextDate(7, (int) '4', (int) '#');
        String str7 = nextDate3.run((int) (short) 1, 2020, (int) (short) 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1588() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1588");
        NextDate nextDate3 = new NextDate(31, (int) (short) 1, 2020);
        String str7 = nextDate3.run(12, (int) (byte) 100, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1589() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1589");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 100, 31);
        String str7 = nextDate3.run((int) (byte) 10, 100, 7);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1590() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1590");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) -1, 2020);
        String str7 = nextDate3.run((int) (short) 100, (int) (byte) 0, (int) '4');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1591() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1591");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 0, 0);
    }

    @Test
    public void test1592() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1592");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 100, (int) (byte) 10);
        String str7 = nextDate3.run(2001, (-1), (int) (short) 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1593() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1593");
        NextDate nextDate3 = new NextDate(7, (-1), (int) (byte) 1);
        String str7 = nextDate3.run(31, (int) 'a', (int) 'a');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1594() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1594");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 0, 12);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1595() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1595");
        NextDate nextDate3 = new NextDate((-1), 12, (int) (byte) 0);
        String str7 = nextDate3.run((int) 'a', (int) (short) 1, (int) (byte) 10);
        String str11 = nextDate3.run((int) (byte) 1, 12, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1596() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1596");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 30, 10);
        String str7 = nextDate3.run(7, 30, (int) '4');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1597() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1597");
        NextDate nextDate3 = new NextDate(1, 10, 31);
        String str7 = nextDate3.run(7, (int) (byte) -1, (int) (short) 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1598() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1598");
        NextDate nextDate3 = new NextDate((int) (short) 1, 1, 2020);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1599() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1599");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 1, 1);
    }

    @Test
    public void test1600() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1600");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        String str15 = nextDate3.run((int) (byte) 1, (int) (short) 0, (int) (short) 100);
        String str19 = nextDate3.run((int) (short) 10, 0, 10);
        String str23 = nextDate3.run((int) (byte) -1, 10, 30);
        String str27 = nextDate3.run((int) (byte) 100, (int) (short) 0, (-1));
        String str31 = nextDate3.run((int) (byte) -1, 10, (int) (short) 10);
        String str35 = nextDate3.run(100, 10, 0);
        String str39 = nextDate3.run(0, 10, (int) (byte) 10);
        Class<?> wildcardClass40 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass40);
    }

    @Test
    public void test1601() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1601");
        NextDate nextDate3 = new NextDate(2001, 0, (int) (byte) -1);
        String str7 = nextDate3.run((int) (short) 1, (int) '4', (int) (byte) -1);
        String str11 = nextDate3.run((int) (byte) -1, 12, (int) (short) 0);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1602() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1602");
        NextDate nextDate3 = new NextDate((int) (short) 0, 0, (-1));
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1603() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1603");
        NextDate nextDate3 = new NextDate(0, (int) (short) -1, (int) (byte) 1);
    }

    @Test
    public void test1604() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1604");
        NextDate nextDate3 = new NextDate(0, (int) ' ', (int) (short) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1605() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1605");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 100, 2020);
        String str7 = nextDate3.run(30, (int) (byte) 100, (-1));
        String str11 = nextDate3.run(0, (int) (short) 0, (int) (byte) -1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1606() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1606");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) 10, 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1607() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1607");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (short) -1, (int) '#');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1608() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1608");
        NextDate nextDate3 = new NextDate(2020, (int) '#', 12);
    }

    @Test
    public void test1609() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1609");
        NextDate nextDate3 = new NextDate(10, (-1), 1);
    }

    @Test
    public void test1610() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1610");
        NextDate nextDate3 = new NextDate((int) (short) 100, 0, 1);
        String str7 = nextDate3.run(31, (int) '#', (int) (short) 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1611() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1611");
        NextDate nextDate3 = new NextDate(12, (int) (byte) 10, 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1612() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1612");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 100, (int) (short) 100);
    }

    @Test
    public void test1613() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1613");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) -1, (int) '4');
        String str7 = nextDate3.run((int) (short) 100, 2001, 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1614() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1614");
        NextDate nextDate3 = new NextDate((int) 'a', 10, (int) '4');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1615() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1615");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) 0, (int) '4');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1616() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1616");
        NextDate nextDate3 = new NextDate(31, 31, 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1617() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1617");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) '#', (int) (byte) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1618() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1618");
        NextDate nextDate3 = new NextDate((int) '4', (int) 'a', (int) (short) 1);
    }

    @Test
    public void test1619() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1619");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 12, (int) (byte) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1620() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1620");
        NextDate nextDate3 = new NextDate(12, (-1), 100);
        String str7 = nextDate3.run((int) (short) -1, 100, 12);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1621() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1621");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 31, (int) '#');
        String str7 = nextDate3.run((int) (short) 100, 31, 10);
        String str11 = nextDate3.run(30, (int) '4', (int) (short) 0);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1622() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1622");
        NextDate nextDate3 = new NextDate(10, 10, 0);
        String str7 = nextDate3.run((int) (short) 1, 1, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1623() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1623");
        NextDate nextDate3 = new NextDate((int) '4', 7, 30);
        String str7 = nextDate3.run(1, (int) (byte) 100, 30);
        String str11 = nextDate3.run((-1), 0, 12);
        String str15 = nextDate3.run((int) '#', (int) '#', (int) (short) 1);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1624() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1624");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) -1, (int) 'a');
    }

    @Test
    public void test1625() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1625");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) 1, (int) 'a');
        String str7 = nextDate3.run(0, 2020, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1626() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1626");
        NextDate nextDate3 = new NextDate(0, 2001, (int) 'a');
    }

    @Test
    public void test1627() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1627");
        NextDate nextDate3 = new NextDate(2001, (int) (short) 1, 31);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1628() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1628");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) 'a', 12, (int) (byte) 10);
        String str11 = nextDate3.run(31, (int) (byte) 1, (int) (byte) 10);
        String str15 = nextDate3.run((int) (short) 10, (int) (short) 100, (int) (short) 1);
        String str19 = nextDate3.run((int) (byte) -1, (int) (short) 1, (int) (byte) 1);
        String str23 = nextDate3.run((int) ' ', 100, (-1));
        String str27 = nextDate3.run((int) '#', (int) ' ', 10);
        Class<?> wildcardClass28 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test1629() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1629");
        NextDate nextDate3 = new NextDate(7, (int) (byte) -1, 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1630() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1630");
        NextDate nextDate3 = new NextDate(10, 31, (int) (byte) 10);
    }

    @Test
    public void test1631() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1631");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (short) -1, (int) (short) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1632() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1632");
        NextDate nextDate3 = new NextDate((int) (short) 100, 1, 2001);
        String str7 = nextDate3.run(2020, (int) '#', (int) '#');
        String str11 = nextDate3.run((int) (short) 10, 30, 100);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1633() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1633");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (-1), (int) 'a');
    }

    @Test
    public void test1634() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1634");
        NextDate nextDate3 = new NextDate(30, (int) '#', (int) ' ');
        String str7 = nextDate3.run(2001, (int) (byte) 10, 31);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1635() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1635");
        NextDate nextDate3 = new NextDate(30, 0, 10);
        String str7 = nextDate3.run((int) (short) 100, (int) ' ', (-1));
        String str11 = nextDate3.run((int) ' ', 31, (int) (short) 100);
        String str15 = nextDate3.run(1, (int) (byte) 100, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test1636() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1636");
        NextDate nextDate3 = new NextDate(2001, (int) '#', (int) '#');
        String str7 = nextDate3.run((int) (byte) 0, (-1), 0);
        String str11 = nextDate3.run(31, 31, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1637() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1637");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) 'a', 12, (int) (byte) 10);
        String str11 = nextDate3.run(31, (int) (byte) 1, (int) (byte) 10);
        String str15 = nextDate3.run((int) (short) 10, (int) (byte) 0, (int) 'a');
        String str19 = nextDate3.run(2001, (-1), 1);
        String str23 = nextDate3.run((int) (byte) 100, (int) (short) 1, (int) 'a');
        String str27 = nextDate3.run((int) (short) 1, 0, (int) (byte) 0);
        String str31 = nextDate3.run((int) (short) 0, 100, (int) ' ');
        Class<?> wildcardClass32 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass32);
    }

    @Test
    public void test1638() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1638");
        NextDate nextDate3 = new NextDate((-1), 0, (int) '#');
        String str7 = nextDate3.run((int) (short) 1, 7, 10);
        String str11 = nextDate3.run((int) (byte) 10, 0, (int) '#');
        String str15 = nextDate3.run((-1), (int) '#', (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test1639() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1639");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) (short) -1, 2001, 100);
        String str23 = nextDate3.run((int) (byte) 0, 12, (int) (short) 1);
        String str27 = nextDate3.run(31, (int) '4', (int) ' ');
        Class<?> wildcardClass28 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test1640() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1640");
        NextDate nextDate3 = new NextDate(2001, (int) (short) 10, 7);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1641() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1641");
        NextDate nextDate3 = new NextDate((int) '#', 0, 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1642() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1642");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) '4', 12, 10);
        String str15 = nextDate3.run(31, (int) (short) 100, 31);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1643() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1643");
        NextDate nextDate3 = new NextDate(7, (int) (short) 100, 1);
        String str7 = nextDate3.run((int) (short) 10, 2001, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1644() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1644");
        NextDate nextDate3 = new NextDate(10, (int) (short) 100, (int) (short) 10);
        String str7 = nextDate3.run(1, 31, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1645() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1645");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, (int) (byte) 1, (int) (short) 100);
        String str11 = nextDate3.run((int) (short) -1, 10, 1);
        String str15 = nextDate3.run((int) ' ', 2020, (-1));
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1646() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1646");
        NextDate nextDate3 = new NextDate(12, (int) (byte) -1, (int) (byte) 0);
        String str7 = nextDate3.run((int) (short) 100, (int) (short) 1, 0);
        String str11 = nextDate3.run((int) (short) 0, 0, 0);
        String str15 = nextDate3.run(31, (-1), 2020);
        String str19 = nextDate3.run(2020, 100, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test1647() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1647");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) -1, (int) (byte) 1);
    }

    @Test
    public void test1648() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1648");
        NextDate nextDate3 = new NextDate(0, (int) (byte) -1, 7);
        String str7 = nextDate3.run(31, (-1), (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1649() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1649");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) 10, 31);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1650() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1650");
        NextDate nextDate3 = new NextDate(100, 30, (int) ' ');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1651() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1651");
        NextDate nextDate3 = new NextDate((int) (short) 1, 1, 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1652() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1652");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 2001, 31);
        String str7 = nextDate3.run((-1), (int) '#', (int) '4');
        String str11 = nextDate3.run((int) (short) 100, (int) (byte) 10, (int) (byte) -1);
        String str15 = nextDate3.run(0, 30, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test1653() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1653");
        NextDate nextDate3 = new NextDate(1, (int) (byte) 0, 100);
        String str7 = nextDate3.run(0, 7, (int) 'a');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1654() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1654");
        NextDate nextDate3 = new NextDate((int) ' ', 100, (-1));
        String str7 = nextDate3.run((int) (short) -1, (int) (byte) 10, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1655() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1655");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) ' ', 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1656() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1656");
        NextDate nextDate3 = new NextDate(12, (int) (byte) 10, 0);
        String str7 = nextDate3.run((int) (byte) 1, 10, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1657() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1657");
        NextDate nextDate3 = new NextDate(12, 2001, 2020);
        String str7 = nextDate3.run(31, (int) (short) 10, 2020);
        String str11 = nextDate3.run((int) (byte) 0, 12, (int) '#');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1658() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1658");
        NextDate nextDate3 = new NextDate(30, (-1), (int) (byte) 10);
    }

    @Test
    public void test1659() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1659");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 0, (-1));
    }

    @Test
    public void test1660() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1660");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 0, (int) '4');
        String str7 = nextDate3.run((-1), (int) (byte) 10, (int) (byte) 0);
        String str11 = nextDate3.run(7, (int) (short) 1, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1661() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1661");
        NextDate nextDate3 = new NextDate((int) '#', 100, (-1));
    }

    @Test
    public void test1662() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1662");
        NextDate nextDate3 = new NextDate((int) (short) 100, 0, 12);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1663() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1663");
        NextDate nextDate3 = new NextDate(31, (int) ' ', 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1664() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1664");
        NextDate nextDate3 = new NextDate((int) (short) 100, 12, 30);
    }

    @Test
    public void test1665() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1665");
        NextDate nextDate3 = new NextDate(0, (int) (short) -1, 2001);
    }

    @Test
    public void test1666() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1666");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 2020, 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1667() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1667");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 0, (int) (byte) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1668() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1668");
        NextDate nextDate3 = new NextDate(0, (int) (short) 100, (int) ' ');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1669() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1669");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 0, (int) (byte) 0);
        String str7 = nextDate3.run((int) (byte) 100, 30, (int) (byte) 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1670() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1670");
        NextDate nextDate3 = new NextDate(31, 30, (int) (short) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1671() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1671");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) 1, 12);
        String str7 = nextDate3.run(2020, 0, (int) (byte) -1);
        String str11 = nextDate3.run((int) '4', (int) '#', (int) (short) 0);
        String str15 = nextDate3.run((int) '#', 0, 10);
        String str19 = nextDate3.run((int) (short) 0, (int) '4', (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test1672() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1672");
        NextDate nextDate3 = new NextDate(1, (int) (byte) 1, (int) (byte) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1673() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1673");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 31, 7);
        String str7 = nextDate3.run((int) (short) 0, (int) ' ', 7);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1674() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1674");
        NextDate nextDate3 = new NextDate((int) (short) -1, 2020, 100);
    }

    @Test
    public void test1675() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1675");
        NextDate nextDate3 = new NextDate(1, 7, (int) (byte) 1);
        String str7 = nextDate3.run((int) (short) -1, 0, 1);
        String str11 = nextDate3.run((int) '#', (int) (byte) 100, 30);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1676() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1676");
        NextDate nextDate3 = new NextDate(0, 2020, 10);
    }

    @Test
    public void test1677() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1677");
        NextDate nextDate3 = new NextDate(100, (int) ' ', 0);
        String str7 = nextDate3.run(7, 30, (int) 'a');
        String str11 = nextDate3.run(2001, (int) (byte) -1, 2001);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1678() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1678");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) 0, (int) ' ');
    }

    @Test
    public void test1679() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1679");
        NextDate nextDate3 = new NextDate(30, (int) ' ', (int) (short) 100);
    }

    @Test
    public void test1680() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1680");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 100, 0);
        String str7 = nextDate3.run((-1), (int) (short) 1, (int) (byte) -1);
        String str11 = nextDate3.run((int) '4', (int) (byte) 10, 2001);
        String str15 = nextDate3.run(10, 0, 10);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1681() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1681");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) '#', 30);
        String str7 = nextDate3.run(7, (-1), 0);
        String str11 = nextDate3.run((int) (byte) 10, 7, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1682() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1682");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 0, 30);
        String str7 = nextDate3.run(31, (int) (byte) 100, 30);
        String str11 = nextDate3.run(12, (int) (short) 10, (int) (short) 100);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1683() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1683");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run(7, (int) (short) 10, (int) (short) 1);
        Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test1684() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1684");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 10, (int) (short) 10);
        String str7 = nextDate3.run(12, (int) (short) -1, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1685() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1685");
        NextDate nextDate3 = new NextDate(0, (int) (short) 100, (int) (short) -1);
        String str7 = nextDate3.run((int) (short) 1, (int) (short) 100, (int) '4');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1686() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1686");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) 100, (int) (short) 100);
    }

    @Test
    public void test1687() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1687");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 2001, 31);
        String str7 = nextDate3.run((-1), (int) '#', (int) '4');
        String str11 = nextDate3.run(1, (int) (short) -1, 0);
        String str15 = nextDate3.run((int) (byte) 10, (int) ' ', (int) '#');
        String str19 = nextDate3.run((int) (short) 0, 2020, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test1688() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1688");
        NextDate nextDate3 = new NextDate(2001, (int) (short) -1, (-1));
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1689() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1689");
        NextDate nextDate3 = new NextDate((int) (short) 100, 31, (-1));
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1690() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1690");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 10, (int) (short) 0);
        String str7 = nextDate3.run(30, (int) 'a', (int) (short) -1);
        String str11 = nextDate3.run((int) '4', (-1), 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1691() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1691");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 1, (int) (short) -1);
        String str7 = nextDate3.run(2020, 31, (int) (short) 10);
        String str11 = nextDate3.run(1, (int) (byte) -1, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1692() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1692");
        NextDate nextDate3 = new NextDate(0, (int) (short) 1, (int) 'a');
        String str7 = nextDate3.run(2001, (int) (short) 0, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1693() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1693");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 0, (int) (byte) 100);
        String str7 = nextDate3.run((int) '#', 12, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1694() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1694");
        NextDate nextDate3 = new NextDate(0, 7, (int) ' ');
        String str7 = nextDate3.run((int) (short) -1, 0, (int) (byte) 10);
        String str11 = nextDate3.run(1, 30, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1695() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1695");
        NextDate nextDate3 = new NextDate(10, (int) (short) 10, (int) (byte) 10);
    }

    @Test
    public void test1696() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1696");
        NextDate nextDate3 = new NextDate((int) (short) -1, 7, (int) (byte) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1697() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1697");
        NextDate nextDate3 = new NextDate(0, 100, (int) (short) 100);
    }

    @Test
    public void test1698() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1698");
        NextDate nextDate3 = new NextDate(10, (int) '#', (int) (byte) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1699() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1699");
        NextDate nextDate3 = new NextDate((int) (short) 0, 31, 0);
    }

    @Test
    public void test1700() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1700");
        NextDate nextDate3 = new NextDate((int) (short) 1, 100, 1);
    }

    @Test
    public void test1701() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1701");
        NextDate nextDate3 = new NextDate(12, 2001, 2020);
        String str7 = nextDate3.run(31, (int) (short) 10, 2020);
        String str11 = nextDate3.run(10, 12, (int) (byte) 100);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1702() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1702");
        NextDate nextDate3 = new NextDate(2001, 100, 10);
        String str7 = nextDate3.run(1, (-1), 1);
        String str11 = nextDate3.run(10, 30, (int) '4');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1703() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1703");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        String str15 = nextDate3.run((int) (byte) 1, (int) (short) 0, (int) (short) 100);
        String str19 = nextDate3.run((int) (short) 10, 0, 10);
        String str23 = nextDate3.run((int) '#', 100, 100);
        String str27 = nextDate3.run((int) (byte) 1, (int) (short) 10, (int) (short) 10);
        String str31 = nextDate3.run((int) 'a', 1, 12);
        String str35 = nextDate3.run((int) '#', (int) 'a', 1);
        Class<?> wildcardClass36 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass36);
    }

    @Test
    public void test1704() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1704");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 0, (int) '#');
        String str7 = nextDate3.run(0, (int) (short) 0, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1705() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1705");
        NextDate nextDate3 = new NextDate((int) 'a', (int) 'a', 31);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1706() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1706");
        NextDate nextDate3 = new NextDate(31, (int) (byte) -1, (int) (short) 100);
    }

    @Test
    public void test1707() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1707");
        NextDate nextDate3 = new NextDate(12, (int) (short) -1, 7);
    }

    @Test
    public void test1708() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1708");
        NextDate nextDate3 = new NextDate(1, 30, (int) (byte) 100);
        String str7 = nextDate3.run((int) (byte) 0, 30, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1709() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1709");
        NextDate nextDate3 = new NextDate(2001, (int) (short) 10, (int) (short) -1);
        String str7 = nextDate3.run((int) (short) -1, 0, (int) (byte) 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1710() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1710");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) '4', (int) (byte) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1711() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1711");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 0, 1);
        String str7 = nextDate3.run((int) (short) 10, 7, (int) (short) 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1712() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1712");
        NextDate nextDate3 = new NextDate(100, (int) (byte) -1, 10);
        String str7 = nextDate3.run((int) (short) 1, (int) (short) 10, 31);
        String str11 = nextDate3.run((int) (byte) 100, (int) (byte) 0, (int) (byte) 10);
        String str15 = nextDate3.run((int) (short) 0, 30, 30);
        String str19 = nextDate3.run(7, (int) 'a', (int) (byte) 0);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test1713() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1713");
        NextDate nextDate3 = new NextDate(100, 7, 7);
    }

    @Test
    public void test1714() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1714");
        NextDate nextDate3 = new NextDate(2020, (int) (short) -1, 7);
        String str7 = nextDate3.run(100, 0, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1715() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1715");
        NextDate nextDate3 = new NextDate(30, (int) (short) -1, (-1));
    }

    @Test
    public void test1716() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1716");
        NextDate nextDate3 = new NextDate(1, 30, (int) (byte) 0);
        String str7 = nextDate3.run(2001, (int) '#', (int) '4');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1717() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1717");
        NextDate nextDate3 = new NextDate(12, (-1), (int) ' ');
        String str7 = nextDate3.run((int) (byte) 0, 0, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1718() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1718");
        NextDate nextDate3 = new NextDate((int) 'a', 7, 30);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1719() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1719");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) 'a', (int) (byte) -1);
        String str7 = nextDate3.run(0, 30, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1720() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1720");
        NextDate nextDate3 = new NextDate((int) (short) 0, 0, 2020);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1721() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1721");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) 'a', (int) '#');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1722() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1722");
        NextDate nextDate3 = new NextDate((int) (short) -1, 31, (int) (byte) 10);
        String str7 = nextDate3.run((int) (short) 1, 2001, (int) '#');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1723() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1723");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run((int) (byte) 10, (int) (short) 100, 100);
        String str27 = nextDate3.run((int) (short) 100, (int) (short) 0, (int) (byte) 0);
        String str31 = nextDate3.run(2020, 30, 7);
        String str35 = nextDate3.run((int) (short) 10, (int) (byte) -1, 100);
        String str39 = nextDate3.run((int) (byte) -1, (int) (byte) 1, (int) (byte) 100);
        String str43 = nextDate3.run((int) (byte) 0, (int) (byte) 10, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str43 + "' != '" + "invalid Input Date" + "'", str43, "invalid Input Date");
    }

    @Test
    public void test1724() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1724");
        NextDate nextDate3 = new NextDate((int) '4', (int) '4', (int) (byte) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1725() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1725");
        NextDate nextDate3 = new NextDate(1, 2020, 0);
    }

    @Test
    public void test1726() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1726");
        NextDate nextDate3 = new NextDate(1, (-1), (-1));
        String str7 = nextDate3.run((int) (short) -1, (int) (byte) 100, 2001);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 0, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1727() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1727");
        NextDate nextDate3 = new NextDate(100, 0, 31);
    }

    @Test
    public void test1728() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1728");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 1, 10);
        String str7 = nextDate3.run((int) (short) 0, 30, (int) (byte) 1);
        String str11 = nextDate3.run((int) (short) 1, 1, 7);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1729() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1729");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) (byte) 10, (int) '#', 31);
        String str11 = nextDate3.run(0, 2020, (int) (short) 10);
        String str15 = nextDate3.run(0, (int) (short) 100, (int) (byte) 100);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1730() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1730");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) ' ', (int) (short) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1731() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1731");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) -1, 31);
        String str7 = nextDate3.run(0, 0, 31);
        String str11 = nextDate3.run((int) (short) 1, (int) (byte) 1, (int) '4');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1732() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1732");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (byte) 100, 7);
    }

    @Test
    public void test1733() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1733");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) '4', (int) (short) 100);
    }

    @Test
    public void test1734() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1734");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) ' ', 7);
    }

    @Test
    public void test1735() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1735");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 1, (int) (short) 10);
        String str7 = nextDate3.run(0, (int) (short) 10, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1736() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1736");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 1, (int) (byte) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1737() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1737");
        NextDate nextDate3 = new NextDate((int) '#', (-1), 31);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1738() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1738");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) 100, (int) (byte) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1739() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1739");
        NextDate nextDate3 = new NextDate(31, (int) '4', (int) (short) -1);
        String str7 = nextDate3.run((int) (short) 1, (int) ' ', 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1740() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1740");
        NextDate nextDate3 = new NextDate(30, (int) 'a', 2001);
        String str7 = nextDate3.run((int) (byte) 10, 1, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1741() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1741");
        NextDate nextDate3 = new NextDate(30, 31, (int) (byte) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1742() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1742");
        NextDate nextDate3 = new NextDate(12, (int) 'a', (int) (byte) 100);
        String str7 = nextDate3.run(2020, (int) (byte) 1, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1743() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1743");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (byte) 0, (int) (short) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1744() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1744");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 0, 100);
    }

    @Test
    public void test1745() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1745");
        NextDate nextDate3 = new NextDate(10, 10, (int) ' ');
        String str7 = nextDate3.run((-1), 30, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1746() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1746");
        NextDate nextDate3 = new NextDate(7, (int) (byte) 0, 7);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1747() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1747");
        NextDate nextDate3 = new NextDate((int) (short) 10, 12, 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1748() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1748");
        NextDate nextDate3 = new NextDate(30, (int) (byte) -1, (int) (short) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1749() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1749");
        NextDate nextDate3 = new NextDate(31, 1, (int) ' ');
    }

    @Test
    public void test1750() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1750");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) -1, (int) (short) 0);
    }

    @Test
    public void test1751() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1751");
        NextDate nextDate3 = new NextDate(12, (int) (short) 10, (int) (byte) 10);
        String str7 = nextDate3.run((int) 'a', (int) (short) 100, 2020);
        String str11 = nextDate3.run((int) (short) 1, 0, (int) (short) 10);
        String str15 = nextDate3.run((int) (short) 10, 2001, 0);
        String str19 = nextDate3.run(31, (int) '#', 0);
        String str23 = nextDate3.run(7, (int) (byte) 0, (int) (short) 10);
        String str27 = nextDate3.run(31, 100, (int) (byte) 1);
        String str31 = nextDate3.run((int) (short) -1, (int) '#', (int) (short) -1);
        String str35 = nextDate3.run(30, (int) (byte) 1, (int) (short) 100);
        Class<?> wildcardClass36 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass36);
    }

    @Test
    public void test1752() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1752");
        NextDate nextDate3 = new NextDate((int) '4', (int) (byte) 10, 100);
    }

    @Test
    public void test1753() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1753");
        NextDate nextDate3 = new NextDate((int) 'a', 2001, 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1754() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1754");
        NextDate nextDate3 = new NextDate(30, (int) '4', (-1));
    }

    @Test
    public void test1755() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1755");
        NextDate nextDate3 = new NextDate(100, (int) (byte) -1, (int) ' ');
        String str7 = nextDate3.run((int) (short) 1, (int) (byte) 100, (int) '4');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1756() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1756");
        NextDate nextDate3 = new NextDate(7, 0, (int) (byte) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1757() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1757");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) '#', (int) (byte) 100);
        String str7 = nextDate3.run(12, 0, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1758() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1758");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) '4', 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1759() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1759");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, (int) (byte) 1, (int) (short) 100);
        String str11 = nextDate3.run((int) (short) -1, 10, 1);
        String str15 = nextDate3.run(12, (int) (byte) 100, (int) (byte) 1);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1760() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1760");
        NextDate nextDate3 = new NextDate(31, (int) '#', (int) 'a');
        String str7 = nextDate3.run(0, 0, (int) (short) 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1761() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1761");
        NextDate nextDate3 = new NextDate(2001, 10, (int) '4');
    }

    @Test
    public void test1762() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1762");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 12, 2001);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1763() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1763");
        NextDate nextDate3 = new NextDate(10, 0, 2001);
        String str7 = nextDate3.run(0, 1, 0);
        String str11 = nextDate3.run((int) '#', (int) (short) 0, (int) 'a');
        String str15 = nextDate3.run((int) (byte) 10, 10, 1);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1764() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1764");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 30, (int) (short) -1);
    }

    @Test
    public void test1765() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1765");
        NextDate nextDate3 = new NextDate(7, (int) (byte) 10, 31);
        String str7 = nextDate3.run((int) 'a', (-1), (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1766() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1766");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 10, (int) (byte) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1767() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1767");
        NextDate nextDate3 = new NextDate(10, 2020, (int) (short) -1);
        String str7 = nextDate3.run(0, (int) 'a', (-1));
        String str11 = nextDate3.run(2020, (int) (short) 100, 100);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1768() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1768");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 100, 2020);
        String str7 = nextDate3.run((-1), (int) (byte) 0, (int) (short) -1);
        String str11 = nextDate3.run(0, (int) (byte) 10, 2020);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1769() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1769");
        NextDate nextDate3 = new NextDate(0, (-1), (-1));
    }

    @Test
    public void test1770() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1770");
        NextDate nextDate3 = new NextDate((int) (short) 100, 0, 1);
        String str7 = nextDate3.run(31, (int) '#', (int) (short) 1);
        String str11 = nextDate3.run(30, (-1), (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1771() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1771");
        NextDate nextDate3 = new NextDate(31, (int) (byte) 0, (int) 'a');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1772() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1772");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) '4', (int) (byte) 1);
    }

    @Test
    public void test1773() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1773");
        NextDate nextDate3 = new NextDate((int) (short) 100, 1, (int) '4');
        String str7 = nextDate3.run((int) (byte) 10, 2020, 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1774() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1774");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) -1, 2020);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1775() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1775");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 10, (int) '4');
    }

    @Test
    public void test1776() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1776");
        NextDate nextDate3 = new NextDate(12, (int) (byte) -1, (int) (short) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1777() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1777");
        NextDate nextDate3 = new NextDate((int) '4', 1, 0);
        String str7 = nextDate3.run((int) '4', 12, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1778() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1778");
        NextDate nextDate3 = new NextDate(10, 0, (int) (short) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1779() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1779");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 100, (int) (short) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1780() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1780");
        NextDate nextDate3 = new NextDate((int) 'a', 2020, (int) (short) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1781() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1781");
        NextDate nextDate3 = new NextDate(12, 2001, 2020);
        String str7 = nextDate3.run((int) (byte) 10, 0, (int) (short) 100);
        String str11 = nextDate3.run((-1), (int) (byte) 10, (int) (byte) -1);
        String str15 = nextDate3.run(10, (int) (byte) 10, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test1782() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1782");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 12, 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1783() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1783");
        NextDate nextDate3 = new NextDate(100, (int) (byte) -1, 10);
        String str7 = nextDate3.run((int) (byte) 1, (int) (short) 1, 30);
        String str11 = nextDate3.run(1, 31, (int) (short) 100);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1784() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1784");
        NextDate nextDate3 = new NextDate(7, (-1), 31);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1785() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1785");
        NextDate nextDate3 = new NextDate((-1), (int) 'a', (int) (byte) 10);
        String str7 = nextDate3.run(12, 12, 12);
        String str11 = nextDate3.run((int) (short) -1, (int) ' ', 0);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1786() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1786");
        NextDate nextDate3 = new NextDate(0, (int) (short) 100, 7);
    }

    @Test
    public void test1787() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1787");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 0, 1);
        String str7 = nextDate3.run((int) (byte) 0, (int) ' ', 7);
        String str11 = nextDate3.run(31, 7, 0);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1788() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1788");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 100, (int) '4');
    }

    @Test
    public void test1789() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1789");
        NextDate nextDate3 = new NextDate(31, 2020, 31);
        String str7 = nextDate3.run((int) (short) -1, (int) 'a', (-1));
        String str11 = nextDate3.run((int) 'a', (int) (short) -1, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1790() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1790");
        NextDate nextDate3 = new NextDate((int) (short) 0, 0, (int) ' ');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1791() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1791");
        NextDate nextDate3 = new NextDate(31, (int) (short) 1, 30);
    }

    @Test
    public void test1792() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1792");
        NextDate nextDate3 = new NextDate((int) (short) -1, 100, 2020);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1793() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1793");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) -1, (int) (byte) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1794() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1794");
        NextDate nextDate3 = new NextDate(2020, (int) '#', (int) '4');
        String str7 = nextDate3.run(1, (int) (short) 1, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1795() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1795");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 1, (int) (byte) 100);
        String str7 = nextDate3.run(10, 2001, (int) (short) 1);
        String str11 = nextDate3.run(7, 12, (int) ' ');
        String str15 = nextDate3.run((-1), 0, (int) (byte) 0);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1796() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1796");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) 100, 2020);
        String str7 = nextDate3.run((int) (byte) 10, 31, (int) (short) 100);
        String str11 = nextDate3.run(30, 10, (int) (byte) 0);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1797() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1797");
        NextDate nextDate3 = new NextDate((int) '4', 1, 12);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1798() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1798");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) -1, 2020);
        String str7 = nextDate3.run((int) (short) 1, 10, (-1));
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1799() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1799");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 10, (int) (byte) 1);
        String str7 = nextDate3.run(0, (int) (byte) 1, 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1800() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1800");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 0, (int) (short) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1801() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1801");
        NextDate nextDate3 = new NextDate(1, 30, (int) (byte) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1802() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1802");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) '#', (int) '#');
        String str7 = nextDate3.run(2001, (int) '4', (int) (byte) -1);
        String str11 = nextDate3.run(2020, 31, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1803() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1803");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 1, 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1804() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1804");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) 1, 12);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1805() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1805");
        NextDate nextDate3 = new NextDate(0, (int) '4', (int) (byte) 1);
        String str7 = nextDate3.run((int) (short) 100, (int) (byte) 10, (int) '#');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1806() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1806");
        NextDate nextDate3 = new NextDate(0, (int) (short) -1, (int) ' ');
    }

    @Test
    public void test1807() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1807");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 31, 0);
        String str7 = nextDate3.run((int) '4', (int) '4', 7);
        String str11 = nextDate3.run((int) (short) 10, 2020, 30);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1808() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1808");
        NextDate nextDate3 = new NextDate(0, 0, 100);
        String str7 = nextDate3.run((int) ' ', (int) (short) 1, (int) (byte) -1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1809() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1809");
        NextDate nextDate3 = new NextDate(2001, (int) (short) -1, 7);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1810() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1810");
        NextDate nextDate3 = new NextDate(10, (int) '#', 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1811() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1811");
        NextDate nextDate3 = new NextDate(1, (int) '#', (-1));
        String str7 = nextDate3.run((int) (byte) 1, 100, (int) (byte) 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1812() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1812");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 31, (int) (byte) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1813() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1813");
        NextDate nextDate3 = new NextDate((int) 'a', 7, 30);
        String str7 = nextDate3.run((int) ' ', 2020, 7);
        String str11 = nextDate3.run(31, 7, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1814() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1814");
        NextDate nextDate3 = new NextDate(1, (int) (short) 100, 12);
    }

    @Test
    public void test1815() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1815");
        NextDate nextDate3 = new NextDate((int) '4', 30, (int) (short) 100);
        String str7 = nextDate3.run((int) (byte) 100, 2001, (int) '4');
        String str11 = nextDate3.run(10, (int) (byte) 100, (int) 'a');
        String str15 = nextDate3.run(2020, (-1), 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test1816() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1816");
        NextDate nextDate3 = new NextDate(0, 31, (int) '4');
        String str7 = nextDate3.run(2020, (int) '4', (int) (byte) 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1817() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1817");
        NextDate nextDate3 = new NextDate(100, (int) (short) 0, 0);
    }

    @Test
    public void test1818() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1818");
        NextDate nextDate3 = new NextDate(31, (int) (byte) 0, (int) '#');
        String str7 = nextDate3.run((int) (short) 1, (int) (short) 10, (-1));
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1819() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1819");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) ' ', (-1));
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1820() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1820");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) 'a', 30);
        String str7 = nextDate3.run((int) (short) 100, 7, 0);
        String str11 = nextDate3.run((int) (short) 100, 0, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1821() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1821");
        NextDate nextDate3 = new NextDate(2020, (-1), 31);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1822() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1822");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (short) -1, 12);
        String str7 = nextDate3.run((int) (byte) 100, (int) 'a', (int) ' ');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1823() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1823");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 0, 31);
    }

    @Test
    public void test1824() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1824");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 10, (int) (byte) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1825() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1825");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 1, 0);
        String str7 = nextDate3.run((-1), 100, (int) (byte) -1);
        String str11 = nextDate3.run((int) (byte) 1, (int) (byte) 10, (int) (byte) -1);
        String str15 = nextDate3.run((int) '4', 1, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test1826() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1826");
        NextDate nextDate3 = new NextDate((int) '#', (-1), 31);
        String str7 = nextDate3.run(100, (int) (short) 1, 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1827() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1827");
        NextDate nextDate3 = new NextDate(12, (int) (short) 10, (int) (byte) 10);
        String str7 = nextDate3.run((int) 'a', (int) (short) 100, 2020);
        String str11 = nextDate3.run((int) (short) 1, 0, (int) (short) 10);
        String str15 = nextDate3.run((int) (short) 10, 2001, 0);
        String str19 = nextDate3.run((int) (byte) 10, (-1), (int) (byte) 0);
        String str23 = nextDate3.run((int) (short) 100, (-1), 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test1828() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1828");
        NextDate nextDate3 = new NextDate((int) '4', 31, 2001);
        String str7 = nextDate3.run((int) 'a', (int) (short) 10, (int) '4');
        String str11 = nextDate3.run(10, 0, 0);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1829() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1829");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 10, (int) (short) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1830() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1830");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 0, (int) (short) 1);
        String str7 = nextDate3.run(2001, (int) ' ', (-1));
        String str11 = nextDate3.run(0, (int) 'a', 10);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1831() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1831");
        NextDate nextDate3 = new NextDate((int) '#', 7, 10);
    }

    @Test
    public void test1832() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1832");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) ' ', 30);
        String str7 = nextDate3.run(30, 0, 7);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1833() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1833");
        NextDate nextDate3 = new NextDate(0, 1, (int) (byte) 1);
        String str7 = nextDate3.run((int) '#', (int) ' ', (int) (short) 0);
        String str11 = nextDate3.run((-1), 0, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1834() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1834");
        NextDate nextDate3 = new NextDate(7, (int) (short) -1, (int) (byte) 10);
        String str7 = nextDate3.run((int) (short) -1, 0, (int) 'a');
        String str11 = nextDate3.run(31, (int) ' ', (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1835() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1835");
        NextDate nextDate3 = new NextDate(10, 2020, (int) '4');
    }

    @Test
    public void test1836() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1836");
        NextDate nextDate3 = new NextDate((-1), (int) '#', (int) (byte) 100);
    }

    @Test
    public void test1837() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1837");
        NextDate nextDate3 = new NextDate(2020, 2001, 0);
        String str7 = nextDate3.run(2001, (-1), (int) (byte) -1);
        String str11 = nextDate3.run((-1), (int) (short) 100, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1838() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1838");
        NextDate nextDate3 = new NextDate(30, (-1), 31);
        String str7 = nextDate3.run(7, (int) (short) 100, 1);
        String str11 = nextDate3.run((int) (byte) 1, 100, 7);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1839() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1839");
        NextDate nextDate3 = new NextDate(7, (int) (short) 10, (int) (byte) -1);
    }

    @Test
    public void test1840() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1840");
        NextDate nextDate3 = new NextDate((-1), 7, 7);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1841() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1841");
        NextDate nextDate3 = new NextDate((int) (short) 0, 2020, (int) (byte) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1842() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1842");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) 10, 31);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1843() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1843");
        NextDate nextDate3 = new NextDate(30, (int) (byte) 100, 1);
        String str7 = nextDate3.run((int) (byte) 10, 0, 30);
        String str11 = nextDate3.run((int) (byte) -1, 30, (-1));
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1844() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1844");
        NextDate nextDate3 = new NextDate(0, (-1), (int) (byte) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1845() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1845");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 0, 30);
    }

    @Test
    public void test1846() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1846");
        NextDate nextDate3 = new NextDate(12, (int) (short) 10, (int) (byte) 10);
        String str7 = nextDate3.run((int) 'a', (int) (short) 100, 2020);
        String str11 = nextDate3.run((int) (short) 1, 0, (int) (short) 10);
        String str15 = nextDate3.run((int) (short) 10, 2001, 0);
        String str19 = nextDate3.run(31, (int) '#', 0);
        String str23 = nextDate3.run(7, (int) (byte) 0, (int) (short) 10);
        Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test1847() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1847");
        NextDate nextDate3 = new NextDate((int) 'a', (int) ' ', (int) (byte) 1);
        String str7 = nextDate3.run((int) (short) -1, 100, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1848() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1848");
        NextDate nextDate3 = new NextDate(2001, 31, (int) (short) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1849() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1849");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) 1, 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1850() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1850");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 0, (int) (byte) 0);
        String str7 = nextDate3.run((int) (short) 10, 10, (int) '#');
        String str11 = nextDate3.run(31, 1, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1851() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1851");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 1, (int) (byte) -1);
    }

    @Test
    public void test1852() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1852");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run(31, 31, 12);
        String str23 = nextDate3.run((int) (short) 1, (int) 'a', (int) (short) 10);
        Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test1853() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1853");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) -1, (int) (short) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1854() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1854");
        NextDate nextDate3 = new NextDate(0, 0, (int) (short) -1);
        String str7 = nextDate3.run(2001, (int) (short) 1, (int) ' ');
        String str11 = nextDate3.run(0, (int) '#', (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1855() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1855");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 0, 31);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1856() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1856");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) 10, 2020);
    }

    @Test
    public void test1857() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1857");
        NextDate nextDate3 = new NextDate(12, (int) (short) 10, (int) (byte) 10);
        String str7 = nextDate3.run((int) 'a', (int) (short) 100, 2020);
        String str11 = nextDate3.run(100, 10, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1858() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1858");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 0, (int) (short) 1);
    }

    @Test
    public void test1859() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1859");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 100, 12);
        String str7 = nextDate3.run((int) (short) 10, 100, (int) '4');
        String str11 = nextDate3.run((int) 'a', (int) (short) 100, (int) '#');
        String str15 = nextDate3.run((int) ' ', 31, 10);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1860() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1860");
        NextDate nextDate3 = new NextDate(10, 10, 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1861() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1861");
        NextDate nextDate3 = new NextDate(0, (int) ' ', 100);
    }

    @Test
    public void test1862() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1862");
        NextDate nextDate3 = new NextDate((int) 'a', 0, 100);
    }

    @Test
    public void test1863() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1863");
        NextDate nextDate3 = new NextDate(10, 2001, 31);
        String str7 = nextDate3.run(12, (int) (byte) 10, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1864() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1864");
        NextDate nextDate3 = new NextDate(2020, (int) ' ', 30);
        String str7 = nextDate3.run(12, 2001, (int) (byte) 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1865() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1865");
        NextDate nextDate3 = new NextDate(30, (int) (short) 10, 31);
        String str7 = nextDate3.run((int) 'a', 2020, 30);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1866() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1866");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) '4', 2020);
        String str7 = nextDate3.run(0, 31, 2020);
        String str11 = nextDate3.run(2020, 2020, (int) (short) 100);
        String str15 = nextDate3.run((int) (short) 0, 0, 0);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1867() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1867");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 30, 30);
        String str7 = nextDate3.run(0, (int) (short) -1, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1868() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1868");
        NextDate nextDate3 = new NextDate(12, 2001, (-1));
        String str7 = nextDate3.run((int) 'a', 100, (int) '#');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1869() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1869");
        NextDate nextDate3 = new NextDate(30, (-1), (int) '#');
        String str7 = nextDate3.run((int) (byte) 100, (int) '#', (int) 'a');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1870() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1870");
        NextDate nextDate3 = new NextDate(10, 0, (int) (byte) 100);
    }

    @Test
    public void test1871() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1871");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) 'a', 12, (int) (byte) 10);
        String str11 = nextDate3.run(31, (int) (byte) 1, (int) (byte) 10);
        String str15 = nextDate3.run((int) (short) 10, (int) (byte) 0, (int) 'a');
        String str19 = nextDate3.run((int) '4', (int) (byte) 0, 31);
        String str23 = nextDate3.run(10, 2001, (int) (byte) 1);
        Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test1872() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1872");
        NextDate nextDate3 = new NextDate(30, (int) '4', 12);
        String str7 = nextDate3.run(31, (int) (byte) 100, (int) '4');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1873() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1873");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) '4', (int) (short) 1);
        String str7 = nextDate3.run((int) (short) -1, (int) '#', (int) (short) 1);
        String str11 = nextDate3.run((int) (byte) -1, 2001, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1874() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1874");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 0, (int) (short) 1);
        String str7 = nextDate3.run((int) (short) -1, 7, 7);
        String str11 = nextDate3.run((int) (short) 1, (int) ' ', (-1));
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1875() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1875");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 0, 7);
        String str7 = nextDate3.run((int) (short) 0, (-1), (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1876() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1876");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 1, (int) (short) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1877() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1877");
        NextDate nextDate3 = new NextDate(1, (int) (short) 10, 0);
        String str7 = nextDate3.run((int) (byte) 10, 100, 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1878() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1878");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (byte) 1, 10);
        String str7 = nextDate3.run((int) (byte) -1, (int) 'a', (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1879() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1879");
        NextDate nextDate3 = new NextDate(0, 12, 1);
    }

    @Test
    public void test1880() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1880");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 0, (int) (short) -1);
        String str7 = nextDate3.run(7, (int) 'a', 12);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1881() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1881");
        NextDate nextDate3 = new NextDate((int) (short) 1, 0, (int) (short) 0);
        String str7 = nextDate3.run((int) (short) 100, (int) (byte) 1, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1882() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1882");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 2001, 31);
        String str7 = nextDate3.run((-1), (int) '#', (int) '4');
        String str11 = nextDate3.run((int) (byte) 0, (-1), 2001);
        String str15 = nextDate3.run((int) (short) 1, 1, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test1883() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1883");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 0, 100);
    }

    @Test
    public void test1884() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1884");
        NextDate nextDate3 = new NextDate(1, (int) ' ', (int) (byte) 100);
        String str7 = nextDate3.run(100, (int) (short) 0, 2020);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1885() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1885");
        NextDate nextDate3 = new NextDate(10, (int) '4', 2020);
        String str7 = nextDate3.run((int) (short) 10, (int) (byte) -1, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1886() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1886");
        NextDate nextDate3 = new NextDate(100, (int) (byte) -1, (int) (byte) 1);
    }

    @Test
    public void test1887() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1887");
        NextDate nextDate3 = new NextDate((int) (short) 10, 10, (int) (byte) 100);
        String str7 = nextDate3.run(0, 2001, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1888() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1888");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 1, (int) (short) 1);
    }

    @Test
    public void test1889() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1889");
        NextDate nextDate3 = new NextDate(2001, (int) (short) 10, (int) (short) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1890() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1890");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 10, (int) (byte) 0);
        String str7 = nextDate3.run(100, (-1), (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1891() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1891");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (byte) 10, (int) (byte) 0);
        String str7 = nextDate3.run(31, (int) (short) 1, (int) 'a');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1892() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1892");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) 100, (int) (byte) 10);
    }

    @Test
    public void test1893() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1893");
        NextDate nextDate3 = new NextDate(12, (int) (short) 10, (int) (byte) 10);
        String str7 = nextDate3.run((int) 'a', (int) (short) 100, 2020);
        String str11 = nextDate3.run((int) (short) 1, 0, (int) (short) 10);
        String str15 = nextDate3.run((int) (short) 10, 2001, 0);
        String str19 = nextDate3.run(10, (int) ' ', (int) (short) 0);
        String str23 = nextDate3.run((int) (byte) 100, 10, 7);
        String str27 = nextDate3.run(10, (int) (byte) 100, 100);
        Class<?> wildcardClass28 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test1894() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1894");
        NextDate nextDate3 = new NextDate(12, 0, (int) '#');
        String str7 = nextDate3.run(12, (int) (byte) -1, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1895() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1895");
        NextDate nextDate3 = new NextDate(10, (int) (short) 1, 2001);
        String str7 = nextDate3.run(0, (int) (short) 1, 30);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1896() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1896");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        String str15 = nextDate3.run((int) (byte) 1, (int) (short) 0, (int) (short) 100);
        String str19 = nextDate3.run((int) (short) 10, 0, 10);
        String str23 = nextDate3.run((int) (byte) -1, 10, 30);
        String str27 = nextDate3.run((int) (byte) 100, (int) (short) 0, (-1));
        String str31 = nextDate3.run((int) (byte) 100, (int) ' ', (int) (byte) 1);
        Class<?> wildcardClass32 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass32);
    }

    @Test
    public void test1897() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1897");
        NextDate nextDate3 = new NextDate((int) (short) 100, 12, (int) (short) 10);
    }

    @Test
    public void test1898() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1898");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 100, (int) '4');
    }

    @Test
    public void test1899() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1899");
        NextDate nextDate3 = new NextDate(0, 0, 100);
        String str7 = nextDate3.run((int) (short) 10, 100, (int) (short) 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1900() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1900");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        String str15 = nextDate3.run((int) (byte) 1, (int) (short) 0, (int) (short) 100);
        String str19 = nextDate3.run((int) (short) 10, 0, 10);
        String str23 = nextDate3.run((int) (byte) -1, 10, 30);
        String str27 = nextDate3.run((int) (byte) 100, (int) (short) 0, (-1));
        String str31 = nextDate3.run((int) (byte) -1, 10, (int) (short) 10);
        String str35 = nextDate3.run(100, 10, 0);
        Class<?> wildcardClass36 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass36);
    }

    @Test
    public void test1901() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1901");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 2020, 100);
        String str7 = nextDate3.run(2001, 10, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1902() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1902");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 100, 12);
    }

    @Test
    public void test1903() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1903");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 0, (int) (short) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1904() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1904");
        NextDate nextDate3 = new NextDate(2020, (int) (short) 0, 31);
        String str7 = nextDate3.run(31, 12, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1905() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1905");
        NextDate nextDate3 = new NextDate(31, (int) 'a', (-1));
        String str7 = nextDate3.run((int) (byte) 1, 100, (int) (byte) -1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1906() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1906");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) 'a', 12);
        String str7 = nextDate3.run(0, 10, 2020);
        String str11 = nextDate3.run((int) (short) 1, 0, 10);
        String str15 = nextDate3.run(0, 100, 12);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1907() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1907");
        NextDate nextDate3 = new NextDate((int) '#', 100, (int) ' ');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1908() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1908");
        NextDate nextDate3 = new NextDate(0, 2001, (int) (short) 100);
    }

    @Test
    public void test1909() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1909");
        NextDate nextDate3 = new NextDate(2020, 30, 1);
        String str7 = nextDate3.run(2020, (int) (byte) -1, (int) (short) 1);
        String str11 = nextDate3.run((int) '4', (int) (short) 100, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1910() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1910");
        NextDate nextDate3 = new NextDate(2001, 2001, 7);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1911() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1911");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 0, (int) (byte) 10);
        String str7 = nextDate3.run(10, (int) '#', 30);
        String str11 = nextDate3.run((int) (short) 1, (int) (short) 0, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1912() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1912");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 0, (int) (byte) 1);
        String str7 = nextDate3.run((int) (short) 100, (int) (byte) 1, (int) '#');
        String str11 = nextDate3.run((int) (short) 100, (int) (short) 0, 10);
        String str15 = nextDate3.run((int) (short) 1, 30, 1);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1913() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1913");
        NextDate nextDate3 = new NextDate(30, (int) (byte) 10, (int) (short) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1914() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1914");
        NextDate nextDate3 = new NextDate((int) (short) 100, 31, (int) (short) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1915() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1915");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 10, (int) (short) 1);
    }

    @Test
    public void test1916() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1916");
        NextDate nextDate3 = new NextDate(0, (int) '#', (int) (byte) -1);
    }

    @Test
    public void test1917() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1917");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) 1, (int) (short) 1);
    }

    @Test
    public void test1918() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1918");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 0, 12);
        String str7 = nextDate3.run((int) (byte) 1, (int) (byte) -1, (int) (short) 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1919() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1919");
        NextDate nextDate3 = new NextDate((int) ' ', (int) ' ', (int) (short) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1920() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1920");
        NextDate nextDate3 = new NextDate((int) (short) 10, 30, 30);
        String str7 = nextDate3.run((-1), (int) (byte) 1, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1921() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1921");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) 10, 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1922() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1922");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) 10, (int) (byte) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1923() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1923");
        NextDate nextDate3 = new NextDate((int) '#', 0, (int) ' ');
    }

    @Test
    public void test1924() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1924");
        NextDate nextDate3 = new NextDate(12, (int) (byte) 1, (int) (short) 10);
        String str7 = nextDate3.run((-1), (int) (short) 0, (int) (byte) 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1925() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1925");
        NextDate nextDate3 = new NextDate(7, 1, (int) (byte) 100);
    }

    @Test
    public void test1926() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1926");
        NextDate nextDate3 = new NextDate(100, (int) ' ', (int) ' ');
    }

    @Test
    public void test1927() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1927");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) 'a', 12);
        String str7 = nextDate3.run((int) (short) 1, 31, (int) ' ');
        String str11 = nextDate3.run((int) (byte) 0, 100, (int) (byte) -1);
        String str15 = nextDate3.run((int) (short) -1, (int) (byte) -1, (int) (short) 0);
        String str19 = nextDate3.run((int) (short) 1, 1, 12);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test1928() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1928");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 1, 0);
        String str7 = nextDate3.run((-1), 100, (int) (byte) -1);
        String str11 = nextDate3.run(0, 100, 1);
        String str15 = nextDate3.run(2001, 12, 7);
        String str19 = nextDate3.run((int) (short) 10, 12, 12);
        String str23 = nextDate3.run(0, 0, 7);
        String str27 = nextDate3.run((int) (short) -1, (int) (byte) 10, 100);
        Class<?> wildcardClass28 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test1929() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1929");
        NextDate nextDate3 = new NextDate(2020, 1, (int) '4');
        String str7 = nextDate3.run(2001, (int) 'a', 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1930() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1930");
        NextDate nextDate3 = new NextDate(10, 30, (int) (byte) 10);
    }

    @Test
    public void test1931() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1931");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 100, 0);
        String str7 = nextDate3.run((int) (short) 1, 30, 2001);
        String str11 = nextDate3.run(1, (int) (byte) 10, 30);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "1/31/2001" + "'", str7, "1/31/2001");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1932() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1932");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) 'a', (int) (byte) 1);
        String str7 = nextDate3.run((int) (byte) 1, 0, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1933() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1933");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) -1, (int) 'a');
        String str7 = nextDate3.run((int) (short) 100, 12, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1934() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1934");
        NextDate nextDate3 = new NextDate(1, (int) (short) 10, 0);
        String str7 = nextDate3.run((int) (byte) -1, (int) ' ', (int) (short) 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1935() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1935");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) 0, (int) (short) 0);
        String str7 = nextDate3.run((int) (byte) 100, 0, (int) (short) 100);
        String str11 = nextDate3.run((int) (byte) 100, 31, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1936() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1936");
        NextDate nextDate3 = new NextDate(2001, (int) (short) 100, (int) (byte) 0);
        String str7 = nextDate3.run(1, (int) (byte) 100, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1937() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1937");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 1, 0);
        String str7 = nextDate3.run((-1), (int) (byte) 10, (int) '#');
        String str11 = nextDate3.run((int) (short) 10, (int) '#', (int) '#');
        String str15 = nextDate3.run(0, (int) (short) 10, 100);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1938() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1938");
        NextDate nextDate3 = new NextDate(0, (-1), 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1939() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1939");
        NextDate nextDate3 = new NextDate(0, (int) (short) 100, (int) (byte) 0);
    }

    @Test
    public void test1940() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1940");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 0, (-1));
        String str7 = nextDate3.run(7, (int) (byte) 1, 31);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1941() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1941");
        NextDate nextDate3 = new NextDate(2020, (int) ' ', (int) (byte) 10);
    }

    @Test
    public void test1942() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1942");
        NextDate nextDate3 = new NextDate((-1), (int) 'a', (int) (byte) 10);
        String str7 = nextDate3.run(12, 12, 12);
        String str11 = nextDate3.run((int) (short) -1, (int) ' ', 0);
        String str15 = nextDate3.run(2001, 1, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test1943() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1943");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        String str15 = nextDate3.run(7, (int) (short) 1, 30);
        String str19 = nextDate3.run((int) 'a', (int) '#', 1);
        String str23 = nextDate3.run((int) (byte) 0, (int) (short) 0, 1);
        String str27 = nextDate3.run((int) (short) 100, (int) ' ', (-1));
        String str31 = nextDate3.run(2020, (int) (short) 100, 30);
        String str35 = nextDate3.run((int) (byte) 100, (int) (byte) -1, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
    }

    @Test
    public void test1944() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1944");
        NextDate nextDate3 = new NextDate(0, 100, 12);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1945() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1945");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) 100, (int) (byte) 0);
        String str7 = nextDate3.run(0, (int) (short) -1, (int) (byte) 10);
        String str11 = nextDate3.run(12, (int) (byte) -1, (int) (short) -1);
        String str15 = nextDate3.run(12, (int) (byte) 1, 12);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1946() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1946");
        NextDate nextDate3 = new NextDate((-1), (int) (short) -1, 31);
        String str7 = nextDate3.run((int) (byte) 100, 30, (int) '4');
        String str11 = nextDate3.run(30, 10, (int) (byte) 1);
        String str15 = nextDate3.run((int) (short) 100, (int) (byte) 100, (int) (short) 100);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1947() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1947");
        NextDate nextDate3 = new NextDate((int) (short) 1, 0, (int) (byte) 100);
    }

    @Test
    public void test1948() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1948");
        NextDate nextDate3 = new NextDate((int) ' ', (int) ' ', (int) (byte) -1);
        String str7 = nextDate3.run((int) ' ', 31, 7);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1949() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1949");
        NextDate nextDate3 = new NextDate((int) '4', 0, 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1950() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1950");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 10, 1);
        String str7 = nextDate3.run((int) 'a', 2020, (int) (short) -1);
        String str11 = nextDate3.run(31, (int) (byte) 1, (int) (byte) 0);
        String str15 = nextDate3.run((int) (short) 1, 0, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test1951() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1951");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 1, (int) (byte) -1);
    }

    @Test
    public void test1952() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1952");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) '#', (int) (short) 1);
        String str7 = nextDate3.run(10, (int) '#', 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1953() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1953");
        NextDate nextDate3 = new NextDate(7, (int) (byte) 0, 2020);
        String str7 = nextDate3.run((int) (short) 0, 31, 2020);
        String str11 = nextDate3.run((int) (byte) 0, (int) (byte) 100, (int) (short) 1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1954() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1954");
        NextDate nextDate3 = new NextDate((int) '#', 10, (int) (byte) 0);
        String str7 = nextDate3.run((int) 'a', (int) (short) 1, (int) (short) 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1955() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1955");
        NextDate nextDate3 = new NextDate(12, (int) (short) 10, (int) (byte) 10);
        String str7 = nextDate3.run((int) 'a', (int) (short) 100, 2020);
        String str11 = nextDate3.run((int) (short) 1, 0, (int) (short) 10);
        String str15 = nextDate3.run((int) ' ', (int) (byte) 1, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test1956() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1956");
        NextDate nextDate3 = new NextDate(12, (int) (short) 100, 0);
    }

    @Test
    public void test1957() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1957");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 0, 1);
        String str7 = nextDate3.run(2020, (int) (short) -1, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1958() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1958");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 10, 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1959() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1959");
        NextDate nextDate3 = new NextDate(10, 2020, (int) (short) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1960() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1960");
        NextDate nextDate3 = new NextDate((int) (short) 100, 31, (int) ' ');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1961() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1961");
        NextDate nextDate3 = new NextDate((int) (short) 1, 31, (int) (short) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1962() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1962");
        NextDate nextDate3 = new NextDate((int) '#', (int) ' ', 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1963() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1963");
        NextDate nextDate3 = new NextDate(7, 2020, (int) (byte) 10);
        String str7 = nextDate3.run((int) (byte) 100, (int) (byte) 10, (int) '4');
        String str11 = nextDate3.run((int) ' ', 12, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1964() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1964");
        NextDate nextDate3 = new NextDate(0, 31, (int) 'a');
        String str7 = nextDate3.run(1, 100, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1965() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1965");
        NextDate nextDate3 = new NextDate(30, (int) (short) 10, 31);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1966() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1966");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) '#', (int) (short) -1);
    }

    @Test
    public void test1967() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1967");
        NextDate nextDate3 = new NextDate(0, (-1), 7);
    }

    @Test
    public void test1968() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1968");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) ' ', 2020, (int) (short) 0);
        String str11 = nextDate3.run((int) (byte) -1, (int) (byte) -1, (int) (short) 0);
        String str15 = nextDate3.run((-1), (int) (byte) 10, 31);
        String str19 = nextDate3.run((int) (short) 1, (int) (byte) 0, 7);
        String str23 = nextDate3.run(2020, 100, 1);
        Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test1969() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1969");
        NextDate nextDate3 = new NextDate(0, 1, (int) (byte) 1);
        String str7 = nextDate3.run((int) (byte) 100, (int) (byte) 100, (int) '4');
        String str11 = nextDate3.run(1, (int) (byte) 10, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1970() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1970");
        NextDate nextDate3 = new NextDate((int) (short) 100, (-1), (int) 'a');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1971() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1971");
        NextDate nextDate3 = new NextDate(7, (int) (byte) 1, (int) '4');
        String str7 = nextDate3.run((int) (short) 10, (int) (short) 100, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1972() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1972");
        NextDate nextDate3 = new NextDate(12, 0, 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1973() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1973");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 10, (int) (byte) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1974() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1974");
        NextDate nextDate3 = new NextDate((int) 'a', (int) '4', (int) (short) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1975() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1975");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 1, (int) (byte) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1976() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1976");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 100, 12);
        String str7 = nextDate3.run((int) (short) 10, 100, (int) '4');
        String str11 = nextDate3.run((int) 'a', (int) (short) 100, (int) '#');
        String str15 = nextDate3.run((int) ' ', 31, 10);
        String str19 = nextDate3.run(0, (int) ' ', (-1));
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test1977() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1977");
        NextDate nextDate3 = new NextDate((-1), 12, (int) '#');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1978() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1978");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) 0, (int) (byte) 1);
    }

    @Test
    public void test1979() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1979");
        NextDate nextDate3 = new NextDate(31, (int) (short) 0, 0);
        String str7 = nextDate3.run((int) (byte) 100, 0, 31);
        String str11 = nextDate3.run((int) (byte) 1, 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1980() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1980");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 0, 2020);
        String str7 = nextDate3.run(0, (int) (byte) 1, (int) '4');
        String str11 = nextDate3.run((int) (byte) 1, (int) (byte) 1, 0);
        String str15 = nextDate3.run((int) '4', (int) (byte) 10, 0);
        String str19 = nextDate3.run((int) '#', 7, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test1981() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1981");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run((int) (byte) 10, (int) (short) 100, 100);
        String str27 = nextDate3.run((int) 'a', 0, (int) '4');
        String str31 = nextDate3.run((int) (short) 1, 0, (int) (byte) -1);
        String str35 = nextDate3.run((int) (short) 100, 10, 100);
        Class<?> wildcardClass36 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass36);
    }

    @Test
    public void test1982() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1982");
        NextDate nextDate3 = new NextDate((int) (short) 10, 10, 2020);
    }

    @Test
    public void test1983() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1983");
        NextDate nextDate3 = new NextDate(12, (int) (short) 100, (int) ' ');
        String str7 = nextDate3.run((int) ' ', (int) (byte) 10, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1984() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1984");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 0, 2001);
        String str7 = nextDate3.run((int) '#', (int) '4', (int) ' ');
        String str11 = nextDate3.run(30, (int) '#', (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1985() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1985");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 0, 30);
    }

    @Test
    public void test1986() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1986");
        NextDate nextDate3 = new NextDate((int) '#', 7, 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1987() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1987");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 10, 0);
        String str7 = nextDate3.run((int) (short) -1, (int) (short) -1, 30);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1988() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1988");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (-1), 2001);
        String str7 = nextDate3.run(10, 10, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "10/11/2020" + "'", str7, "10/11/2020");
    }

    @Test
    public void test1989() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1989");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (short) 10, (int) (short) 100);
    }

    @Test
    public void test1990() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1990");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 31, (int) '#');
        String str7 = nextDate3.run((int) (short) 100, 31, 10);
        String str11 = nextDate3.run((-1), (int) ' ', (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1991() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1991");
        NextDate nextDate3 = new NextDate((int) '#', (int) 'a', 2001);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1992() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1992");
        NextDate nextDate3 = new NextDate(30, (int) (short) 1, (int) ' ');
        String str7 = nextDate3.run(12, 0, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1993() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1993");
        NextDate nextDate3 = new NextDate(12, (int) '4', 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1994() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1994");
        NextDate nextDate3 = new NextDate(12, (int) (byte) 10, (int) '#');
        String str7 = nextDate3.run((int) (short) 10, 12, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test1995() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1995");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 100, 10);
        String str7 = nextDate3.run((int) (short) 100, 0, (int) 'a');
        String str11 = nextDate3.run(12, (int) (byte) -1, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test1996() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1996");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run(7, (int) (short) 10, (int) (short) 1);
        String str27 = nextDate3.run((int) 'a', (int) (byte) 1, 0);
        String str31 = nextDate3.run((int) (byte) 0, (int) ' ', (int) (short) 10);
        String str35 = nextDate3.run((int) (short) -1, (int) '4', 7);
        String str39 = nextDate3.run(0, (int) (short) 10, 12);
        Class<?> wildcardClass40 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass40);
    }

    @Test
    public void test1997() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1997");
        NextDate nextDate3 = new NextDate(0, (int) (short) 1, (int) (byte) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1998() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1998");
        NextDate nextDate3 = new NextDate(2001, (int) ' ', 1);
    }

    @Test
    public void test1999() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test1999");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 0, (int) (byte) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2000() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest3.test2000");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 1, 10);
        String str7 = nextDate3.run((int) (byte) 0, (int) (short) 0, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }
}

