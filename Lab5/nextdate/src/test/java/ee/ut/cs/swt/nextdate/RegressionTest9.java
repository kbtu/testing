package ee.ut.cs.swt.nextdate;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest9 {

    public static boolean debug = false;

    @Test
    public void test4501() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4501");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 10, 1);
    }

    @Test
    public void test4502() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4502");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 1, 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4503() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4503");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 1, 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4504() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4504");
        NextDate nextDate3 = new NextDate((int) '4', (int) 'a', 1);
        String str7 = nextDate3.run((int) (byte) -1, 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4505() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4505");
        NextDate nextDate3 = new NextDate((int) (short) -1, (-1), 2020);
        String str7 = nextDate3.run(0, 31, (int) (short) 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4506() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4506");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 1, (int) (byte) 10);
        String str7 = nextDate3.run(1, 100, 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4507() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4507");
        NextDate nextDate3 = new NextDate((int) '#', 1, (int) (short) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4508() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4508");
        NextDate nextDate3 = new NextDate(30, (int) ' ', (int) (byte) 1);
    }

    @Test
    public void test4509() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4509");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 0, 2001);
        String str7 = nextDate3.run(10, 2020, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4510() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4510");
        NextDate nextDate3 = new NextDate(2001, (int) 'a', 0);
        String str7 = nextDate3.run((int) (short) 10, (int) (byte) 0, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4511() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4511");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 2001, (int) (short) 0);
    }

    @Test
    public void test4512() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4512");
        NextDate nextDate3 = new NextDate(2020, 2001, (int) ' ');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4513() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4513");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) ' ', 2020, (int) (short) 0);
        String str11 = nextDate3.run(7, (int) (byte) 1, (int) (byte) -1);
        String str15 = nextDate3.run((-1), (int) (short) 10, 1);
        String str19 = nextDate3.run(31, 2020, 7);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test4514() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4514");
        NextDate nextDate3 = new NextDate((int) (short) 1, 1, (int) (short) 10);
        String str7 = nextDate3.run((int) 'a', 0, 2001);
        String str11 = nextDate3.run((int) (short) -1, (int) (byte) 0, (int) (short) 1);
        String str15 = nextDate3.run((-1), 30, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test4515() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4515");
        NextDate nextDate3 = new NextDate(100, (int) (short) 0, (int) (short) 10);
    }

    @Test
    public void test4516() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4516");
        NextDate nextDate3 = new NextDate(1, 0, (int) (short) 100);
    }

    @Test
    public void test4517() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4517");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 1, (int) (short) 1);
        String str7 = nextDate3.run((int) ' ', (int) (short) 1, (int) (byte) 0);
        String str11 = nextDate3.run(2001, (int) (byte) 10, (int) '4');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4518() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4518");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) -1, 100);
        String str7 = nextDate3.run(7, 10, (int) '4');
        String str11 = nextDate3.run((int) (byte) 1, 2020, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4519() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4519");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 0, (int) (short) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4520() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4520");
        NextDate nextDate3 = new NextDate(2020, 100, (int) ' ');
    }

    @Test
    public void test4521() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4521");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run((int) (byte) 10, (int) (short) 100, 100);
        String str27 = nextDate3.run((int) (short) 100, (int) (short) 0, (int) (byte) 0);
        String str31 = nextDate3.run(2020, 30, 7);
        String str35 = nextDate3.run((int) (short) -1, (int) (short) 10, 2020);
        String str39 = nextDate3.run((-1), 2001, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
    }

    @Test
    public void test4522() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4522");
        NextDate nextDate3 = new NextDate((int) (short) 0, 7, (-1));
        String str7 = nextDate3.run((int) (short) 10, 100, (int) ' ');
        String str11 = nextDate3.run(100, 12, (int) (short) -1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4523() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4523");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 100, (int) (short) 100);
        String str7 = nextDate3.run((int) (byte) 0, 0, 30);
        String str11 = nextDate3.run(12, 31, (-1));
        String str15 = nextDate3.run((int) (short) -1, (int) '#', (int) (byte) -1);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4524() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4524");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        String str15 = nextDate3.run((int) (byte) 1, (int) (short) 0, (int) (short) 100);
        String str19 = nextDate3.run((int) (short) 10, 0, 10);
        String str23 = nextDate3.run((int) (short) 10, 12, 1);
        String str27 = nextDate3.run(100, (int) ' ', (int) (byte) 0);
        String str31 = nextDate3.run(100, 10, 12);
        String str35 = nextDate3.run((int) ' ', 7, 1);
        Class<?> wildcardClass36 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass36);
    }

    @Test
    public void test4525() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4525");
        NextDate nextDate3 = new NextDate(7, 7, (int) (byte) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4526() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4526");
        NextDate nextDate3 = new NextDate(10, 2020, (int) (byte) 100);
        String str7 = nextDate3.run(2001, 31, (int) (byte) 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4527() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4527");
        NextDate nextDate3 = new NextDate(31, (int) (short) 100, (int) (short) 100);
        String str7 = nextDate3.run((int) 'a', (int) (byte) 1, 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4528() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4528");
        NextDate nextDate3 = new NextDate(10, (int) (short) 0, 10);
        String str7 = nextDate3.run((int) (short) 10, 100, 1);
        String str11 = nextDate3.run(1, 10, (int) '4');
        String str15 = nextDate3.run((int) (byte) -1, (int) (short) -1, 10);
        String str19 = nextDate3.run(1, 12, (int) 'a');
        String str23 = nextDate3.run((int) (short) 100, 2001, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test4529() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4529");
        NextDate nextDate3 = new NextDate((int) (short) 0, 0, (int) (byte) 0);
    }

    @Test
    public void test4530() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4530");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) -1, 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4531() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4531");
        NextDate nextDate3 = new NextDate((int) (short) 0, 2001, (int) (short) 10);
        String str7 = nextDate3.run((int) (short) 10, 1, (-1));
        String str11 = nextDate3.run((-1), (int) (short) -1, (int) (short) 10);
        String str15 = nextDate3.run(2001, (int) '4', 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test4532() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4532");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) 'a', 12);
        String str7 = nextDate3.run((int) (short) 1, 31, (int) ' ');
        String str11 = nextDate3.run((int) (byte) 0, 100, (int) (byte) -1);
        String str15 = nextDate3.run((int) (short) 100, (int) (byte) -1, 31);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4533() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4533");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) ' ', 0);
    }

    @Test
    public void test4534() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4534");
        NextDate nextDate3 = new NextDate((int) (short) 1, 0, (int) '4');
        String str7 = nextDate3.run(30, (int) '#', 31);
        String str11 = nextDate3.run(100, 30, (int) '4');
        String str15 = nextDate3.run((int) 'a', 0, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test4535() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4535");
        NextDate nextDate3 = new NextDate(7, 10, (int) (short) -1);
        String str7 = nextDate3.run(7, (-1), (int) (byte) 0);
        String str11 = nextDate3.run((int) (short) 1, 1, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4536() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4536");
        NextDate nextDate3 = new NextDate(10, (int) (short) 100, (int) (byte) 1);
    }

    @Test
    public void test4537() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4537");
        NextDate nextDate3 = new NextDate((int) (short) 1, 7, 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4538() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4538");
        NextDate nextDate3 = new NextDate((int) 'a', (int) '#', 12);
        String str7 = nextDate3.run((-1), (int) (byte) 10, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4539() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4539");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 100, 1);
        String str7 = nextDate3.run((-1), (int) ' ', 31);
        String str11 = nextDate3.run((int) (byte) -1, (int) (byte) -1, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4540() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4540");
        NextDate nextDate3 = new NextDate(0, 0, 100);
        String str7 = nextDate3.run(0, 0, (int) ' ');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4541() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4541");
        NextDate nextDate3 = new NextDate((int) '4', 30, (int) (short) 100);
        String str7 = nextDate3.run((int) (byte) 10, (int) (byte) 1, (int) (byte) 10);
        String str11 = nextDate3.run((-1), (int) (byte) 10, (int) (byte) 1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4542() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4542");
        NextDate nextDate3 = new NextDate(10, 0, 2001);
        String str7 = nextDate3.run(10, 0, (int) (short) 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4543() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4543");
        NextDate nextDate3 = new NextDate((-1), 30, 0);
    }

    @Test
    public void test4544() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4544");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 7, 7);
    }

    @Test
    public void test4545() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4545");
        NextDate nextDate3 = new NextDate(12, (int) (short) 10, (int) (byte) 10);
        String str7 = nextDate3.run((int) 'a', (int) (short) 100, 2020);
        String str11 = nextDate3.run((int) (short) 0, 2001, (int) (short) -1);
        String str15 = nextDate3.run((int) (short) 0, 1, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test4546() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4546");
        NextDate nextDate3 = new NextDate(12, (int) (byte) 1, 30);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4547() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4547");
        NextDate nextDate3 = new NextDate((int) '4', 31, 0);
        String str7 = nextDate3.run((int) ' ', (int) 'a', (int) 'a');
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 10, (int) (short) 1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4548() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4548");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) ' ', 12);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4549() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4549");
        NextDate nextDate3 = new NextDate(2020, (int) (short) -1, 7);
        String str7 = nextDate3.run((int) (byte) 10, (int) (short) 0, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4550() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4550");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (-1), (int) (byte) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4551() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4551");
        NextDate nextDate3 = new NextDate(0, (int) (byte) -1, 31);
        String str7 = nextDate3.run((int) (short) 100, (int) (short) 100, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4552() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4552");
        NextDate nextDate3 = new NextDate((int) (short) 100, 12, (int) (short) 0);
        String str7 = nextDate3.run(2001, (int) 'a', 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4553() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4553");
        NextDate nextDate3 = new NextDate(0, 12, 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4554() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4554");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) 0, 2020);
    }

    @Test
    public void test4555() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4555");
        NextDate nextDate3 = new NextDate(100, 2020, (-1));
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4556() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4556");
        NextDate nextDate3 = new NextDate(7, (int) (short) 100, (int) (byte) -1);
    }

    @Test
    public void test4557() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4557");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) -1, (int) ' ');
    }

    @Test
    public void test4558() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4558");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 10, 0);
        String str7 = nextDate3.run(1, (int) (short) 1, 10);
        String str11 = nextDate3.run((int) (short) 1, (int) (short) 10, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4559() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4559");
        NextDate nextDate3 = new NextDate((int) 'a', 2020, 0);
    }

    @Test
    public void test4560() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4560");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 10, 1);
        String str7 = nextDate3.run((int) 'a', 2020, (int) (short) -1);
        String str11 = nextDate3.run((int) (byte) 0, 100, 0);
        String str15 = nextDate3.run(7, (int) (short) 0, 0);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4561() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4561");
        NextDate nextDate3 = new NextDate(31, (int) 'a', (-1));
        String str7 = nextDate3.run((int) (byte) 1, 100, (int) (byte) -1);
        String str11 = nextDate3.run((int) (byte) 10, (int) (byte) -1, 10);
        String str15 = nextDate3.run(1, (int) (byte) 100, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test4562() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4562");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) '4', 1);
    }

    @Test
    public void test4563() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4563");
        NextDate nextDate3 = new NextDate(0, 100, (int) (short) 1);
        String str7 = nextDate3.run((int) (short) 100, 12, (int) ' ');
        String str11 = nextDate3.run((int) ' ', (int) (byte) -1, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4564() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4564");
        NextDate nextDate3 = new NextDate(2020, 100, (int) '#');
    }

    @Test
    public void test4565() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4565");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) '4', 0);
    }

    @Test
    public void test4566() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4566");
        NextDate nextDate3 = new NextDate((int) (short) -1, 0, (int) (short) 10);
        String str7 = nextDate3.run((int) 'a', (int) '4', (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4567() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4567");
        NextDate nextDate3 = new NextDate((int) '4', 31, 2001);
        String str7 = nextDate3.run((int) 'a', (int) (short) 10, (int) '4');
        String str11 = nextDate3.run(0, 0, 31);
        String str15 = nextDate3.run((int) (short) 0, (int) (short) 10, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test4568() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4568");
        NextDate nextDate3 = new NextDate(1, (int) (byte) 0, 100);
        String str7 = nextDate3.run(0, 7, (int) 'a');
        String str11 = nextDate3.run((int) (byte) -1, 12, (int) '4');
        String str15 = nextDate3.run(2001, (int) (byte) -1, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test4569() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4569");
        NextDate nextDate3 = new NextDate(31, 12, (int) (byte) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4570() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4570");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) '#', (int) ' ');
        String str7 = nextDate3.run((-1), 1, (-1));
        String str11 = nextDate3.run(100, 10, (int) (short) 100);
        String str15 = nextDate3.run((int) '4', 0, (int) (byte) 1);
        String str19 = nextDate3.run(30, 0, 30);
        String str23 = nextDate3.run((int) '4', 30, 30);
        Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test4571() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4571");
        NextDate nextDate3 = new NextDate((int) '#', (-1), (int) (short) 1);
    }

    @Test
    public void test4572() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4572");
        NextDate nextDate3 = new NextDate((int) ' ', 12, (int) (short) -1);
    }

    @Test
    public void test4573() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4573");
        NextDate nextDate3 = new NextDate((int) 'a', 1, 7);
        String str7 = nextDate3.run(10, 100, 0);
        String str11 = nextDate3.run(31, (int) (byte) 1, (int) ' ');
        String str15 = nextDate3.run((int) (short) 0, (int) (short) 0, 0);
        String str19 = nextDate3.run((int) (short) 1, 0, 0);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test4574() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4574");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) -1, (int) (short) -1);
        String str7 = nextDate3.run(0, 7, (int) (byte) 10);
        String str11 = nextDate3.run(100, (int) '#', 12);
        String str15 = nextDate3.run(2020, (int) (byte) 10, (int) (byte) 0);
        String str19 = nextDate3.run((int) (byte) 100, 30, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test4575() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4575");
        NextDate nextDate3 = new NextDate((int) (short) 0, 10, 2001);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4576() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4576");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 7, (int) (short) 0);
    }

    @Test
    public void test4577() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4577");
        NextDate nextDate3 = new NextDate(100, 7, 12);
    }

    @Test
    public void test4578() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4578");
        NextDate nextDate3 = new NextDate((-1), 10, 10);
    }

    @Test
    public void test4579() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4579");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 1, (int) 'a');
    }

    @Test
    public void test4580() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4580");
        NextDate nextDate3 = new NextDate(12, (int) '4', 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4581() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4581");
        NextDate nextDate3 = new NextDate(7, (int) (short) -1, 12);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4582() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4582");
        NextDate nextDate3 = new NextDate(2020, 2001, 10);
        String str7 = nextDate3.run((int) (short) -1, 7, (int) (short) -1);
        String str11 = nextDate3.run(0, (int) (byte) -1, (int) (short) 1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4583() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4583");
        NextDate nextDate3 = new NextDate(0, 1, (int) ' ');
        String str7 = nextDate3.run((int) (byte) 100, (int) (byte) 100, 31);
        String str11 = nextDate3.run((int) '4', 100, (int) (byte) 10);
        String str15 = nextDate3.run((int) (short) 1, 31, (int) (short) 100);
        String str19 = nextDate3.run((int) ' ', (int) (short) -1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test4584() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4584");
        NextDate nextDate3 = new NextDate(7, (int) (short) -1, (int) (byte) 10);
        String str7 = nextDate3.run((int) 'a', (int) '4', 100);
        String str11 = nextDate3.run((int) '#', (int) (byte) 1, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4585() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4585");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 1, (int) (short) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4586() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4586");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 10, 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4587() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4587");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 100, (int) (byte) 10);
        String str7 = nextDate3.run((int) (byte) 10, (int) (short) 100, (-1));
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4588() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4588");
        NextDate nextDate3 = new NextDate(31, (-1), (-1));
    }

    @Test
    public void test4589() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4589");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 7, 100);
    }

    @Test
    public void test4590() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4590");
        NextDate nextDate3 = new NextDate((-1), 0, 7);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4591() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4591");
        NextDate nextDate3 = new NextDate(100, (int) (short) 1, (int) (byte) 10);
    }

    @Test
    public void test4592() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4592");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) '4', 2001);
        String str7 = nextDate3.run((int) (short) 0, (int) (byte) 0, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4593() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4593");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) -1, (int) (short) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4594() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4594");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 10, 7);
        String str7 = nextDate3.run(2001, 2020, 2001);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4595() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4595");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 31, (int) (byte) 1);
        String str7 = nextDate3.run(1, (int) (short) -1, 30);
        String str11 = nextDate3.run((int) (short) -1, 2020, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4596() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4596");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 1, 0);
        String str7 = nextDate3.run((-1), (int) (byte) 10, (int) '#');
        String str11 = nextDate3.run((int) (short) 10, (int) '#', (int) '#');
        String str15 = nextDate3.run(0, (int) (short) 10, 100);
        String str19 = nextDate3.run(31, (int) (short) -1, 7);
        String str23 = nextDate3.run(10, 2020, 7);
        Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test4597() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4597");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) 'a', 30);
    }

    @Test
    public void test4598() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4598");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 2020, 100);
        String str7 = nextDate3.run((int) (byte) 10, (int) 'a', (int) '#');
        String str11 = nextDate3.run(2001, 10, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4599() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4599");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 31, (int) (byte) 10);
    }

    @Test
    public void test4600() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4600");
        NextDate nextDate3 = new NextDate(12, (int) (byte) 100, 31);
        String str7 = nextDate3.run((int) (short) 10, (int) (short) 100, (int) (short) 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4601() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4601");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) 0, (int) '4');
        String str7 = nextDate3.run(31, (int) (short) 1, (int) (byte) 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4602() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4602");
        NextDate nextDate3 = new NextDate((int) '#', (-1), (int) '4');
    }

    @Test
    public void test4603() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4603");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) 10, 30);
    }

    @Test
    public void test4604() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4604");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, (int) (short) 100);
    }

    @Test
    public void test4605() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4605");
        NextDate nextDate3 = new NextDate((int) '4', (int) ' ', (int) (byte) 10);
        String str7 = nextDate3.run((int) (byte) 100, 30, 2001);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4606() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4606");
        NextDate nextDate3 = new NextDate((int) (short) 0, 0, 2020);
        String str7 = nextDate3.run((int) (short) -1, (int) (byte) 100, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4607() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4607");
        NextDate nextDate3 = new NextDate((int) (short) 0, 7, (int) (byte) 0);
    }

    @Test
    public void test4608() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4608");
        NextDate nextDate3 = new NextDate((int) ' ', 30, 30);
        String str7 = nextDate3.run((int) ' ', 100, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4609() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4609");
        NextDate nextDate3 = new NextDate(31, 12, (int) (short) 100);
        String str7 = nextDate3.run((int) '#', (int) 'a', (int) (short) 100);
        String str11 = nextDate3.run(0, (-1), (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4610() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4610");
        NextDate nextDate3 = new NextDate((int) 'a', 0, (int) (byte) 100);
    }

    @Test
    public void test4611() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4611");
        NextDate nextDate3 = new NextDate(7, (int) '4', (int) (byte) -1);
        String str7 = nextDate3.run(10, (int) (short) 1, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4612() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4612");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 0, 7);
        String str7 = nextDate3.run((int) ' ', 0, (int) (short) 1);
        String str11 = nextDate3.run(2001, (int) (short) -1, (int) ' ');
        String str15 = nextDate3.run((int) 'a', (int) (short) 100, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test4613() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4613");
        NextDate nextDate3 = new NextDate(31, (int) (byte) 0, (int) 'a');
        String str7 = nextDate3.run((int) '#', (int) (short) 1, 100);
        String str11 = nextDate3.run((int) (short) 10, (int) (byte) 100, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4614() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4614");
        NextDate nextDate3 = new NextDate((int) 'a', 30, (int) (short) 100);
        String str7 = nextDate3.run(12, 12, 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4615() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4615");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) -1, (int) (byte) 0);
        String str7 = nextDate3.run(31, 7, 0);
        String str11 = nextDate3.run((-1), (int) 'a', (int) 'a');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4616() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4616");
        NextDate nextDate3 = new NextDate(100, 2020, (-1));
        String str7 = nextDate3.run((int) (byte) 10, (int) (byte) -1, (int) (byte) 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4617() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4617");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) 1, 12);
        String str7 = nextDate3.run((int) (byte) 0, (int) (short) -1, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4618() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4618");
        NextDate nextDate3 = new NextDate((int) ' ', 0, 2020);
        String str7 = nextDate3.run(1, 12, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4619() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4619");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 0, (int) (byte) 0);
        String str7 = nextDate3.run(12, 30, (int) '#');
        String str11 = nextDate3.run(31, 2001, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4620() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4620");
        NextDate nextDate3 = new NextDate(1, 0, (int) 'a');
    }

    @Test
    public void test4621() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4621");
        NextDate nextDate3 = new NextDate((int) (short) 1, 0, (int) (byte) 1);
    }

    @Test
    public void test4622() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4622");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) 1, 1);
    }

    @Test
    public void test4623() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4623");
        NextDate nextDate3 = new NextDate(0, 0, 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4624() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4624");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 1, (int) ' ');
    }

    @Test
    public void test4625() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4625");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 1, (int) (byte) 100);
    }

    @Test
    public void test4626() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4626");
        NextDate nextDate3 = new NextDate((int) 'a', 2020, (int) (short) 0);
        String str7 = nextDate3.run(7, (int) (byte) 1, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4627() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4627");
        NextDate nextDate3 = new NextDate(30, 0, (int) (byte) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4628() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4628");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 1, (int) (short) -1);
        String str7 = nextDate3.run(2020, (int) (byte) 100, (int) '#');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4629() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4629");
        NextDate nextDate3 = new NextDate((int) '4', (int) (byte) 0, 0);
        String str7 = nextDate3.run((int) (byte) 0, 2020, 0);
        String str11 = nextDate3.run(1, 7, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4630() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4630");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 0, 7);
        String str7 = nextDate3.run((int) (short) -1, 30, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4631() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4631");
        NextDate nextDate3 = new NextDate(1, (-1), (int) (byte) 100);
        String str7 = nextDate3.run((int) (short) 10, 30, 1);
        String str11 = nextDate3.run(100, (int) (short) 0, (-1));
        String str15 = nextDate3.run(2020, 0, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test4632() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4632");
        NextDate nextDate3 = new NextDate(2020, (int) (short) 100, (int) (byte) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4633() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4633");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 0, (int) 'a');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4634() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4634");
        NextDate nextDate3 = new NextDate((int) (short) 1, 7, 2001);
        String str7 = nextDate3.run((int) (short) 10, (int) ' ', 0);
        String str11 = nextDate3.run((int) (short) 1, (int) ' ', 7);
        String str15 = nextDate3.run((int) ' ', (int) 'a', (int) (short) 0);
        String str19 = nextDate3.run(7, 31, (int) 'a');
        String str23 = nextDate3.run((int) (byte) 10, (int) (byte) 1, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test4635() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4635");
        NextDate nextDate3 = new NextDate((-1), 1, 100);
        String str7 = nextDate3.run((int) (short) -1, (int) ' ', 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4636() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4636");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 0, (int) 'a');
        String str7 = nextDate3.run(0, 0, (int) 'a');
        String str11 = nextDate3.run((int) ' ', (int) (byte) 0, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4637() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4637");
        NextDate nextDate3 = new NextDate(30, (int) (short) 10, 10);
        String str7 = nextDate3.run(12, 100, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4638() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4638");
        NextDate nextDate3 = new NextDate(0, (int) ' ', (int) (short) 0);
        String str7 = nextDate3.run((int) (byte) 0, 100, (int) (short) -1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4639() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4639");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) -1, 0);
    }

    @Test
    public void test4640() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4640");
        NextDate nextDate3 = new NextDate((int) '#', (int) '4', (int) (short) -1);
        String str7 = nextDate3.run((int) '4', (int) (byte) -1, (int) '4');
        String str11 = nextDate3.run(30, 100, (int) (short) 10);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4641() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4641");
        NextDate nextDate3 = new NextDate(2020, 100, (int) (short) 100);
        String str7 = nextDate3.run(2020, (int) (short) 100, (int) (byte) 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4642() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4642");
        NextDate nextDate3 = new NextDate(100, 0, 30);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4643() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4643");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 1, 12);
    }

    @Test
    public void test4644() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4644");
        NextDate nextDate3 = new NextDate(10, 0, 2001);
        String str7 = nextDate3.run(0, 1, 0);
        String str11 = nextDate3.run((int) (byte) 100, 10, (int) '4');
        String str15 = nextDate3.run((int) (short) -1, (int) ' ', 2020);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4645() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4645");
        NextDate nextDate3 = new NextDate(100, (int) '4', (int) ' ');
    }

    @Test
    public void test4646() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4646");
        NextDate nextDate3 = new NextDate(7, (int) 'a', 7);
        String str7 = nextDate3.run((int) 'a', 0, 2020);
        String str11 = nextDate3.run(2020, (int) 'a', 30);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4647() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4647");
        NextDate nextDate3 = new NextDate(7, 2020, (int) (short) 1);
        String str7 = nextDate3.run((int) 'a', (int) (byte) 100, 1);
        String str11 = nextDate3.run(1, 31, 100);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4648() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4648");
        NextDate nextDate3 = new NextDate(0, 12, 2001);
        String str7 = nextDate3.run(12, 0, (int) '4');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4649() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4649");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run(7, (int) (short) 10, (int) (short) 1);
        String str27 = nextDate3.run(2020, 7, (int) (short) 0);
        String str31 = nextDate3.run(0, 31, (int) (short) 0);
        String str35 = nextDate3.run(7, (int) (short) 0, (int) 'a');
        String str39 = nextDate3.run(7, 0, 12);
        Class<?> wildcardClass40 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass40);
    }

    @Test
    public void test4650() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4650");
        NextDate nextDate3 = new NextDate(30, 10, 1);
    }

    @Test
    public void test4651() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4651");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) -1, 2020);
        String str7 = nextDate3.run(31, 0, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4652() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4652");
        NextDate nextDate3 = new NextDate((int) '4', (int) (byte) 100, (int) '#');
    }

    @Test
    public void test4653() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4653");
        NextDate nextDate3 = new NextDate(10, (int) (short) 0, 10);
        String str7 = nextDate3.run((int) (short) 10, 100, 1);
        String str11 = nextDate3.run(1, 10, (int) '4');
        String str15 = nextDate3.run((int) (byte) -1, (int) (short) -1, 10);
        String str19 = nextDate3.run(2020, 12, 31);
        String str23 = nextDate3.run(0, (int) (byte) 1, 31);
        Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test4654() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4654");
        NextDate nextDate3 = new NextDate(0, 0, 100);
        String str7 = nextDate3.run((int) ' ', (int) (short) 1, (int) (byte) -1);
        String str11 = nextDate3.run((int) (short) 10, (-1), (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4655() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4655");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 0, (int) (short) 1);
        String str7 = nextDate3.run(2001, (int) ' ', (-1));
        String str11 = nextDate3.run(0, (int) 'a', 10);
        String str15 = nextDate3.run(10, (int) (short) -1, 100);
        String str19 = nextDate3.run((int) '#', 2020, 2020);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test4656() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4656");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 12, (int) (byte) 100);
        String str7 = nextDate3.run(2020, (int) (byte) 10, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4657() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4657");
        NextDate nextDate3 = new NextDate(0, 100, (int) (short) 10);
        String str7 = nextDate3.run(0, 0, 10);
        String str11 = nextDate3.run(2001, 1, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4658() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4658");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 100, (int) (byte) 0);
    }

    @Test
    public void test4659() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4659");
        NextDate nextDate3 = new NextDate(10, 2020, (int) (short) -1);
        String str7 = nextDate3.run((int) (short) 1, 2001, (int) (byte) -1);
        String str11 = nextDate3.run((int) '#', (int) (byte) 1, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4660() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4660");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 1, (int) '#');
    }

    @Test
    public void test4661() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4661");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run(7, (int) (short) 10, (int) (short) 1);
        String str27 = nextDate3.run((int) 'a', (int) (byte) 1, 0);
        String str31 = nextDate3.run((int) (short) 1, (int) (byte) 1, 100);
        String str35 = nextDate3.run((int) ' ', (int) (short) 0, 0);
        String str39 = nextDate3.run((int) (short) 1, (int) (short) 1, 2020);
        String str43 = nextDate3.run(2020, 1, 30);
        String str47 = nextDate3.run((int) (short) 10, (int) '#', (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "1/2/2020" + "'", str39, "1/2/2020");
        org.junit.Assert.assertEquals("'" + str43 + "' != '" + "invalid Input Date" + "'", str43, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str47 + "' != '" + "invalid Input Date" + "'", str47, "invalid Input Date");
    }

    @Test
    public void test4662() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4662");
        NextDate nextDate3 = new NextDate(31, (int) (byte) 1, 7);
    }

    @Test
    public void test4663() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4663");
        NextDate nextDate3 = new NextDate(30, 7, 12);
    }

    @Test
    public void test4664() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4664");
        NextDate nextDate3 = new NextDate((-1), 31, (int) (short) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4665() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4665");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 10, 2001);
        String str7 = nextDate3.run(7, 0, (int) '4');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4666() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4666");
        NextDate nextDate3 = new NextDate(2020, 31, 30);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4667() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4667");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) 'a', 12);
    }

    @Test
    public void test4668() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4668");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) 100, (int) '#');
        String str7 = nextDate3.run((int) (short) 100, 30, 1);
        String str11 = nextDate3.run((int) (short) 1, (int) (short) 10, (int) (short) 0);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4669() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4669");
        NextDate nextDate3 = new NextDate(12, (int) (short) 10, (int) (byte) 100);
        String str7 = nextDate3.run((int) (byte) 100, 0, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4670() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4670");
        NextDate nextDate3 = new NextDate(0, (int) ' ', (int) (short) 10);
    }

    @Test
    public void test4671() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4671");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 2001, 7);
    }

    @Test
    public void test4672() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4672");
        NextDate nextDate3 = new NextDate(31, (-1), 0);
        String str7 = nextDate3.run((int) (byte) 1, (int) (short) 0, (int) '#');
        String str11 = nextDate3.run((int) 'a', (int) (byte) -1, (int) (byte) -1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4673() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4673");
        NextDate nextDate3 = new NextDate((int) '#', 31, 7);
    }

    @Test
    public void test4674() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4674");
        NextDate nextDate3 = new NextDate((int) (short) 0, 2001, 100);
    }

    @Test
    public void test4675() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4675");
        NextDate nextDate3 = new NextDate((int) '4', (-1), (int) (byte) 100);
        String str7 = nextDate3.run((int) (byte) 0, 0, 1);
        String str11 = nextDate3.run((-1), (int) (short) 1, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4676() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4676");
        NextDate nextDate3 = new NextDate(2020, 2020, (int) 'a');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4677() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4677");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 0, (int) (byte) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4678() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4678");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (-1), 2020);
    }

    @Test
    public void test4679() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4679");
        NextDate nextDate3 = new NextDate((int) ' ', (int) '#', 2020);
    }

    @Test
    public void test4680() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4680");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 1, (int) (short) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4681() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4681");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) ' ', (int) (byte) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4682() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4682");
        NextDate nextDate3 = new NextDate(12, 2020, (int) (short) 0);
        String str7 = nextDate3.run((int) '4', (int) (short) 100, (int) 'a');
        String str11 = nextDate3.run((int) ' ', 7, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4683() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4683");
        NextDate nextDate3 = new NextDate((int) '4', 7, 30);
        String str7 = nextDate3.run((int) '4', (int) (short) 0, (int) '4');
        String str11 = nextDate3.run(0, 30, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4684() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4684");
        NextDate nextDate3 = new NextDate((int) 'a', 30, (int) (byte) -1);
    }

    @Test
    public void test4685() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4685");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 0, (int) '4');
        String str7 = nextDate3.run((int) (byte) 10, (int) (short) -1, 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4686() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4686");
        NextDate nextDate3 = new NextDate(0, 7, (int) (short) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4687() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4687");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 1, (int) (short) 1);
    }

    @Test
    public void test4688() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4688");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) (byte) 0, (int) (byte) 10, 31);
        String str23 = nextDate3.run((-1), 0, (int) (byte) -1);
        String str27 = nextDate3.run((int) (byte) 1, (int) (byte) 1, (int) 'a');
        String str31 = nextDate3.run((int) (short) 1, 30, (int) (byte) 100);
        String str35 = nextDate3.run(1, 31, (-1));
        String str39 = nextDate3.run((-1), 2020, (int) (byte) 100);
        Class<?> wildcardClass40 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass40);
    }

    @Test
    public void test4689() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4689");
        NextDate nextDate3 = new NextDate(7, (int) (short) -1, (-1));
    }

    @Test
    public void test4690() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4690");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) -1, 7);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4691() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4691");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 2020, 100);
        String str7 = nextDate3.run(100, (int) '4', (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4692() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4692");
        NextDate nextDate3 = new NextDate(7, 7, (int) ' ');
        String str7 = nextDate3.run((int) '#', (int) (short) 1, 31);
        String str11 = nextDate3.run((int) '#', (int) (short) 0, (-1));
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4693() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4693");
        NextDate nextDate3 = new NextDate(2020, 2001, 0);
        String str7 = nextDate3.run(2001, (-1), (int) (byte) -1);
        String str11 = nextDate3.run(2001, (int) ' ', 12);
        String str15 = nextDate3.run(12, (int) (short) 1, 31);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4694() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4694");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 1, (int) (byte) 100);
        String str7 = nextDate3.run(10, 2001, (int) (short) 1);
        String str11 = nextDate3.run(7, 12, (int) ' ');
        String str15 = nextDate3.run((int) (short) 100, (int) (byte) 100, (int) (short) 100);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4695() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4695");
        NextDate nextDate3 = new NextDate((int) 'a', 1, 7);
        String str7 = nextDate3.run(10, 100, 0);
        String str11 = nextDate3.run(31, (int) (byte) 1, (int) ' ');
        String str15 = nextDate3.run((int) (short) 0, (int) (short) 0, 0);
        String str19 = nextDate3.run((-1), (int) 'a', (int) (short) 0);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test4696() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4696");
        NextDate nextDate3 = new NextDate(31, 10, (int) (byte) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4697() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4697");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) '4', 0);
        String str7 = nextDate3.run(10, (int) (short) 10, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4698() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4698");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) 0, 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4699() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4699");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) ' ', 2020);
        String str7 = nextDate3.run(1, (int) (short) 100, (int) 'a');
        String str11 = nextDate3.run((-1), (int) (short) 10, 31);
        String str15 = nextDate3.run(1, 12, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test4700() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4700");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) 'a', 12);
        String str7 = nextDate3.run((int) (short) 1, 31, (int) ' ');
        String str11 = nextDate3.run(0, (int) (byte) -1, (-1));
        String str15 = nextDate3.run((int) (byte) 0, (int) (byte) 0, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test4701() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4701");
        NextDate nextDate3 = new NextDate(0, 0, (int) (short) 100);
    }

    @Test
    public void test4702() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4702");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 100, 10);
    }

    @Test
    public void test4703() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4703");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 100, (int) (short) 1);
    }

    @Test
    public void test4704() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4704");
        NextDate nextDate3 = new NextDate(31, (int) (short) 1, (int) (short) 10);
    }

    @Test
    public void test4705() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4705");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) 1, 12);
        String str7 = nextDate3.run((int) 'a', 0, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4706() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4706");
        NextDate nextDate3 = new NextDate(100, (int) 'a', (int) (short) -1);
    }

    @Test
    public void test4707() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4707");
        NextDate nextDate3 = new NextDate(30, 2001, 30);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4708() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4708");
        NextDate nextDate3 = new NextDate(7, (int) (byte) -1, 10);
        String str7 = nextDate3.run((int) ' ', (int) (byte) 10, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4709() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4709");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) 100, (int) (byte) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4710() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4710");
        NextDate nextDate3 = new NextDate(10, 2020, (int) (short) -1);
        String str7 = nextDate3.run(0, (int) 'a', (-1));
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4711() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4711");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) 0, 7);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4712() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4712");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run((int) (byte) 10, (int) (short) 100, 100);
        String str27 = nextDate3.run((int) (short) 100, (int) (short) 0, (int) (byte) 0);
        Class<?> wildcardClass28 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test4713() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4713");
        NextDate nextDate3 = new NextDate(31, (int) (short) -1, (int) (byte) 0);
    }

    @Test
    public void test4714() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4714");
        NextDate nextDate3 = new NextDate(2020, 100, 2020);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4715() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4715");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 2020, (-1));
    }

    @Test
    public void test4716() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4716");
        NextDate nextDate3 = new NextDate((-1), (int) '#', (int) 'a');
    }

    @Test
    public void test4717() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4717");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 100, 12);
        String str7 = nextDate3.run(7, (int) (byte) 1, 12);
        String str11 = nextDate3.run(10, (int) (short) 10, 7);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4718() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4718");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) -1, 31);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4719() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4719");
        NextDate nextDate3 = new NextDate((int) (short) -1, 10, 30);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4720() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4720");
        NextDate nextDate3 = new NextDate(1, (int) '#', (-1));
        String str7 = nextDate3.run((int) ' ', (int) (byte) 0, (int) (short) 1);
        String str11 = nextDate3.run((int) (short) 1, (int) (short) 10, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4721() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4721");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 7, 12);
        String str7 = nextDate3.run((int) (byte) 1, (int) (short) 100, (int) '4');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4722() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4722");
        NextDate nextDate3 = new NextDate(10, 100, (int) (byte) 100);
        String str7 = nextDate3.run((int) (short) 0, (int) (byte) 1, (int) 'a');
        String str11 = nextDate3.run(0, (-1), 0);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4723() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4723");
        NextDate nextDate3 = new NextDate((int) (short) 10, 12, 10);
        String str7 = nextDate3.run(2020, 12, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4724() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4724");
        NextDate nextDate3 = new NextDate((int) '4', 100, (-1));
    }

    @Test
    public void test4725() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4725");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 31, (-1));
        String str7 = nextDate3.run((int) '#', (-1), 100);
        String str11 = nextDate3.run((int) 'a', 0, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4726() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4726");
        NextDate nextDate3 = new NextDate(0, 31, (int) 'a');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4727() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4727");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 0, (int) '4');
        String str7 = nextDate3.run(2020, 12, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4728() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4728");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) -1, 31);
        String str7 = nextDate3.run(0, 0, 31);
        String str11 = nextDate3.run((int) (byte) 10, (int) (byte) 0, (int) (byte) 0);
        String str15 = nextDate3.run((int) (short) -1, (int) (short) 1, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test4729() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4729");
        NextDate nextDate3 = new NextDate((int) (short) 0, 2001, (int) (short) 10);
        String str7 = nextDate3.run((int) (short) 10, 1, (-1));
        String str11 = nextDate3.run((-1), (int) (short) -1, (int) (short) 10);
        String str15 = nextDate3.run((int) '#', (int) 'a', (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test4730() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4730");
        NextDate nextDate3 = new NextDate(7, 0, (int) '4');
    }

    @Test
    public void test4731() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4731");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) '#', 100);
        String str7 = nextDate3.run(0, (int) (byte) 10, (int) '#');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4732() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4732");
        NextDate nextDate3 = new NextDate((int) '#', 7, 100);
        String str7 = nextDate3.run(1, (int) (short) 1, 2020);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "1/2/2020" + "'", str7, "1/2/2020");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4733() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4733");
        NextDate nextDate3 = new NextDate(31, 2020, (int) 'a');
    }

    @Test
    public void test4734() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4734");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) -1, (int) (byte) -1);
    }

    @Test
    public void test4735() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4735");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) -1, 2001);
        String str7 = nextDate3.run(10, (int) '4', (-1));
        String str11 = nextDate3.run((int) (byte) 1, (int) (byte) -1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4736() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4736");
        NextDate nextDate3 = new NextDate(12, (int) (byte) -1, (int) (byte) 0);
        String str7 = nextDate3.run((int) (short) 100, (int) (short) 1, 0);
        String str11 = nextDate3.run(0, (int) (short) 10, 10);
        String str15 = nextDate3.run((int) (short) 0, 0, 1);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4737() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4737");
        NextDate nextDate3 = new NextDate(1, (int) (byte) 1, (int) (byte) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4738() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4738");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) ' ', 2020, (int) (short) 0);
        String str11 = nextDate3.run(31, (int) '4', (int) (byte) -1);
        String str15 = nextDate3.run((int) '#', (int) ' ', (int) (short) 0);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4739() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4739");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (short) 10, (int) (byte) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4740() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4740");
        NextDate nextDate3 = new NextDate(30, (-1), (int) '#');
        String str7 = nextDate3.run((int) (byte) 100, (int) '#', (int) 'a');
        String str11 = nextDate3.run(31, (int) (byte) 10, (int) (byte) 10);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4741() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4741");
        NextDate nextDate3 = new NextDate(31, 7, 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4742() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4742");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) 'a', 0);
    }

    @Test
    public void test4743() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4743");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 1, (int) (short) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4744() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4744");
        NextDate nextDate3 = new NextDate((int) '#', (int) 'a', (int) '#');
    }

    @Test
    public void test4745() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4745");
        NextDate nextDate3 = new NextDate(1, 100, (int) ' ');
        String str7 = nextDate3.run(100, (int) (short) 10, (int) (byte) -1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4746() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4746");
        NextDate nextDate3 = new NextDate(2001, (-1), 10);
    }

    @Test
    public void test4747() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4747");
        NextDate nextDate3 = new NextDate(31, (int) (byte) 100, (int) (short) -1);
    }

    @Test
    public void test4748() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4748");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (byte) 100, (int) (byte) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4749() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4749");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 7, 2020);
    }

    @Test
    public void test4750() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4750");
        NextDate nextDate3 = new NextDate((int) '4', (int) '4', 12);
        String str7 = nextDate3.run((int) (byte) 100, (int) (byte) 1, (int) (byte) 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4751() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4751");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 10, (int) '#');
        String str7 = nextDate3.run(31, 0, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4752() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4752");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        String str15 = nextDate3.run(7, (int) (short) 1, 30);
        String str19 = nextDate3.run(30, (int) 'a', (int) (short) 0);
        String str23 = nextDate3.run(31, 100, 12);
        String str27 = nextDate3.run(1, (int) (short) 10, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test4753() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4753");
        NextDate nextDate3 = new NextDate((int) (short) -1, 30, 0);
        String str7 = nextDate3.run((int) (short) 10, 12, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4754() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4754");
        NextDate nextDate3 = new NextDate(7, (int) (byte) 100, 7);
    }

    @Test
    public void test4755() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4755");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 30, (int) (short) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4756() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4756");
        NextDate nextDate3 = new NextDate(7, (int) (byte) 0, 0);
        String str7 = nextDate3.run((int) (byte) 10, 7, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4757() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4757");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 31, (int) '#');
        String str7 = nextDate3.run(7, (int) (short) -1, (int) (short) 1);
        String str11 = nextDate3.run((int) (byte) -1, (int) (short) 1, (int) (byte) -1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4758() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4758");
        NextDate nextDate3 = new NextDate((int) ' ', 7, (int) '#');
    }

    @Test
    public void test4759() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4759");
        NextDate nextDate3 = new NextDate(12, (int) (byte) -1, (int) (byte) 0);
        String str7 = nextDate3.run((int) (short) 100, (int) (short) 1, 0);
        String str11 = nextDate3.run((-1), (int) ' ', (int) (short) -1);
        String str15 = nextDate3.run(2001, (-1), 12);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4760() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4760");
        NextDate nextDate3 = new NextDate((int) ' ', (int) ' ', 1);
    }

    @Test
    public void test4761() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4761");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) ' ', 2020);
        String str7 = nextDate3.run(1, (int) (short) 100, (int) 'a');
        String str11 = nextDate3.run(0, (int) (byte) 10, 30);
        String str15 = nextDate3.run(0, (int) 'a', (int) (short) 10);
        String str19 = nextDate3.run((int) '4', 12, (int) ' ');
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test4762() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4762");
        NextDate nextDate3 = new NextDate((int) (short) -1, 10, 31);
        String str7 = nextDate3.run((int) (short) 1, (int) (byte) 1, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4763() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4763");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) ' ', 12);
        String str7 = nextDate3.run(12, 100, 2001);
        String str11 = nextDate3.run((int) (byte) 10, (int) '4', 12);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "1/1/2002" + "'", str7, "1/1/2002");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4764() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4764");
        NextDate nextDate3 = new NextDate((int) 'a', 0, (int) '#');
    }

    @Test
    public void test4765() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4765");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        String str15 = nextDate3.run((int) (byte) 1, (int) (short) 0, (int) (short) 100);
        String str19 = nextDate3.run((int) (short) 10, 0, 10);
        String str23 = nextDate3.run((int) (byte) -1, 10, 30);
        String str27 = nextDate3.run((int) (byte) 100, (int) (short) 0, (-1));
        String str31 = nextDate3.run((int) (byte) 100, (int) ' ', (int) ' ');
        String str35 = nextDate3.run(2001, 1, 100);
        String str39 = nextDate3.run((int) (short) 10, 0, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
    }

    @Test
    public void test4766() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4766");
        NextDate nextDate3 = new NextDate(0, (int) (short) 1, (int) (byte) 10);
        String str7 = nextDate3.run((int) (short) 0, 100, (-1));
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4767() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4767");
        NextDate nextDate3 = new NextDate(1, 1, (int) (byte) 100);
    }

    @Test
    public void test4768() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4768");
        NextDate nextDate3 = new NextDate(30, (int) (short) 0, 0);
        String str7 = nextDate3.run(12, (int) 'a', (int) (short) 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4769() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4769");
        NextDate nextDate3 = new NextDate((-1), (int) 'a', 31);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4770() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4770");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) ' ', (int) ' ');
    }

    @Test
    public void test4771() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4771");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run((int) (byte) 10, (int) (short) 100, 100);
        String str27 = nextDate3.run((int) (short) 100, (int) (short) 0, (int) (byte) 0);
        String str31 = nextDate3.run(2020, 30, 7);
        String str35 = nextDate3.run((int) (short) 10, (int) (byte) -1, 100);
        String str39 = nextDate3.run((int) (byte) -1, (int) (byte) 1, (int) (byte) 100);
        String str43 = nextDate3.run(0, 0, 2020);
        String str47 = nextDate3.run(30, (int) (byte) 0, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str43 + "' != '" + "invalid Input Date" + "'", str43, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str47 + "' != '" + "invalid Input Date" + "'", str47, "invalid Input Date");
    }

    @Test
    public void test4772() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4772");
        NextDate nextDate3 = new NextDate(1, 10, 31);
        String str7 = nextDate3.run(2020, 0, (int) (byte) 0);
        String str11 = nextDate3.run(30, 12, (int) (short) 100);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4773() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4773");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) '#', 2001);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4774() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4774");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 0, (int) (short) 10);
        String str7 = nextDate3.run(2001, (int) (byte) -1, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4775() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4775");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 10, 31);
        String str7 = nextDate3.run((int) (byte) 0, 2020, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4776() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4776");
        NextDate nextDate3 = new NextDate((int) '4', 0, 7);
        String str7 = nextDate3.run(2001, (int) (short) 100, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4777() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4777");
        NextDate nextDate3 = new NextDate(12, (int) 'a', 10);
    }

    @Test
    public void test4778() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4778");
        NextDate nextDate3 = new NextDate(2001, 12, 2020);
        String str7 = nextDate3.run((int) (short) 10, (int) (short) 100, (int) (byte) 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4779() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4779");
        NextDate nextDate3 = new NextDate(7, (int) (byte) 0, 2020);
        String str7 = nextDate3.run((int) (short) 0, 31, 2020);
        String str11 = nextDate3.run((-1), (int) '4', 1);
        String str15 = nextDate3.run((int) (byte) 1, (int) (byte) 10, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test4780() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4780");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) '#', (int) (byte) 0);
    }

    @Test
    public void test4781() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4781");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 2001, 31);
        String str7 = nextDate3.run((-1), (int) '#', (int) '4');
        String str11 = nextDate3.run(1, (int) (short) -1, 0);
        String str15 = nextDate3.run(100, 2020, 0);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4782() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4782");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) 1, 12);
        String str7 = nextDate3.run(100, 12, 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4783() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4783");
        NextDate nextDate3 = new NextDate((-1), 1, 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4784() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4784");
        NextDate nextDate3 = new NextDate((int) (short) 1, 7, 2001);
        String str7 = nextDate3.run((int) (short) 10, (int) ' ', 0);
        String str11 = nextDate3.run((int) (short) 1, (int) ' ', 7);
        String str15 = nextDate3.run((int) 'a', 1, 2020);
        String str19 = nextDate3.run((int) (short) 10, (-1), (int) (short) 10);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test4785() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4785");
        NextDate nextDate3 = new NextDate((int) (short) 10, 0, 2001);
        String str7 = nextDate3.run((int) '4', (int) '4', (int) (short) -1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4786() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4786");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 7, (-1));
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4787() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4787");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (short) 0, 10);
        String str7 = nextDate3.run((int) (byte) -1, (int) '#', (int) 'a');
        String str11 = nextDate3.run((int) (byte) 10, 30, 10);
        String str15 = nextDate3.run(7, 1, 0);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4788() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4788");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) (byte) 10, (int) '#', 31);
        String str11 = nextDate3.run(0, 2020, (int) (short) 10);
        String str15 = nextDate3.run((int) '#', (int) (short) 10, (int) (byte) -1);
        String str19 = nextDate3.run(2001, 2001, (int) 'a');
        String str23 = nextDate3.run(30, 2001, 2020);
        Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test4789() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4789");
        NextDate nextDate3 = new NextDate(30, (int) (short) 0, (int) (short) 10);
    }

    @Test
    public void test4790() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4790");
        NextDate nextDate3 = new NextDate(12, (int) (short) 100, 7);
        String str7 = nextDate3.run(12, (int) (byte) 100, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4791() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4791");
        NextDate nextDate3 = new NextDate((int) (short) 1, 12, 2020);
        String str7 = nextDate3.run(31, 2020, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4792() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4792");
        NextDate nextDate3 = new NextDate(31, (-1), 0);
        String str7 = nextDate3.run((int) (byte) 0, (int) (byte) 1, (-1));
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4793() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4793");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) -1, (int) (byte) 0);
        String str7 = nextDate3.run(12, 0, (int) (byte) 100);
        String str11 = nextDate3.run(31, (int) (short) 0, 0);
        String str15 = nextDate3.run(0, (int) (byte) 10, 2020);
        String str19 = nextDate3.run((int) (short) 10, (int) (byte) 1, (-1));
        String str23 = nextDate3.run((int) (byte) 100, 12, (int) (byte) 1);
        String str27 = nextDate3.run(100, 7, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test4794() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4794");
        NextDate nextDate3 = new NextDate(2020, 0, 100);
        String str7 = nextDate3.run((int) (short) 10, (int) (short) 100, (int) (byte) 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4795() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4795");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 10, (int) (short) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4796() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4796");
        NextDate nextDate3 = new NextDate(31, 2001, (int) (byte) 0);
        String str7 = nextDate3.run((int) '#', (int) (byte) 100, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4797() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4797");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) -1, 2001);
        String str7 = nextDate3.run(0, 1, 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4798() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4798");
        NextDate nextDate3 = new NextDate(0, (int) (short) -1, 31);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4799() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4799");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 1, (int) (short) -1);
        String str7 = nextDate3.run((int) 'a', (-1), 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4800() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4800");
        NextDate nextDate3 = new NextDate(30, (int) (byte) 100, (int) (short) 1);
    }

    @Test
    public void test4801() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4801");
        NextDate nextDate3 = new NextDate(100, (int) (short) 100, (int) ' ');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4802() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4802");
        NextDate nextDate3 = new NextDate(10, 0, 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4803() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4803");
        NextDate nextDate3 = new NextDate(7, (int) '4', (int) (byte) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4804() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4804");
        NextDate nextDate3 = new NextDate(100, (int) (short) 0, 7);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4805() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4805");
        NextDate nextDate3 = new NextDate((int) (short) 100, 0, (int) '4');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4806() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4806");
        NextDate nextDate3 = new NextDate(2020, (int) (short) 100, 12);
        String str7 = nextDate3.run((int) (byte) 1, (int) (short) 100, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4807() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4807");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) 100, (int) (short) 10);
        String str7 = nextDate3.run((int) 'a', 2020, 12);
        String str11 = nextDate3.run((int) (short) -1, (int) (short) 1, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4808() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4808");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 0, 2001);
        String str7 = nextDate3.run((int) ' ', 0, (int) (byte) 1);
        String str11 = nextDate3.run((int) (byte) 100, 0, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4809() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4809");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) 'a', (int) (short) 0);
    }

    @Test
    public void test4810() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4810");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) '4', (int) (short) 100);
    }

    @Test
    public void test4811() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4811");
        NextDate nextDate3 = new NextDate(0, 100, 100);
        String str7 = nextDate3.run((int) '#', 10, 7);
        String str11 = nextDate3.run(1, (int) (byte) -1, (int) (byte) 100);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4812() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4812");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) -1, 7);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4813() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4813");
        NextDate nextDate3 = new NextDate(100, (int) 'a', 0);
        String str7 = nextDate3.run(1, (int) '4', (int) '4');
        String str11 = nextDate3.run(1, (int) (byte) 100, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4814() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4814");
        NextDate nextDate3 = new NextDate((int) (short) -1, 0, (int) 'a');
        String str7 = nextDate3.run(10, 12, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4815() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4815");
        NextDate nextDate3 = new NextDate(0, 7, 2020);
    }

    @Test
    public void test4816() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4816");
        NextDate nextDate3 = new NextDate((int) (short) 100, 2020, 2020);
        String str7 = nextDate3.run((int) (short) 0, 1, 7);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4817() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4817");
        NextDate nextDate3 = new NextDate(7, 2020, 100);
    }

    @Test
    public void test4818() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4818");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 2020, 7);
    }

    @Test
    public void test4819() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4819");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 0, (int) (short) 1);
        String str7 = nextDate3.run((int) '#', (int) '4', 2001);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4820() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4820");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 30, (int) (byte) -1);
        String str7 = nextDate3.run((int) (short) 0, (-1), 0);
        String str11 = nextDate3.run(7, (int) ' ', (-1));
        String str15 = nextDate3.run(100, (int) (short) 10, (int) (short) -1);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4821() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4821");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) 'a', 12, (int) (byte) 10);
        String str11 = nextDate3.run(31, (int) (byte) 1, (int) (byte) 10);
        String str15 = nextDate3.run((int) (short) 10, (int) (byte) 0, (int) 'a');
        String str19 = nextDate3.run(2001, (-1), 1);
        String str23 = nextDate3.run(2020, (int) '#', (int) (short) 10);
        String str27 = nextDate3.run((int) (short) 100, (int) (byte) 0, 0);
        String str31 = nextDate3.run((int) (byte) -1, (int) (short) 100, 30);
        Class<?> wildcardClass32 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass32);
    }

    @Test
    public void test4822() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4822");
        NextDate nextDate3 = new NextDate(10, 0, 2001);
        String str7 = nextDate3.run(0, 1, 0);
        String str11 = nextDate3.run((int) (short) 1, (int) (short) 100, 2001);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "2/1/2001" + "'", str11, "2/1/2001");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4823() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4823");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 10, 31);
    }

    @Test
    public void test4824() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4824");
        NextDate nextDate3 = new NextDate(31, (int) (byte) 1, 12);
        String str7 = nextDate3.run((int) (short) 0, (int) (short) -1, (int) (short) 0);
        String str11 = nextDate3.run(0, 10, (-1));
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4825() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4825");
        NextDate nextDate3 = new NextDate(0, (int) (short) 1, 12);
    }

    @Test
    public void test4826() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4826");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 0, (int) (byte) 1);
        String str7 = nextDate3.run((int) (short) 100, (int) (byte) 1, (int) '#');
        String str11 = nextDate3.run(7, 0, 2001);
        String str15 = nextDate3.run(1, (-1), (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test4827() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4827");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) '4', 12, 10);
        String str15 = nextDate3.run(31, (int) (short) 100, 31);
        String str19 = nextDate3.run((int) '#', (-1), (int) (short) 1);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test4828() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4828");
        NextDate nextDate3 = new NextDate(30, (int) (short) 0, 0);
        String str7 = nextDate3.run(1, (int) (short) 100, 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4829() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4829");
        NextDate nextDate3 = new NextDate(10, 12, 0);
    }

    @Test
    public void test4830() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4830");
        NextDate nextDate3 = new NextDate((int) (short) -1, 10, 10);
        String str7 = nextDate3.run(0, (int) (short) 10, 2020);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4831() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4831");
        NextDate nextDate3 = new NextDate(1, 10, 100);
        String str7 = nextDate3.run((int) '4', (int) (short) 100, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4832() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4832");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) '#', 7);
    }

    @Test
    public void test4833() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4833");
        NextDate nextDate3 = new NextDate((int) '4', (int) ' ', 0);
        String str7 = nextDate3.run(12, 0, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4834() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4834");
        NextDate nextDate3 = new NextDate(100, (int) (short) 10, (int) (byte) 0);
        String str7 = nextDate3.run((int) ' ', 1, (int) (byte) 1);
        String str11 = nextDate3.run(10, 7, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4835() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4835");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 1, (int) (short) 1);
    }

    @Test
    public void test4836() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4836");
        NextDate nextDate3 = new NextDate((int) (short) -1, 31, (int) (byte) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4837() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4837");
        NextDate nextDate3 = new NextDate(31, (-1), (int) ' ');
    }

    @Test
    public void test4838() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4838");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) 100, (int) (byte) 0);
        String str7 = nextDate3.run((int) (short) 100, (int) (byte) 1, (int) (short) 10);
        String str11 = nextDate3.run(7, (int) (short) 0, 7);
        String str15 = nextDate3.run((int) '4', 0, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test4839() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4839");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) 10, (-1));
    }

    @Test
    public void test4840() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4840");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 10, 0);
        String str7 = nextDate3.run((int) (short) 0, (int) (short) 0, 7);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4841() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4841");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) -1, (int) (byte) 10);
        String str7 = nextDate3.run(30, 10, (int) (short) 0);
        String str11 = nextDate3.run(7, 10, (int) (byte) 0);
        String str15 = nextDate3.run(100, (int) (short) 10, 0);
        String str19 = nextDate3.run((int) (short) -1, 100, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test4842() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4842");
        NextDate nextDate3 = new NextDate(2020, (int) ' ', (int) ' ');
        String str7 = nextDate3.run(2020, 31, 7);
        String str11 = nextDate3.run(100, (int) (short) 10, 0);
        String str15 = nextDate3.run(12, 0, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test4843() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4843");
        NextDate nextDate3 = new NextDate(30, (int) (short) 1, (int) ' ');
        String str7 = nextDate3.run(30, 0, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4844() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4844");
        NextDate nextDate3 = new NextDate(10, (int) '4', 0);
        String str7 = nextDate3.run(10, 100, 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4845() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4845");
        NextDate nextDate3 = new NextDate((int) '#', 100, (int) ' ');
        String str7 = nextDate3.run((int) (short) 0, (int) (short) 1, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4846() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4846");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 100, (int) (byte) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4847() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4847");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 7, (-1));
        String str7 = nextDate3.run((int) ' ', (-1), (int) (byte) 10);
        String str11 = nextDate3.run((int) (byte) 100, 1, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4848() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4848");
        NextDate nextDate3 = new NextDate(0, (int) (short) 10, (int) 'a');
        String str7 = nextDate3.run((int) (byte) -1, 100, (int) 'a');
        String str11 = nextDate3.run((int) 'a', (int) (byte) 100, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4849() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4849");
        NextDate nextDate3 = new NextDate(7, (int) (byte) -1, (-1));
    }

    @Test
    public void test4850() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4850");
        NextDate nextDate3 = new NextDate(30, 1, (int) (byte) -1);
        String str7 = nextDate3.run(0, (int) (short) 1, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4851() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4851");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) 'a', 12, (int) (byte) 10);
        String str11 = nextDate3.run(31, (int) (byte) 1, (int) (byte) 10);
        String str15 = nextDate3.run((int) (short) 10, (int) (short) 100, (int) (short) 1);
        String str19 = nextDate3.run((int) (byte) -1, (int) (short) 1, (int) (byte) 1);
        String str23 = nextDate3.run((int) ' ', 100, (-1));
        String str27 = nextDate3.run((int) '#', (int) ' ', 10);
        String str31 = nextDate3.run(31, (int) '4', 1);
        String str35 = nextDate3.run(12, 2001, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
    }

    @Test
    public void test4852() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4852");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) 10, 0);
    }

    @Test
    public void test4853() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4853");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 2001, 12);
        String str7 = nextDate3.run(30, (int) (byte) -1, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4854() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4854");
        NextDate nextDate3 = new NextDate((int) 'a', 30, (int) (short) 1);
    }

    @Test
    public void test4855() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4855");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 10, 12);
        String str7 = nextDate3.run(2020, (int) (byte) 100, (int) (byte) 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4856() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4856");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 1, (int) (short) -1);
        String str7 = nextDate3.run((int) (byte) 10, (int) (short) 0, 2001);
        String str11 = nextDate3.run((int) (byte) -1, (int) (byte) 0, 31);
        String str15 = nextDate3.run((int) ' ', (int) (short) 10, (int) (byte) 10);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4857() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4857");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 100, 0);
        String str7 = nextDate3.run(10, (int) (short) 10, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4858() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4858");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) -1, 7);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4859() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4859");
        NextDate nextDate3 = new NextDate(1, 100, (int) (byte) 10);
        String str7 = nextDate3.run((int) (short) -1, (int) '#', (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4860() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4860");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (short) 100, (int) (byte) 10);
        String str7 = nextDate3.run(7, (int) (short) 0, (int) '4');
        String str11 = nextDate3.run((int) (short) 1, (int) (short) 10, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4861() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4861");
        NextDate nextDate3 = new NextDate(12, (int) (byte) 1, (int) '#');
        String str7 = nextDate3.run((int) '#', 12, 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4862() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4862");
        NextDate nextDate3 = new NextDate((int) 'a', 0, 2001);
        String str7 = nextDate3.run((int) (short) 1, 0, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4863() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4863");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) 'a', (int) (short) 0);
        String str7 = nextDate3.run(31, (int) (short) 100, (int) (byte) 1);
        String str11 = nextDate3.run((int) (byte) 0, (int) (byte) 0, (int) (byte) 0);
        String str15 = nextDate3.run((int) '4', (int) ' ', (int) (short) 0);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4864() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4864");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 100, (int) (short) 10);
        String str7 = nextDate3.run(2001, 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4865() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4865");
        NextDate nextDate3 = new NextDate((int) (short) 10, 31, (int) (byte) -1);
    }

    @Test
    public void test4866() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4866");
        NextDate nextDate3 = new NextDate((int) (short) 100, 0, 1);
        String str7 = nextDate3.run(0, (int) ' ', 2001);
        String str11 = nextDate3.run(7, 31, 0);
        String str15 = nextDate3.run((int) (byte) 0, (int) (byte) -1, 1);
        String str19 = nextDate3.run((int) (byte) 1, (int) (short) -1, 30);
        String str23 = nextDate3.run(31, (int) (byte) 1, 0);
        String str27 = nextDate3.run(2001, (-1), (int) (short) -1);
        Class<?> wildcardClass28 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test4867() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4867");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 100, (int) (short) 1);
        String str7 = nextDate3.run((int) (short) 10, (int) '#', 12);
        String str11 = nextDate3.run((int) (short) -1, 10, (int) (byte) 10);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4868() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4868");
        NextDate nextDate3 = new NextDate((int) '4', 31, 2001);
        String str7 = nextDate3.run((int) 'a', (int) (short) 10, (int) '4');
        String str11 = nextDate3.run(0, 0, 31);
        String str15 = nextDate3.run((int) (short) 1, 7, 100);
        String str19 = nextDate3.run((int) (byte) 1, (int) (short) 0, 0);
        String str23 = nextDate3.run((int) (byte) 1, (int) ' ', (int) (byte) -1);
        Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test4869() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4869");
        NextDate nextDate3 = new NextDate(0, 0, 100);
        String str7 = nextDate3.run(100, (int) '#', 7);
        String str11 = nextDate3.run(12, (int) (byte) 100, (int) (byte) 100);
        String str15 = nextDate3.run((int) (short) 100, 31, (int) (byte) 10);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4870() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4870");
        NextDate nextDate3 = new NextDate(1, 12, 30);
    }

    @Test
    public void test4871() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4871");
        NextDate nextDate3 = new NextDate((int) (short) 1, 0, (int) (short) 0);
        String str7 = nextDate3.run((int) '4', (int) (byte) 1, 100);
        String str11 = nextDate3.run((int) '4', 2020, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4872() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4872");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 100, 1);
        String str7 = nextDate3.run(31, (int) 'a', (int) ' ');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4873() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4873");
        NextDate nextDate3 = new NextDate((int) '#', 100, 2001);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4874() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4874");
        NextDate nextDate3 = new NextDate(0, (int) (short) 0, 10);
    }

    @Test
    public void test4875() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4875");
        NextDate nextDate3 = new NextDate(7, 12, (int) (byte) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4876() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4876");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) -1, (int) (byte) 10);
    }

    @Test
    public void test4877() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4877");
        NextDate nextDate3 = new NextDate((int) '4', 7, 100);
        String str7 = nextDate3.run((-1), 31, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4878() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4878");
        NextDate nextDate3 = new NextDate(31, 2001, 2001);
        String str7 = nextDate3.run((int) '4', (int) 'a', 7);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4879() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4879");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) -1, (int) 'a');
    }

    @Test
    public void test4880() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4880");
        NextDate nextDate3 = new NextDate((int) (short) 100, 100, (int) (short) 0);
        String str7 = nextDate3.run((-1), (-1), (int) (short) 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4881() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4881");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) 'a', 30);
        String str7 = nextDate3.run((int) (short) 0, 31, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4882() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4882");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 100, (int) (byte) 10);
        String str7 = nextDate3.run(1, (int) '4', (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4883() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4883");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) -1, 10);
        String str7 = nextDate3.run(30, (int) (short) 100, 31);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4884() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4884");
        NextDate nextDate3 = new NextDate((int) '4', (int) ' ', (int) (byte) 10);
        String str7 = nextDate3.run(1, (int) ' ', (int) (short) 100);
        String str11 = nextDate3.run((int) (byte) 0, (int) 'a', (int) (short) 100);
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test4885() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4885");
        NextDate nextDate3 = new NextDate(30, 12, 7);
    }

    @Test
    public void test4886() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4886");
        NextDate nextDate3 = new NextDate(2020, 0, (-1));
        String str7 = nextDate3.run((int) (byte) 100, 2020, (int) 'a');
        String str11 = nextDate3.run((int) (byte) 10, 12, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4887() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4887");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 10, (int) (byte) 100);
        String str7 = nextDate3.run(2001, 0, (int) (byte) 100);
        String str11 = nextDate3.run(2001, 100, 100);
        String str15 = nextDate3.run((int) (byte) 1, (int) ' ', 2020);
        String str19 = nextDate3.run((int) (short) 0, 100, (-1));
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "2/1/2020" + "'", str15, "2/1/2020");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test4888() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4888");
        NextDate nextDate3 = new NextDate((-1), (int) ' ', 30);
    }

    @Test
    public void test4889() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4889");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) 0, (int) (short) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4890() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4890");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) -1, 31);
        String str7 = nextDate3.run(0, 0, 31);
        String str11 = nextDate3.run((int) (byte) 1, (int) 'a', (int) '4');
        String str15 = nextDate3.run((int) 'a', 31, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test4891() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4891");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (short) 10, 31);
        String str7 = nextDate3.run((int) (short) 100, 1, 1);
        String str11 = nextDate3.run((int) (byte) -1, 31, (int) '#');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4892() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4892");
        NextDate nextDate3 = new NextDate(2020, 1, (int) ' ');
    }

    @Test
    public void test4893() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4893");
        NextDate nextDate3 = new NextDate(31, (int) (short) -1, (int) (byte) 100);
        String str7 = nextDate3.run(31, (int) (byte) 100, 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4894() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4894");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 31, (int) '#');
        String str7 = nextDate3.run(7, (int) (short) -1, (int) (short) 1);
        String str11 = nextDate3.run((int) (byte) -1, (int) (short) 1, (int) (byte) -1);
        String str15 = nextDate3.run((int) (byte) 100, 10, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test4895() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4895");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 100, 0);
        String str7 = nextDate3.run((int) (short) -1, (int) (byte) 100, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4896() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4896");
        NextDate nextDate3 = new NextDate(7, 2020, 12);
        String str7 = nextDate3.run((int) (short) 0, 31, (int) (short) 100);
        String str11 = nextDate3.run(1, 30, (-1));
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4897() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4897");
        NextDate nextDate3 = new NextDate(10, 7, (int) (short) 10);
    }

    @Test
    public void test4898() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4898");
        NextDate nextDate3 = new NextDate(31, (int) '#', (int) 'a');
        String str7 = nextDate3.run((int) (short) 100, 0, (int) '#');
        String str11 = nextDate3.run(0, (int) (byte) 100, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4899() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4899");
        NextDate nextDate3 = new NextDate((int) ' ', 1, (int) '#');
        String str7 = nextDate3.run(10, (int) (byte) 1, (int) (short) 0);
        String str11 = nextDate3.run(0, 31, (int) (short) 100);
        String str15 = nextDate3.run((int) (short) -1, (int) (byte) 10, (int) (byte) 1);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4900() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4900");
        NextDate nextDate3 = new NextDate(7, 7, (int) (short) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4901() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4901");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run(7, (int) (short) 10, (int) (short) 1);
        String str27 = nextDate3.run((int) 'a', (int) (byte) 1, 0);
        String str31 = nextDate3.run((int) (byte) 0, (int) ' ', (int) (short) 10);
        String str35 = nextDate3.run((int) (short) -1, (int) '4', 7);
        String str39 = nextDate3.run(0, (int) (short) 10, 12);
        String str43 = nextDate3.run((int) (byte) -1, (int) (short) 0, (int) '4');
        String str47 = nextDate3.run(0, (int) (short) 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str43 + "' != '" + "invalid Input Date" + "'", str43, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str47 + "' != '" + "invalid Input Date" + "'", str47, "invalid Input Date");
    }

    @Test
    public void test4902() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4902");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) (byte) 0, (int) (byte) 10, 31);
        String str23 = nextDate3.run((int) (short) -1, 0, 7);
        String str27 = nextDate3.run(30, 12, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test4903() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4903");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 1, 0);
        String str7 = nextDate3.run(2001, (int) (byte) 100, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4904() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4904");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 0, (int) (short) 1);
        String str7 = nextDate3.run(0, (int) (byte) -1, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4905() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4905");
        NextDate nextDate3 = new NextDate(12, 1, (int) (short) -1);
        String str7 = nextDate3.run((int) (short) 0, 1, 12);
        String str11 = nextDate3.run((int) (byte) 10, 1, (int) (short) -1);
        String str15 = nextDate3.run((int) (byte) 10, 2001, (int) (byte) 0);
        String str19 = nextDate3.run((int) (short) 100, 7, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test4906() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4906");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 10, (int) (byte) -1);
        String str7 = nextDate3.run(2001, (int) (byte) -1, (int) (byte) -1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4907() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4907");
        NextDate nextDate3 = new NextDate(2001, 31, (int) (byte) -1);
        String str7 = nextDate3.run(0, 2020, 2001);
        String str11 = nextDate3.run((int) (byte) 10, (int) (byte) -1, (int) (short) 0);
        String str15 = nextDate3.run((int) ' ', 10, (int) (byte) -1);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4908() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4908");
        NextDate nextDate3 = new NextDate((int) '4', 31, (int) (short) 100);
    }

    @Test
    public void test4909() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4909");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 0, (int) 'a');
        String str7 = nextDate3.run((int) (short) 100, 2001, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4910() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4910");
        NextDate nextDate3 = new NextDate(31, (int) (short) 1, (int) (short) -1);
        String str7 = nextDate3.run((int) '4', 12, (int) (short) 10);
        String str11 = nextDate3.run((int) (short) -1, 7, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4911() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4911");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) -1, (int) (byte) 10);
        String str7 = nextDate3.run((int) '4', (int) (byte) 100, 7);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4912() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4912");
        NextDate nextDate3 = new NextDate(2020, 7, (int) (short) 10);
    }

    @Test
    public void test4913() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4913");
        NextDate nextDate3 = new NextDate((int) (short) 100, 12, 7);
    }

    @Test
    public void test4914() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4914");
        NextDate nextDate3 = new NextDate(2001, (int) (short) 0, 1);
        String str7 = nextDate3.run((int) (byte) 1, 1, 31);
        String str11 = nextDate3.run(31, (int) (byte) 0, (int) '#');
        String str15 = nextDate3.run((int) (short) 10, (int) ' ', (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test4915() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4915");
        NextDate nextDate3 = new NextDate(7, 7, (int) (byte) 1);
        String str7 = nextDate3.run(0, 12, 100);
        String str11 = nextDate3.run((int) 'a', 30, (int) (short) 10);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4916() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4916");
        NextDate nextDate3 = new NextDate(31, 12, 31);
    }

    @Test
    public void test4917() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4917");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) -1, 7);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4918() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4918");
        NextDate nextDate3 = new NextDate(0, 2001, 31);
        String str7 = nextDate3.run((int) '#', (int) (short) 100, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4919() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4919");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) 0, 12);
    }

    @Test
    public void test4920() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4920");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) -1, 100);
        String str7 = nextDate3.run((int) (short) 1, (int) (byte) 100, (int) '#');
        String str11 = nextDate3.run(2020, (int) (byte) 10, (int) (byte) 0);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4921() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4921");
        NextDate nextDate3 = new NextDate((int) (short) 10, 7, (int) (byte) 10);
        String str7 = nextDate3.run(2001, (int) (short) 1, (-1));
        String str11 = nextDate3.run(0, 7, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4922() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4922");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) ' ', 2020, (int) (short) 0);
        String str11 = nextDate3.run((int) (byte) -1, (int) (byte) -1, (int) (short) 0);
        String str15 = nextDate3.run((-1), 30, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test4923() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4923");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) 10, (int) 'a');
    }

    @Test
    public void test4924() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4924");
        NextDate nextDate3 = new NextDate(7, 12, (int) (short) 0);
        String str7 = nextDate3.run((int) 'a', 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4925() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4925");
        NextDate nextDate3 = new NextDate((int) (short) 100, 2001, 10);
        String str7 = nextDate3.run((int) (byte) 0, (int) 'a', (int) 'a');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4926() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4926");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) (byte) 0, (int) (byte) 10, 31);
        String str23 = nextDate3.run((-1), 0, (int) (byte) -1);
        String str27 = nextDate3.run((int) '4', (int) (short) -1, (int) '4');
        String str31 = nextDate3.run(0, (-1), (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
    }

    @Test
    public void test4927() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4927");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) 'a', 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4928() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4928");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 10, 0);
        String str7 = nextDate3.run(1, (int) (byte) 100, 1);
        String str11 = nextDate3.run((int) (short) 1, (-1), (int) (short) -1);
        String str15 = nextDate3.run(100, (int) (byte) 10, (int) (byte) 100);
        String str19 = nextDate3.run((int) '#', 0, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test4929() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4929");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) 10, (int) '#');
        String str7 = nextDate3.run(2020, 2020, 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4930() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4930");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) 10, 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4931() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4931");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 10, 7);
    }

    @Test
    public void test4932() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4932");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) 0, 1);
    }

    @Test
    public void test4933() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4933");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 100, 1);
        String str7 = nextDate3.run(31, 0, (int) (short) 10);
        String str11 = nextDate3.run((int) (short) -1, 0, 7);
        String str15 = nextDate3.run((int) '#', (int) (byte) -1, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test4934() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4934");
        NextDate nextDate3 = new NextDate(0, (int) ' ', 30);
        String str7 = nextDate3.run((int) (byte) -1, (int) 'a', 100);
        String str11 = nextDate3.run((int) (byte) 100, (int) (short) 0, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4935() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4935");
        NextDate nextDate3 = new NextDate(31, (int) (byte) 0, (int) (byte) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4936() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4936");
        NextDate nextDate3 = new NextDate(12, 1, (int) '#');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4937() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4937");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 1, 0);
    }

    @Test
    public void test4938() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4938");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) '4', (int) (byte) 1);
        String str7 = nextDate3.run((int) (short) 10, 31, (int) '#');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4939() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4939");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) -1, 0);
        String str7 = nextDate3.run((int) (short) 10, (int) (short) 1, 10);
        String str11 = nextDate3.run((int) (short) -1, (-1), 31);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4940() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4940");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 100, 2001);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4941() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4941");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 2001, 31);
        String str7 = nextDate3.run((-1), (int) '#', (int) '4');
        String str11 = nextDate3.run(1, (int) (short) -1, 0);
        String str15 = nextDate3.run(10, (int) (short) 10, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test4942() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4942");
        NextDate nextDate3 = new NextDate(7, (int) (short) 100, 1);
        String str7 = nextDate3.run((int) (short) 1, (int) (byte) 10, (int) (short) -1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4943() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4943");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run(7, (int) (short) 10, (int) (short) 1);
        String str27 = nextDate3.run((int) 'a', (int) (byte) 1, 0);
        String str31 = nextDate3.run((int) (byte) 0, (int) ' ', (int) (short) 10);
        String str35 = nextDate3.run(7, 10, 0);
        String str39 = nextDate3.run((int) '#', 30, 31);
        Class<?> wildcardClass40 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass40);
    }

    @Test
    public void test4944() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4944");
        NextDate nextDate3 = new NextDate(2001, 10, 12);
    }

    @Test
    public void test4945() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4945");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 0, (-1));
        String str7 = nextDate3.run((-1), (int) '#', 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4946() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4946");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) 'a', 12);
        String str7 = nextDate3.run((int) (byte) 0, 1, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4947() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4947");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 1, 0);
        String str7 = nextDate3.run((int) '4', 0, (int) (byte) 10);
        String str11 = nextDate3.run(31, 0, (int) (byte) -1);
        String str15 = nextDate3.run((int) (byte) -1, (int) (byte) 0, (int) (short) 1);
        String str19 = nextDate3.run((int) (short) -1, 2001, (int) (short) -1);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test4948() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4948");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (short) 0, (int) (short) 1);
    }

    @Test
    public void test4949() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4949");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) 10, 1);
        String str7 = nextDate3.run(0, (int) (byte) 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4950() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4950");
        NextDate nextDate3 = new NextDate((int) 'a', 0, (int) (short) 100);
    }

    @Test
    public void test4951() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4951");
        NextDate nextDate3 = new NextDate(1, (int) (short) 1, (int) (byte) 100);
    }

    @Test
    public void test4952() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4952");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 12, 0);
    }

    @Test
    public void test4953() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4953");
        NextDate nextDate3 = new NextDate((int) '4', 31, 2001);
        String str7 = nextDate3.run((int) (byte) 1, 12, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4954() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4954");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 10, 30);
    }

    @Test
    public void test4955() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4955");
        NextDate nextDate3 = new NextDate((int) ' ', (int) ' ', 2001);
        String str7 = nextDate3.run((int) (short) 10, 12, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4956() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4956");
        NextDate nextDate3 = new NextDate((int) (short) 10, 0, 2001);
        String str7 = nextDate3.run(12, (int) (short) 10, (int) (byte) -1);
        String str11 = nextDate3.run(2020, 12, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4957() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4957");
        NextDate nextDate3 = new NextDate(0, (int) (short) 10, (int) '#');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4958() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4958");
        NextDate nextDate3 = new NextDate((int) '#', 10, 7);
        String str7 = nextDate3.run(0, (int) '4', 2020);
        String str11 = nextDate3.run((int) (byte) 0, 12, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4959() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4959");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 1, (int) (short) 0);
        String str7 = nextDate3.run((int) (short) 1, (int) ' ', 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4960() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4960");
        NextDate nextDate3 = new NextDate(7, (int) (short) 10, (int) (short) 100);
    }

    @Test
    public void test4961() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4961");
        NextDate nextDate3 = new NextDate((int) ' ', (int) 'a', 10);
        String str7 = nextDate3.run((int) (byte) 0, 2001, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4962() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4962");
        NextDate nextDate3 = new NextDate((-1), (int) '#', 12);
        String str7 = nextDate3.run((int) (short) -1, 7, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4963() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4963");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) '#', (int) '4');
    }

    @Test
    public void test4964() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4964");
        NextDate nextDate3 = new NextDate((int) ' ', 0, 7);
        String str7 = nextDate3.run((int) (short) 1, (int) (short) 1, (int) (short) 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4965() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4965");
        NextDate nextDate3 = new NextDate(100, (int) (byte) -1, 10);
        String str7 = nextDate3.run((int) (short) 1, (int) (short) 10, 31);
        String str11 = nextDate3.run((int) (byte) 100, (int) (byte) 0, (int) (byte) 10);
        String str15 = nextDate3.run((int) (short) 10, (int) '#', (int) 'a');
        String str19 = nextDate3.run((int) (short) 100, (int) (short) 0, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test4966() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4966");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run(7, (int) (short) 10, (int) (short) 1);
        String str27 = nextDate3.run((int) 'a', (int) (byte) 1, 0);
        String str31 = nextDate3.run((int) (byte) 0, (int) ' ', (int) (short) 10);
        String str35 = nextDate3.run((int) ' ', (int) (short) 1, (int) (byte) 1);
        String str39 = nextDate3.run(100, 2020, 2020);
        String str43 = nextDate3.run((int) (short) 10, (int) (short) -1, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str43 + "' != '" + "invalid Input Date" + "'", str43, "invalid Input Date");
    }

    @Test
    public void test4967() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4967");
        NextDate nextDate3 = new NextDate(31, (int) (byte) 1, (int) ' ');
        String str7 = nextDate3.run(31, 0, 2001);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4968() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4968");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) '#', (int) (byte) 1);
        String str7 = nextDate3.run((int) '4', (int) (byte) 0, (int) (byte) 100);
        String str11 = nextDate3.run(7, 0, (int) 'a');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4969() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4969");
        NextDate nextDate3 = new NextDate(1, (int) (short) -1, (int) '4');
        String str7 = nextDate3.run(10, 100, 0);
        String str11 = nextDate3.run(0, (int) (short) -1, (int) (short) -1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4970() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4970");
        NextDate nextDate3 = new NextDate(10, 2020, 1);
        String str7 = nextDate3.run(7, 12, (int) (byte) 0);
        String str11 = nextDate3.run(0, (int) (byte) 10, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4971() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4971");
        NextDate nextDate3 = new NextDate(7, (int) '4', 2020);
    }

    @Test
    public void test4972() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4972");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 31, (int) '4');
        String str7 = nextDate3.run((int) (short) 100, (int) (short) 10, (int) (short) 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4973() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4973");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) ' ', (int) (short) 1);
        String str7 = nextDate3.run((int) '4', (int) '#', (int) (short) 0);
        String str11 = nextDate3.run(0, (int) (byte) -1, (int) (short) 0);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4974() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4974");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 10, (int) (byte) 10);
        String str7 = nextDate3.run(31, (int) (byte) 100, (int) (byte) 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4975() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4975");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 0, (int) (byte) 1);
        String str7 = nextDate3.run((int) (short) 100, (int) (byte) 1, (int) '#');
        String str11 = nextDate3.run((int) (short) 0, (int) '#', 30);
        String str15 = nextDate3.run((int) (byte) 10, 31, (int) ' ');
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4976() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4976");
        NextDate nextDate3 = new NextDate(12, (int) (short) 10, (int) (byte) 10);
        String str7 = nextDate3.run((int) 'a', (int) (short) 100, 2020);
        String str11 = nextDate3.run((int) (short) 0, 2001, (int) (short) -1);
        String str15 = nextDate3.run((int) (short) 100, (int) (byte) 0, (int) (short) 10);
        String str19 = nextDate3.run((int) (short) 10, (int) '4', 10);
        String str23 = nextDate3.run((int) (short) 0, (int) (short) -1, (int) (byte) 100);
        String str27 = nextDate3.run((int) (short) 100, (int) (byte) -1, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test4977() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4977");
        NextDate nextDate3 = new NextDate(10, (int) '4', 0);
        String str7 = nextDate3.run(10, 100, 1);
        String str11 = nextDate3.run(100, 10, (int) (short) -1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4978() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4978");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) 1, 100);
        String str7 = nextDate3.run((-1), (-1), 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4979() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4979");
        NextDate nextDate3 = new NextDate((int) ' ', (int) '#', (int) ' ');
        String str7 = nextDate3.run(7, (int) (short) 100, (int) (short) 1);
        String str11 = nextDate3.run((int) '4', 1, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test4980() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4980");
        NextDate nextDate3 = new NextDate(2001, 31, (int) (byte) -1);
        String str7 = nextDate3.run(0, 2020, 2001);
        String str11 = nextDate3.run((int) (byte) 10, (int) (byte) -1, (int) (short) 0);
        String str15 = nextDate3.run((int) ' ', 10, (int) (byte) -1);
        String str19 = nextDate3.run((int) '#', (int) (short) -1, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test4981() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4981");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 0, 10);
        String str7 = nextDate3.run(2001, (int) '4', 12);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4982() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4982");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 10, (-1));
        String str7 = nextDate3.run((-1), (int) '#', (int) (byte) 100);
        String str11 = nextDate3.run((int) (short) 100, 2001, (int) '#');
        String str15 = nextDate3.run(0, 1, (int) '4');
        String str19 = nextDate3.run((int) (short) 100, (int) (short) 100, (-1));
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test4983() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4983");
        NextDate nextDate3 = new NextDate(2020, 2020, (int) (short) -1);
        String str7 = nextDate3.run((int) (short) 1, (int) (byte) 100, (int) (byte) -1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test4984() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4984");
        NextDate nextDate3 = new NextDate(0, 1, 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4985() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4985");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) '#', (int) (short) 100);
        String str7 = nextDate3.run((int) (byte) 10, (int) (byte) 100, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4986() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4986");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 0, (int) (short) 10);
        String str7 = nextDate3.run((int) (short) 0, 1, 10);
        String str11 = nextDate3.run((int) (short) 1, (int) (byte) 10, 7);
        String str15 = nextDate3.run((int) (byte) 10, 30, 12);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test4987() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4987");
        NextDate nextDate3 = new NextDate(2001, (int) '#', 100);
    }

    @Test
    public void test4988() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4988");
        NextDate nextDate3 = new NextDate(1, (int) (byte) 10, (int) (byte) -1);
    }

    @Test
    public void test4989() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4989");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4990() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4990");
        NextDate nextDate3 = new NextDate((int) (short) 1, 7, 2001);
        String str7 = nextDate3.run((int) (short) 10, (int) ' ', 0);
        String str11 = nextDate3.run((int) (short) 1, (int) ' ', 7);
        String str15 = nextDate3.run((int) 'a', 1, 2020);
        String str19 = nextDate3.run((int) (short) 100, (int) 'a', (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test4991() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4991");
        NextDate nextDate3 = new NextDate(0, 10, (int) (byte) 1);
        String str7 = nextDate3.run((int) ' ', 100, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4992() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4992");
        NextDate nextDate3 = new NextDate((int) (short) 0, 10, 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test4993() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4993");
        NextDate nextDate3 = new NextDate((int) '#', 0, (int) (byte) 1);
    }

    @Test
    public void test4994() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4994");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) 100, (int) (short) 1);
    }

    @Test
    public void test4995() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4995");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) '4', (int) (short) 100);
    }

    @Test
    public void test4996() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4996");
        NextDate nextDate3 = new NextDate((-1), (int) (short) -1, 31);
        String str7 = nextDate3.run((int) (byte) 100, 30, (int) '4');
        String str11 = nextDate3.run(30, 10, (int) (byte) 1);
        String str15 = nextDate3.run((int) (byte) 1, 2001, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test4997() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4997");
        NextDate nextDate3 = new NextDate(0, (int) ' ', (int) (short) 0);
        String str7 = nextDate3.run(0, (int) (byte) 0, 12);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, (int) (short) 0);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test4998() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4998");
        NextDate nextDate3 = new NextDate(30, 7, 10);
        String str7 = nextDate3.run((int) (byte) 10, (int) 'a', (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test4999() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test4999");
        NextDate nextDate3 = new NextDate(12, (int) (byte) 100, (int) (short) 100);
    }

    @Test
    public void test5000() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest9.test5000");
        NextDate nextDate3 = new NextDate(100, (int) (byte) -1, 10);
        String str7 = nextDate3.run((int) (short) 1, (int) (short) 10, 31);
        String str11 = nextDate3.run((int) (byte) 100, (int) (byte) 0, (int) (byte) 10);
        String str15 = nextDate3.run((int) (short) 10, (int) '#', (int) 'a');
        String str19 = nextDate3.run((int) '#', 1, 31);
        String str23 = nextDate3.run(0, (int) (short) 100, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }
}

