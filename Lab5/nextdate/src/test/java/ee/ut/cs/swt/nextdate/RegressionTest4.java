package ee.ut.cs.swt.nextdate;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest4 {

    public static boolean debug = false;

    @Test
    public void test2001() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2001");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (short) 1, (int) (short) -1);
    }

    @Test
    public void test2002() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2002");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) 100, 30);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2003() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2003");
        NextDate nextDate3 = new NextDate(2001, (int) '4', (int) '#');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2004() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2004");
        NextDate nextDate3 = new NextDate((int) '4', (int) ' ', (int) (byte) 10);
        String str7 = nextDate3.run(1, (int) ' ', (int) (short) 100);
        String str11 = nextDate3.run(100, (int) (short) 0, (int) ' ');
        String str15 = nextDate3.run(2020, (int) (byte) 1, (int) ' ');
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test2005() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2005");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 1, 0);
        String str7 = nextDate3.run((-1), 100, (int) (byte) -1);
        String str11 = nextDate3.run((int) 'a', 10, (int) (byte) 10);
        String str15 = nextDate3.run((int) 'a', (int) (byte) -1, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test2006() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2006");
        NextDate nextDate3 = new NextDate((int) '#', (int) '4', 12);
        String str7 = nextDate3.run(2020, (int) (byte) 0, (int) (byte) 1);
        String str11 = nextDate3.run(100, (int) ' ', 31);
        String str15 = nextDate3.run(100, (int) (short) 10, (int) (byte) 100);
        String str19 = nextDate3.run(7, (-1), (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test2007() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2007");
        NextDate nextDate3 = new NextDate((int) ' ', 100, 31);
        String str7 = nextDate3.run((int) ' ', 7, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2008() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2008");
        NextDate nextDate3 = new NextDate(10, (int) (byte) -1, 30);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2009() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2009");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 0, (int) (short) 10);
    }

    @Test
    public void test2010() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2010");
        NextDate nextDate3 = new NextDate(2001, (int) (short) 0, (int) '4');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2011() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2011");
        NextDate nextDate3 = new NextDate(12, (int) (short) 10, (int) (byte) 10);
        String str7 = nextDate3.run((int) 'a', (int) (short) 100, 2020);
        String str11 = nextDate3.run((int) (short) 1, 0, (int) (short) 10);
        String str15 = nextDate3.run((int) (short) 10, 2001, 0);
        String str19 = nextDate3.run(31, (int) '#', 0);
        String str23 = nextDate3.run(0, 100, 0);
        String str27 = nextDate3.run((int) '4', 7, 10);
        Class<?> wildcardClass28 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test2012() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2012");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 31, (int) (byte) 100);
        String str7 = nextDate3.run((int) '#', (int) 'a', 31);
        String str11 = nextDate3.run((-1), (int) (byte) 1, (int) (byte) 100);
        String str15 = nextDate3.run(100, 100, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test2013() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2013");
        NextDate nextDate3 = new NextDate((int) ' ', (int) ' ', (int) '#');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2014() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2014");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) 'a', (-1));
    }

    @Test
    public void test2015() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2015");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) '#', 31);
    }

    @Test
    public void test2016() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2016");
        NextDate nextDate3 = new NextDate(1, 10, 31);
        String str7 = nextDate3.run((int) (byte) 1, (int) (byte) 10, (int) (short) 100);
        String str11 = nextDate3.run(0, (int) (short) 1, 12);
        String str15 = nextDate3.run(10, (int) ' ', 1);
        String str19 = nextDate3.run(100, (int) (byte) 0, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test2017() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2017");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 0, (int) '#');
    }

    @Test
    public void test2018() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2018");
        NextDate nextDate3 = new NextDate((int) 'a', 30, (int) (byte) 0);
    }

    @Test
    public void test2019() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2019");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 7, (int) (byte) 0);
        String str7 = nextDate3.run((int) (short) -1, (int) (short) -1, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2020() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2020");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 1, (-1));
        String str7 = nextDate3.run((-1), (int) '4', (int) (short) 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2021() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2021");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 100, (int) (byte) -1);
        String str7 = nextDate3.run((int) (short) 10, (int) (short) 100, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2022() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2022");
        NextDate nextDate3 = new NextDate(2001, (int) (short) 1, 2001);
        String str7 = nextDate3.run((int) (short) 1, (int) '#', (int) (byte) -1);
        String str11 = nextDate3.run((int) 'a', (int) 'a', 7);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2023() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2023");
        NextDate nextDate3 = new NextDate(2020, 30, 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2024() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2024");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 100, 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2025() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2025");
        NextDate nextDate3 = new NextDate(10, 0, (int) (short) 10);
        String str7 = nextDate3.run(31, (int) '#', (int) (byte) 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2026() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2026");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 10, 2001);
    }

    @Test
    public void test2027() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2027");
        NextDate nextDate3 = new NextDate((int) (short) 100, 0, (int) (short) -1);
        String str7 = nextDate3.run((int) 'a', (int) (byte) 0, 100);
        String str11 = nextDate3.run(7, (int) (short) 100, (int) (short) 1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2028() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2028");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 100, (int) '4');
        String str7 = nextDate3.run(100, 0, 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2029() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2029");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) 100, (int) (byte) 100);
        String str7 = nextDate3.run((int) (byte) -1, (int) (short) 10, (int) (short) 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2030() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2030");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 0, 1);
        String str7 = nextDate3.run((int) (byte) 100, 2020, (-1));
        String str11 = nextDate3.run(2020, (int) '4', (int) (short) 100);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2031() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2031");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 100, 2001);
    }

    @Test
    public void test2032() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2032");
        NextDate nextDate3 = new NextDate(30, (int) '4', (int) 'a');
        String str7 = nextDate3.run(30, 31, (int) (byte) 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2033() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2033");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 100, (int) (byte) 10);
        String str7 = nextDate3.run(100, (int) (byte) -1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2034() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2034");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 0, 30);
        String str7 = nextDate3.run((int) (short) 10, 0, (int) (byte) -1);
        String str11 = nextDate3.run((int) ' ', (int) (byte) 10, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2035() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2035");
        NextDate nextDate3 = new NextDate(10, (int) '#', 0);
        String str7 = nextDate3.run((int) '4', (int) (short) 10, 7);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2036() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2036");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) -1, 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2037() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2037");
        NextDate nextDate3 = new NextDate((-1), (int) '4', (-1));
        String str7 = nextDate3.run((int) (short) 0, (int) (short) 100, 0);
        String str11 = nextDate3.run(1, 10, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2038() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2038");
        NextDate nextDate3 = new NextDate(1, (int) '4', (int) (short) 10);
        String str7 = nextDate3.run((int) (short) 10, (int) (short) 0, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2039() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2039");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 2001, 31);
        String str7 = nextDate3.run((-1), (int) '#', (int) '4');
        String str11 = nextDate3.run(1, (int) (short) -1, 0);
        String str15 = nextDate3.run((int) (byte) 10, (int) ' ', (int) '#');
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test2040() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2040");
        NextDate nextDate3 = new NextDate((int) (short) 100, 100, 12);
        String str7 = nextDate3.run(1, (int) (byte) 0, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2041() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2041");
        NextDate nextDate3 = new NextDate(100, (int) 'a', (int) (short) 100);
        String str7 = nextDate3.run((int) (short) -1, (int) '4', 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2042() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2042");
        NextDate nextDate3 = new NextDate(30, 0, 10);
        String str7 = nextDate3.run((int) (short) 100, (int) ' ', (-1));
        String str11 = nextDate3.run((int) ' ', 31, (int) (short) 100);
        String str15 = nextDate3.run(30, (int) (byte) 0, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test2043() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2043");
        NextDate nextDate3 = new NextDate(12, 12, (int) '4');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2044() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2044");
        NextDate nextDate3 = new NextDate(2020, (int) '#', (int) '4');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2045() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2045");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) 1, 12);
        String str7 = nextDate3.run(30, (int) ' ', 12);
        String str11 = nextDate3.run((int) (short) 100, (int) (byte) 10, 7);
        String str15 = nextDate3.run((int) (byte) 100, (int) '#', 2001);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test2046() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2046");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (short) -1, 7);
        String str7 = nextDate3.run((int) (short) -1, (int) (byte) 0, (int) 'a');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2047() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2047");
        NextDate nextDate3 = new NextDate((int) (short) -1, (-1), 31);
    }

    @Test
    public void test2048() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2048");
        NextDate nextDate3 = new NextDate((int) (short) 1, 2020, 10);
    }

    @Test
    public void test2049() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2049");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 100, 0);
        String str7 = nextDate3.run(1, 7, 0);
        String str11 = nextDate3.run((int) (short) 100, 12, (int) (short) 1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2050() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2050");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 0, (int) (short) 1);
        String str7 = nextDate3.run((int) (byte) 10, 10, (int) (byte) -1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2051() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2051");
        NextDate nextDate3 = new NextDate((int) '4', 1, (int) '4');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2052() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2052");
        NextDate nextDate3 = new NextDate((int) 'a', (int) ' ', (int) (short) 10);
        String str7 = nextDate3.run((int) ' ', 0, (int) (byte) -1);
        String str11 = nextDate3.run((int) (byte) 100, (int) (byte) 0, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2053() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2053");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 10, (int) '#');
        String str7 = nextDate3.run(100, 0, (int) (byte) 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2054() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2054");
        NextDate nextDate3 = new NextDate((int) '#', 31, (int) (byte) 0);
    }

    @Test
    public void test2055() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2055");
        NextDate nextDate3 = new NextDate(7, (int) (byte) 0, 7);
        String str7 = nextDate3.run((int) (short) 10, 12, 31);
        String str11 = nextDate3.run((-1), 100, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2056() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2056");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 12, (int) (byte) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2057() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2057");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) ' ', 2020, (int) (short) 0);
        String str11 = nextDate3.run(30, (int) (byte) 100, (int) (byte) 100);
        String str15 = nextDate3.run((int) (byte) 100, 31, (int) (short) -1);
        String str19 = nextDate3.run((int) (byte) 100, (-1), (int) (short) -1);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test2058() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2058");
        NextDate nextDate3 = new NextDate(7, (int) (short) 10, (int) (short) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2059() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2059");
        NextDate nextDate3 = new NextDate(2001, 2020, (int) (byte) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2060() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2060");
        NextDate nextDate3 = new NextDate(10, (int) ' ', (int) 'a');
    }

    @Test
    public void test2061() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2061");
        NextDate nextDate3 = new NextDate(1, (int) (byte) -1, (int) (short) -1);
        String str7 = nextDate3.run(2020, (int) (byte) 1, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2062() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2062");
        NextDate nextDate3 = new NextDate((int) '4', (int) '4', 1);
        String str7 = nextDate3.run(0, (int) (short) 10, 1);
        String str11 = nextDate3.run((int) 'a', 31, 0);
        String str15 = nextDate3.run(1, 30, (int) (byte) 100);
        String str19 = nextDate3.run((int) (byte) 1, (int) (short) 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test2063() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2063");
        NextDate nextDate3 = new NextDate(0, (int) 'a', (int) (short) 100);
        String str7 = nextDate3.run((int) 'a', 30, 12);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2064() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2064");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) -1, 2001);
        String str7 = nextDate3.run(0, 1, 10);
        String str11 = nextDate3.run(2001, 2001, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2065() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2065");
        NextDate nextDate3 = new NextDate(1, 10, 31);
        String str7 = nextDate3.run((int) '#', 0, 0);
        String str11 = nextDate3.run(10, 12, (int) '#');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2066() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2066");
        NextDate nextDate3 = new NextDate(100, 0, 0);
        String str7 = nextDate3.run(12, (int) (short) 0, (int) (byte) 1);
        String str11 = nextDate3.run(100, (int) (short) 100, (int) (short) 10);
        String str15 = nextDate3.run((int) (short) -1, (int) '#', 30);
        String str19 = nextDate3.run((int) 'a', 7, (int) (byte) -1);
        String str23 = nextDate3.run((int) '#', (int) (byte) -1, 100);
        Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test2067() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2067");
        NextDate nextDate3 = new NextDate((int) 'a', 0, (int) (short) -1);
        String str7 = nextDate3.run((int) ' ', 0, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2068() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2068");
        NextDate nextDate3 = new NextDate(10, (int) (short) 0, 10);
        String str7 = nextDate3.run(1, (int) (short) 0, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2069() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2069");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) '4', 0);
        String str7 = nextDate3.run(12, 1, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2070() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2070");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 0, (int) (byte) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2071() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2071");
        NextDate nextDate3 = new NextDate((int) '4', (int) ' ', (int) (short) 0);
        String str7 = nextDate3.run(100, (-1), 0);
        String str11 = nextDate3.run((int) '#', 30, 100);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2072() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2072");
        NextDate nextDate3 = new NextDate(0, 2001, (int) (byte) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2073() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2073");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 0, (int) (byte) 1);
        String str7 = nextDate3.run(31, 30, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2074() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2074");
        NextDate nextDate3 = new NextDate((int) '4', 7, 30);
        String str7 = nextDate3.run(1, (int) (byte) 100, 30);
        String str11 = nextDate3.run((-1), 0, 12);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2075() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2075");
        NextDate nextDate3 = new NextDate(30, (int) (byte) -1, (int) (byte) 10);
    }

    @Test
    public void test2076() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2076");
        NextDate nextDate3 = new NextDate((int) (short) 100, 0, 1);
        String str7 = nextDate3.run(0, (int) ' ', 2001);
        String str11 = nextDate3.run((int) '4', (int) (byte) 1, (int) (byte) 0);
        String str15 = nextDate3.run(10, 12, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "10/13/2001" + "'", str15, "10/13/2001");
    }

    @Test
    public void test2077() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2077");
        NextDate nextDate3 = new NextDate((int) (short) 1, 7, (int) (byte) 10);
    }

    @Test
    public void test2078() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2078");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) ' ', 12);
        String str7 = nextDate3.run(30, 12, 12);
        String str11 = nextDate3.run((int) (short) 0, (int) '#', (int) '#');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2079() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2079");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 0, (int) (short) 1);
        String str7 = nextDate3.run(2001, (int) ' ', (-1));
        String str11 = nextDate3.run(0, (int) 'a', 10);
        String str15 = nextDate3.run(10, (int) (short) -1, 100);
        String str19 = nextDate3.run((int) '#', 2020, 2020);
        String str23 = nextDate3.run((int) (short) 100, (int) ' ', (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test2080() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2080");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 7, (int) (byte) 1);
        String str7 = nextDate3.run(30, (int) (short) 100, 100);
        String str11 = nextDate3.run(10, 1, (int) '4');
        String str15 = nextDate3.run(1, (int) (byte) 1, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test2081() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2081");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) 100, (int) (byte) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2082() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2082");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) 'a', 12, (int) (byte) 10);
        String str11 = nextDate3.run(31, (int) (byte) 1, (int) (byte) 10);
        String str15 = nextDate3.run(30, (int) (short) 10, (int) (byte) 10);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test2083() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2083");
        NextDate nextDate3 = new NextDate((int) '#', 10, (int) (byte) 0);
        String str7 = nextDate3.run(100, 12, 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2084() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2084");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) '#', (int) (byte) 100);
        String str7 = nextDate3.run((int) (byte) -1, 100, 2020);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2085() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2085");
        NextDate nextDate3 = new NextDate((int) '4', (int) (byte) 100, (-1));
        String str7 = nextDate3.run((int) (byte) 100, 7, 2020);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2086() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2086");
        NextDate nextDate3 = new NextDate((int) '#', 7, 0);
    }

    @Test
    public void test2087() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2087");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 30, (int) (short) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2088() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2088");
        NextDate nextDate3 = new NextDate(12, (int) (short) 100, (int) ' ');
        String str7 = nextDate3.run(2020, 7, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2089() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2089");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        String str15 = nextDate3.run((int) (byte) 1, (int) (short) 0, (int) (short) 100);
        String str19 = nextDate3.run((int) (short) 10, 0, 10);
        String str23 = nextDate3.run(2020, (-1), (int) (byte) 10);
        String str27 = nextDate3.run(7, 31, 100);
        Class<?> wildcardClass28 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test2090() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2090");
        NextDate nextDate3 = new NextDate(2020, 2020, (int) (byte) 0);
    }

    @Test
    public void test2091() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2091");
        NextDate nextDate3 = new NextDate((int) (short) 0, 7, 10);
        String str7 = nextDate3.run((int) (short) 10, 1, 0);
        String str11 = nextDate3.run((int) (short) 0, 31, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2092() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2092");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 10, (int) (short) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2093() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2093");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 10, 10);
    }

    @Test
    public void test2094() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2094");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 10, 100);
    }

    @Test
    public void test2095() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2095");
        NextDate nextDate3 = new NextDate((int) (short) 10, 12, 0);
        String str7 = nextDate3.run((int) (byte) 1, (int) '#', 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2096() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2096");
        NextDate nextDate3 = new NextDate(2020, (int) (short) 100, (int) (byte) -1);
    }

    @Test
    public void test2097() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2097");
        NextDate nextDate3 = new NextDate(2001, (int) '4', (int) '#');
        String str7 = nextDate3.run((int) (byte) 10, 30, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2098() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2098");
        NextDate nextDate3 = new NextDate(0, 2020, (int) (short) 100);
        String str7 = nextDate3.run(31, 0, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2099() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2099");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) 10, (int) (short) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2100() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2100");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 1, 10);
    }

    @Test
    public void test2101() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2101");
        NextDate nextDate3 = new NextDate((int) (short) -1, 12, 1);
        String str7 = nextDate3.run((int) (short) 100, 1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2102() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2102");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) 1, 2020);
        String str7 = nextDate3.run((int) (short) 10, 12, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2103() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2103");
        NextDate nextDate3 = new NextDate(100, (-1), (int) (byte) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2104() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2104");
        NextDate nextDate3 = new NextDate(2020, (int) (short) 0, 12);
        String str7 = nextDate3.run((int) (short) -1, (int) (byte) 0, (int) (short) 0);
        String str11 = nextDate3.run((int) (short) 0, 30, 2001);
        String str15 = nextDate3.run((int) (short) 10, (int) (byte) 0, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test2105() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2105");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 0, (int) '#');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2106() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2106");
        NextDate nextDate3 = new NextDate((int) (short) 100, 0, (int) (short) -1);
        String str7 = nextDate3.run((int) 'a', (int) (byte) 0, 100);
        String str11 = nextDate3.run(7, (int) (short) 100, (int) (short) 1);
        String str15 = nextDate3.run((int) (short) 100, 0, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test2107() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2107");
        NextDate nextDate3 = new NextDate((int) (short) 10, 30, 2001);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2108() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2108");
        NextDate nextDate3 = new NextDate((int) (short) 10, 12, 10);
        String str7 = nextDate3.run(100, (int) (byte) 10, (int) (short) 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2109() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2109");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) 'a', 12);
        String str7 = nextDate3.run(0, 10, 2020);
        String str11 = nextDate3.run((int) (short) 1, 0, 10);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2110() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2110");
        NextDate nextDate3 = new NextDate((int) (short) 10, 7, 100);
    }

    @Test
    public void test2111() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2111");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) '#', (int) (short) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2112() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2112");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 1, 12);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2113() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2113");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 0, (int) (byte) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2114() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2114");
        NextDate nextDate3 = new NextDate(31, 2001, 2001);
        String str7 = nextDate3.run((int) ' ', (int) (byte) -1, (-1));
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2115() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2115");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) ' ', (int) '4');
    }

    @Test
    public void test2116() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2116");
        NextDate nextDate3 = new NextDate(1, (-1), (-1));
        String str7 = nextDate3.run((int) (short) -1, (int) (byte) 100, 2001);
        String str11 = nextDate3.run((int) (short) 1, 0, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2117() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2117");
        NextDate nextDate3 = new NextDate((int) '#', (int) ' ', 30);
        String str7 = nextDate3.run((int) (byte) 10, 10, (int) (byte) 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2118() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2118");
        NextDate nextDate3 = new NextDate(2001, (int) ' ', (int) (short) 0);
    }

    @Test
    public void test2119() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2119");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) 'a', 12, (int) (byte) 10);
        String str11 = nextDate3.run(31, (int) (byte) 1, (int) (byte) 10);
        String str15 = nextDate3.run((int) (short) 10, (int) (byte) 0, (int) 'a');
        String str19 = nextDate3.run(2001, (-1), 1);
        String str23 = nextDate3.run((int) (byte) 100, (int) (short) 1, (int) 'a');
        String str27 = nextDate3.run((int) (short) 1, 0, (int) (byte) 0);
        String str31 = nextDate3.run((int) (short) 0, 100, (int) ' ');
        String str35 = nextDate3.run((int) '#', (-1), 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
    }

    @Test
    public void test2120() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2120");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) 10, 30);
    }

    @Test
    public void test2121() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2121");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 10, (int) (byte) 100);
    }

    @Test
    public void test2122() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2122");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 100, (int) (byte) 100);
    }

    @Test
    public void test2123() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2123");
        NextDate nextDate3 = new NextDate(2001, 2001, 7);
        String str7 = nextDate3.run((int) ' ', 1, (int) (byte) -1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2124() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2124");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 10, (int) ' ');
        String str7 = nextDate3.run((int) (byte) 1, 2001, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2125() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2125");
        NextDate nextDate3 = new NextDate((int) '4', (int) (byte) 100, 0);
        String str7 = nextDate3.run((int) ' ', (int) ' ', (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2126() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2126");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) '#', (int) (short) -1);
        String str7 = nextDate3.run((int) (short) 0, (int) (byte) -1, (int) '#');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2127() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2127");
        NextDate nextDate3 = new NextDate(1, 30, (int) (byte) 0);
        String str7 = nextDate3.run(2001, (int) '#', (int) '4');
        String str11 = nextDate3.run(0, 0, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2128() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2128");
        NextDate nextDate3 = new NextDate((int) '4', (int) (byte) 10, (int) ' ');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2129() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2129");
        NextDate nextDate3 = new NextDate((int) '#', 100, (int) (short) 1);
        String str7 = nextDate3.run((int) (short) -1, 0, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2130() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2130");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 0, 7);
        String str7 = nextDate3.run(12, 1, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2131() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2131");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) 'a', 12, (int) (byte) 10);
        String str11 = nextDate3.run(31, (int) (byte) 1, (int) (byte) 10);
        String str15 = nextDate3.run((int) (short) 10, (int) (byte) 0, (int) 'a');
        String str19 = nextDate3.run(2001, (-1), 1);
        String str23 = nextDate3.run((int) (byte) 100, (int) (short) 1, (int) 'a');
        String str27 = nextDate3.run((int) (short) 1, 0, (int) (byte) 0);
        String str31 = nextDate3.run((int) (short) 0, 100, (int) ' ');
        String str35 = nextDate3.run(7, 0, (int) '4');
        Class<?> wildcardClass36 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass36);
    }

    @Test
    public void test2132() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2132");
        NextDate nextDate3 = new NextDate(12, (int) 'a', (int) (short) 0);
        String str7 = nextDate3.run((int) (short) 10, 1, (int) (byte) -1);
        String str11 = nextDate3.run(30, (int) '#', 30);
        String str15 = nextDate3.run(2020, 0, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test2133() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2133");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) -1, 2001);
        String str7 = nextDate3.run((int) (short) 0, 1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2134() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2134");
        NextDate nextDate3 = new NextDate((-1), 100, (int) (byte) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2135() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2135");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 100, 2020);
        String str7 = nextDate3.run((int) (short) 0, (-1), 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2136() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2136");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 31, 0);
        String str7 = nextDate3.run((int) (short) 10, 100, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2137() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2137");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 1, 7);
    }

    @Test
    public void test2138() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2138");
        NextDate nextDate3 = new NextDate(1, (int) (byte) 10, (int) '#');
    }

    @Test
    public void test2139() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2139");
        NextDate nextDate3 = new NextDate(0, 1, 7);
        String str7 = nextDate3.run((int) (short) 100, 31, (int) (short) 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2140() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2140");
        NextDate nextDate3 = new NextDate((int) (short) 10, 7, 0);
    }

    @Test
    public void test2141() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2141");
        NextDate nextDate3 = new NextDate(0, 12, 0);
        String str7 = nextDate3.run((int) (short) -1, (int) (byte) 0, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2142() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2142");
        NextDate nextDate3 = new NextDate(10, (int) '#', (int) (short) 1);
    }

    @Test
    public void test2143() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2143");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) 'a', 12, (int) (byte) 10);
        String str11 = nextDate3.run(31, (int) (byte) 1, (int) (byte) 10);
        String str15 = nextDate3.run((int) (short) 10, (int) (short) 100, (int) (short) 1);
        String str19 = nextDate3.run(30, (int) '4', (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test2144() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2144");
        NextDate nextDate3 = new NextDate((int) (short) 1, 0, (-1));
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2145() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2145");
        NextDate nextDate3 = new NextDate(31, 2001, 100);
        String str7 = nextDate3.run(7, (int) (short) 10, (int) (short) -1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2146() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2146");
        NextDate nextDate3 = new NextDate(0, (int) '4', 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2147() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2147");
        NextDate nextDate3 = new NextDate(1, 0, (int) (short) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2148() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2148");
        NextDate nextDate3 = new NextDate((int) (short) 10, 30, (int) (byte) 10);
    }

    @Test
    public void test2149() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2149");
        NextDate nextDate3 = new NextDate(31, (-1), 0);
        String str7 = nextDate3.run((int) (byte) 1, (int) (short) 0, (int) '#');
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2150() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2150");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 100, (int) (short) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2151() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2151");
        NextDate nextDate3 = new NextDate((int) ' ', 1, 7);
        String str7 = nextDate3.run(31, 0, 0);
        String str11 = nextDate3.run(0, (int) (byte) 10, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2152() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2152");
        NextDate nextDate3 = new NextDate((int) '#', (-1), 31);
        String str7 = nextDate3.run(100, (int) (short) 1, 0);
        String str11 = nextDate3.run((int) (short) -1, 0, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2153() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2153");
        NextDate nextDate3 = new NextDate((int) '#', (int) ' ', 30);
        String str7 = nextDate3.run((int) (byte) 10, 10, (int) (byte) 1);
        String str11 = nextDate3.run((-1), (-1), 10);
        String str15 = nextDate3.run((int) (byte) 10, (int) ' ', (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test2154() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2154");
        NextDate nextDate3 = new NextDate((int) (short) 10, (-1), (int) (short) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2155() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2155");
        NextDate nextDate3 = new NextDate(0, 7, (int) (short) 1);
        String str7 = nextDate3.run(12, 31, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "1/1/2002" + "'", str7, "1/1/2002");
    }

    @Test
    public void test2156() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2156");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 0, 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2157() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2157");
        NextDate nextDate3 = new NextDate(2001, 100, (int) (short) 0);
        String str7 = nextDate3.run(0, 12, 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2158() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2158");
        NextDate nextDate3 = new NextDate(7, (int) (byte) 0, 2020);
        String str7 = nextDate3.run((int) (short) 0, 31, 2020);
        String str11 = nextDate3.run((-1), (int) '4', 1);
        String str15 = nextDate3.run(30, 30, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test2159() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2159");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 31, (int) (short) 0);
    }

    @Test
    public void test2160() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2160");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) '#', (int) (short) 10);
        String str7 = nextDate3.run((int) (byte) -1, (int) '#', (int) (byte) -1);
        String str11 = nextDate3.run((int) (short) -1, 0, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2161() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2161");
        NextDate nextDate3 = new NextDate(2020, (-1), 31);
        String str7 = nextDate3.run(7, 2020, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2162() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2162");
        NextDate nextDate3 = new NextDate(1, (int) (short) 1, (int) (byte) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2163() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2163");
        NextDate nextDate3 = new NextDate((int) (short) 1, 7, 2001);
        String str7 = nextDate3.run((int) (short) 10, (int) ' ', 0);
        String str11 = nextDate3.run((int) (short) 1, (int) ' ', 7);
        String str15 = nextDate3.run((int) 'a', 1, 2020);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test2164() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2164");
        NextDate nextDate3 = new NextDate((int) '#', 10, (int) ' ');
    }

    @Test
    public void test2165() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2165");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 0, (int) (short) 1);
        String str7 = nextDate3.run((int) (short) -1, 7, 7);
        String str11 = nextDate3.run((int) (short) 0, 12, 2001);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2166() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2166");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) -1, (int) (byte) -1);
        String str7 = nextDate3.run(30, 0, (int) (short) 1);
        String str11 = nextDate3.run((int) (short) 0, 100, 31);
        String str15 = nextDate3.run((int) '#', 0, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test2167() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2167");
        NextDate nextDate3 = new NextDate(12, (int) (short) 100, (int) ' ');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2168() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2168");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) 100, (int) '#');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2169() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2169");
        NextDate nextDate3 = new NextDate((int) '4', (-1), (int) (byte) -1);
    }

    @Test
    public void test2170() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2170");
        NextDate nextDate3 = new NextDate(0, 7, (int) (byte) 10);
        String str7 = nextDate3.run((int) '4', (int) '4', 10);
        String str11 = nextDate3.run((int) (short) 100, 12, (int) (byte) 0);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2171() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2171");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) '#', 12);
        String str7 = nextDate3.run(0, (int) (short) -1, 2001);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2172() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2172");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 100, (int) (short) 1);
        String str7 = nextDate3.run(2020, (int) '4', 12);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2173() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2173");
        NextDate nextDate3 = new NextDate(100, 1, (int) '#');
        String str7 = nextDate3.run((int) '#', 0, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2174() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2174");
        NextDate nextDate3 = new NextDate(31, (int) (short) 0, 30);
        String str7 = nextDate3.run((int) ' ', 0, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2175() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2175");
        NextDate nextDate3 = new NextDate(10, (int) '#', 0);
        String str7 = nextDate3.run((int) (short) 0, 10, (int) (short) 10);
        String str11 = nextDate3.run(100, (-1), 31);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2176() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2176");
        NextDate nextDate3 = new NextDate((int) '4', (int) ' ', (int) (byte) 10);
        String str7 = nextDate3.run(1, (int) ' ', (int) (short) 100);
        String str11 = nextDate3.run((int) 'a', (int) (byte) 1, (int) (byte) 0);
        String str15 = nextDate3.run(100, (int) (byte) 1, (int) (short) 10);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test2177() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2177");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 0, 2001);
        String str7 = nextDate3.run((-1), 0, (int) 'a');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2178() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2178");
        NextDate nextDate3 = new NextDate(2001, 2001, 7);
        String str7 = nextDate3.run((int) ' ', 1, (int) (byte) -1);
        String str11 = nextDate3.run((int) '#', (int) ' ', 1);
        String str15 = nextDate3.run(30, 7, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test2179() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2179");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 2001, 31);
        String str7 = nextDate3.run((-1), (int) '#', (int) '4');
        String str11 = nextDate3.run(1, (int) (short) -1, 0);
        String str15 = nextDate3.run((int) (byte) 10, (int) ' ', (int) '#');
        String str19 = nextDate3.run(2001, 2001, 100);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test2180() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2180");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) (byte) 10, (int) '#', 31);
        String str11 = nextDate3.run((int) (short) 1, (int) 'a', (int) (byte) -1);
        String str15 = nextDate3.run((int) (byte) 0, (int) ' ', (int) (byte) 1);
        String str19 = nextDate3.run(12, (int) (short) 100, 0);
        String str23 = nextDate3.run(0, (int) (short) 1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test2181() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2181");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) 'a', (int) (short) -1);
    }

    @Test
    public void test2182() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2182");
        NextDate nextDate3 = new NextDate(0, 7, 30);
    }

    @Test
    public void test2183() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2183");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (byte) 0, (int) (byte) 1);
    }

    @Test
    public void test2184() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2184");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) 'a', (int) (short) -1);
    }

    @Test
    public void test2185() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2185");
        NextDate nextDate3 = new NextDate((int) ' ', 10, (int) 'a');
        String str7 = nextDate3.run((int) 'a', 7, 12);
        String str11 = nextDate3.run(31, (int) 'a', 31);
        String str15 = nextDate3.run((int) (byte) 100, (int) 'a', (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test2186() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2186");
        NextDate nextDate3 = new NextDate((int) (short) 10, 31, (int) (byte) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2187() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2187");
        NextDate nextDate3 = new NextDate((int) ' ', 2020, (int) (short) 0);
    }

    @Test
    public void test2188() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2188");
        NextDate nextDate3 = new NextDate(7, 12, 30);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2189() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2189");
        NextDate nextDate3 = new NextDate((int) '4', 31, 2001);
        String str7 = nextDate3.run(1, (int) (short) 0, 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2190() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2190");
        NextDate nextDate3 = new NextDate(10, 0, 2001);
        String str7 = nextDate3.run(0, 1, 0);
        String str11 = nextDate3.run((int) '#', (int) (short) 0, (int) 'a');
        String str15 = nextDate3.run((int) (byte) 10, 10, 1);
        String str19 = nextDate3.run((int) '4', (int) (short) -1, 0);
        String str23 = nextDate3.run((int) 'a', (int) (short) 0, (int) (byte) 1);
        String str27 = nextDate3.run(2020, (int) ' ', (int) (short) 100);
        String str31 = nextDate3.run(7, 12, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
    }

    @Test
    public void test2191() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2191");
        NextDate nextDate3 = new NextDate(31, 12, 12);
        String str7 = nextDate3.run((int) (byte) 1, (int) (short) 100, 100);
        String str11 = nextDate3.run(10, 31, 2020);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "11/1/2020" + "'", str11, "11/1/2020");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2192() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2192");
        NextDate nextDate3 = new NextDate(2001, 100, (int) (short) 0);
        String str7 = nextDate3.run((int) (byte) 10, (int) (byte) -1, (int) (short) -1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2193() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2193");
        NextDate nextDate3 = new NextDate(0, (-1), 2020);
        String str7 = nextDate3.run((int) (short) 10, (int) ' ', (int) (byte) 10);
        String str11 = nextDate3.run(2001, (int) (byte) 10, 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2194() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2194");
        NextDate nextDate3 = new NextDate(12, (int) (byte) -1, (int) (byte) 0);
        String str7 = nextDate3.run((int) (byte) 1, 30, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2195() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2195");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 10, (int) (short) -1);
        String str7 = nextDate3.run(10, (int) 'a', (int) '4');
        String str11 = nextDate3.run(12, (int) (byte) 1, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2196() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2196");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) 'a', 12, (int) (byte) 10);
        String str11 = nextDate3.run(31, (int) (byte) 1, (int) (byte) 10);
        String str15 = nextDate3.run(2001, (int) (short) 100, (-1));
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test2197() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2197");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) (byte) 0, (int) (byte) 10, 31);
        String str23 = nextDate3.run((-1), 0, (int) (byte) -1);
        String str27 = nextDate3.run((int) (byte) 1, (int) (byte) 1, (int) 'a');
        String str31 = nextDate3.run((int) (short) 1, 30, (int) (byte) 100);
        String str35 = nextDate3.run((int) '4', (int) (byte) -1, (int) (short) 10);
        String str39 = nextDate3.run(0, 2001, 7);
        Class<?> wildcardClass40 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass40);
    }

    @Test
    public void test2198() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2198");
        NextDate nextDate3 = new NextDate(0, (int) (short) 0, (int) (byte) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2199() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2199");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) 100, (int) (byte) 0);
        String str7 = nextDate3.run(0, (int) (short) -1, (int) (byte) 10);
        String str11 = nextDate3.run((int) ' ', (int) '#', 0);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2200() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2200");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) 1, 12);
        String str7 = nextDate3.run(1, 12, (int) (short) 0);
        String str11 = nextDate3.run((int) ' ', (int) (short) 10, (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2201() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2201");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (short) 1, (int) (byte) 100);
    }

    @Test
    public void test2202() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2202");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) 1, 2001);
    }

    @Test
    public void test2203() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2203");
        NextDate nextDate3 = new NextDate((int) '4', (int) '4', 1);
        String str7 = nextDate3.run(0, (int) (short) 10, 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2204() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2204");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 7, (int) (byte) 1);
        String str7 = nextDate3.run(30, (int) (short) 100, 100);
        String str11 = nextDate3.run((int) (byte) 10, 7, (int) (short) 1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2205() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2205");
        NextDate nextDate3 = new NextDate((int) (short) 100, 31, 2020);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2206() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2206");
        NextDate nextDate3 = new NextDate(1, (int) ' ', 10);
    }

    @Test
    public void test2207() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2207");
        NextDate nextDate3 = new NextDate(30, (-1), (int) '#');
        String str7 = nextDate3.run((int) (byte) 100, (int) '#', (int) 'a');
        String str11 = nextDate3.run(31, (int) (byte) 10, (int) (byte) 10);
        String str15 = nextDate3.run((int) (short) 1, (int) (byte) -1, (int) (short) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test2208() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2208");
        NextDate nextDate3 = new NextDate((int) (short) 100, 100, (int) (short) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2209() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2209");
        NextDate nextDate3 = new NextDate(12, (int) (short) 10, (int) (byte) 10);
        String str7 = nextDate3.run((int) 'a', (int) (short) 100, 2020);
        String str11 = nextDate3.run((int) (short) 0, 2001, (int) (short) -1);
        String str15 = nextDate3.run((int) (short) 100, (int) (byte) 0, (int) (short) 10);
        String str19 = nextDate3.run((int) (short) 10, (int) '4', 10);
        String str23 = nextDate3.run((int) (short) -1, (int) '#', (int) (short) 1);
        String str27 = nextDate3.run((int) ' ', 0, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test2210() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2210");
        NextDate nextDate3 = new NextDate(0, 12, 2001);
        String str7 = nextDate3.run(12, 0, (int) '4');
        String str11 = nextDate3.run(7, 30, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2211() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2211");
        NextDate nextDate3 = new NextDate(7, (int) '4', 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2212() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2212");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) '#', 0);
        String str7 = nextDate3.run(31, (int) (byte) 100, 100);
        String str11 = nextDate3.run((int) (byte) 1, (int) (byte) -1, (int) (byte) 1);
        String str15 = nextDate3.run((int) (byte) 10, 30, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test2213() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2213");
        NextDate nextDate3 = new NextDate(12, (int) (short) 10, (int) (byte) 10);
        String str7 = nextDate3.run((int) 'a', (int) (short) 100, 2020);
        String str11 = nextDate3.run((int) (short) 1, 0, (int) (short) 10);
        String str15 = nextDate3.run((int) (short) 10, 2001, 0);
        String str19 = nextDate3.run(31, (int) '#', 0);
        String str23 = nextDate3.run(7, (int) (byte) 0, (int) (short) 10);
        String str27 = nextDate3.run(31, 100, (int) (byte) 1);
        String str31 = nextDate3.run((int) (short) -1, (int) '#', (int) (short) -1);
        String str35 = nextDate3.run(30, (int) (byte) 1, (int) (short) 100);
        String str39 = nextDate3.run((int) '#', (int) ' ', (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
    }

    @Test
    public void test2214() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2214");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 100, 2001);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2215() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2215");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 7, (int) (byte) 1);
        String str7 = nextDate3.run(30, (int) (short) 100, 100);
        String str11 = nextDate3.run(10, 1, (int) '4');
        String str15 = nextDate3.run(31, (int) (byte) 10, 1);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test2216() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2216");
        NextDate nextDate3 = new NextDate((int) (short) 100, 10, (-1));
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2217() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2217");
        NextDate nextDate3 = new NextDate((int) (short) 0, 1, (int) (short) -1);
    }

    @Test
    public void test2218() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2218");
        NextDate nextDate3 = new NextDate((-1), (int) (short) 0, 10);
    }

    @Test
    public void test2219() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2219");
        NextDate nextDate3 = new NextDate(1, 10, 31);
        String str7 = nextDate3.run(2020, 0, (int) (byte) 0);
        String str11 = nextDate3.run(30, 0, (int) (byte) -1);
        String str15 = nextDate3.run((int) (short) -1, 0, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test2220() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2220");
        NextDate nextDate3 = new NextDate(12, 2001, 2020);
        String str7 = nextDate3.run(31, (int) (short) 10, 2020);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2221() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2221");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 0, (int) (byte) 0);
    }

    @Test
    public void test2222() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2222");
        NextDate nextDate3 = new NextDate(2020, (int) 'a', (int) (byte) -1);
        String str7 = nextDate3.run((-1), 7, (int) '4');
        String str11 = nextDate3.run(30, (int) (short) 0, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2223() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2223");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (short) 0, (int) (short) 1);
        String str7 = nextDate3.run(0, (int) (byte) -1, (int) (byte) 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2224() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2224");
        NextDate nextDate3 = new NextDate(2020, (int) '#', (int) (byte) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2225() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2225");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 0, 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2226() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2226");
        NextDate nextDate3 = new NextDate(10, 10, 1);
        String str7 = nextDate3.run((int) '#', (int) ' ', (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2227() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2227");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (byte) 10, 10);
    }

    @Test
    public void test2228() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2228");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 10, (int) (short) 0);
        String str7 = nextDate3.run(30, (int) 'a', (int) (short) -1);
        String str11 = nextDate3.run((int) (byte) -1, (int) (short) 10, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2229() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2229");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) 'a', (int) '#');
        String str7 = nextDate3.run((int) (short) 1, 0, 1);
        String str11 = nextDate3.run((int) (short) 100, (int) (byte) 10, 0);
        String str15 = nextDate3.run(0, (int) '#', (int) '4');
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test2230() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2230");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) '#', (int) (byte) 100);
        String str7 = nextDate3.run((int) (byte) -1, 100, 2020);
        String str11 = nextDate3.run(30, 30, 31);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2231() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2231");
        NextDate nextDate3 = new NextDate(2001, (int) (short) 0, 1);
        String str7 = nextDate3.run(100, (int) (short) 1, (int) (short) 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2232() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2232");
        NextDate nextDate3 = new NextDate((-1), 30, (int) (byte) -1);
        String str7 = nextDate3.run(0, (int) (byte) -1, (int) (byte) 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2233() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2233");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) 'a', 12);
        String str7 = nextDate3.run((int) (short) 1, 31, (int) ' ');
        String str11 = nextDate3.run((int) (byte) 0, 100, (int) (byte) -1);
        String str15 = nextDate3.run((int) (short) 100, (int) (byte) -1, 31);
        String str19 = nextDate3.run((int) (byte) 0, (int) (short) 1, (int) '#');
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test2234() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2234");
        NextDate nextDate3 = new NextDate((int) (short) 1, 30, (int) (short) 100);
        String str7 = nextDate3.run((int) (byte) -1, (int) (byte) -1, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2235() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2235");
        NextDate nextDate3 = new NextDate((int) (short) -1, 10, 2020);
        String str7 = nextDate3.run((int) ' ', (int) ' ', 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2236() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2236");
        NextDate nextDate3 = new NextDate((int) ' ', (int) 'a', (int) '4');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2237() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2237");
        NextDate nextDate3 = new NextDate((int) (short) -1, 2020, (int) (short) 100);
        String str7 = nextDate3.run(1, (int) (short) 100, (int) (short) -1);
        String str11 = nextDate3.run((-1), 30, (int) (byte) 10);
        String str15 = nextDate3.run((int) '#', (int) '#', 0);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test2238() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2238");
        NextDate nextDate3 = new NextDate(0, (int) '4', 7);
    }

    @Test
    public void test2239() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2239");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(0, 2020, (int) (byte) 10);
        String str19 = nextDate3.run(0, (int) (short) 0, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test2240() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2240");
        NextDate nextDate3 = new NextDate(30, 30, (int) (short) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2241() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2241");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 0, (int) (short) 0);
    }

    @Test
    public void test2242() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2242");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 10, 7);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2243() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2243");
        NextDate nextDate3 = new NextDate((int) (short) 100, 2020, 2020);
        String str7 = nextDate3.run((int) (byte) -1, (-1), 7);
        String str11 = nextDate3.run((int) (byte) 0, (int) (byte) -1, (int) (short) 0);
        String str15 = nextDate3.run((int) (short) 100, (int) (byte) 100, (int) (byte) 0);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test2244() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2244");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run((int) (byte) 10, (int) (short) 100, 100);
        String str27 = nextDate3.run((int) 'a', 0, (int) '4');
        String str31 = nextDate3.run((int) (short) 1, 0, (int) (byte) -1);
        String str35 = nextDate3.run((int) 'a', (int) (short) 100, (int) (short) 100);
        String str39 = nextDate3.run(2001, 1, 7);
        String str43 = nextDate3.run(31, (int) ' ', (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str43 + "' != '" + "invalid Input Date" + "'", str43, "invalid Input Date");
    }

    @Test
    public void test2245() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2245");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) 0, (int) (short) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2246() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2246");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) 0, (int) ' ');
    }

    @Test
    public void test2247() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2247");
        NextDate nextDate3 = new NextDate((int) '#', 10, (int) (byte) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2248() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2248");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) ' ', 2001);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2249() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2249");
        NextDate nextDate3 = new NextDate((int) (short) 0, 2001, 0);
    }

    @Test
    public void test2250() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2250");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) '4', (int) (byte) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2251() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2251");
        NextDate nextDate3 = new NextDate(1, 31, (int) (byte) 0);
        String str7 = nextDate3.run(2020, (int) '#', (int) 'a');
        String str11 = nextDate3.run((int) 'a', (int) (byte) 10, 10);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2252() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2252");
        NextDate nextDate3 = new NextDate(2020, (int) (short) -1, 2020);
    }

    @Test
    public void test2253() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2253");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) 0, (int) '4');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2254() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2254");
        NextDate nextDate3 = new NextDate((-1), 12, (int) (short) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2255() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2255");
        NextDate nextDate3 = new NextDate((int) (short) 10, 0, (int) (byte) 1);
        String str7 = nextDate3.run(100, (int) '#', (int) (short) 1);
        String str11 = nextDate3.run(7, (int) (byte) 1, 10);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2256() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2256");
        NextDate nextDate3 = new NextDate(7, 2001, (int) '#');
    }

    @Test
    public void test2257() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2257");
        NextDate nextDate3 = new NextDate((-1), 12, 0);
    }

    @Test
    public void test2258() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2258");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) '#', (int) (short) -1);
        String str7 = nextDate3.run(10, (int) (byte) 0, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2259() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2259");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 100, 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2260() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2260");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) 1, (int) (short) 0);
    }

    @Test
    public void test2261() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2261");
        NextDate nextDate3 = new NextDate(10, (int) (short) 0, 10);
        String str7 = nextDate3.run((int) (short) 10, 100, 1);
        String str11 = nextDate3.run(1, 10, (int) '4');
        String str15 = nextDate3.run((int) (byte) -1, (int) (short) -1, 10);
        String str19 = nextDate3.run(0, (int) 'a', (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test2262() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2262");
        NextDate nextDate3 = new NextDate(100, 1, (int) (short) 1);
    }

    @Test
    public void test2263() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2263");
        NextDate nextDate3 = new NextDate(12, 30, (int) (byte) 100);
        String str7 = nextDate3.run(7, 100, (int) (short) 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2264() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2264");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 0, (int) (byte) 0);
        String str7 = nextDate3.run((int) (short) 10, 10, (int) '#');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2265() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2265");
        NextDate nextDate3 = new NextDate((int) (short) 1, (-1), 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2266() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2266");
        NextDate nextDate3 = new NextDate(0, 2020, (int) (short) 0);
        String str7 = nextDate3.run(12, (-1), 2020);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2267() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2267");
        NextDate nextDate3 = new NextDate((int) (short) 0, 7, 10);
        String str7 = nextDate3.run((int) '#', 30, (int) (short) 1);
        String str11 = nextDate3.run(100, (int) (short) 1, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2268() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2268");
        NextDate nextDate3 = new NextDate(2020, 0, 0);
        String str7 = nextDate3.run(7, (int) '#', (int) (short) 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2269() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2269");
        NextDate nextDate3 = new NextDate((int) (short) 1, 0, (int) (byte) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2270() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2270");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) 'a', (int) ' ');
        String str7 = nextDate3.run(100, (int) (byte) -1, 12);
        String str11 = nextDate3.run(7, (int) (byte) 0, 30);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2271() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2271");
        NextDate nextDate3 = new NextDate(30, (int) 'a', 1);
        String str7 = nextDate3.run(0, (int) (byte) 1, 7);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2272() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2272");
        NextDate nextDate3 = new NextDate(2020, 2001, 0);
        String str7 = nextDate3.run(0, (int) 'a', 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2273() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2273");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) 'a', (-1));
    }

    @Test
    public void test2274() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2274");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 2001, 31);
        String str7 = nextDate3.run((-1), (int) '#', (int) '4');
        String str11 = nextDate3.run((int) (short) 100, 100, (int) ' ');
        String str15 = nextDate3.run(2001, 2020, (int) (short) 0);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test2275() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2275");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        String str15 = nextDate3.run((int) (byte) 1, (int) (short) 0, (int) (short) 100);
        String str19 = nextDate3.run((int) (short) 10, 0, 10);
        String str23 = nextDate3.run((int) '#', 100, 100);
        String str27 = nextDate3.run((int) (byte) 1, (int) (short) 10, (int) (short) 10);
        String str31 = nextDate3.run((int) 'a', 1, 12);
        String str35 = nextDate3.run((int) '#', (int) 'a', 1);
        String str39 = nextDate3.run((int) (short) 1, 0, 2001);
        Class<?> wildcardClass40 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass40);
    }

    @Test
    public void test2276() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2276");
        NextDate nextDate3 = new NextDate(12, (int) '4', (int) (short) 1);
        String str7 = nextDate3.run(10, 7, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2277() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2277");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 2020, 12);
        String str7 = nextDate3.run((-1), 0, (int) (short) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2278() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2278");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 10, (int) (short) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2279() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2279");
        NextDate nextDate3 = new NextDate((int) '#', 1, (int) (byte) 1);
    }

    @Test
    public void test2280() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2280");
        NextDate nextDate3 = new NextDate(1, 10, (int) (byte) -1);
        String str7 = nextDate3.run(2020, (int) 'a', 1);
        String str11 = nextDate3.run((int) (byte) 100, 12, (int) 'a');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2281() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2281");
        NextDate nextDate3 = new NextDate(0, 7, (int) (short) 1);
        String str7 = nextDate3.run((int) (byte) -1, (int) (byte) 100, (int) (short) 0);
        String str11 = nextDate3.run(0, (int) (byte) 100, 30);
        String str15 = nextDate3.run(100, (int) '#', (int) 'a');
        String str19 = nextDate3.run((int) (byte) -1, 0, 31);
        String str23 = nextDate3.run((int) (byte) 100, (int) (short) 10, (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
    }

    @Test
    public void test2282() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2282");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (short) -1, (int) (byte) 0);
        String str7 = nextDate3.run(12, 0, (int) (byte) 100);
        String str11 = nextDate3.run(31, (int) (short) 0, 0);
        String str15 = nextDate3.run(0, (int) (byte) 10, 2020);
        String str19 = nextDate3.run((int) (byte) -1, 12, 1);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test2283() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2283");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (short) 10, (int) (short) 1);
        String str7 = nextDate3.run(1, 100, (int) (byte) 100);
        String str11 = nextDate3.run(2020, (int) (short) 100, (int) '#');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2284() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2284");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 10, 31);
        String str7 = nextDate3.run(0, (int) (byte) 1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2285() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2285");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 0, (int) (short) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2286() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2286");
        NextDate nextDate3 = new NextDate((int) ' ', 100, 31);
        String str7 = nextDate3.run(30, 30, (int) '4');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2287() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2287");
        NextDate nextDate3 = new NextDate((int) '4', 0, 12);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2288() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2288");
        NextDate nextDate3 = new NextDate((int) (short) 0, 0, (int) (short) 100);
        String str7 = nextDate3.run(12, 0, (int) ' ');
        String str11 = nextDate3.run((int) (short) 0, (int) '4', (int) (short) 10);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2289() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2289");
        NextDate nextDate3 = new NextDate(31, (int) '#', 1);
    }

    @Test
    public void test2290() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2290");
        NextDate nextDate3 = new NextDate(1, 2001, (int) (short) 0);
    }

    @Test
    public void test2291() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2291");
        NextDate nextDate3 = new NextDate((int) (short) 0, 0, (int) (short) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2292() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2292");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 7, (int) (short) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2293() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2293");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (byte) 1, 10);
        String str7 = nextDate3.run(2020, 7, 100);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2294() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2294");
        NextDate nextDate3 = new NextDate((int) (byte) 100, 7, (int) (byte) 100);
    }

    @Test
    public void test2295() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2295");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) -1, 10);
        String str7 = nextDate3.run(30, (int) (short) 100, 31);
        String str11 = nextDate3.run(7, 12, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2296() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2296");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 10, 1);
        String str7 = nextDate3.run((int) 'a', 2020, (int) (short) -1);
        String str11 = nextDate3.run(31, (int) (byte) 1, (int) (byte) 0);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2297() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2297");
        NextDate nextDate3 = new NextDate((-1), (int) (byte) -1, 30);
        String str7 = nextDate3.run((int) (byte) -1, 2001, (int) (byte) 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2298() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2298");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (short) 0, 1);
        String str7 = nextDate3.run((int) (short) 1, 2020, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2299() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2299");
        NextDate nextDate3 = new NextDate((int) '4', 2001, 1);
        String str7 = nextDate3.run((int) (short) 1, 30, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2300() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2300");
        NextDate nextDate3 = new NextDate(100, (int) (short) 0, 2020);
        String str7 = nextDate3.run((int) (byte) 10, (int) (short) 10, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2301() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2301");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 0, (int) '#');
    }

    @Test
    public void test2302() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2302");
        NextDate nextDate3 = new NextDate(100, 0, 12);
    }

    @Test
    public void test2303() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2303");
        NextDate nextDate3 = new NextDate((int) '#', (int) (byte) -1, 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2304() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2304");
        NextDate nextDate3 = new NextDate(7, 12, (int) (short) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2305() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2305");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) -1, 31);
    }

    @Test
    public void test2306() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2306");
        NextDate nextDate3 = new NextDate(2020, 0, (-1));
        String str7 = nextDate3.run(30, (int) (byte) 100, (-1));
        String str11 = nextDate3.run((int) (byte) 100, 12, 2001);
        String str15 = nextDate3.run((int) 'a', (-1), 0);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test2307() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2307");
        NextDate nextDate3 = new NextDate(2001, (int) (short) -1, 7);
        String str7 = nextDate3.run((int) '4', 30, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2308() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2308");
        NextDate nextDate3 = new NextDate(10, (int) (short) 100, (int) (short) 10);
        String str7 = nextDate3.run(2001, (int) (short) 0, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2309() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2309");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) 'a', 30);
        String str7 = nextDate3.run((int) (short) 100, 7, 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2310() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2310");
        NextDate nextDate3 = new NextDate(0, 1, 31);
    }

    @Test
    public void test2311() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2311");
        NextDate nextDate3 = new NextDate(10, (int) (byte) 100, 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2312() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2312");
        NextDate nextDate3 = new NextDate((int) '4', (int) 'a', 31);
        String str7 = nextDate3.run((int) (byte) 10, 0, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2313() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2313");
        NextDate nextDate3 = new NextDate(12, (int) (byte) -1, (int) (byte) 0);
        String str7 = nextDate3.run((int) (short) 100, (int) (short) 1, 0);
        String str11 = nextDate3.run((int) (short) 0, 0, 0);
        String str15 = nextDate3.run(2001, (int) (byte) 0, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test2314() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2314");
        NextDate nextDate3 = new NextDate((int) ' ', 30, 0);
    }

    @Test
    public void test2315() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2315");
        NextDate nextDate3 = new NextDate(30, (int) (byte) 0, 2001);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2316() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2316");
        NextDate nextDate3 = new NextDate(12, (int) (short) 1, (int) '4');
        String str7 = nextDate3.run((int) (short) 10, (int) (short) 1, (-1));
        String str11 = nextDate3.run(1, (int) (byte) -1, (int) (short) 1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2317() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2317");
        NextDate nextDate3 = new NextDate(10, 10, (int) ' ');
        String str7 = nextDate3.run((int) '4', (int) 'a', (-1));
        String str11 = nextDate3.run(0, (int) (short) -1, (int) 'a');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2318() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2318");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 10, 0);
    }

    @Test
    public void test2319() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2319");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) 100, 0);
        String str7 = nextDate3.run(31, 1, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2320() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2320");
        NextDate nextDate3 = new NextDate(12, (int) (byte) -1, (int) (byte) 0);
        String str7 = nextDate3.run((int) (short) 100, (int) (short) 1, 0);
        String str11 = nextDate3.run((-1), (int) ' ', (int) (short) -1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2321() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2321");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) (byte) -1, 31);
        String str7 = nextDate3.run((int) 'a', (int) (byte) 10, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2322() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2322");
        NextDate nextDate3 = new NextDate(1, 12, 7);
    }

    @Test
    public void test2323() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2323");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 100, (int) ' ');
    }

    @Test
    public void test2324() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2324");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) 10, (int) (byte) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2325() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2325");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) '#', (int) (byte) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2326() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2326");
        NextDate nextDate3 = new NextDate((int) (short) -1, 0, (int) (short) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2327() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2327");
        NextDate nextDate3 = new NextDate(2001, (int) (short) 0, 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2328() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2328");
        NextDate nextDate3 = new NextDate((int) '#', (int) '4', (int) (short) -1);
        String str7 = nextDate3.run((int) '4', (int) (byte) -1, (int) '4');
        String str11 = nextDate3.run((int) 'a', (int) '4', 30);
        String str15 = nextDate3.run((int) (byte) 1, (int) (short) 100, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test2329() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2329");
        NextDate nextDate3 = new NextDate((int) 'a', (int) 'a', 0);
        String str7 = nextDate3.run((int) (short) -1, (int) '4', (int) (byte) 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2330() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2330");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 12, 12);
    }

    @Test
    public void test2331() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2331");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (short) 0, (-1));
        String str7 = nextDate3.run(0, 1, (int) (short) 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2332() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2332");
        NextDate nextDate3 = new NextDate((int) (short) -1, (int) (byte) 100, 10);
        String str7 = nextDate3.run((int) 'a', (int) 'a', (int) '4');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2333() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2333");
        NextDate nextDate3 = new NextDate(30, (-1), 31);
        String str7 = nextDate3.run(7, (int) (short) 100, 1);
        String str11 = nextDate3.run((int) (byte) 1, (int) (byte) 100, (int) (short) 1);
        String str15 = nextDate3.run(0, (int) (byte) 10, 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test2334() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2334");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) '#', 100);
        String str7 = nextDate3.run((int) (short) 10, (-1), (int) (short) 0);
        String str11 = nextDate3.run((int) '#', 100, (int) '4');
        String str15 = nextDate3.run((int) (byte) 100, (-1), (-1));
        String str19 = nextDate3.run((int) (short) -1, (-1), (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test2335() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2335");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) 10, 12);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2336() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2336");
        NextDate nextDate3 = new NextDate(2020, 100, (int) (byte) 1);
        String str7 = nextDate3.run(2001, 0, (int) (short) 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2337() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2337");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) '4', (int) (short) 0);
    }

    @Test
    public void test2338() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2338");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) (short) 10, 31);
        String str7 = nextDate3.run((int) (short) 100, 1, 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2339() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2339");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 100, (int) ' ');
        String str7 = nextDate3.run((int) (byte) 10, 100, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2340() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2340");
        NextDate nextDate3 = new NextDate(0, 100, (int) (short) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2341() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2341");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 0, (int) (short) 100);
        String str7 = nextDate3.run((int) (short) 0, (int) (short) 1, (int) '#');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2342() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2342");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 10, (int) (short) 0);
        String str7 = nextDate3.run(30, (int) 'a', (int) (short) -1);
        String str11 = nextDate3.run((int) '4', 1, 31);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2343() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2343");
        NextDate nextDate3 = new NextDate(10, 0, 30);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2344() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2344");
        NextDate nextDate3 = new NextDate((int) (short) 100, 2001, 1);
    }

    @Test
    public void test2345() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2345");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 100, (int) ' ');
    }

    @Test
    public void test2346() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2346");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, (int) (byte) 1, (int) (short) 100);
        String str11 = nextDate3.run((int) (short) -1, 10, 1);
        String str15 = nextDate3.run((int) ' ', 2020, (-1));
        String str19 = nextDate3.run((int) ' ', (int) '#', (int) (short) 1);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test2347() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2347");
        NextDate nextDate3 = new NextDate(30, (int) (short) 0, (int) (byte) -1);
    }

    @Test
    public void test2348() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2348");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (byte) 0, 2020);
        String str7 = nextDate3.run(0, (int) (byte) 1, (int) '4');
        String str11 = nextDate3.run((int) (byte) 1, (int) (byte) 1, 0);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2349() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2349");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 10, (-1));
        String str7 = nextDate3.run((-1), (int) '#', (int) (byte) 100);
        String str11 = nextDate3.run((int) (short) 100, 2001, (int) '#');
        String str15 = nextDate3.run((int) (byte) 10, (int) (short) 1, 100);
        String str19 = nextDate3.run((int) (byte) 1, (int) (short) -1, 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test2350() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2350");
        NextDate nextDate3 = new NextDate(1, (int) (byte) 100, (int) 'a');
        String str7 = nextDate3.run(7, 100, (int) (byte) -1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2351() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2351");
        NextDate nextDate3 = new NextDate(30, (int) ' ', (int) ' ');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2352() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2352");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 1, (int) (short) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2353() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2353");
        NextDate nextDate3 = new NextDate(1, (int) (short) 10, 2001);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2354() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2354");
        NextDate nextDate3 = new NextDate(1, (int) '#', (int) (byte) 0);
    }

    @Test
    public void test2355() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2355");
        NextDate nextDate3 = new NextDate(30, (int) ' ', 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2356() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2356");
        NextDate nextDate3 = new NextDate(0, 7, (int) (byte) 10);
        String str7 = nextDate3.run((int) '4', (int) '4', 10);
        String str11 = nextDate3.run((int) (byte) 10, 0, 2020);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2357() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2357");
        NextDate nextDate3 = new NextDate(30, (int) (short) 10, 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2358() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2358");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) 100, (int) (short) 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2359() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2359");
        NextDate nextDate3 = new NextDate(2001, 12, 31);
        String str7 = nextDate3.run(100, 31, (-1));
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2360() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2360");
        NextDate nextDate3 = new NextDate(7, 100, (int) ' ');
        String str7 = nextDate3.run(100, 1, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2361() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2361");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) '#', (int) '#');
        String str7 = nextDate3.run(2001, (int) '4', (int) (byte) -1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2362() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2362");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) -1, (int) (short) 0);
        String str7 = nextDate3.run(2001, (int) (byte) 100, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2363() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2363");
        NextDate nextDate3 = new NextDate(12, (int) (byte) -1, (int) '#');
    }

    @Test
    public void test2364() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2364");
        NextDate nextDate3 = new NextDate(100, 12, 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2365() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2365");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) 100, (-1));
        String str7 = nextDate3.run(30, 1, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2366() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2366");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        String str15 = nextDate3.run(7, (int) (short) 1, 30);
        String str19 = nextDate3.run(30, (int) 'a', (int) (short) 0);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test2367() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2367");
        NextDate nextDate3 = new NextDate(31, (int) (byte) 1, (int) ' ');
        String str7 = nextDate3.run(0, (int) (byte) -1, 1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2368() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2368");
        NextDate nextDate3 = new NextDate((int) '4', (int) (short) 10, (int) (byte) 0);
    }

    @Test
    public void test2369() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2369");
        NextDate nextDate3 = new NextDate((int) '4', 1, 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2370() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2370");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 0, 1);
        String str7 = nextDate3.run((int) '4', (-1), (int) 'a');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2371() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2371");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 31, (int) 'a');
    }

    @Test
    public void test2372() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2372");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 0, 2001);
        String str7 = nextDate3.run((int) ' ', (-1), (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2373() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2373");
        NextDate nextDate3 = new NextDate((int) (short) 0, 0, (int) (short) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2374() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2374");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 10, (int) (short) 10);
    }

    @Test
    public void test2375() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2375");
        NextDate nextDate3 = new NextDate(100, 100, (int) (short) 1);
        String str7 = nextDate3.run(100, (int) (short) 1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2376() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2376");
        NextDate nextDate3 = new NextDate(100, (int) (byte) 10, 2001);
        String str7 = nextDate3.run((int) (short) 0, 1, 0);
        String str11 = nextDate3.run((int) (short) 100, (int) (byte) -1, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2377() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2377");
        NextDate nextDate3 = new NextDate((-1), 100, (int) 'a');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2378() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2378");
        NextDate nextDate3 = new NextDate(2001, (int) (byte) -1, 2001);
        String str7 = nextDate3.run((int) 'a', 2001, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2379() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2379");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 100, 7);
        String str7 = nextDate3.run((int) (short) -1, 30, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2380() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2380");
        NextDate nextDate3 = new NextDate((int) '#', (int) '#', (int) (short) 1);
        String str7 = nextDate3.run(2001, (int) (byte) 1, 0);
        String str11 = nextDate3.run((-1), 0, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2381() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2381");
        NextDate nextDate3 = new NextDate(7, 2020, 10);
    }

    @Test
    public void test2382() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2382");
        NextDate nextDate3 = new NextDate(10, (-1), 0);
    }

    @Test
    public void test2383() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2383");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, (int) (byte) 1, (int) (short) 100);
        String str11 = nextDate3.run((int) (short) -1, 10, 1);
        String str15 = nextDate3.run((int) (short) 1, 31, (int) (byte) -1);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test2384() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2384");
        NextDate nextDate3 = new NextDate(7, (int) '4', (int) (short) 1);
    }

    @Test
    public void test2385() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2385");
        NextDate nextDate3 = new NextDate(30, (int) 'a', 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2386() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2386");
        NextDate nextDate3 = new NextDate(7, (int) (byte) 0, 0);
        String str7 = nextDate3.run((int) (byte) 100, (int) (byte) 1, 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2387() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2387");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 7, (int) (byte) 1);
        String str7 = nextDate3.run(30, (int) (short) 100, 100);
        String str11 = nextDate3.run((int) (byte) 10, 7, (int) (short) 1);
        String str15 = nextDate3.run((int) (byte) 0, (int) (short) 0, (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test2388() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2388");
        NextDate nextDate3 = new NextDate(31, (int) (short) 10, (int) (short) 10);
    }

    @Test
    public void test2389() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2389");
        NextDate nextDate3 = new NextDate((int) (short) 0, 0, (int) (short) 100);
        String str7 = nextDate3.run((int) ' ', 2001, 10);
        String str11 = nextDate3.run((int) (byte) 1, 100, (int) (byte) 1);
        String str15 = nextDate3.run((int) (byte) 100, 100, 12);
        String str19 = nextDate3.run(30, (int) (byte) 100, 2001);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test2390() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2390");
        NextDate nextDate3 = new NextDate(0, 30, 100);
        String str7 = nextDate3.run((int) (short) 100, (int) '4', (int) 'a');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2391() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2391");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (short) 0, 2001);
        String str7 = nextDate3.run(100, (-1), 2001);
        String str11 = nextDate3.run(12, (int) (byte) -1, 31);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2392() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2392");
        NextDate nextDate3 = new NextDate((int) (short) 10, 0, (int) (byte) 1);
        String str7 = nextDate3.run(100, (int) '#', (int) (short) 1);
        String str11 = nextDate3.run((int) (short) -1, 10, 10);
        String str15 = nextDate3.run(12, (int) (short) 1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test2393() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2393");
        NextDate nextDate3 = new NextDate((-1), 31, 31);
    }

    @Test
    public void test2394() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2394");
        NextDate nextDate3 = new NextDate((int) (short) 10, (int) (short) 10, 10);
        String str7 = nextDate3.run((int) '#', (int) '#', (int) '4');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2395() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2395");
        NextDate nextDate3 = new NextDate((int) '4', (int) (byte) 10, 10);
    }

    @Test
    public void test2396() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2396");
        NextDate nextDate3 = new NextDate((int) (short) 100, 10, (int) '4');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2397() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2397");
        NextDate nextDate3 = new NextDate(7, (int) (short) 10, (int) (byte) 0);
        String str7 = nextDate3.run((int) (byte) 0, 100, (int) (short) -1);
        String str11 = nextDate3.run(0, (int) (short) 10, 12);
        String str15 = nextDate3.run(0, 100, (int) 'a');
        String str19 = nextDate3.run(2001, (int) (short) 10, 1);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test2398() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2398");
        NextDate nextDate3 = new NextDate(0, 0, 100);
        String str7 = nextDate3.run(100, (int) '#', 7);
        String str11 = nextDate3.run(12, (int) (byte) 100, (int) (byte) 100);
        String str15 = nextDate3.run((int) '4', 2020, 100);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test2399() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2399");
        NextDate nextDate3 = new NextDate(0, 31, (int) 'a');
        String str7 = nextDate3.run((int) (short) -1, (int) ' ', 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2400() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2400");
        NextDate nextDate3 = new NextDate(1, 2001, (int) (byte) 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2401() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2401");
        NextDate nextDate3 = new NextDate((int) (short) 100, 2001, 10);
        String str7 = nextDate3.run((int) (short) 0, 2020, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2402() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2402");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 100, (int) '4');
        String str7 = nextDate3.run(100, 0, 10);
        String str11 = nextDate3.run((int) (byte) 1, 7, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2403() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2403");
        NextDate nextDate3 = new NextDate((int) (byte) 1, (int) (byte) 100, 2020);
        String str7 = nextDate3.run(30, (int) (byte) 100, (-1));
        String str11 = nextDate3.run((int) (short) 10, 0, (int) '4');
        String str15 = nextDate3.run(30, 0, 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test2404() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2404");
        NextDate nextDate3 = new NextDate(2020, (int) 'a', (int) (short) 0);
    }

    @Test
    public void test2405() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2405");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 0, 0);
        String str7 = nextDate3.run((int) (byte) 1, 1, (int) (byte) 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2406() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2406");
        NextDate nextDate3 = new NextDate((int) (short) 100, 12, (int) (short) 0);
        String str7 = nextDate3.run((int) '4', (int) (short) 10, (int) (short) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2407() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2407");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 100, 12);
        String str7 = nextDate3.run((int) (short) 10, 100, (int) '4');
        String str11 = nextDate3.run((int) (byte) 100, 0, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2408() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2408");
        NextDate nextDate3 = new NextDate(100, (int) (short) 10, (int) ' ');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2409() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2409");
        NextDate nextDate3 = new NextDate(2001, (int) (short) -1, 7);
        String str7 = nextDate3.run((int) (short) -1, 7, 31);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2410() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2410");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (byte) 1, (int) '4');
    }

    @Test
    public void test2411() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2411");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 10, (-1));
        String str7 = nextDate3.run((-1), (int) '#', (int) (byte) 100);
        String str11 = nextDate3.run((int) (short) 100, 2001, (int) '#');
        String str15 = nextDate3.run((int) (byte) 10, (int) (short) 1, 100);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test2412() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2412");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) 'a', (int) ' ');
        String str7 = nextDate3.run(100, (int) (byte) -1, 12);
        String str11 = nextDate3.run(30, 12, 0);
        String str15 = nextDate3.run((int) (byte) 1, (int) '4', (int) 'a');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test2413() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2413");
        NextDate nextDate3 = new NextDate(1, (int) (short) 100, 0);
        String str7 = nextDate3.run(100, (int) (byte) 10, (int) (byte) 0);
        String str11 = nextDate3.run(2001, (int) (byte) 0, (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2414() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2414");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) (byte) 1, 10);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2415() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2415");
        NextDate nextDate3 = new NextDate((int) (byte) 10, 31, (int) (byte) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2416() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2416");
        NextDate nextDate3 = new NextDate(10, (-1), (int) (short) 1);
        String str7 = nextDate3.run(31, 2001, 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2417() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2417");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) 100, (int) (byte) 0);
        String str7 = nextDate3.run((-1), (int) ' ', (int) (short) -1);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2418() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2418");
        NextDate nextDate3 = new NextDate(10, 0, 2001);
        String str7 = nextDate3.run(0, 1, 0);
        String str11 = nextDate3.run((int) '#', (int) (short) 0, (int) 'a');
        String str15 = nextDate3.run((int) '4', 7, 100);
        String str19 = nextDate3.run(30, (int) (short) 100, (int) (byte) 1);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test2419() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2419");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) (byte) 10, (int) '#', 31);
        String str11 = nextDate3.run((int) (short) 1, (int) 'a', (int) (byte) -1);
        String str15 = nextDate3.run((int) (short) 0, 0, (int) '4');
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test2420() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2420");
        NextDate nextDate3 = new NextDate((int) (short) -1, 2020, (int) (short) 100);
        String str7 = nextDate3.run(10, (int) 'a', (int) (byte) 0);
        String str11 = nextDate3.run((int) (short) 10, (int) (short) -1, (int) 'a');
        String str15 = nextDate3.run(0, (int) (byte) 0, (int) ' ');
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test2421() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2421");
        NextDate nextDate3 = new NextDate(30, 30, (int) (short) 0);
        String str7 = nextDate3.run(100, (int) (short) 10, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2422() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2422");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) 100, (int) (byte) 0);
        String str7 = nextDate3.run((int) (byte) 10, (int) (short) 100, 2001);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "11/1/2001" + "'", str7, "11/1/2001");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2423() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2423");
        NextDate nextDate3 = new NextDate(12, (int) (byte) 10, (int) (byte) -1);
        String str7 = nextDate3.run(10, (int) (short) 0, (int) (byte) 0);
        String str11 = nextDate3.run(12, (int) (byte) 10, (int) (byte) 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2424() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2424");
        NextDate nextDate3 = new NextDate(30, (int) (short) 10, (int) ' ');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2425() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2425");
        NextDate nextDate3 = new NextDate((int) (short) 1, 7, 2001);
        String str7 = nextDate3.run((int) (short) 10, (int) ' ', 0);
        String str11 = nextDate3.run(0, 2001, (int) ' ');
        String str15 = nextDate3.run((int) '4', 10, 2020);
        String str19 = nextDate3.run((int) '#', 12, (int) (short) 10);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test2426() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2426");
        NextDate nextDate3 = new NextDate(10, 7, 1);
        String str7 = nextDate3.run((int) (short) 100, 0, 31);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2427() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2427");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) 10, 7);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2428() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2428");
        NextDate nextDate3 = new NextDate((int) (short) -1, 100, (int) ' ');
        String str7 = nextDate3.run((int) (short) 0, (int) (byte) 100, (int) (short) 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2429() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2429");
        NextDate nextDate3 = new NextDate(10, 0, 2001);
        String str7 = nextDate3.run(0, 1, 0);
        String str11 = nextDate3.run((int) '#', (int) (short) 0, (int) 'a');
        String str15 = nextDate3.run((int) (byte) 10, 10, 1);
        String str19 = nextDate3.run((int) '4', (int) (short) -1, 0);
        String str23 = nextDate3.run((int) (byte) 100, (int) (byte) 100, (int) (short) 0);
        String str27 = nextDate3.run(0, 0, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
    }

    @Test
    public void test2430() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2430");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) '#', 0);
        String str7 = nextDate3.run(2020, 0, 30);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2431() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2431");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 0, (int) (short) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2432() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2432");
        NextDate nextDate3 = new NextDate(10, (int) (short) 0, (int) (byte) -1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2433() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2433");
        NextDate nextDate3 = new NextDate(7, (int) (short) 0, (int) (short) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2434() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2434");
        NextDate nextDate3 = new NextDate(100, (int) (short) 1, (int) (short) 10);
    }

    @Test
    public void test2435() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2435");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (short) 1, (int) (byte) 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2436() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2436");
        NextDate nextDate3 = new NextDate((int) ' ', (int) 'a', (int) (short) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2437() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2437");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 1, 31);
    }

    @Test
    public void test2438() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2438");
        NextDate nextDate3 = new NextDate((int) (byte) 10, (int) '#', (int) (byte) 100);
        String str7 = nextDate3.run(2020, 31, (int) '#');
        String str11 = nextDate3.run(31, 1, (int) '4');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2439() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2439");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) 100, (int) '4');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2440() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2440");
        NextDate nextDate3 = new NextDate((int) (short) 0, 2020, (int) '#');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2441() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2441");
        NextDate nextDate3 = new NextDate((int) '#', 0, 0);
        String str7 = nextDate3.run((int) (short) -1, 2001, 30);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2442() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2442");
        NextDate nextDate3 = new NextDate(0, 7, (int) (short) 1);
        String str7 = nextDate3.run((int) (byte) -1, (int) (byte) 100, (int) (short) 0);
        String str11 = nextDate3.run(0, (int) (byte) 100, 30);
        String str15 = nextDate3.run(100, (int) '#', (int) 'a');
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test2443() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2443");
        NextDate nextDate3 = new NextDate((int) '#', 7, (int) (byte) 100);
        String str7 = nextDate3.run((int) (short) 10, (int) '4', 12);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2444() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2444");
        NextDate nextDate3 = new NextDate((int) (short) 100, 0, (int) (short) -1);
        String str7 = nextDate3.run((int) (byte) -1, (int) ' ', (-1));
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2445() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2445");
        NextDate nextDate3 = new NextDate((int) 'a', (-1), 2001);
        String str7 = nextDate3.run((int) '4', 2001, (int) (byte) 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2446() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2446");
        NextDate nextDate3 = new NextDate((int) (short) 1, (int) (short) 100, 100);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2447() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2447");
        NextDate nextDate3 = new NextDate(0, 0, (int) '#');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2448() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2448");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) 'a', (int) '#');
        String str7 = nextDate3.run(10, (int) (short) 1, (int) (byte) 10);
        String str11 = nextDate3.run((-1), 0, (int) ' ');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2449() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2449");
        NextDate nextDate3 = new NextDate(1, 7, (int) 'a');
    }

    @Test
    public void test2450() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2450");
        NextDate nextDate3 = new NextDate((int) ' ', 10, 2020);
        String str7 = nextDate3.run((int) (short) -1, 100, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2451() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2451");
        NextDate nextDate3 = new NextDate(12, (int) (byte) -1, (int) (byte) 0);
        String str7 = nextDate3.run((int) (short) 100, (int) (short) 1, 0);
        String str11 = nextDate3.run((int) (short) 0, 0, 0);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2452() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2452");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) '#', 0);
        String str7 = nextDate3.run(2020, 0, 30);
        String str11 = nextDate3.run((int) '#', 100, (int) 'a');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2453() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2453");
        NextDate nextDate3 = new NextDate(30, 0, 31);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2454() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2454");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (short) 1, 100);
    }

    @Test
    public void test2455() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2455");
        NextDate nextDate3 = new NextDate(7, 2020, 12);
        String str7 = nextDate3.run((int) (short) 0, 31, (int) (short) 100);
        String str11 = nextDate3.run(1, 30, (-1));
        String str15 = nextDate3.run((int) (short) 0, (int) '4', 7);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
    }

    @Test
    public void test2456() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2456");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 30, 31);
        String str7 = nextDate3.run((int) '4', 2001, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2457() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2457");
        NextDate nextDate3 = new NextDate(10, 1, (int) (short) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2458() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2458");
        NextDate nextDate3 = new NextDate(0, (int) '#', 30);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2459() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2459");
        NextDate nextDate3 = new NextDate((int) '4', 0, (int) (byte) 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2460() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2460");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) (byte) 100, (int) ' ');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2461() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2461");
        NextDate nextDate3 = new NextDate((int) (byte) 100, (int) 'a', 1);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2462() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2462");
        NextDate nextDate3 = new NextDate(2020, 0, 2001);
        String str7 = nextDate3.run((int) (short) 10, 7, (int) 'a');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2463() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2463");
        NextDate nextDate3 = new NextDate(0, 30, 31);
    }

    @Test
    public void test2464() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2464");
        NextDate nextDate3 = new NextDate(1, 7, (int) (byte) 1);
        String str7 = nextDate3.run((int) (short) -1, 0, 1);
        String str11 = nextDate3.run((int) '#', (int) (byte) 100, 30);
        String str15 = nextDate3.run((int) (short) -1, (int) (short) 1, 2020);
        String str19 = nextDate3.run((-1), (int) '#', 0);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test2465() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2465");
        NextDate nextDate3 = new NextDate(0, 100, (int) (short) 0);
        String str7 = nextDate3.run((int) 'a', (int) (byte) 10, (int) (byte) 0);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2466() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2466");
        NextDate nextDate3 = new NextDate(12, 1, (int) (short) -1);
        String str7 = nextDate3.run((int) (short) 0, 1, 12);
        String str11 = nextDate3.run((int) (byte) 10, 1, (int) (short) -1);
        String str15 = nextDate3.run((int) (byte) 100, 2020, (int) (byte) 100);
        String str19 = nextDate3.run((int) (short) -1, (int) (byte) 1, 7);
        Class<?> wildcardClass20 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test2467() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2467");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (short) -1, (int) (byte) 100);
        String str7 = nextDate3.run(2020, 12, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2468() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2468");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run(7, (int) (short) 10, (int) (short) 1);
        String str27 = nextDate3.run((int) 'a', (int) (byte) 1, 0);
        String str31 = nextDate3.run((int) (byte) 0, (int) ' ', (int) (short) 10);
        String str35 = nextDate3.run((int) (short) -1, (int) '4', 7);
        Class<?> wildcardClass36 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass36);
    }

    @Test
    public void test2469() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2469");
        NextDate nextDate3 = new NextDate((int) '#', (int) (short) 10, 30);
        String str7 = nextDate3.run((-1), (int) (short) 1, (int) (byte) -1);
        String str11 = nextDate3.run(12, 0, 0);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2470() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2470");
        NextDate nextDate3 = new NextDate(2020, (int) (byte) 100, 7);
        String str7 = nextDate3.run((int) (short) -1, (int) (short) -1, (int) '4');
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2471() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2471");
        NextDate nextDate3 = new NextDate(12, (int) (short) 1, (int) '4');
        String str7 = nextDate3.run((int) (byte) 10, 0, (int) ' ');
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2472() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2472");
        NextDate nextDate3 = new NextDate((int) ' ', (int) (byte) -1, 100);
        String str7 = nextDate3.run(12, (int) (byte) 1, (int) (byte) 1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2473() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2473");
        NextDate nextDate3 = new NextDate((int) (short) 0, (int) (byte) 10, 30);
    }

    @Test
    public void test2474() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2474");
        NextDate nextDate3 = new NextDate(12, (int) (short) 10, (int) (short) 0);
    }

    @Test
    public void test2475() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2475");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 2020, (int) 'a');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2476() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2476");
        NextDate nextDate3 = new NextDate((int) (short) 100, (int) (short) 0, (int) '4');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2477() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2477");
        NextDate nextDate3 = new NextDate(7, (-1), (int) '#');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2478() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2478");
        NextDate nextDate3 = new NextDate((int) (byte) 0, 100, 12);
        String str7 = nextDate3.run((int) (short) 10, 100, (int) '4');
        String str11 = nextDate3.run((int) 'a', (int) (short) 100, (int) '#');
        String str15 = nextDate3.run(30, (int) ' ', (int) '4');
        String str19 = nextDate3.run(2001, (int) (byte) 1, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
    }

    @Test
    public void test2479() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2479");
        NextDate nextDate3 = new NextDate(30, (int) (byte) -1, (int) (short) 100);
        String str7 = nextDate3.run((int) 'a', 31, 0);
        String str11 = nextDate3.run((-1), 1, 2001);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2480() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2480");
        NextDate nextDate3 = new NextDate(10, (int) '#', 0);
        String str7 = nextDate3.run((int) (short) 0, 10, (int) (short) 10);
        Class<?> wildcardClass8 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test2481() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2481");
        NextDate nextDate3 = new NextDate(2020, 7, (int) (byte) 0);
        String str7 = nextDate3.run((int) (short) 10, 0, (int) (short) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2482() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2482");
        NextDate nextDate3 = new NextDate(2020, 0, 31);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2483() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2483");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (int) ' ', 0);
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2484() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2484");
        NextDate nextDate3 = new NextDate((int) (byte) -1, (int) 'a', 2020);
        String str7 = nextDate3.run((int) 'a', 1, (int) (short) 0);
        String str11 = nextDate3.run((int) '4', (int) (short) 100, (int) (byte) 1);
        String str15 = nextDate3.run(0, (int) ' ', (int) (short) 1);
        Class<?> wildcardClass16 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test2485() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2485");
        NextDate nextDate3 = new NextDate((int) 'a', (int) (byte) 0, (-1));
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2486() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2486");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run(2020, (int) (byte) 10, (-1));
        String str15 = nextDate3.run(10, (int) (byte) 1, (int) (byte) 1);
        String str19 = nextDate3.run((int) '4', 30, 0);
        String str23 = nextDate3.run((int) (byte) 10, (int) (short) 100, 100);
        String str27 = nextDate3.run((int) (short) 100, (int) (short) 0, (int) (byte) 0);
        String str31 = nextDate3.run((int) (byte) -1, (int) (short) 10, 7);
        String str35 = nextDate3.run((-1), (int) ' ', (int) (byte) 10);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
    }

    @Test
    public void test2487() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2487");
        NextDate nextDate3 = new NextDate(10, (int) '#', (int) (byte) -1);
        String str7 = nextDate3.run(100, (int) (byte) 10, (int) (byte) 10);
        String str11 = nextDate3.run((-1), (int) '4', 2020);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2488() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2488");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 100, (int) ' ');
        Class<?> wildcardClass4 = nextDate3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test2489() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2489");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        String str15 = nextDate3.run((int) (byte) 1, (int) (short) 0, (int) (short) 100);
        String str19 = nextDate3.run((int) (short) 10, 0, 10);
        String str23 = nextDate3.run((int) '#', 100, 100);
        String str27 = nextDate3.run((int) (byte) 1, (int) (short) 10, (int) (short) 10);
        String str31 = nextDate3.run((int) 'a', 1, 12);
        String str35 = nextDate3.run((int) '#', (int) 'a', 1);
        String str39 = nextDate3.run((int) (short) -1, 2020, 2001);
        String str43 = nextDate3.run((int) (byte) 0, (int) 'a', (int) (byte) 10);
        Class<?> wildcardClass44 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str35 + "' != '" + "invalid Input Date" + "'", str35, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str39 + "' != '" + "invalid Input Date" + "'", str39, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str43 + "' != '" + "invalid Input Date" + "'", str43, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass44);
    }

    @Test
    public void test2490() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2490");
        NextDate nextDate3 = new NextDate((int) 'a', 31, 100);
    }

    @Test
    public void test2491() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2491");
        NextDate nextDate3 = new NextDate(7, (int) (byte) 0, 2020);
        String str7 = nextDate3.run((int) (short) 0, 31, 2020);
        String str11 = nextDate3.run((-1), (int) '4', 1);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2492() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2492");
        NextDate nextDate3 = new NextDate((int) (byte) 0, (-1), (int) (byte) 0);
    }

    @Test
    public void test2493() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2493");
        NextDate nextDate3 = new NextDate(30, (int) '4', (int) (byte) -1);
        String str7 = nextDate3.run((int) (short) -1, (int) (byte) -1, (int) (byte) -1);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2494() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2494");
        NextDate nextDate3 = new NextDate(0, 100, 12);
        String str7 = nextDate3.run(10, (int) (short) 1, (-1));
        String str11 = nextDate3.run(0, (int) ' ', (int) (byte) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2495() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2495");
        NextDate nextDate3 = new NextDate(0, (int) (byte) 10, 0);
        String str7 = nextDate3.run(1, (int) (byte) 100, 1);
        String str11 = nextDate3.run(2001, 31, (int) (short) 100);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
    }

    @Test
    public void test2496() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2496");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run(0, 2001, 0);
        String str11 = nextDate3.run((int) (byte) 1, (int) (short) 1, 0);
        String str15 = nextDate3.run((int) (byte) 1, (int) (short) 0, (int) (short) 100);
        String str19 = nextDate3.run((int) (short) 10, 0, 10);
        String str23 = nextDate3.run((int) (byte) -1, 10, 30);
        String str27 = nextDate3.run((int) (byte) 100, (int) (short) 0, (-1));
        String str31 = nextDate3.run((int) (byte) 100, (int) ' ', (int) ' ');
        Class<?> wildcardClass32 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str27 + "' != '" + "invalid Input Date" + "'", str27, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str31 + "' != '" + "invalid Input Date" + "'", str31, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass32);
    }

    @Test
    public void test2497() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2497");
        NextDate nextDate3 = new NextDate(31, (int) (byte) 1, 12);
        String str7 = nextDate3.run((int) (byte) 10, 12, 31);
        String str11 = nextDate3.run(2020, 0, (int) '4');
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test2498() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2498");
        NextDate nextDate3 = new NextDate((int) (byte) -1, 30, (int) (byte) 10);
        String str7 = nextDate3.run(12, 2020, 0);
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
    }

    @Test
    public void test2499() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2499");
        NextDate nextDate3 = new NextDate((int) (byte) 1, 1, 0);
        String str7 = nextDate3.run((int) ' ', 2020, (int) (short) 0);
        String str11 = nextDate3.run((int) (byte) -1, (int) (byte) -1, (int) (short) 0);
        String str15 = nextDate3.run((-1), (int) (byte) 10, 31);
        String str19 = nextDate3.run((int) (short) 1, (int) (byte) 0, 7);
        String str23 = nextDate3.run((int) (short) 1, 0, 100);
        Class<?> wildcardClass24 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str15 + "' != '" + "invalid Input Date" + "'", str15, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str19 + "' != '" + "invalid Input Date" + "'", str19, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str23 + "' != '" + "invalid Input Date" + "'", str23, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test2500() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest4.test2500");
        NextDate nextDate3 = new NextDate((int) (short) 100, 2020, 2020);
        String str7 = nextDate3.run((int) (short) 0, 1, 7);
        String str11 = nextDate3.run((int) (byte) 1, (int) (byte) 100, 7);
        Class<?> wildcardClass12 = nextDate3.getClass();
        org.junit.Assert.assertEquals("'" + str7 + "' != '" + "invalid Input Date" + "'", str7, "invalid Input Date");
        org.junit.Assert.assertEquals("'" + str11 + "' != '" + "invalid Input Date" + "'", str11, "invalid Input Date");
        org.junit.Assert.assertNotNull(wildcardClass12);
    }
}

