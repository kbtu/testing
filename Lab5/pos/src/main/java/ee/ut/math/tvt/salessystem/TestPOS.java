package ee.ut.math.tvt.salessystem;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;

public class TestPOS {
    public static void main(String[] args) {
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ShoppingCart cart = new ShoppingCart(dao);
        cart.submitCurrentPurchase();
        System.out.println(cart.getAll());
    }
}
